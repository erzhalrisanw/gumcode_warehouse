<?php
defined('BASEPATH') or exit('No direct script access allowed');

class All_process extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function change_domain_user($domain)
	{
		if ($this->session->userdata('domain') == 'all') {
			$this->session->set_userdata('domain', $domain);
			redirect(base_url() . $domain);
		} else {
			redirect(base_url() . "login");
		}
		// $this->load->view('demo/home');
	}

	public function changeToMd5($pass)
	{
		print_r(md5($pass));
	}

	public function add_sn_good_faulty($id_doc) {
		$data['doc_id'] = $id_doc;
		$this->load->view('huawei/layout');
		$this->load->view('huawei/inventory/cwh_inbound/sn_good', $data);
	}

	public function makePdfSN($doc_id)
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$doc = $db_huawei->select('hst.*, d.doc_no')
		->from('dt_io_bound d')
		->join('dt_sn_history hst', 'hst.id_doc = d.id')
		->where('d.id', $doc_id)
		->group_by('hst.sn')
		->get();
		$doc = $doc->result_array();

		for($i=0; $i<count($doc);$i++) {
			$sn = $db_huawei->select('*')
			->from('dt_sn')
			->where('id_sn', $doc[$i]['sn'])
			->get();
			$sn = $sn->row();

			$pn = $db_huawei->select('name_pn')
			->from('dm_pn')
			->where('id_pn', $sn->id_pn)
			->get();
			$pn = $pn->row();

			$data_sn[$i] = array('loc_bin' => $sn->locbin, 'sn' => $sn->sn, 'pn' => $pn->name_pn);
		};

		require APPPATH.'libraries/fpdf_barcode.php';
		$pdf = new PDF_Code128('P', 'mm', 'A4');
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(190,7,$doc[0]['doc_no'],0,1,'C');
		$pdf->Cell(10,7,'',0,1);
		$pdf->SetFont('Arial','B',10);

		$y = 50;
		$h_bar = 35;
		for($k = 0; $k < count($data_sn); $k++){
			$pdf->Cell(60,6,'Loc Bin',1,0);
			$pdf->Cell(60,6,'Part Number',1,0);
			$pdf->Cell(60,6,'Serial Number',1,1);
			$pdf->SetY($y);
			$pdf->SetFont('Arial','B',9);
			$pdf->Code128(20,$h_bar,$data_sn[$k]['loc_bin'],45,15);
			$pdf->Code128(80,$h_bar,$data_sn[$k]['pn'],45,15);
			$pdf->Code128(140,$h_bar,$data_sn[$k]['sn'],45,15);
			$pdf->Cell(60,10,$data_sn[$k]['loc_bin'],0,0,'C');
			$pdf->Cell(60,10,$data_sn[$k]['pn'],0,0,'C');
			$pdf->Cell(60,10,$data_sn[$k]['sn'],0,1,'C');

			$y = $y + 35;
			$h_bar = $h_bar + 35;
		}

		// $pdf->Output();

		$pdf->Output('D', $doc[0]['doc_no'].'-SN.pdf');
	}

	public function docPrint($doc_id){
		$db_huawei = $this->load->database('huawei', TRUE);

		$doc = $db_huawei->select('*, warehouse.address AS warehouse_address, od.address AS od_address, logistic.address AS log_address')
		->from('dt_io_bound doc')
		->join('dm_io_boundtype bt', 'bt.id_io_boundtype = doc.id_boundtype', 'left')
		->join('dm_outdestination od', 'od.id_outdestination = doc.id_boundfrom', 'left')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundto', 'left')
		->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic', 'left')
		->where('doc.id', $doc_id)
		->get();
		$data['doc'] = $doc->result_array();

		$doc = $doc->row();

		// if($doc->id_io_boundtype){
		// }

		$from_det = $db_huawei->select('*')
		->from('dt_io_bound doc')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
		->join('dm_city city', 'city.id_city = warehouse.id_city')
		->join('dm_country country', 'country.id_country = city.id_country')
		->where('doc.id', $doc_id)
		->get();
		$data['from_det'] = $from_det->result_array();

		$to_det = $db_huawei->select('*')
		->from('dt_io_bound doc')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundto')
		->join('dm_city city', 'city.id_city = warehouse.id_city')
		->join('dm_country country', 'country.id_country = city.id_country')
		->where('doc.id', $doc_id)
		->get();
		$data['to_det'] = $to_det->result_array();

		$sn_det = $db_huawei->select('proj.name_project, sn.sn, pn.name_pn, basecode.description, stock_stat.name_stockstatus, sn.remark')
		->from('dt_io_bound doc')
		->join('dm_project proj', 'proj.id_project = doc.id_project')
		->join('dt_sn_history sn_hst', 'sn_hst.id_doc = doc.id')
		->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
		->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
		->where('doc.id', $doc_id)
		->group_by('sn_hst.sn')
		->get();

		$data['sn_det'] = $sn_det->result_array();

		$view = $this->load->view('huawei/inventory/print_doc', $data, true);
		$doc_no = explode('/', $doc->doc_no);
		$pdf_name = $doc_no[0].'-'.$doc_no[1].'-'.$doc_no[2].'-'.$doc_no[3];

		$this->load->library('pdfgenerator');

		$this->pdfgenerator->generate($view, $pdf_name);
	}

	public function add_sn_io_tes()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);

		$loc_bin = $this->input->post('add-sn-bin');
		$pn = $this->input->post('add-sn-pn');
		$sn = $this->input->post('add-sn-sn');
		$doc_id = $this->input->post('add-sn-docid');

		$doc_dtl = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id', $doc_id)
		->get();
		$doc_dtl = $doc_dtl->row();

		$pn_exist = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $pn)
		->get();

		$sn_exist = $db_huawei->select('*')
		->from('dt_sn')
		->where('sn', $sn)
		->get();

		$stock_stat = '';
		if($doc_dtl->id_stockstatus == '1'){
			$stock_stat = 1;
		}else{
			$stock_stat = 2;
		}

		echo json_encode($doc_dtl);
	}

	public function add_sn_io()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);

		$loc_bin = $this->input->post('add-sn-bin');
		$pn = $this->input->post('add-sn-pn');
		$sn = $this->input->post('add-sn-sn');
		$doc_id = $this->input->post('add-sn-docid');

		$doc_dtl = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id', $doc_id)
		->get();
		$doc_dtl = $doc_dtl->row();

		$pn_exist = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $pn)
		->get();

		$sn_exist = $db_huawei->select('*')
		->from('dt_sn')
		->where('sn', $sn)
		->get();

		$stock_stat = '';
		if($doc_dtl->id_stockstatus == '1'){
			$stock_stat = 1;
		}else{
			$stock_stat = 2;
		}

		if($doc_dtl->id_boundtype == '1' || $doc_dtl->id_boundtype == '11'){
			if($pn == $sn){
				if($pn_exist->num_rows() < 1){
					$return = array('stat' => false, 'det' => 'PN does not exist in database');
				} else {
					$format = 'NS'.$pn;
					$sn_num = $db_huawei->select('*')
					->from('no_sn_counter')
					->get();
					$sn_num = $sn_num->row();
					$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

					$upd_count = array(
						'number' => $sn_num->number+1
					);

					$db_huawei->where('id_sn_counter', '1');

					if($db_huawei->update('no_sn_counter', $upd_count)) {
						$pn_exist = $pn_exist->row();
						$sn_insert = array(
							'sn' => $generated_sn,
							'id_pn' => $pn_exist->id_pn,
							'id_stockstatus' => $stock_stat,
							'id_stockaccess' => '6',
							'id_stockposition' => '1',
							'id_warehouse' => $doc_dtl->id_boundto,
							'id_project' => $doc_dtl->id_project,
							'locbin' => $loc_bin,
							'stockTaker' => $this->session->userdata('login_id'),
							'stocktake_time' => date("Y-m-d H:i:s")
						);

						if($db_huawei->insert('dt_sn', $sn_insert)){
							$sn_id = $db_huawei->insert_id();

							$sn_history = array(
								'sn' => $sn_id,
								'id_doc' => $doc_id,
								'datetime_history' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn_history', $sn_history)){
								$return = array('stat' => true, 'det' => 'SN successfully saved');
							} else {
								$return = array('stat' => false, 'det' => 'SN cannot be saved');
							}
						}
					}
				}
			} else {
				if($pn_exist->num_rows() < 1){
					$return = array('stat' => false, 'det' => 'PN does not exist in database');
				} else {
					$return = array('tes1' => $sn_exist, 'tes2' => $pn_exist);
					if($sn_exist->num_rows() > 0) {
						$sn_exist = $sn_exist->row();
						$pn_exist = $pn_exist->row();

						$return = array('tes1' => $sn_exist, 'tes2' => $pn_exist);
						// if($sn_exist->id_pn == $pn_exist->id_pn){
						// 	$return = array('stat' => false, 'det' => 'SN has a different PN with your input!');
						// }else{
						// 	$upd = array(
						// 		'id_stockaccess' => '6',
						// 		'stocktake_time' => date('Y-m-d H:i:s'),
						// 		'stocktaker' => $this->session->userdata('login_id'),
						// 		'id_stockposition' => '1',
						// 		'id_stockstatus' => $stock_stat,
						// 		'locbin' => $loc_bin,
						// 		'id_warehouse' => $doc_dtl->id_boundto,
						// 		'id_project' => $doc_dtl->id_project,
						// 	);

						// 	$db_huawei->where('id_sn', $sn_exist->id_sn);

						// 	if($db_huawei->update('dt_sn', $upd)){
						// 		$sn_history = array(
						// 			'sn' => $sn_exist->id_sn,
						// 			'id_doc' => $doc_id,
						// 			'datetime_history' => date("Y-m-d H:i:s")
						// 		);

						// 		if($db_huawei->insert('dt_sn_history', $sn_history)){
						// 			$return = array('stat' => true, 'det' => 'SN successfully saved');
						// 		} else {
						// 			$return = array('stat' => false, 'det' => 'SN cannot be saved');
						// 		}
						// 	}
						// }
					} else {
						// $pn_exist = $pn_exist->row();
						// $sn_insert = array(
						// 	'sn' => $sn,
						// 	'id_pn' => $pn_exist->id_pn,
						// 	'id_stockstatus' => $stock_stat,
						// 	'id_stockaccess' => '6',
						// 	'id_stockposition' => '1',
						// 	'id_warehouse' => $doc_dtl->id_boundto,
						// 	'id_project' => $doc_dtl->id_project,
						// 	'locbin' => $loc_bin,
						// 	'stocktaker' => $this->session->userdata('login_id'),
						// 	'stocktake_time' => date("Y-m-d H:i:s")
						// );

						// if($db_huawei->insert('dt_sn', $sn_insert)){
						// 	$sn_id = $db_huawei->insert_id();

						// 	$sn_history = array(
						// 		'sn' => $sn_id,
						// 		'id_doc' => $doc_id,
						// 		'datetime_history' => date("Y-m-d H:i:s")
						// 	);

						// 	if($db_huawei->insert('dt_sn_history', $sn_history)){
						// 		$return = array('stat' => true, 'det' => 'SN successfully saved');
						// 	} else {
						// 		$return = array('stat' => false, 'det' => 'SN cannot be saved');
						// 	}
						// }
					}
				}
			}
		} elseif($doc_dtl->id_boundtype == '2') {
			if($pn == $sn){
				if($pn_exist->num_rows() < 1){
					$return = array('stat' => false, 'det' => 'PN does not exist in database');
				} else {
					$format = 'NS'.$pn;
					$sn_num = $db_huawei->select('*')
					->from('no_sn_counter')
					->get();
					$sn_num = $sn_num->row();
					$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

					$upd_count = array(
						'number' => $sn_num->number+1
					);

					$db_huawei->where('id_sn_counter', '1');

					if($db_huawei->update('no_sn_counter', $upd_count)) {
						$pn_exist = $pn_exist->row();
						$sn_insert = array(
							'sn' => $generated_sn,
							'id_pn' => $pn_exist->id_pn,
							'id_stockstatus' => '1',
							'id_stockaccess' => '6',
							'id_stockposition' => '1',
							'id_warehouse' => $doc_dtl->id_boundto,
							'id_project' => $doc_dtl->id_project,
							'locbin' => $loc_bin,
							'stockTaker' => $this->session->userdata('login_id'),
							'stocktake_time' => date("Y-m-d H:i:s")
						);

						if($db_huawei->insert('dt_sn', $sn_insert)){
							$sn_id = $db_huawei->insert_id();

							$sn_history = array(
								'sn' => $sn_id,
								'id_doc' => $doc_id,
								'datetime_history' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn_history', $sn_history)){
								$return = array('stat' => true, 'det' => 'SN successfully saved');
							} else {
								$return = array('stat' => false, 'det' => 'SN cannot be saved');
							}
						}
					}
				}
			} else {
				if($pn_exist->num_rows() < 1){
					$return = array('stat' => false, 'det' => 'PN does not exist in database');
				} else {
					if($sn_exist->num_rows() > 0) {
						$return = array('stat' => false, 'det' => 'SN already exist in database');
					} else {
						$pn_exist = $pn_exist->row();
						$sn_insert = array(
							'sn' => $sn,
							'id_pn' => $pn_exist->id_pn,
							'id_stockstatus' => '1',
							'id_stockaccess' => '6',
							'id_stockposition' => '1',
							'id_warehouse' => $doc_dtl->id_boundto,
							'id_project' => $doc_dtl->id_project,
							'locbin' => $loc_bin,
							'stockTaker' => $this->session->userdata('login_id'),
							'stocktake_time' => date("Y-m-d H:i:s")
						);

						if($db_huawei->insert('dt_sn', $sn_insert)){
							$sn_id = $db_huawei->insert_id();

							$sn_history = array(
								'sn' => $sn_id,
								'id_doc' => $doc_id,
								'datetime_history' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn_history', $sn_history)){
								$return = array('stat' => true, 'det' => 'SN successfully saved');
							} else {
								$return = array('stat' => false, 'det' => 'SN cannot be saved');
							}
						}
					}
				}
			}
		} elseif($doc_dtl->id_boundtype == '3' || $doc_dtl->id_boundtype == '4' || $doc_dtl->id_boundtype == '5' || $doc_dtl->id_boundtype == '6' || $doc_dtl->id_boundtype == '7' || $doc_dtl->id_boundtype == '8' || $doc_dtl->id_boundtype == '9' || $doc_dtl->id_boundtype == '10') {

			$pn_exist_out = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp temp')
			->join('dm_pn pn', 'pn.id_pn = temp.product_number')
			->where('temp.doc_id', $doc_id)
			->where('pn.name_pn', $pn)
			->get();

			$sn_exist_exit = $db_huawei->select('*')
			->from('dt_sn')
			->where('sn', $sn)
			->where('id_stockaccess', '1')
			->where('status_cek', '1')
			->get();

			if($pn == $sn){
				$return = array('stat' => false, 'det' => 'PN & SN cannot be same');
			} else {
				if($pn_exist_out->num_rows() < 1){
					$return = array('stat' => false, 'det' => 'PN does not exist in this document');
				} else {
					if($sn_exist_exit->num_rows() < 1) {
						$return = array('stat' => false, 'det' => 'SN cannot be use');
					} else {
						$sn_exist_exit = $sn_exist_exit->row();
						$pn_exist_out = $pn_exist_out->row();
						if($sn_exist_exit->id_pn !== $pn_exist_out->id_pn){
							$return = array('stat' => false, 'det' => 'SN & PN does not match');
						} else {
							if($sn_exist_exit->id_stockaccess !== '3'){
								$upd = array(
									'id_stockaccess' => '3',
									'id_stockstatus' => $stock_stat,
									'stocktake_time' => date('Y-m-d H:i:s'),
									'stocktaker' => $this->session->userdata('login_id'),
									'locbin' => $loc_bin
								);

								$db_huawei->where('id_sn', $sn_exist_exit->id_sn);

								if($db_huawei->update('dt_sn', $upd)){
									$sn_history = array(
										'sn' => $sn_exist_exit->id_sn,
										'id_doc' => $doc_id,
										'datetime_history' => date("Y-m-d H:i:s")
									);

									if($db_huawei->insert('dt_sn_history', $sn_history)){
										$return = array('stat' => true, 'det' => 'SN successfully saved');
									} else {
										$return = array('stat' => false, 'det' => 'SN cannot be saved');
									}
								}
							}else{
								$return = array('stat' => false, 'det' => 'SN not available');
							}
						}
					}
				}
			}
		}

		echo json_encode($return);
	}
}
