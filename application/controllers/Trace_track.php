<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trace_track extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// $model = $this->admin_model;
		// $img = $model->get_slide_img();
		// $data['img'] = $img;
		if ($this->session->userdata('logged')) {
			$this->load->view('admin/layout');
			$this->load->view('admin/trace_track');
		} else {
			redirect(base_url() . "index.php/login");
		}
	}
}
