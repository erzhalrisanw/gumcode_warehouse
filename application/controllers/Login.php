<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public $count_wrong_password;
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->count_wrong_password = 0;
	}

	public function index()
	{
		if ($this->session->userdata('logged')) {
			redirect(base_url() . $this->session->userdata('domain'). '/home');
		} else {
			$this->load->view('login');
		}
	}

	public function change_profile()
	{
		if ($this->session->userdata('logged')) {
			$user = $this->db->select('*')
			->from('dt_user')
			->where('id_user', $this->session->userdata('login_id'))
			->get();

			$data['user'] = $user->row();

			$this->load->view('huawei/layout');
			$this->load->view('change_profile', $data);
		} else {
			$this->load->view('login');
		}
	}

	public function force_change_password()
	{
		$random = rand();

		$edit_pass = array(
			'password' => md5($random)
		);
		$this->db->where('id_user', $this->input->post('id_user'));

		if($this->db->update('dt_user', $edit_pass)){
			$return = array('stat' => true, 'det' => 'Successfully change password', 'data' => $random);
		}else{
			$return = array('stat' => false, 'det' => 'Cannot change password');
		}

		echo json_encode($return);
	}

	public function force_delete_user()
	{
		$user = $this->db->select('*')
		->from('dt_user')
		->where('id_user', $this->input->post('id_user'))
		->get();
		$user = $user->row();

		if($user->url_foto != null){
			unlink(realpath(APPPATH . '../public/upload/file_photo_profile'.'/'.$user->url_foto));
		}
		
		if($this->db->delete('dt_user', array('id_user' => $this->input->post('id_user')))){
			$return = array('stat' => true, 'det' => 'Successfully delete user');
		}else{
			$return = array('stat' => false, 'det' => 'Cannot change password');
		}

		echo json_encode($return);
		
	}

	public function force_create_user()
	{
		$config['upload_path']   = realpath(APPPATH . '../public/upload/file_photo_profile'); 
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$new_name = time().'-'.$_FILES["photo_user"]['name'];
		$config['file_name'] = $new_name;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('photo_user')) {
			$return = array('stat' => false, 'det' => $this->upload->display_errors());
		}else { 
			$uploadedImage = $this->upload->data();

			$source_path = realpath(APPPATH . '../public/upload/file_photo_profile') .'/'. $uploadedImage['file_name'];
			$target_path = realpath(APPPATH . '../public/upload/file_photo_profile') .'/'. $uploadedImage['file_name'];
			$config_manip = array(
				'image_library' => 'gd2',
				'source_image' => $source_path,
				'new_image' => $target_path,
				'maintain_ratio' => false,
				'width' => 160,
				'height' => 160,
				'overwrite' => true
			);

			$this->load->library('image_lib', $config_manip);
			if (!$this->image_lib->resize()) {
				$return = array('stat' => false, 'det' => $this->image_lib->display_errors());
			} else {
				$data = 
				array(
					'name_user' => $this->input->post('name_user'),
					'email' => $this->input->post('email_user'),
					'password' => md5($this->input->post('pass_user')),
					'no_telp' => $this->input->post('phone_user'),
					'url_foto' => $uploadedImage['file_name'],
					'id_domain' => $this->input->post('id_domain'),
					'id_project' => $this->input->post('id_project'),
					'id_customer' => $this->input->post('id_customer'),
					'id_warehouse' => $this->input->post('id_warehouse'),
					'id_userlevel' => $this->input->post('id_userlevel'),
					'activated' => '1'
				);
				if($this->db->insert('dt_user', $data)){
					$return = array('stat' => true, 'det' => 'Data successfully saved');
				}else{
					$return = array('stat' => false, 'det' => 'Data cannot be saved');
				}
			}
		}

		echo json_encode($return);
	}

	public function change_profile_proceed()
	{
		$old_pass = $this->input->post('old_password');
		$new_pass = $this->input->post('password');
		$return = '';

		if($old_pass == ''){
			$edit_pass = array(
				'name_user' => $this->input->post('name_user'),
				'no_telp' => $this->input->post('no_telp')
			);
			$this->db->where('id_user', $this->session->userdata('login_id'));
			$this->db->update('dt_user', $edit_pass);
			$return = array('stat' => true, 'det' => 'Successfully change profile');
		}else{
			$check_old = $this->db->select('*')
			->from('dt_user')
			->where('id_user', $this->session->userdata('login_id'))
			->where('password', md5($old_pass))
			->get();
			$old_photo = $check_old->row();

			if($check_old->num_rows() > 0){
				$edit_pass = array(
					'name_user' => $this->input->post('name_user'),
					'no_telp' => $this->input->post('no_telp'),
					'password' => md5($new_pass)
				);
				$this->db->where('id_user', $this->session->userdata('login_id'));
				$this->db->update('dt_user', $edit_pass);

				$this->session->sess_destroy();

				$return = array('stat' => true, 'det' => 'Successfully change profile', 'change_pass' => true);
			}else{
				$return = array('stat' => false, 'det' => 'Cannot change profile');
			}
		}

		if ($_FILES['photo_profile']['name']) {
			$config['upload_path']   = realpath(APPPATH . '../public/upload/file_photo_profile'); 
			$config['allowed_types'] = 'gif|jpg|png';
			$new_name = time().'-'.$_FILES["photo_profile"]['name'];
			$config['file_name'] = $new_name;
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('photo_profile')) {
				$return = array('stat' => false, 'det' => $this->upload->display_errors());
			}else { 
				$uploadedImage = $this->upload->data();

				$source_path = realpath(APPPATH . '../public/upload/file_photo_profile') .'/'. $uploadedImage['file_name'];
				$target_path = realpath(APPPATH . '../public/upload/file_photo_profile') .'/'. $uploadedImage['file_name'];
				$config_manip = array(
					'image_library' => 'gd2',
					'source_image' => $source_path,
					'new_image' => $target_path,
					'maintain_ratio' => false,
					'width' => 160,
					'height' => 160,
					'overwrite' => true
				);

				$this->load->library('image_lib', $config_manip);
				if (!$this->image_lib->resize()) {
					$return = array('stat' => false, 'det' => $this->image_lib->display_errors());
				} else {
					$old_photo = $this->db->select('*')
					->from('dt_user')
					->where('id_user', $this->session->userdata('login_id'))
					->get();
					$old_photo = $old_photo->row();
					if($old_photo->url_foto == null){
						$edit_foto = array(
							'url_foto' => $uploadedImage['file_name']
						);
						$this->db->where('id_user', $this->session->userdata('login_id'));
						$this->db->update('dt_user', $edit_foto);

						$this->image_lib->clear();
						$return = array('stat' => true, 'det' => 'Data successfully saved');
					}else{
						$edit_foto = array(
							'url_foto' => $uploadedImage['file_name']
						);
						$this->db->where('id_user', $this->session->userdata('login_id'));
						$this->db->update('dt_user', $edit_foto);

						$this->image_lib->clear();
						if(is_readable(realpath(APPPATH . '../public/upload/file_photo_profile').'/'.$old_photo->url_foto) && unlink(realpath(APPPATH . '../public/upload/file_photo_profile').'/'.$old_photo->url_foto)){
							$return = array('stat' => true, 'det' => 'Data successfully saved');
						}
					}
				}
			}
		}

		echo json_encode($return);
	}

	public function auth()
	{
		$rules = [
			['field' => 'username', 'label' => 'Username', 'rules' => 'required'],
			['field' => 'password', 'label' => 'Password', 'rules' => 'required']
		];

		$post = $this->input->post();

		$data = $this->db->select('*, d.name_domain AS domain')
		->from('dt_user u')
		->join('dm_domain d', 'd.id_domain = u.id_domain')
		->where('u.email', $post['username'])
		->get();

		$validation = $this->form_validation;
		$validation->set_rules($rules);
		if ($validation->run()) {
			if ($data->num_rows() > 0) {
				$row = $data->row();
				if($row->password == md5($post['password'])){
					$mysession = array(
						'logged' => TRUE,
						'login_id' => $row->id_user,
						'level' => $row->id_userlevel,
						'email' => $row->email,
						'domain' => $row->domain,
						'id_warehouse' => $row->id_warehouse,
						'photo_profile' => $row->url_foto,
						'id_customer' => $row->id_customer,
						'id_project' => $row->id_project
					);
					$this->session->set_userdata($mysession);

					$data = 
					array(
						'id_user' => $row->id_user,
						'timestamp' => date('Y-m-d H:i:s'),
						'log_wrong_pass' => $this->session->userdata('count_wrong_password') == null ? 0 : $this->session->userdata('count_wrong_password')
					);
					$this->db->insert('dt_user_log', $data);
					$this->session->set_userdata('count_wrong_password', 0);

					// $db_huawei = $this->load->database('huawei', TRUE);

					// $req_list = $db_huawei->select('id_request')
					// ->from('dt_request')
					// ->where('eta_received <', date('Y-m-d H:i:s'))
					// ->where_not_in('id_status_request', array('5', '3', '4'))
					// ->where('id_pod', null)
					// ->where('id_pickup', null)
					// ->get();
					// $req_list = $req_list->result_array();

					// for($i = 0; $i < count($req_list); $i++){
					// 	$data = array(
					// 		'id_status_request' => '5'
					// 	);

					// 	$db_huawei->where('id_request', $req_list[$i]['id_request']);
					// 	$db_huawei->update('dt_request', $data);
					// }

					if($this->session->userdata('domain') == 'all'){
						redirect(base_url() . 'menu_all');
					} else {
						redirect(base_url() . $this->session->userdata('domain').'/home');
					}
				} else {
					$this->session->set_flashdata('error', 'Password is wrong!');
					$this->session->userdata('count_wrong_password') ? $this->session->set_userdata('count_wrong_password', $this->session->userdata('count_wrong_password') + 1) : $this->session->set_userdata(array('count_wrong_password' => 1));
					redirect(base_url() . "login");
				}
			} else {
				$this->session->set_flashdata('error', 'Email is wrong!');
				redirect(base_url() . "login");
			}
		} else {
			$this->session->set_flashdata('error', 'Username or password cannot be empty!');
			redirect(base_url() . "login");
		}
	}

	public function do_logout(){
		$this->session->sess_destroy();
		redirect(base_url() . "login");
	}
}
