<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Monitoring extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function int_trans_stock()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$this->load->view('huawei/layout');
		$this->load->view('huawei/monitoring/int_trans_stock');
	}

	public function replenishment()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$this->load->view('huawei/layout');
		$this->load->view('huawei/monitoring/replenishment');
	}

	public function transaction()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$check_list = $db_huawei->select('*')
		->from('dm_status_request')
		->where('id_status_request !=', '6')
		->get();

		$data['check_list'] = $check_list->result_array();
		$data['check_list_edit'] = $check_list->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/monitoring/transaction', $data);
	}

	public function available_stock()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$this->load->view('huawei/layout');
		$this->load->view('huawei/monitoring/available_stock');
	}

	public function performance()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$proj = $db_huawei->select('*')
		->from('dm_project')
		->get();

		$data['proj'] = $proj->result_array();

		$customer = $db_huawei->select('*')
		->from('dm_customer')
		->get();

		$data['customer'] = $customer->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/monitoring/performance', $data);
	}

	public function get_int_trans_stock_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 10){
			$customer_list = $db_huawei->select('doc.doc_no, sn.sn AS sn, warehouse2.name_warehouse AS to, warehouse.name_warehouse AS from, access.name_stockaccess AS status, doc.awb_no, logistic.name_logistic')
			->from('dt_sn_history hst')
			->join('dt_io_bound doc', 'doc.id = hst.id_doc')
			->join('dt_sn sn', 'sn.id_sn = hst.sn')
			->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
			->where('(doc.id_boundto ='.$this->session->userdata('id_warehouse').' and doc.id_boundtype = 9) or (doc.id_boundfrom ='.$this->session->userdata('id_warehouse').' and doc.id_boundtype = 9 )')
			->where('doc.date_received', null)
			->group_by('hst.id_doc')
			->get();
		} elseif($this->session->userdata('level') == 4){
			$customer_list = $db_huawei->select('doc.doc_no, sn.sn AS sn, warehouse2.name_warehouse AS to, warehouse.name_warehouse AS from, access.name_stockaccess AS status, doc.awb_no, logistic.name_logistic')
			->from('dt_sn_history hst')
			->join('dt_io_bound doc', 'doc.id = hst.id_doc')
			->join('dt_sn sn', 'sn.id_sn = hst.sn')
			->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
			->where('(warehouse.id_spv ='.$this->session->userdata('login_id').' and doc.id_boundtype = 9) or (warehouse2.id_spv ='.$this->session->userdata('login_id').' and doc.id_boundtype = 9)')
			->where('doc.date_received', null)
			->group_by('hst.id_doc')
			->get();
		} elseif($this->session->userdata('level') == 12 || $this->session->userdata('level') == 69){
			$customer_list = $db_huawei->select('doc.doc_no, sn.sn AS sn, warehouse2.name_warehouse AS to, warehouse.name_warehouse AS from, access.name_stockaccess AS status, doc.awb_no, logistic.name_logistic')
			->from('dt_sn_history hst')
			->join('dt_io_bound doc', 'doc.id = hst.id_doc')
			->join('dt_sn sn', 'sn.id_sn = hst.sn')
			->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
			->where('doc.id_boundtype', '9')
			->where('doc.date_received', null)
			->group_by('hst.id_doc')
			->get();
		}

		echo json_encode($customer_list->result_array());
	}

	public function get_replenishment_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 10){
			$customer_list = $db_huawei->select('doc.doc_no, warehouse2.name_warehouse AS to, warehouse.name_warehouse AS from, access.name_stockaccess AS status, doc.awb_no, logistic.name_logistic')
			->from('dt_sn_history hst')
			->join('dt_io_bound doc', 'doc.id = hst.id_doc')
			->join('dt_sn sn', 'sn.id_sn = hst.sn')
			->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
			->where('doc.id_boundtype', '3')
			->where('doc.id_boundfrom', '1')
			->where('doc.id_boundto', $this->session->userdata('id_warehouse'))
			->where('doc.date_received', null)
			->group_by('hst.id_doc')
			->get();
		} elseif($this->session->userdata('level') == 4){
			$customer_list = $db_huawei->select('doc.doc_no, warehouse2.name_warehouse AS to, warehouse.name_warehouse AS from, access.name_stockaccess AS status, doc.awb_no, logistic.name_logistic')
			->from('dt_sn_history hst')
			->join('dt_io_bound doc', 'doc.id = hst.id_doc')
			->join('dt_sn sn', 'sn.id_sn = hst.sn')
			->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
			->where('doc.id_boundtype', '3')
			->where('doc.id_boundfrom', '1')
			->where('warehouse2.id_spv', $this->session->userdata('login_id'))
			->where('doc.date_received', null)
			->group_by('hst.id_doc')
			->get();
		} elseif($this->session->userdata('level') == 12 || $this->session->userdata('level') == 69){
			$customer_list = $db_huawei->select('doc.doc_no, warehouse2.name_warehouse AS to, warehouse.name_warehouse AS from, access.name_stockaccess AS status, doc.awb_no, logistic.name_logistic')
			->from('dt_sn_history hst')
			->join('dt_io_bound doc', 'doc.id = hst.id_doc')
			->join('dt_sn sn', 'sn.id_sn = hst.sn')
			->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
			->where('doc.id_boundtype', '3')
			->where('doc.id_boundfrom', '1')
			->where('doc.date_received', null)
			->group_by('hst.id_doc')
			->get();
		}

		echo json_encode($customer_list->result_array());
	}

	public function transaction_io_bound_list()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 69 || $this->session->userdata('level') == 4) {

			$list = $db_huawei->select('req.*, req.time_request, req.order, req.rma, pod.pod_no, pod.awb_no, pod.actual_received, pod.consignee, user.name_user, dop.name_dop, warehouse.name_warehouse, pn.name_pn AS name_pn_del, sn.sn AS name_sn_del, statReq.status_request, pickup.pickup_no, pickup.actual_pickup, statPick.pickup_status, pn2.name_pn AS name_pn_pickup, sn2.sn AS name_sn_pickup')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn2', 'pn2.id_pn = req.id_pn_pickup', 'left')
			->join('dt_sn sn2', 'sn2.id_sn = req.id_sn_pickup', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter')
			->join('dm_status_request statReq', 'statReq.id_status_request = req.id_status_request', 'left')
			->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
			->join('dt_status_pickup statPick', 'statPick.id_statuspickup = req.id_statuspickup', 'left')
			->order_by('req.inbound_order', 'ASC')
			->order_by('req.outbound_order', 'ASC')
			->order_by('req.time_request', 'DESC')
			->get();
		}else{
			$list = $db_huawei->select('req.*, req.time_request, req.order, req.rma, pod.pod_no, pod.awb_no, pod.actual_received, pod.consignee, user.name_user, dop.name_dop, warehouse.name_warehouse, pn.name_pn AS name_pn_del, sn.sn AS name_sn_del, statReq.status_request, pickup.pickup_no, pickup.actual_pickup, statPick.pickup_status, pn2.name_pn AS name_pn_pickup, sn2.sn AS name_sn_pickup')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn2', 'pn2.id_pn = req.id_pn_pickup', 'left')
			->join('dt_sn sn2', 'sn2.id_sn = req.id_sn_pickup', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter')
			->join('dm_status_request statReq', 'statReq.id_status_request = req.id_status_request', 'left')
			->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
			->join('dt_status_pickup statPick', 'statPick.id_statuspickup = req.id_statuspickup', 'left')
		//	->where('((dop.id_warehouse = '.$this->session->userdata('id_warehouse').' AND req.id_warehouse = 0) OR req.id_warehouse = '.$this->session->userdata('id_warehouse').')')
		    ->where('dop.id_warehouse = '.$this->session->userdata('id_warehouse'))
			->order_by('req.inbound_order', 'ASC')
			->order_by('req.outbound_order', 'ASC')
			->order_by('req.time_request', 'DESC')
			->get();
		}

		echo json_encode($list->result_array());
	}

	public function count_not_matter()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 69 || $this->session->userdata('level') == 4) {
			$pickup = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('(req.url_photo_mdn IS NOT NULL OR req.id_status_request = 1)')
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
		//	->where('req.id_status_request !=', '7')
			->order_by('req.time_request', 'DESC')
			->get();

			$progress = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, stat_req.*')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
			->where('req.id_sn', null)
			->where_not_in('req.id_status_request', array('3', '4', '7'))
			->order_by('req.time_request', 'DESC')
			->get();

			$deliver = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_pod !=', null)
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
		//	->where('req.id_status_request !=', '7')
			->order_by('req.time_request', 'DESC')
			->get();
		}else{
			$pickup = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('(req.url_photo_mdn IS NOT NULL OR req.id_status_request = 1)')
			->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
		//	->where('req.id_status_request !=', '7')
			->order_by('req.time_request', 'DESC')
			->get();

			$progress = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, stat_req.*')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
			->where('((req.id_warehouse = '.$this->session->userdata('id_warehouse').' AND req.id_pod IS NULL) OR (dop.id_warehouse = '.$this->session->userdata('id_warehouse').' AND req.id_status_request = 1))')
			->where_not_in('req.id_status_request', array('3', '4', '7'))
			->order_by('req.time_request', 'DESC')
			->get();

			$deliver = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_pod !=', null)
			->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
		//	->where('req.id_status_request !=', '7')
			->order_by('req.time_request', 'DESC')
			->get();
		}

		echo json_encode(array('progress' => $progress->num_rows(), 'deliver' => $deliver->num_rows(), 'pickup' => $pickup->num_rows()));
	}

	public function search_available_stock()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$table_pn = $db_huawei->select('id_pn')
		->from('dm_pn')
		->like('name_pn', $this->input->post('rmr'))
		->get();

		$return = '';

		if($table_pn->num_rows() > 0){
			$overall = $db_huawei->select('warehouse.name_warehouse, project.name_project, SUM(CASE WHEN sn.id_stockaccess = 1 THEN 1 ELSE 0 END) AS total_available, SUM(CASE WHEN sn.id_stockaccess = 2 THEN 1 ELSE 0 END) AS total_delivery', FALSE)
			->from('dm_pn pn')
			->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_project project', 'project.id_project = sn.id_project')
			->like('pn.name_pn', $this->input->post('rmr'))
			->where('sn.id_stockstatus', '1')
			->group_by('sn.id_warehouse')
			->get();

			$detail = $db_huawei->select('warehouse.name_warehouse, project.name_project, basecode.name_basecode, basecode.description, pn.name_pn, sn.sn, status.name_stockstatus, position.name_stockposition, access.name_stockaccess, sn.stocktake_time, user.name_user')
			->from('dm_pn pn')
			->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
			->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
			->join('dm_stockstatus status', 'status.id_stockstatus = sn.id_stockstatus')
			->join('dm_stockposition position', 'position.id_stockposition = sn.id_stockposition')
			->join('dm_project project', 'project.id_project = sn.id_project')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = sn.stocktaker')
			->like('pn.name_pn', $this->input->post('rmr'))
			->where('sn.id_stockstatus', '1')
			// ->group_by('sn.id_warehouse')
			// ->group_by('sn.id_project')
			// ->group_by('sn.id_stockaccess')
			// ->where_in('sn.id_stockaccess', array('1', '2'))
			->get();

			$return = array('overall' => $overall->result_array(), 'detail' => $detail->result_array());
		} else {
			$table_basecode = $table_pn = $db_huawei->select('id_basecode')
			->from('dm_basecode')
			->like('description', $this->input->post('rmr'))
			->get();

			if($table_basecode->num_rows() > 0){
				$overall = $db_huawei->select('warehouse.name_warehouse, project.name_project, SUM(CASE WHEN sn.id_stockaccess = 1 THEN 1 ELSE 0 END) AS total_available, SUM(CASE WHEN sn.id_stockaccess = 2 THEN 1 ELSE 0 END) AS total_delivery', FALSE)
				->from('dm_basecode basecode')
				->join('dm_pn pn', 'pn.id_basecode = basecode.id_basecode')
				->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
				->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
				->join('dm_project project', 'project.id_project = sn.id_project')
				->like('basecode.description', $this->input->post('rmr'))
				->where('sn.id_stockstatus', '1')
				->group_by('sn.id_warehouse')
				->get();

				$detail = $db_huawei->select('warehouse.name_warehouse, project.name_project, basecode.name_basecode, basecode.description, pn.name_pn, sn.sn, status.name_stockstatus, position.name_stockposition, access.name_stockaccess, sn.stocktake_time, user.name_user')
				->from('dm_pn pn')
				->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
				->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
				->join('dm_stockstatus status', 'status.id_stockstatus = sn.id_stockstatus')
				->join('dm_stockposition position', 'position.id_stockposition = sn.id_stockposition')
				->join('dm_project project', 'project.id_project = sn.id_project')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
				->join('wmshilco_user_mgm.dt_user user', 'user.id_user = sn.stocktaker')
				->like('basecode.description', $this->input->post('rmr'))
				->where('sn.id_stockstatus', '1')
				->get();

				$return = array('overall' => $overall->result_array(), 'detail' => $detail->result_array());
			} else {
				$return = 'nothing';
			}
		}

		echo json_encode($return);
	}

	public function get_data_performance()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$date = explode(' - ', $this->input->post('input_daterange'));

		if($this->input->post('id_customer') != 'all'){
			$sla = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->input->post('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_status_request', '6')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$sla = $sla->result_array();

			$failed = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->input->post('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_status_request', '5')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$failed = $failed->result_array();

			$in_delivery = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->input->post('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_pod !=', null)
			->where('req.url_photo_mdn', null)
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$in_delivery = $in_delivery->result_array();

			$oos = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->input->post('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_status_request', '3')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$oos = $oos->result_array();

			$total_request = $db_huawei->select('COUNT(req.id_request) AS total')
			->from('dt_request req')
			->where('req.id_customer', $this->input->post('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_project', $this->input->post('id_project'))
			->where('req.id_status_request !=', '7')
			->get();
			// no inject_rmr to show
			$total_request = $total_request->result_array();
		}elseif($this->input->post('id_customer') == 'all'){
			$sla = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_status_request', '6')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$sla = $sla->result_array();

			$failed = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_status_request', '5')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$failed = $failed->result_array();

			$in_delivery = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_pod !=', null)
			->where('req.url_photo_mdn', null)
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$in_delivery = $in_delivery->result_array();

			$oos = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_status_request', '3')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$oos = $oos->result_array();

			$total_request = $db_huawei->select('COUNT(req.id_request) AS total')
			->from('dt_request req')
			->where('id_status_request !=', '7')
			->get();
			// no inject_rmr to show
			$total_request = $total_request->result_array();
		}else{
			$sla = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->session->userdata('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_status_request', '6')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$sla = $sla->result_array();

			$failed = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->session->userdata('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_status_request', '5')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$failed = $failed->result_array();

			$in_delivery = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->session->userdata('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_pod !=', null)
			->where('req.url_photo_mdn', null)
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$in_delivery = $in_delivery->result_array();

			$oos = $db_huawei->select('stat_req.status_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS inner_city, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS outer_city, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS pool_to_pool')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
			->where('req.id_customer', $this->session->userdata('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_status_request', '3')
			->where('req.id_project', $this->input->post('id_project'))
			->get();
			$oos = $oos->result_array();

			$total_request = $db_huawei->select('COUNT(req.id_request) AS total')
			->from('dt_request req')
			->where('req.id_customer', $this->session->userdata('id_customer'))
			->where('req.time_request >=', $date[0])
			->where('req.time_request <=', $date[1])
			->where('req.id_project', $this->input->post('id_project'))
			->where('req.id_status_request !=', '7')
			->get();
			// no inject_rmr to show
			$total_request = $total_request->result_array();
		}

		echo json_encode(array('sla' => $sla, 'failed' => $failed, 'in_delivery' => $in_delivery, 'oos' => $oos, 'total_request' => $total_request));
	}

	public function get_all_request_to_pickup()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 69 || $this->session->userdata('level') == 4) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('(req.url_photo_mdn IS NOT NULL OR req.id_status_request = 1)')
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
		//	->where('req.id_status_request !=', '7')
			// ->where('req.id_status_request !=', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('(req.url_photo_mdn IS NOT NULL OR req.id_status_request = 1)')
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
		//	->where('req.id_status_request !=', '7')
			// ->where('req.id_status_request !=', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}

	public function get_all_request_in_delivery()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 69 || $this->session->userdata('level') == 4) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_pod !=', null)
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.id_pod !=', null)
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}
}