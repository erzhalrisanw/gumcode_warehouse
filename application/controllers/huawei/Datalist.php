<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datalist extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function warehouse() {
		$db_huawei = $this->load->database('default', TRUE);
		$spv = $db_huawei->select('*')
		->from('dt_user user')
		->where('id_userlevel=4 and activated=1')
		->get();
		$data['spv'][0]="";
		foreach($spv->result_array() as $key => $row) {
			$data['spv'][$row['id_user']]=$row['name_user'];
		}

		$fsl = $db_huawei->select('*')
		->from('dt_user user')
		->where('id_userlevel in (5,10) and id_warehouse>0 and activated=1')
		->get();
		$data['fslpic'][0]="";
		$data['fslemail'][0]="";
		$data['fslphone'][0]="";
		foreach($fsl->result_array() as $key => $row) {
			$data['fslpic'][$row['id_warehouse']]=$row['name_user'];
			$data['fslemail'][$row['id_warehouse']]=$row['email'];
			$data['fslphone'][$row['id_warehouse']]=$row['no_telp'];
		}

		$db_huawei = $this->load->database('huawei', TRUE);
		$warehouse = $db_huawei->select('*')
		->from('dm_warehouse warehouse')
		->join('dm_city city', 'city.id_city = warehouse.id_city')
		->join('dm_country country', 'country.id_country = city.id_country')
		->where('id_warehouseclass<3')
		->get();
		$data['warehouse'] = $warehouse->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/report/datalist/warehouse', $data);
	}

	public function pnlist() {
		$this->load->view('huawei/layout');
		$this->load->view('huawei/report/datalist/pnlist');
	}

	public function get_pn_by_input()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$dbdatalist = $db_huawei->select('name_brand, name_basecode, name_pn, basecode.description, weight')
		->from('dm_pn pn')
		->join('dm_basecode basecode', 'pn.id_basecode = basecode.id_basecode')
		->join('dm_brand brand', 'brand.id_brand = basecode.id_brand')
		->like('basecode.name_basecode', $this->input->post('input'))
		->get();

		if($dbdatalist->num_rows() > 0){
			$ret = $dbdatalist->result_array();
		}else{
			$dbdatalist_2 = $db_huawei->select('name_brand, name_basecode, name_pn, basecode.description, weight')
			->from('dm_pn pn')
			->join('dm_basecode basecode', 'pn.id_basecode = basecode.id_basecode')
			->join('dm_brand brand', 'brand.id_brand = basecode.id_brand')
			->like('basecode.description', $this->input->post('input'))
			->get();

			$ret = $dbdatalist_2->result_array();
		}

		echo json_encode($ret);
	}

	public function pncompatibility() {
		$this->load->view('huawei/layout');
		$this->load->view('huawei/report/datalist/pncompatibility');
	}

	public function get_comp_by_input()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$dbdatalist = $db_huawei->select('*, pncomp.pn AS pn_comp, pncomp.type AS type_comp, pncomp.status AS status_1, pncomp.status_compatible AS stat_comp')
		->from('dm_pn pn')
		->join('dm_basecode basecode', 'pn.id_basecode = basecode.id_basecode')
		->join('dm_brand brand', 'brand.id_brand = basecode.id_brand')
		->join('dt_compatibility pncomp', 'pn.id_pn = pncomp.id_pn')
		->like('basecode.name_basecode', $this->input->post('input'))
		->get();

		if($dbdatalist->num_rows() > 0){
			$ret = $dbdatalist->result_array();
		}else{
			$dbdatalist_2 = $db_huawei->select('*, pncomp.pn AS pn_comp, pncomp.type AS type_comp, pncomp.status AS status_1, pncomp.status_compatible AS stat_comp')
			->from('dm_pn pn')
			->join('dm_basecode basecode', 'pn.id_basecode = basecode.id_basecode')
			->join('dm_brand brand', 'brand.id_brand = basecode.id_brand')
			->join('dt_compatibility pncomp', 'pn.id_pn = pncomp.id_pn')
			->like('basecode.description', $this->input->post('input'))
			->get();

			$ret = $dbdatalist_2->result_array();
		}

		echo json_encode($ret);
	}

	public function document() {
		$db_huawei = $this->load->database('default', TRUE);
		$dbdatalist = $db_huawei->select('id_user, name_user')
		->from('dt_user user')
		->get();
		$data['datauser'][0]="";
		foreach($dbdatalist->result_array() as $key => $row) {
			$data['datauser'][$row['id_user']]=$row['name_user'];
		}

		$db_huawei = $this->load->database('huawei', TRUE);
		$dbdatalist = $db_huawei->select('*, boundfrom.code_warehouse as whfrom, boundto.code_warehouse as whto, if(io_bound.status=0,"Closed","Open") as docstatus, ifnull(consignee,0) as consignee')
		->from('dt_io_bound io_bound')
		->join('dm_io_boundtype boundtype', 'io_bound.id_boundtype=boundtype.id_io_boundtype', 'LEFT')
		->join('dm_project project', 'io_bound.id_project=project.id_project', 'LEFT')
		->join('dm_warehouse boundfrom', 'io_bound.id_boundfrom=boundfrom.id_warehouse', 'LEFT')
		->join('dm_warehouse boundto', 'io_bound.id_boundto=boundto.id_warehouse', 'LEFT')
		->join('dm_logistic logistic', 'io_bound.id_logistic=logistic.id_logistic', 'LEFT')
		->join('dm_logisticservice logservice', 'io_bound.id_logistic_service=logservice.id_logisticservice', 'LEFT')
		->get();
		$data['datalist'] = $dbdatalist->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/report/datalist/document', $data);
	}


	public function userlist() {

		$db_huawei = $this->load->database('huawei', TRUE);
		$dbdatalist = $db_huawei->select('*')
		->from('dm_project project')
		->get();
		$data['dataproject'][0]="";
		foreach($dbdatalist->result_array() as $key => $row) {
			$data['dataproject'][$row['id_project']]=$row['name_project'];
		}

		$dbdatalist = $db_huawei->select('*')
		->from('dm_customer customer')
		->get();
		$data['datacustomer'][0]="";
		foreach($dbdatalist->result_array() as $key => $row) {
			$data['datacustomer'][$row['id_customer']]=$row['name_customer'];
		}

		$dbdatalist = $db_huawei->select('*')
		->from('dm_warehouse warehouse')
		->join('dm_warehouseclass warehouseclass', 'warehouse.id_warehouseclass = warehouseclass.id_warehouseclass', 'LEFT')
		->get();
		$data['datawarehouse'][0]="";
		$data['datawarehouseclass'][0]="";
		foreach($dbdatalist->result_array() as $key => $row) {
			$data['datawarehouse'][$row['id_warehouse']]=$row['name_warehouse'];
			$data['datawarehouseclass'][$row['id_warehouse']]=$row['name_warehouseclass'];
		}

		$db_huawei = $this->load->database('default', TRUE);
		$dbdatalist = $db_huawei->select('*, if(activated=1,"Active","Deactivated") as activated')
		->from('dt_user user')
		->join('dm_domain domain', 'user.id_domain = domain.id_domain', 'LEFT')
		->join('dm_userlevel userlevel', 'user.id_userlevel = userlevel.id_userlevel', 'LEFT')
		->where('activated=1')
		->where_not_in('user.id_userlevel', array('1', '13', '11', '69'))
		->get();
		$data['datalist'] = $dbdatalist->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/report/datalist/userlist', $data);
	}



}
