<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Counting_stock extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function stocktake()
	{
		$this->load->view('huawei/layout');
		$this->load->view('huawei/stock/stocktake');
	}

	public function print_sn()
	{
		$this->load->view('huawei/layout');
		$this->load->view('huawei/stock/print_sn');
	}

	public function get_sn_by_loc_bin()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$sn_list = $db_huawei->select('*, pn.name_pn')
		->from('dt_sn sn')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->where('sn.locbin', $this->input->post('loc_bin'))
		->where('sn.id_stockaccess', '1')
		->where('sn.id_warehouse', $this->session->userdata('id_warehouse'))
		->get();

		echo json_encode($sn_list->result_array());
	}

	public function upd_sn_by_sn()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$sn_pn = $db_huawei->select('*, pn.name_pn')
		->from('dt_sn sn')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->where('sn.sn', $this->input->post('stocktake-sn-sn'))
		->where('sn.id_stockaccess', '1')
		->where('sn.id_warehouse', $this->session->userdata('id_warehouse'))
		->get();

		if($sn_pn->num_rows() > 0){
			$sn_pn = $sn_pn->row();

			if($sn_pn->name_pn == $this->input->post('stocktake-sn-pn')){
				$data = array(
					'locbin' => $this->input->post('stocktake-sn-bin'),
					'stocktake_time' => date('Y-m-d H:i:s'),
					'status_cek' => 1,
					// 'id_warehouse' => $this->session->userdata('id_warehouse'),
					'stocktaker' => $this->session->userdata('login_id')
				);

				$db_huawei->where('sn', $this->input->post('stocktake-sn-sn'));
				if($db_huawei->update('dt_sn', $data)){
					$return = true;
				}else{
					$return = false;
				}
			}
		}else{
			$return = false;
		}
		echo json_encode($return);
	}
//Nyoba Gar
	public function progress_stocktake()
	{
		if ($this->session->userdata('domain') == 'huawei') {
			$this->load->view('huawei/layout');
			$this->load->view('huawei/stock/progress_stocktake');
		}else{
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function get_progress_stocktake()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$progress_stocktake = $db_huawei->query("SELECT sn.id_warehouse, warehouse.name_warehouse, 
			SUM(CASE WHEN sn.id_stockaccess='1' THEN 1 ELSE 0 END) as 'total',
			SUM(CASE WHEN sn.id_stockaccess='1' AND sn.status_cek='1' THEN 1 ELSE 0 END) as 'no_cek',
			SUM(CASE WHEN sn.id_stockaccess='2' THEN 1 ELSE 0 END) as 'indelivery', 
			SUM(CASE WHEN sn.id_stockstatus ='1' AND sn.id_stockaccess='1' AND sn.status_cek='1' THEN 1 ELSE 0 END) as 'good_cek', 
			SUM(CASE WHEN sn.id_stockstatus ='2' AND sn.id_stockaccess='1' AND sn.status_cek='1' THEN 1 ELSE 0 END) as 'faulty_cek', 
			SUM(CASE WHEN sn.id_stockstatus ='1' AND sn.id_stockaccess='1' AND sn.status_cek='0' THEN 1 ELSE 0 END) as 'good_uncek', 
			SUM(CASE WHEN sn.id_stockstatus ='2' AND sn.id_stockaccess='1' AND sn.status_cek='0' THEN 1 ELSE 0 END) as 'faulty_uncek' 
			FROM dt_sn sn JOIN dm_warehouse warehouse ON warehouse.id_warehouse = sn.id_warehouse 
			where sn.id_stockposition='1'
			GROUP BY sn.id_warehouse");

		echo json_encode($progress_stocktake->result_array());
	}

	public function uncek_list()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$sn_cek = $db_huawei->select('*, sn.sn, sn.locbin, sn.status_cek, stat.name_stockstatus, akses.name_stockaccess')
		->from('dt_sn sn')
		->join('dm_stockstatus stat', 'sn.id_stockstatus = stat.id_stockstatus')
		->join('dm_stockaccess akses', 'sn.id_stockaccess = akses.id_stockaccess')
		->where('sn.id_warehouse', $this->input->post('cek'))
		->where('sn.status_cek', '0')
		->where('sn.id_stockaccess', '1')
		->where('sn.id_stockposition', '1')
		
		->get();

		$sn_cek = $sn_cek->result_array();

		echo json_encode($sn_cek);
	}

	public function list_HO()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$sn_list = $db_huawei->select('*, sn.sn, pn.name_pn, sn.locbin, sn.status_cek, stat.name_stockstatus, akses.name_stockaccess, sn.stocktake_time')
		->from('dt_sn sn')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->join('dm_stockstatus stat', 'sn.id_stockstatus = stat.id_stockstatus')
		->join('dm_stockaccess akses', 'sn.id_stockaccess = akses.id_stockaccess')
		->where('sn.id_warehouse', $this->input->post('list'))
		->where('sn.status_cek', '1')
		->where('sn.id_stockaccess', '1')
		->where('sn.id_stockposition', '1')
		
		->get();

		$sn_list = $sn_list->result_array();

		echo json_encode($sn_list);
	}

	public function print_sn_manually()
	{
		$input = $this->input->post();
		require APPPATH.'libraries/fpdf_barcode.php';
		$pdf = new PDF_Code128('P', 'mm', 'A4');
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',9);

		$count_change_page = 0;

		$y = 30;
		$h_bar = 20;
		for($k = 0; $k < count($input['locbin']); $k++){
			$pdf->Cell(60,5,'Loc Bin',1,0);
			$pdf->Cell(60,5,'Part Number',1,0);
			$pdf->Cell(60,5,'Serial Number',1,1);
			$pdf->SetY($y);
			$pdf->SetFont('Arial','B',9);
			$pdf->Code128(18,$h_bar,$input['locbin'][$k],45,10);
			$pdf->Code128(78,$h_bar,$input['pn'][$k],45,10);
			$pdf->Code128(133,$h_bar,$input['sn'][$k],50,10);
			$pdf->Cell(60,6,$input['locbin'][$k],0,0,'C');
			$pdf->Cell(60,6,$input['pn'][$k],0,0,'C');
			$pdf->Cell(60,6,$input['sn'][$k],0,1,'C');

			$y = $y + 25;
			$h_bar = $h_bar + 25;
			$count_change_page = $count_change_page + 1;

			if($count_change_page == 10){
				$pdf->AddPage();
				$pdf->SetFont('Arial','B',9);
				$count_change_page = 0;
				$y = 30;
				$h_bar = 20;
			}
		}

		$pdf->Output();

		// print_r($input);
	}

}