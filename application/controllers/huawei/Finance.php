<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Finance extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function create_awb()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$warehouse = $db_huawei->select('*')
		->from('dm_warehouse')
		->get();

		$data['from'] = $warehouse->result_array();
		$data['to'] = $warehouse->result_array();

		$docs = $db_huawei->select('*')
		->from('dt_io_bound')
		->get();

		$data['docs'] = $docs->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/finance/create_awb', $data);
	}

	public function time_sheet()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		if($this->session->userdata('level') == 10){
			$warehouse = $db_huawei->select('*')
			->from('dm_dop')
			->where('activated', '1')
			->where('id_warehouse', $this->session->userdata('id_warehouse'))
			->get();
		}else{
			$warehouse = $db_huawei->select('*')
			->from('dm_dop')
			->where('activated', '1')
			->get();
		}

		$city = $db_huawei->select('*')
		->from('dm_city')
		->get();

		$data['dop'] = $warehouse->result_array();
		$data['city'] = $city->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/finance/time_sheet', $data);
	}

	public function get_pod_cmn_list_by_dop()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_dop = $this->input->post('id_dop');

		$pod = $db_huawei->select('*')
		->from('dt_request req')
		->join('dt_pod pod', 'pod.id_pod = req.id_pod')
		->where('req.id_dop', $id_dop)
		->like('pod.time_created', $this->input->post('date'))
		->group_by('req.id_pod')
		->get();

		$data['pod'] = $pod->result_array();

		$cmn = $db_huawei->select('*')
		->from('dt_request req')
		->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup')
		->where('req.id_dop', $id_dop)
		->like('pickup.time_created', $this->input->post('date'))
		->group_by('req.id_pickup')
		->get();

		$data['pickup'] = $cmn->result_array();

		echo json_encode($data);
	}

	public function get_doc_by_from_to(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$list = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundfrom', $from)
		->where('id_boundto', $to)
		->get();

		echo json_encode($list->result_array());
	}

	public function get_qty_by_doc()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc = $this->input->post('doc_id');

		$list = $db_huawei->select('*')
		->from('dt_sn_history')
		->where('id_doc', $doc)
		->group_by('sn')
		->get();

		echo json_encode(count($list->result_array()));
	}

	public function get_dop_by_city()
	{
		$id_city = $this->input->post('id_city');

		$db_huawei = $this->load->database('huawei', TRUE);

		$list = $db_huawei->select('dop.*')
		->from('dm_warehouse warehouse')
		->join('dm_dop dop', 'dop.id_warehouse = warehouse.id_warehouse')
		->where('warehouse.id_city', $id_city)
		->where('dop.activated', '1')
		// ->group_by('warehouse.id_warehouse')
		->get();

		echo json_encode($list->result_array());
	}
}