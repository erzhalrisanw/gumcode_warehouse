<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Callcenter extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function dashboard()
	{
		/*
		$db_huawei = $this->load->database('huawei', TRUE);

		$id_warehouseclass = $db_huawei->select('*')
		->from('dm_warehouseclass')
		->where_in('id_warehouseclass', array('1', '2'))
		->get();
		$data['id_warehouseclass'] = $id_warehouseclass->result_array();

		$fsl = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouseclass', '2')
		->get();
		$data['fsl'] = $fsl->result_array();
		*/
		//$data=array();

		$hostname = '{mail.harrasima.id:143}INBOX';
		$hostname = "{srv76.niagahoster.com:993/imap/ssl/novalidate-cert}";
		$username = 'callcenter@harrasima.id';
		$password = 'WM5H1L2019*';
		/* koneksi dengan imap */
		$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to mailer: ' . imap_last_error());
		/* ambil semua email di inbox */
		$emails = imap_search($inbox,'UNFLAGGED');
		/* jika ada email, perulangan mengambil semua email */
		if($emails) {
		 	/* urutkan email terbaru di atas */
		 	rsort($emails); 
		 	/* urutkan email terlama di atas */
		 	sort($emails); 
		 	$i=0;
		 	/* ambil semua email */
		 	foreach($emails as $email_number) {
		  		/* informasi email */
		  		$overview = imap_fetch_overview($inbox,$email_number,0);
		  		//$message = imap_fetchbody($inbox,$email_number,2);
		  		/* header email */
				//$check=($overview[0]->seen ? 'read' : 'unread');
				$check=($overview[0]->flagged ? 'read' : 'unread');
				if($check=='read'){
					/*
					$listemail[$i]['read']="<i class='fa fa-check-square'></i>";
			  		$listemail[$i]['subject']=$overview[0]->subject;
			  		$listemail[$i]['from']=$overview[0]->from;
			  		$listemail[$i]['date']=$overview[0]->date;
			  		$listemail[$i]['msgno']=$overview[0]->msgno;
			  		*/
				} else {
					$listemail[$i]['read']="<i class='fa fa-envelope-o'></i>";
			  		$listemail[$i]['subject']=$overview[0]->subject;
			  		$listemail[$i]['from']=$overview[0]->from;
			  		$listemail[$i]['date']=$overview[0]->date;
			  		$listemail[$i]['msgno']=$overview[0]->msgno;
	  				$i++;
				}
		  		/* message email */
		  		//$output.= '<div class="body">'.$message.'</div>';
		 	}
		} 
		imap_close($inbox);

		$i = 0;
		//$resp = new stdClass();
		if(isset($listemail)) {
			if (count($listemail)>0) {
				foreach ($listemail as $val) {
					//$resp->data[$i] = array(
					//	($i+1),
						$listemail[$i]['date'];
						$listemail[$i]['from'];
						$listemail[$i]['subject'];
					//	);
				 	$i++;
				}
			}
			$data['listemail']=$listemail;
		}

		$db_huawei = $this->load->database('huawei', TRUE);
		$warehouse = $db_huawei->select("concat(hour(timediff(a.eta_received,NOW())),':',right(concat('0',minute(timediff(a.eta_received,NOW()))),2) ) as sisawaktu, f.status_request, a.eta_received, a.rmr, a. rma, b.name_dop, e.name_warehouse, if(b.id_warehouse<>a.id_warehouse,'Pool to Pool',d.name_deliverytype) as delivery_type, if(a.eta_received>NOW(),'yellow','red') as bgcolor")
		->from('dt_request a')
		->join('dm_dop b','a.id_dop=b.id_dop','left')
		->join('dt_pod c','a.id_pod=c.id_pod','left')
		->join('dm_deliverytype d','b.id_deliverytype=d.id_deliverytype','left')
		->join('dm_warehouse e','b.id_warehouse=e.id_warehouse','left')
		->join('dm_status_request f','a.id_status_request=f.id_status_request','left')
		->where('a.id_status_request in (0,1,2,8)')
		->order_by('a.eta_received')
		->get();

		$data['inprocess'] = $warehouse->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/callcenter/dashboard', $data);
	}

	public function shift()
	{
		if(isset($_GET['saveshift'])) {
			$db_huawei = $this->load->database('huawei', TRUE);
			$cc_list = $db_huawei->select("*")
			->from('dt_ccshift')
			->where("tanggal='".$_GET['input_date']."' and id_shift=".$_GET['input_shift']." and id_user=".$_GET['input_user'])
			->get();
			if($cc_list->result_array()) {
				foreach($cc_list->result_array() as $key => $row) {
					$sel_id=$row['id_ccshift'];
				}
				$edit_user = array(
					'id_user' => $_GET['input_user']
				);
				$db_huawei->where('id_ccshift', $sel_id);
				$db_huawei->update('dt_ccshift', $edit_user);
			} else {
				$insert_shift = array(
					'tanggal' => $_GET['input_date'],
					'id_shift' => $_GET['input_shift'],
					'id_user' => $_GET['input_user']
				);
				$db_huawei->insert('dt_ccshift', $insert_shift);
			}

		} elseif(isset($_GET['deleteshift'])) {
			$db_huawei = $this->load->database('huawei', TRUE);
			$delete_shift = array(
				'tanggal' => $_GET['input_date'],
				'id_shift' => $_GET['input_shift'],
				'id_user' => $_GET['input_user']
			);
			$db_huawei->delete('dt_ccshift', $delete_shift);
		}

		$db_huawei = $this->load->database('default', TRUE);
		$cc_list = $db_huawei->select("*")
		->from('dt_user')
		->where('id_userlevel in (2,3)')
		->order_by('name_user')
		->get();
		$data['cc_list'] = $cc_list->result_array();

		if(isset($_GET['week'])) $setweek=$_GET['week']; else $setweek=(int)date('W');
		if(isset($_GET['thn'])) $setyear=$_GET['thn']; else $setyear=date('Y');
		$dto = new DateTime();
	    $dto->setISODate($setyear,$setweek);
	    $startweek=$dto->format('Y-m-d');
	    $dto->modify('+6 days');
	    $endweek=$dto->format('Y-m-d');

		$db_huawei = $this->load->database('huawei', TRUE);
		$cc_list = $db_huawei->select("*")
		->from('dt_ccshift a')
		->join('dm_shift b','a.id_shift=b.id_shift','left')
		->where("a.tanggal>='$startweek'")
		->where("a.tanggal<='$endweek'")
		->get();
		$data['jadwal_cc'] = $cc_list->result_array();

		$cc_list = $db_huawei->select("*")
		->from('dm_shift')
		->get();
		$data['data_shift'] = $cc_list->result_array();


		$this->load->view('huawei/layout');
		$this->load->view('huawei/callcenter/shift', $data);
	}

}