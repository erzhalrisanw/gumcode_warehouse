<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_manage extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function crud_city()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_city city')
			->join('dm_country country', 'country.id_country = city.id_country')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_country = $db_huawei->select('*')
			->from('dm_country')
			->get();
		$data['dm_country'] = $dm_country->result_array();
		$data['dm_country_edit'] = $dm_country->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_city', $data);
	}

	public function get_city_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_city = $this->input->post('id_city');
		$user = $db_huawei->select('*')
			->from('dm_city')
			->where('id_city', $id_city)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_city_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'id_country' => $this->input->post('id_country_edit'),
			'name_city' => $this->input->post('name_city_edit')
		);
		$db_huawei->where('id_city', $this->input->post('id_city'));
		if ($db_huawei->update('dm_city', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function open_doc_inventory()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_city city')
			->join('dm_country country', 'country.id_country = city.id_country')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_country = $db_huawei->select('*')
			->from('dm_country')
			->get();
		$data['dm_country'] = $dm_country->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/open_doc_inventory', $data);
	}

	public function user_list()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$list_user = $this->db->select('*')
			->from('dt_user')
			->where('activated', '1')
			->get();
		$data['user_list'] = $list_user->result_array();

		$domain = $db_huawei->select('*')
			->from('dm_domain')
			->get();
		$data['domain'] = $domain->result_array();
		$data['domain_edit'] = $domain->result_array();

		$project = $db_huawei->select('*')
			->from('dm_project')
			->get();
		$data['project'] = $project->result_array();
		$data['project_edit'] = $project->result_array();

		$customer = $db_huawei->select('*')
			->from('dm_customer')
			->get();
		$data['customer'] = $customer->result_array();
		$data['customer_edit'] = $customer->result_array();

		$warehouse = $db_huawei->select('*')
			->from('dm_warehouse')
			->get();
		$data['warehouse'] = $warehouse->result_array();
		$data['warehouse_edit'] = $warehouse->result_array();

		$level = $this->db->select('*')
			->from('dm_userlevel')
			->get();
		$data['level'] = $level->result_array();
		$data['level_edit'] = $level->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/user_list', $data);
	}

	public function get_user_data()
	{
		$id_user = $this->input->post('id_user');
		$user = $this->db->select('*')
			->from('dt_user')
			->where('id_user', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_user_data()
	{
		$edit_user = array(
			'id_domain' => $this->input->post('id_domain_edit'),
			'id_customer' => $this->input->post('id_customer_edit'),
			'id_project' => $this->input->post('id_project_edit'),
			'id_warehouse' => $this->input->post('id_warehouse_edit'),
			'id_userlevel' => $this->input->post('id_userlevel_edit'),
			'activated' => $this->input->post('activated_edit')
		);
		$this->db->where('id_user', $this->input->post('id_user'));
		if ($this->db->update('dt_user', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_customer()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_customer')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_country = $db_huawei->select('*')
			->from('dm_projecttype')
			->get();
		$data['dm_country'] = $dm_country->result_array();
		$data['dm_country_edit'] = $dm_country->result_array();

		$dm_region = $db_huawei->select('*')
			->from('dm_region')
			->get();
		$data['dm_region'] = $dm_region->result_array();
		$data['dm_region_edit'] = $dm_region->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_customer', $data);
	}

	public function get_customer_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_customer');
		$user = $db_huawei->select('*')
			->from('dm_customer')
			->where('id_customer', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_customer_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_customer' => $this->input->post('name_customer_edit'),
			'code_customer' => $this->input->post('code_customer_edit'),
			'tipe_customer' => $this->input->post('tipe_customer_edit'),
			'id_projecttype' => $this->input->post('id_projecttype_edit'),
			'id_region' => $this->input->post('id_region_edit')
		);
		$db_huawei->where('id_customer', $this->input->post('id_customer'));
		if ($db_huawei->update('dm_customer', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_logistic()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_logistic')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_country = $db_huawei->select('*')
			->from('dm_city')
			->get();
		$data['dm_country'] = $dm_country->result_array();
		$data['dm_country_edit'] = $dm_country->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_logistic', $data);
	}

	public function get_logistic_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_logistic');
		$user = $db_huawei->select('*')
			->from('dm_logistic')
			->where('id_logistic', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_logistic_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'id_city' => $this->input->post('id_city_edit'),
			'name_logistic' => $this->input->post('name_logistic_edit'),
			'code_logistic' => $this->input->post('code_logistic_edit'),
			'address' => $this->input->post('address_logistic_edit'),
			'phone' => $this->input->post('phone_logistic_edit')
		);
		$db_huawei->where('id_logistic', $this->input->post('id_logistic'));
		if ($db_huawei->update('dm_logistic', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_logistic_service()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_logisticservice')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_logistic_service', $data);
	}

	public function get_logserv_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_logserv');
		$user = $db_huawei->select('*')
			->from('dm_logisticservice')
			->where('id_logisticservice', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_logserv_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_logisticservice' => $this->input->post('name_logisticservice_edit')
		);
		$db_huawei->where('id_logisticservice', $this->input->post('id_logserv'));
		if ($db_huawei->update('dm_logisticservice', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_project()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_project project')
			->join('dm_country country', 'country.id_country = project.id_country')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_country = $db_huawei->select('*')
			->from('dm_country')
			->get();
		$data['dm_country'] = $dm_country->result_array();
		$data['dm_country_edit'] = $dm_country->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_project', $data);
	}

	public function get_project_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_project');
		$user = $db_huawei->select('*')
			->from('dm_project')
			->where('id_project', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_project_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_project' => $this->input->post('name_project_edit'),
			'rmr_code' => $this->input->post('rmr_code_edit'),
			'id_country' => $this->input->post('id_country_edit')
		);
		$db_huawei->where('id_project', $this->input->post('id_project'));
		if ($db_huawei->update('dm_project', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_project_type()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('type.*, project.name_project')
			->from('dm_projecttype type')
			->join('dm_project project', 'project.id_project = type.id_project')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_project = $db_huawei->select('*')
			->from('dm_project')
			->get();
		$data['dm_project'] = $dm_project->result_array();
		$data['dm_project_edit'] = $dm_project->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_project_type', $data);
	}

	public function get_projecttype_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_projecttype');
		$user = $db_huawei->select('*')
			->from('dm_projecttype')
			->where('id_projecttype', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_projecttype_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_projecttype' => $this->input->post('name_projecttype_edit'),
			'rmr_code' => $this->input->post('rmr_code_edit'),
			'id_project' => $this->input->post('id_project_edit')
		);
		$db_huawei->where('id_projecttype', $this->input->post('id_projecttype'));
		if ($db_huawei->update('dm_projecttype', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_warehouse()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$first = $db_huawei->select('*')
			->from('dm_warehouse w')
			->join('dm_city c', 'c.id_city = w.id_city')
			->get();
		$data['first'] = $first->result_array();

		$dm_warehouseclass = $db_huawei->select('*')
			->from('dm_warehouseclass')
			->get();
		$data['dm_warehouseclass'] = $dm_warehouseclass->result_array();
		$data['dm_warehouseclass_edit'] = $dm_warehouseclass->result_array();

		$dm_city = $db_huawei->select('*')
			->from('dm_city')
			->get();
		$data['dm_city'] = $dm_city->result_array();
		$data['dm_city_edit'] = $dm_city->result_array();

		$dt_user = $this->db->select('*')
			->from('dt_user')
			->where('id_userlevel', '4')
			->get();
		$data['dt_user'] = $dt_user->result_array();
		$data['dt_user_edit'] = $dt_user->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_warehouse', $data);
	}

	public function get_warehouse_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_warehouse');
		$user = $db_huawei->select('*')
			->from('dm_warehouse')
			->where('id_warehouse', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_warehouse_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_warehouse' => $this->input->post('name_warehouse_edit'),
			'code_warehouse' => $this->input->post('code_warehouse_edit'),
			'address' => $this->input->post('address_warehouse_edit'),
			'id_warehouseclass' => $this->input->post('id_warehouseclass_edit'),
			'id_city' => $this->input->post('id_city_edit'),
			'id_spv' => $this->input->post('id_spv_edit')
		);
		$db_huawei->where('id_warehouse', $this->input->post('id_warehouse'));
		if ($db_huawei->update('dm_warehouse', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_compatibility()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$first = $db_huawei->select('c.*, p.name_pn')
			->from('dt_compatibility c')
			->join('dm_pn p', 'p.id_pn = c.id_pn')
			->get();
		$data['first'] = $first->result_array();

		$dm_pn = $db_huawei->select('*')
			->from('dm_pn')
			->get();
		$data['dm_pn'] = $dm_pn->result_array();
		$data['dm_pn_edit'] = $dm_pn->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_compatibility', $data);
	}

	public function get_compatibility_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_compatibility');
		$user = $db_huawei->select('*')
			->from('dt_compatibility')
			->where('id_compatibility', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_compatibility_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'pn' => $this->input->post('pn_edit'),
			'status' => $this->input->post('status_edit'),
			'type' => $this->input->post('type_edit'),
			'status_compatible' => $this->input->post('status_compatible_edit'),
			'id_pn' => $this->input->post('id_pn_edit')
		);
		$db_huawei->where('id_compatibility', $this->input->post('id_compatibility'));
		if ($db_huawei->update('dt_compatibility', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_region()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_region')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_region', $data);
	}

	public function get_region_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_region');
		$user = $db_huawei->select('*')
			->from('dm_region')
			->where('id_region', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_region_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_region' => $this->input->post('name_region_edit')
		);
		$db_huawei->where('id_region', $this->input->post('id_region'));
		if ($db_huawei->update('dm_region', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_site()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_site site')
			->join('dm_dop dop', 'dop.id_dop = site.id_dop')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_dop = $db_huawei->select('*')
			->from('dm_dop')
			->where('activated', '1')
			->get();
		$data['dm_dop'] = $dm_dop->result_array();
		$data['dm_dop_edit'] = $dm_dop->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_site', $data);
	}

	public function get_site_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_site');
		$user = $db_huawei->select('*')
			->from('dm_site')
			->where('id_site', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_site_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_site' => $this->input->post('name_site_site'),
			'code_site' => $this->input->post('code_site_site'),
			'id_dop' => $this->input->post('id_dop_site')
		);
		$db_huawei->where('id_site', $this->input->post('id_site'));
		if ($db_huawei->update('dm_site', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function crud_bound_type()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$dm_city = $db_huawei->select('*')
			->from('dm_io_boundtype d')
			->join('dm_warehouseclass c', 'c.id_warehouseclass = d.id_warehouse_class')
			->get();
		$data['dm_city'] = $dm_city->result_array();

		$dm_warehouseclass = $db_huawei->select('*')
			->from('dm_warehouseclass')
			->get();
		$data['dm_warehouseclass'] = $dm_warehouseclass->result_array();
		$data['dm_warehouseclass_edit'] = $dm_warehouseclass->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/admin/crud_bound_type', $data);
	}

	public function get_boundtype_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_user = $this->input->post('id_io_boundtype');
		$user = $db_huawei->select('*')
			->from('dm_io_boundtype')
			->where('id_io_boundtype', $id_user)
			->get();
		$user = $user->row();
		echo json_encode($user);
	}

	public function edit_boundtype_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$edit_user = array(
			'name_io_boundtype' => $this->input->post('name_io_boundtype_edit'),
			'io_boundtype_code' => $this->input->post('io_boundtype_code_edit'),
			'id_warehouse_class' => $this->input->post('id_warehouse_class_edit')
		);
		$db_huawei->where('id_io_boundtype', $this->input->post('id_io_boundtype'));
		if ($db_huawei->update('dm_io_boundtype', $edit_user)) {
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function force_create_city()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_city' => $this->input->post('name_city'),
				'id_country' => $this->input->post('id_country')
			);
		if ($db_huawei->insert('dm_city', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_city()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_city', array('id_city' => $this->input->post('id_city')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete city');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete city');
		}

		echo json_encode($return);
	}

	public function force_create_customer()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_customer' => $this->input->post('name_customer'),
				'code_customer' => $this->input->post('code_customer'),
				'tipe_customer' => $this->input->post('tipe_customer'),
				'id_projecttype' => $this->input->post('id_projecttype'),
				'id_region' => $this->input->post('id_region')
			);
		if ($db_huawei->insert('dm_customer', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_customer()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_customer', array('id_customer' => $this->input->post('id_customer')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete customer');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete customer');
		}

		echo json_encode($return);
	}

	public function force_create_logistic()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_logistic' => $this->input->post('name_logistic'),
				'code_logistic' => $this->input->post('code_logistic'),
				'address' => $this->input->post('address_logistic'),
				'phone' => $this->input->post('phone_logistic'),
				'id_city' => $this->input->post('id_city'),
				'activated' => '1'
			);
		if ($db_huawei->insert('dm_logistic', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_logistic()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_logistic', array('id_logistic' => $this->input->post('id_logistic')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete logistic');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete logistic');
		}

		echo json_encode($return);
	}

	public function force_create_logisticservice()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_logisticservice' => $this->input->post('name_logisticservice')
			);
		if ($db_huawei->insert('dm_logisticservice', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_logisticservice()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_logisticservice', array('id_logisticservice' => $this->input->post('id_logisticservice')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete logistic service');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete logistic service');
		}

		echo json_encode($return);
	}

	public function force_create_project()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_project' => $this->input->post('name_project'),
				'rmr_code' => $this->input->post('rmr_code'),
				'id_country' => $this->input->post('id_country')
			);
		if ($db_huawei->insert('dm_project', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_project()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_project', array('id_project' => $this->input->post('id_project')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete project');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete project');
		}

		echo json_encode($return);
	}

	public function force_create_projecttype()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_projecttype' => $this->input->post('name_projecttype'),
				'rmr_code' => $this->input->post('rmr_code'),
				'id_project' => $this->input->post('id_project')
			);
		if ($db_huawei->insert('dm_projecttype', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_projecttype()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_projecttype', array('id_projecttype' => $this->input->post('id_projecttype')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete project type');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete project type');
		}

		echo json_encode($return);
	}

	public function force_create_warehouse()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_warehouse' => $this->input->post('name_warehouse'),
				'code_warehouse' => $this->input->post('code_warehouse'),
				'address' => $this->input->post('address_warehouse'),
				'id_warehouseclass' => $this->input->post('id_warehouseclass'),
				'id_city' => $this->input->post('id_city'),
				'id_spv' => $this->input->post('id_spv')
			);
		if ($db_huawei->insert('dm_warehouse', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_warehouse()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_warehouse', array('id_warehouse' => $this->input->post('id_warehouse')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete warehouse');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete warehouse');
		}

		echo json_encode($return);
	}

	public function force_create_compatible()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'pn' => $this->input->post('pn'),
				'id_pn' => $this->input->post('id_pn'),
				'status' => $this->input->post('status'),
				'type' => $this->input->post('type'),
				'status_compatible' => $this->input->post('status_compatible')
			);
		if ($db_huawei->insert('dt_compatibility', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_compatible()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dt_compatibility', array('id_compatibility' => $this->input->post('id_compatibility')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete compatibility');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete compatibility');
		}

		echo json_encode($return);
	}

	public function force_create_region()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_region' => $this->input->post('name_region')
			);
		if ($db_huawei->insert('dm_region', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_region()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_region', array('id_region' => $this->input->post('id_region')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete region');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete region');
		}

		echo json_encode($return);
	}

	public function force_create_site()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_site' => $this->input->post('name_site'),
				'code_site' => $this->input->post('code_site'),
				'id_dop' => $this->input->post('id_dop')
			);
		if ($db_huawei->insert('dm_site', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_site()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_site', array('id_site' => $this->input->post('id_site')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete site');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete site');
		}

		echo json_encode($return);
	}

	public function force_create_bound_type()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data =
			array(
				'name_io_boundtype' => $this->input->post('name_io_boundtype'),
				'io_boundtype_code' => $this->input->post('io_boundtype_code'),
				'id_warehouse_class' => $this->input->post('id_warehouse_class')
			);
		if ($db_huawei->insert('dm_io_boundtype', $data)) {
			$return = array('stat' => true, 'det' => 'Data successfully saved');
		} else {
			$return = array('stat' => false, 'det' => 'Data cannot be saved');
		}

		echo json_encode($return);
	}

	public function force_delete_bound_type()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if ($db_huawei->delete('dm_io_boundtype', array('id_io_boundtype' => $this->input->post('id_io_boundtype')))) {
			$return = array('stat' => true, 'det' => 'Successfully delete bound type');
		} else {
			$return = array('stat' => false, 'det' => 'Cannot delete bound type');
		}

		echo json_encode($return);
	}

	public function get_all_doc_outbound()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$list = $db_huawei->select('*')
			->from('dt_io_bound doc')
			->join('dm_io_boundtype type', 'type.id_io_boundtype = doc.id_boundtype')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = doc.create_by')
			->like('type.name_io_boundtype', 'outbound')
			->where('doc.date_received', null)
			->get();

		echo json_encode($list->result_array());
	}

	public function get_all_doc_inbound()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$list = $db_huawei->select('doc.*, hst.sn AS sn_in_doc')
			->from('dt_io_bound doc')
			->join('dm_io_boundtype type', 'type.id_io_boundtype = doc.id_boundtype')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = doc.create_by')
			->join('dt_sn_history hst', 'hst.id_doc = doc.id')
			->like('type.name_io_boundtype', 'inbound')
			->group_by('hst.sn')
			->get();
		$list = $list->result_array();

		$count_except = 0;
		for ($i = 0; $i < count($list); $i++) {
			$check_sn_doc = $db_huawei->select('*')
				->from('dt_sn_history')
				->where('sn', $list[$i]['sn_in_doc'])
				->where('(id_doc != ' . $list[$i]['id'] . ' OR (id_pod IS NOT NULL OR id_pickup IS NOT NULL))')
				->get();
			if ($check_sn_doc->num_rows()) {
				$except[$count_except] = $list[$i]['id'];
				$count_except++;
			}
		}

		$res = $db_huawei->select('*')
			->from('dt_io_bound doc')
			->join('dm_io_boundtype type', 'type.id_io_boundtype = doc.id_boundtype')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = doc.create_by')
			->like('type.name_io_boundtype', 'inbound')
			->where_not_in('doc.id', $except)
			->get();

		echo json_encode($res->result_array());
	}

	public function open_doc_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$type = $this->input->post('type');
		$id_doc = $this->input->post('id_doc');

		if ($type == 1) {
			$upd_doc = array(
				'status' => '1',
				'close_date' => null
			);
			$db_huawei->where('id', $id_doc);
			if ($db_huawei->update('dt_io_bound', $upd_doc)) {
				$check_sn_doc = $db_huawei->select('*')
					->from('dt_sn_history')
					->where('id_doc', $id_doc)
					->group_by('sn')
					->get();
				$check_sn_doc = $check_sn_doc->result_array();

				for ($i = 0; $i < count($check_sn_doc); $i++) {
					$upd_sn = array(
						'id_stockaccess' => '6'
					);
					$db_huawei->where('id_sn', $check_sn_doc[$i]['sn']);
					$db_huawei->update('dt_sn', $upd_sn);
				}

				$ret = true;
			} else {
				$ret = false;
			}
		} else {
			$upd_doc = array(
				'status' => '1',
				'close_date' => null,
				'date_send' => null,
				'awb_no' => null,
				'id_logistic_service' => null,
				'id_logistic' => null,
				'consignee' => null
			);
			$db_huawei->where('id', $id_doc);
			if ($db_huawei->update('dt_io_bound', $upd_doc)) {
				$check_sn_doc = $db_huawei->select('*')
					->from('dt_sn_history')
					->where('id_doc', $id_doc)
					->group_by('sn')
					->get();
				$check_sn_doc = $check_sn_doc->result_array();

				$get_doc_det = $db_huawei->select('*')
					->from('dt_io_bound')
					->where('id', $id_doc)
					->get();
				$get_doc_det = $get_doc_det->row();

				for ($i = 0; $i < count($check_sn_doc); $i++) {
					$upd_sn = array(
						'id_stockaccess' => '3',
						'id_warehouse' => $get_doc_det->id_boundfrom
					);
					$db_huawei->where('id_sn', $check_sn_doc[$i]['sn']);
					$db_huawei->update('dt_sn', $upd_sn);
				}

				$ret = true;
			} else {
				$ret = false;
			}
		}

		echo json_encode($ret);
	}

	public function userlog()
	{
		if ($this->session->userdata('level') == 69) {
			if(isset($_GET['bln'])) $setmonth=$_GET['bln']; else $setmonth=date('n');
			if(isset($_GET['thn'])) $setyear=$_GET['thn']; else $setyear=date('Y');
			$db_huawei = $this->load->database('default', TRUE);
			$log = $db_huawei->select('*, day(a.timestamp) as userday, count(a.id_user) as usercount')
			->from('dt_user_log a')
			->join('dt_user b', 'a.id_user = b.id_user')
			->where('month(timestamp)='.$setmonth.' and year(timestamp)='.$setyear)
			->order_by('name_user')
			->group_by('a.id_user, day(a.timestamp)')
			->get();
			$data['log'] = $log->result_array();

			$this->load->view('huawei/layout');
			$this->load->view('huawei/manage/userlog', $data);
		}
	}

	public function get_user_log()
	{
		// $days = $this->input->post('days_this_month');
		if(isset($_GET['bln'])) $setmonth=$_GET['bln']; else $setmonth=date('n');
		if(isset($_GET['thn'])) $setyear=$_GET['thn']; else $setyear=date('Y');
		$dom=cal_days_in_month(CAL_GREGORIAN, $setmonth, $setyear);

		$db_huawei = $this->load->database('default', TRUE);
		for($i = 0; $i < $dom; $i++){
			$req_today = $db_huawei->select('COUNT(id_user) as total')
			->from('dt_user_log')
			->where('day(timestamp)', $i+1)
			->where('month(timestamp)', $setmonth)
			->where('year(timestamp)', $setyear)
			->get();

			$req_today = $req_today->row();
			$req_today = $req_today->total;
			$all_data[$i] = intval($req_today);
		}

		echo json_encode($all_data);
	}
}