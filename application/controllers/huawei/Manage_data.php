<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manage_data extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function reset_stocktake()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$id_warehouseclass = $db_huawei->select('*')
		->from('dm_warehouseclass')
		->where_in('id_warehouseclass', array('1', '2'))
		->get();
		$data['id_warehouseclass'] = $id_warehouseclass->result_array();

		$fsl = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouseclass', '2')
		->get();
		$data['fsl'] = $fsl->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/reset_stocktake', $data);
	}

	public function add_requestor()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$project = $db_huawei->select('*')
		->from('dm_project')
		->get();
		$data['project'] = $project->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/add_requestor', $data);
	}

	public function add_dop()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$del_type = $db_huawei->select('*')
		->from('dm_deliverytype')
		->get();
		$data['del_type'] = $del_type->result_array();

		$customer = $db_huawei->select('*')
		->from('dm_customer')
		->get();
		$data['customer'] = $customer->result_array();

		$warehouse = $db_huawei->select('*')
		->from('dm_warehouse')
		->get();
		$data['warehouse'] = $warehouse->result_array();

		$region = $db_huawei->select('*')
		->from('dm_region')
		->get();
		$data['region'] = $region->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/add_dop', $data);
	}

	public function add_pn_new()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$basecode = $db_huawei->select('*')
		->from('dm_basecode')
		->get();
		$data['basecode'] = $basecode->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/manage/add_pn_new', $data);
	}

	public function reset_stocktake_process()
	{
		$id_warehouseclass = $this->input->post('id_warehouseclass');
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->input->post('type_name') == 1){
			$warehouse_list = $db_huawei->select('*')
			->from('dm_warehouse')
			->where('id_warehouseclass', $id_warehouseclass)
			->get();
			$warehouse_list = $warehouse_list->result_array();

			for($i = 0; $i < count($warehouse_list); $i++){
				$upd_stock = array(
					'status_cek' => '0'
				);
				$db_huawei->where('id_warehouse', $warehouse_list[$i]['id_warehouse']);
				$db_huawei->update('dt_sn', $upd_stock);
			}
		}else{
			$upd_stock = array(
				'status_cek' => '0'
			);
			$db_huawei->where('id_warehouse', $this->input->post('id_fsl'));
			$db_huawei->update('dt_sn', $upd_stock);
		}

		echo json_encode(true);
	}

	public function get_customer_list_requestor()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$list = $db_huawei->select('cust.*')
		->from('dm_customer cust')
		->join('dm_projecttype type', 'type.id_projecttype = cust.id_projecttype')
		->where('type.id_project', $this->input->post('id_project'))
		->get();

		echo json_encode($list->result_array());
	}

	public function get_region_list_requestor()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$cust = $db_huawei->select('*')
		->from('dm_customer')
		->where('id_customer', $this->input->post('id_customer'))
		->get();
		$cust = $cust->row();

		if($cust->id_region > 0){
			$list = $db_huawei->select('*')
			->from('dm_region')
			->where('id_region', $cust->id_region)
			->get();
		}else{
			$list = $db_huawei->select('*')
			->from('dm_region')
			->get();
		}

		echo json_encode($list->result_array());
	}

	public function add_requestor_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data = 
		array(
			'id_project' => $this->input->post('id_project'),
			'id_customer' => $this->input->post('id_customer'),
			'id_region' => $this->input->post('id_region'),
			'name_requestor' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'activated' => '1'
		);
		$db_huawei->insert('dt_requestor', $data);
		echo json_encode(true);
	}

	public function add_dop_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data = 
		array(
			'name_dop' => $this->input->post('name'),
			'alamat_dop' => $this->input->post('alamat'),
			'id_deliverytype' => $this->input->post('id_deliverytype'),
			'id_customer' => $this->input->post('id_customer'),
			'id_warehouse' => $this->input->post('id_warehouse'),
			'sla' => $this->input->post('sla'),
			'id_district' => '1',
			'id_region' => $this->input->post('id_region'),
			'activated' => '1'
		);
		$db_huawei->insert('dm_dop', $data);
		echo json_encode(true);
	}

	public function add_pn_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data = 
		array(
			'name_pn' => $this->input->post('pn'),
			'id_basecode' => $this->input->post('id_basecode'),
			'pn_ver' => ''
		);
		$db_huawei->insert('dm_pn', $data);
		echo json_encode(true);
	}

	public function change_status_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$sn_col = explode(', ', $this->input->post('sn'));
		$count = 0;
		for($i = 0; $i < count($sn_col); $i++){
			$data = array(
				'id_stockaccess' => '10'
			);

			$db_huawei->where('sn', $sn_col[$i]);
			if(!$db_huawei->update('dt_sn', $data)){
				break;
			}
			$count++;
		}

		if($count == count($sn_col)){
			$return = true;
		}else{
			$return = false;
		}
		echo json_encode($return);
	}

	public function get_all_request_deliver()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$list = $db_huawei->select('*')
		->from('dt_request req')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_user = id_callcenter')
		->where('req.id_sn !=', null)
		->where('req.id_pickup', null)
		->get();
		$list = $list->result_array();
		echo json_encode($list);
	}

	public function get_all_request_pickup()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$list = $db_huawei->select('*')
		->from('dt_request req')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_user = id_callcenter')
		->where('req.id_pickup !=', null)
		->where('req.inbound_order', null)
		->get();
		$list = $list->result_array();
		echo json_encode($list);
	}

	public function open_rmr_processs()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$type = $this->input->post('type');
		$id_request = $this->input->post('id_request');
		
		if($type == 1){
			$rmr_det = $db_huawei->select('*')
			->from('dt_request')
			->where('id_request', $id_request)
			->get();
			$rmr_det = $rmr_det->row();

			if($db_huawei->delete('dt_pod', array('id_pod' => $rmr_det->id_pod))){
				$upd_sn = array(
					'id_stockaccess' => '1',
					'id_stockposition' => '1'
				);
				$db_huawei->where('id_sn', $rmr_det->id_sn);
				if($db_huawei->update('dt_sn', $upd_sn)){
					if($rmr_det->url_photo_mdn != null){
						unlink(realpath(APPPATH . '..'.$rmr_det->url_photo_mdn));
					}

					if($rmr_det->id_pickup != null){
						$upd_rmr = array(
							'url_photo_mdn' => null,
							'outbound_order' => null,
							'id_status_request' => '2',
							'id_sn' => null,
							'id_pod' => null
						);
					}else{
						$upd_rmr = array(
							'url_photo_mdn' => null,
							'outbound_order' => null,
							'id_status_request' => '0',
							'id_sn' => null,
							'id_pod' => null
						);
					}
					$db_huawei->where('id_request', $id_request);
					if($db_huawei->update('dt_request', $upd_rmr)){
						$ret = true;
					}else{
						$ret = false;
					}
				}else{
					$ret = false;
				}
			}else{
				$ret = false;
			}
		}else{
			$rmr_det = $db_huawei->select('*')
			->from('dt_request')
			->where('id_request', $id_request)
			->get();
			$rmr_det = $rmr_det->row();

			if($db_huawei->delete('dt_pickup', array('id_pickup' => $rmr_det->id_pickup))){
				$check_sn_doc = $db_huawei->select('*')
				->from('dt_sn_history')
				->where('sn', $rmr_det->id_sn_pickup)
				->where('id_doc !=', null)
				->get();

				if($check_sn_doc->num_rows() > 0){
					$upd_sn = array(
						'id_stockaccess' => '4',
						'id_stockposition' => '5'
					);
					$db_huawei->where('id_sn', $rmr_det->id_sn_pickup);
					$db_huawei->update('dt_sn', $upd_sn);
				}else{
					$db_huawei->delete('dt_sn', array('id_sn' => $rmr_det->id_sn_pickup));
				}

				if($rmr_det->url_photo_cmn != null){
					unlink(realpath(APPPATH . '..'.$rmr_det->url_photo_cmn));
				}

				if($rmr_det->id_pod == null){
					$upd_rmr = array(
						'id_pn_pickup' => null,
						'aging_stop' => null,
						'id_statuspickup' => null,
						'id_sn_pickup' => null,
						'url_photo_cmn' => null,
						'remark_pickup' => null,
						'id_pickup' => null,
						'inbound_order' => null
					);
				}else{
					$upd_rmr = array(
						'id_pn_pickup' => null,
						'aging_stop' => null,
						'id_statuspickup' => null,
						'id_sn_pickup' => null,
						'url_photo_cmn' => null,
						'remark_pickup' => null,
						'id_pickup' => null,
						'inbound_order' => null
					);
				}
				$db_huawei->where('id_request', $id_request);
				if($db_huawei->update('dt_request', $upd_rmr)){
					$ret = true;
				}else{
					$ret = false;
				}
			}else{
				$ret = false;
			}
		}

		echo json_encode($ret);
	}
}