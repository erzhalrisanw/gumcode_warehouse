<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fsl extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function inbound_good()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$ib_type = $db_huawei->select('*')
		->from('dm_io_boundtype')
		->where('id_warehouse_class', '2')
		->where_in('id_io_boundtype', array('11', '13'))
		->get();

		$data['ib_type'] = $ib_type->result_array();

		$logistic = $db_huawei->select('*')
		->from('dm_logistic')
		->where('activated', '1')
		->get();

		$data['logistic'] = $logistic->result_array();

		$logistic_service = $db_huawei->select('*')
		->from('dm_logisticservice')
			// ->where('activated', '1')
		->get();

		$data['logistic_service'] = $logistic_service->result_array();

		$from = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouseclass', '3')
		->get();

		$data['from'] = $from->result_array();

		$proj = $db_huawei->select('*')
		->from('dm_project')
		->get();

		$data['proj'] = $proj->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/inventory/fsl_inbound/good_unit', $data);
	}

	public function outbound_good()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$ib_type = $db_huawei->select('*')
		->from('dm_io_boundtype')
		->where('id_warehouse_class', '2')
		->like('name_io_boundtype', 'outbound')
		->where('id_io_boundtype !=', '7')
		->get();

		$data['ib_type'] = $ib_type->result_array();

		$to = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouse !=', $this->session->userdata('id_warehouse'))
		->get();

		$data['to'] = $to->result_array();

		$proj = $db_huawei->select('*')
		->from('dm_project')
		->get();

		$data['proj'] = $proj->result_array();

		$logistic = $db_huawei->select('*')
		->from('dm_logistic')
		->where('activated', '1')
		->get();

		$data['logistic'] = $logistic->result_array();

		$logistic_service = $db_huawei->select('*')
		->from('dm_logisticservice')
			// ->where('activated', '1')
		->get();

		$data['logistic_service'] = $logistic_service->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/inventory/fsl_outbound/good_unit', $data);
	}

	public function inbound_faulty()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$ib_type = $db_huawei->select('*')
		->from('dm_io_boundtype')
		->where('id_warehouse_class', '2')
		->like('name_io_boundtype', 'inbound handover')
		->get();

		$data['ib_type'] = $ib_type->result_array();

		$logistic = $db_huawei->select('*')
		->from('dm_logistic')
		->where('activated', '1')
		->get();

		$data['logistic'] = $logistic->result_array();

		$logistic_service = $db_huawei->select('*')
		->from('dm_logisticservice')
			// ->where('activated', '1')
		->get();

		$data['logistic_service'] = $logistic_service->result_array();

		$from = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouseclass', '3')
		->get();

		$data['from'] = $from->result_array();

		$proj = $db_huawei->select('*')
		->from('dm_project')
		->get();

		$data['proj'] = $proj->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/inventory/fsl_inbound/faulty_unit', $data);
	}

	public function outbound_faulty()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$ib_type = $db_huawei->select('*')
		->from('dm_io_boundtype')
		->where('id_warehouse_class', '2')
		->like('name_io_boundtype', 'outbound')
		->where_in('id_io_boundtype', array('7', '10'))
		->get();

		$data['ib_type'] = $ib_type->result_array();

		$to = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouseclass !=', '2')
		->get();

		$data['to'] = $to->result_array();

		$proj = $db_huawei->select('*')
		->from('dm_project')
		->get();

		$data['proj'] = $proj->result_array();

		$logistic = $db_huawei->select('*')
		->from('dm_logistic')
		->where('activated', '1')
		->get();

		$data['logistic'] = $logistic->result_array();

		$logistic_service = $db_huawei->select('*')
		->from('dm_logisticservice')
			// ->where('activated', '1')
		->get();

		$data['logistic_service'] = $logistic_service->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/inventory/fsl_outbound/faulty_unit', $data);
	}

	public function inbound_good_doc_submit()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = false;
		$checkDocCount = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundto', $this->session->userdata('id_warehouse'))
		->where('id_boundtype', $this->input->post('id_boundtype'))
		->where('id_boundfrom', $this->input->post('id_boundfrom'))
		->like('doc_date', date('Y-m-d'))
		->get();
		$checkDocCount = $checkDocCount->num_rows() + 1;

		$boundCode = $db_huawei->select('io_boundtype_code')
		->from('dm_io_boundtype')
		->where('id_io_boundtype', $this->input->post('id_boundtype'))
		->get();
		$boundCode = $boundCode->row();

		$fromName = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouse', $this->input->post('id_boundfrom'))
		->get();
		$fromName = $fromName->row();

		$bondTo = $db_huawei->select('code_warehouse')
		->from('dm_warehouse')
		->where('id_warehouse', $this->session->userdata('id_warehouse'))
		->get();
		$bondTo = $bondTo->row();

		$doc_no = $boundCode->io_boundtype_code.'/'.$fromName->code_warehouse.$bondTo->code_warehouse.'/'.date("ymd").'/'.sprintf("%03s", $checkDocCount);

		$dataDocInbound = 
		array(
			'doc_no' => $doc_no,
			'doc_date' => date('Y-m-d H:i:s'),
			'id_boundtype' => $this->input->post('id_boundtype'),
			'id_boundfrom' => $this->input->post('id_boundfrom'),
			'id_boundto' => $this->session->userdata('id_warehouse'),
			'id_logistic_service' => $this->input->post('id_logistic_service'),
			'id_stockstatus' => '1',
			'id_project' => $this->input->post('id_project'),
			'id_logistic' => $this->input->post('id_logistic'),
			'awb_no' => $this->input->post('awb_no'),
			'create_by' => $this->session->userdata('login_id')
		);

		if($db_huawei->insert('dt_io_bound', $dataDocInbound)){
			$return = array('doc_no' => $doc_no, 'doc_date' => $dataDocInbound['doc_date']);
		} else {
			$return = false;
		}
		echo json_encode($return);
	}

	public function get_inbound_fsl_doc_list(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc_list = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundto', $this->session->userdata('id_warehouse'))
		->where('id_stockstatus', '1')
		->order_by('doc_date', 'DESC')
		->get();

		$doc_list = $doc_list->result_array();

		for($i=0; $i<count($doc_list);$i++) {
			$qty = $db_huawei->select('qty')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_list[$i]['id'])
			->get();

			$qty = $qty->result_array();
			$qty_total = 0;

			for($k=0; $k<count($qty);$k++) {
				$qty_total = intval($qty_total+$qty[$k]['qty']);
			};

			$sn_added = $db_huawei->select('*')
			->from('dt_sn_history')
			->where('id_doc', $doc_list[$i]['id'])
			->where('id_request', null)
			->where('id_pod', null)
			->where('id_pickup', null)
			->group_by('sn')
			->get();

			$sn_added_check_putaway = $sn_added->result_array();
			$doc_list[$i]['for_close'] = 0;

			for($z=0; $z<count($sn_added_check_putaway);$z++) {
				$sn_ini = $db_huawei->select('*')
				->from('dt_sn')
				->where('id_sn', $sn_added_check_putaway[$z]['sn'])
				->where('id_stockaccess', '7')
				->get();
				$doc_list[$i]['for_close'] = $doc_list[$i]['for_close']+$sn_ini->num_rows();
			};

			$doc_list[$i]['qty'] = $qty_total;
			if($doc_list[$i]['date_send'] != null){
				$sn_hst = $db_huawei->select('*')
				->from('dt_sn_history hst')
				->join('dt_sn sn', 'sn.id_sn = hst.sn')
				->where('hst.id_doc', $doc_list[$i]['id'])
				->where('sn.id_stockaccess', '1')
				->where('hst.id_request', null)
				->where('hst.id_pod', null)
				->where('hst.id_pickup', null)
				->group_by('hst.sn')
				->get();

				$doc_list[$i]['sn_added'] = $sn_hst->num_rows();
			}else{
				$doc_list[$i]['sn_added'] = $sn_added->num_rows();
			}
		};

		echo json_encode($doc_list);
	}

	public function get_temp_inbound_fsl_list(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc_list = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundto', $this->session->userdata('id_warehouse'))
		->where('id_stockstatus', '1')
		->order_by('doc_date', 'DESC')
		->get();

		$doc_list = $doc_list->result_array();

		for($i=0; $i<count($doc_list);$i++) {
			$pn_list = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_list[$i]['id'])
			->order_by('id', 'ASC')
			->get();

			$pn_list = $pn_list->result_array();

			for($j=0; $j<count($pn_list);$j++) {
				$pn_name = $db_huawei->select('name_pn')
				->from('dm_pn')
				->where('id_pn', $pn_list[$j]['product_number'])
				->get();
				$pn_name = $pn_name->row();
				$pn_list[$j]['product_number'] = $pn_name->name_pn;
			};

			$doc_list[$i]['pn_list'] = $pn_list;

			// $doc_list['pn_list'] = $pn_list->result_array();
		};

		echo json_encode($doc_list);
	}

	public function get_temp_inbound_fsl_list_faulty(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc_list = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundto', $this->session->userdata('id_warehouse'))
		->where('id_stockstatus', '2')
		->order_by('doc_date', 'DESC')
		->get();

		$doc_list = $doc_list->result_array();

		for($i=0; $i<count($doc_list);$i++) {
			$pn_list = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_list[$i]['id'])
			->order_by('id', 'ASC')
			->get();

			$pn_list = $pn_list->result_array();

			for($j=0; $j<count($pn_list);$j++) {
				$pn_name = $db_huawei->select('name_pn')
				->from('dm_pn')
				->where('id_pn', $pn_list[$j]['product_number'])
				->get();
				$pn_name = $pn_name->row();
				$pn_list[$j]['product_number'] = $pn_name->name_pn;
			};

			$doc_list[$i]['pn_list'] = $pn_list;

			// $doc_list['pn_list'] = $pn_list->result_array();
		};

		echo json_encode($doc_list);
	}

	public function add_pn_temp_inbound() {
		$return = false;
		$db_huawei = $this->load->database('huawei', TRUE);

		$pn_exist = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $this->input->post('product_number'))
		->get();

		if($pn_exist->num_rows() > 0){
			$pn_exist = $pn_exist->row();
			$pn_exist_by_doc = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $this->input->post('doc_id_temp'))
			->where('product_number', $pn_exist->id_pn)
			->get();

			if($pn_exist_by_doc->num_rows() > 0){
				$pn_exist_by_doc = $pn_exist_by_doc->row();
				$total = intval($pn_exist_by_doc->qty) + intval($this->input->post('qty'));
				$data = array(
					'qty' => $total
				);

				$db_huawei->where('doc_id', $this->input->post('doc_id_temp'));
				$db_huawei->where('product_number', $pn_exist->id_pn);
				$db_huawei->update('dt_inbound_cwh_pn_temp', $data);

				$return = true;
			} else {
				$tempPN = array(
					'product_number' => $pn_exist->id_pn,
					'doc_id' => $this->input->post('doc_id_temp'),
					'qty' => $this->input->post('qty')
				);

				$db_huawei->insert('dt_inbound_cwh_pn_temp', $tempPN);
				$return = true;
			}
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function close_doc_inbound(){
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$data = array(
			'status' => '0',
			'close_date' => date('Y-m-d H:i:s')
		);

		$db_huawei->where('id', $this->input->post('doc_id'));

		if($db_huawei->update('dt_io_bound', $data)){
			$sn_hst = $db_huawei->select('*')
			->from('dt_sn_history')
			->where('id_doc', $this->input->post('doc_id'))
			->where('id_request', null)
			->where('id_pod', null)
			->where('id_pickup', null)
			->group_by('sn')
			->get();
			$sn_hst = $sn_hst->result_array();

			for($i=0;$i<count($sn_hst);$i++){
				$upd = array(
					'id_stockaccess' => '1',
					'id_warehouse' => $this->session->userdata('id_warehouse'),
					'id_stockposition' => '1'
				);
				$db_huawei->where('id_sn', $sn_hst[$i]['sn']);
				if($db_huawei->update('dt_sn', $upd)){
					$sn_history = array(
						'sn' => $sn_hst[$i]['sn'],
						'id_doc' => $this->input->post('doc_id'),
						'datetime_history' => date("Y-m-d H:i:s")
					);

					if($db_huawei->insert('dt_sn_history', $sn_history)){
						$return = array('stat' => true, 'det' => 'Document successfully closed');
					} else {
						$return = array('stat' => false, 'det' => 'Document cannot be closed');
					}
				}
			}
		}

		echo json_encode($return);
	}

	public function receive_doc_inbound(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id', $this->input->post('doc_id'))
		->get();
		$doc = $doc->row();
		if($doc->date_send !== null){
			$data = array(
				'date_received' => date('Y-m-d H:i:s'),
				'consignee' => $this->session->userdata('login_id')
			);

			$db_huawei->where('id', $this->input->post('doc_id'));
			if($db_huawei->update('dt_io_bound', $data)){
				$sn_hst = $db_huawei->select('*')
				->from('dt_sn_history')
				->where('id_doc', $this->input->post('doc_id'))
				->where('id_request', null)
				->where('id_pod', null)
				->where('id_pickup', null)
				->group_by('sn')
				->get();
				$sn_hst = $sn_hst->result_array();

				for($i=0;$i<count($sn_hst);$i++){
					$upd = array(
						'id_stockaccess' => '6',
						'id_stockposition' => '1',
						'id_warehouse' => $this->session->userdata('id_warehouse')
					);
					$db_huawei->where('id_sn', $sn_hst[$i]['sn']);
					if($db_huawei->update('dt_sn', $upd)){
						$sn_history = array(
							'sn' => $sn_hst[$i]['sn'],
							'id_doc' => $this->input->post('doc_id'),
							'datetime_history' => date("Y-m-d H:i:s")
						);

						if($db_huawei->insert('dt_sn_history', $sn_history)){
							$return = array(true);
						} else {
							$return = array(false);
						}
					}
				}
			}

		} else {
			$data = array(
				'date_received' => date('Y-m-d H:i:s'),
				'consignee' => $this->session->userdata('login_id')
			);

			$db_huawei->where('id', $this->input->post('doc_id'));
			$db_huawei->update('dt_io_bound', $data);
		}

		echo json_encode(true);
	}

	public function check_doc_exist_putaway_inbound(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('doc_no', $this->input->post('doc_no'))
		->get();
		$doc = $doc->row();
		$result = false;

		if($doc->date_send !== null){
			$sn_hst = $db_huawei->select('*')
			->from('dt_sn_history hst')
			->join('dt_sn sn', 'sn.id_sn = hst.sn')
			->where('hst.id_doc', $doc->id)
			->where('hst.id_request', null)
			->where('hst.id_pod', null)
			->where('hst.id_pickup', null)
			->where('sn.id_stockaccess', '1')
			->group_by('hst.sn')
			->get();

			$qty = $db_huawei->select('qty')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc->id)
			->get();

			$qty = $qty->result_array();
			$qty_total = 0;

			for($k=0; $k<count($qty);$k++) {
				$qty_total = intval($qty_total+$qty[$k]['qty']);
			};

			if($sn_hst->num_rows() == $qty_total){
				$result = false;
			}else{
				$result = $doc->id;
			}
			
		} else {
			if($doc->status == '1'){
				$result = $doc->id;
			} else {
				$result = false;
			}
		}
		echo json_encode($result);
	}

	public function putaway_process_inbound()
	{
		$return = false;
		$db_huawei = $this->load->database('huawei', TRUE);

		$loc_bin = $this->input->post('putaway-sn-bin');
		$pn = $this->input->post('putaway-sn-pn');
		$sn = $this->input->post('putaway-sn-sn');
		$doc_id = $this->input->post('putaway-sn-docid');

		$sn_exist = $db_huawei->select('*')
		->from('dt_sn')
		->where('sn', $sn)
		->where('id_stockaccess', '6')
		->get();

		$doc = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id', $doc_id)
		->get();
		$doc = $doc->row();

		if($doc->date_send !== null){
			if($sn_exist->num_rows()>0){
				$sn_exist_1st = $sn_exist->row();
				$pn_exist = $db_huawei->select('*')
				->from('dm_pn')
				->where('id_pn', $sn_exist_1st->id_pn)
				->where('name_pn', $pn)
				->get();
				if($pn_exist->num_rows()>0){
					$doc_check = $db_huawei->select('*')
					->from('dt_sn_history')
					->where('sn', $sn_exist_1st->id_sn)
					->where('id_doc', $doc_id)
					->where('id_request', null)
					->where('id_pod', null)
					->where('id_pickup', null)
					->group_by('sn')
					->get();

					if($doc_check->num_rows()>0){
						$upd = array(
							'id_stockaccess' => '1',
							'id_warehouse' => $this->session->userdata('id_warehouse'),
							'stocktake_time' => date('Y-m-d H:i:s'),
							'stocktaker' => $this->session->userdata('login_id'),
							'locbin' => $loc_bin
						);

						$db_huawei->where('id_sn', $sn_exist_1st->id_sn);

						if($db_huawei->update('dt_sn', $upd)) {
							$sn_history = array(
								'sn' => $sn_exist_1st->id_sn,
								'id_doc' => $doc_id,
								'datetime_history' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn_history', $sn_history)){
								$return = array('stat' => true, 'det' => 'SN successfully saved');
							} else {
								$return = array('stat' => false, 'det' => 'SN cannot be updated');
							}
						} else {
							$return = array('stat' => false, 'det' => 'SN cannot be updated');
						}		
					} else {
						$return = array('stat' => false, 'det' => 'SN or PN not registered in this document');
					}
				} else {
					$return = array('stat' => false, 'det' => 'SN not registered in this PN');
				}
			}else{
				$return = array('stat' => false, 'det' => 'SN cannot be found');
			}
		} else {
			if($sn_exist->num_rows()>0){
				$sn_exist_1st = $sn_exist->row();
				$pn_exist = $db_huawei->select('*')
				->from('dm_pn')
				->where('id_pn', $sn_exist_1st->id_pn)
				->where('name_pn', $pn)
				->get();
				if($pn_exist->num_rows()>0){
					$doc_check = $db_huawei->select('*')
					->from('dt_sn_history')
					->where('sn', $sn_exist_1st->id_sn)
					->where('id_doc', $doc_id)
					->where('id_request', null)
					->where('id_pod', null)
					->where('id_pickup', null)
					->group_by('sn')
					->get();

					if($doc_check->num_rows()>0){
						$upd = array(
							'id_stockaccess' => '7',
							'stocktake_time' => date('Y-m-d H:i:s'),
							'stocktaker' => $this->session->userdata('login_id'),
							'locbin' => $loc_bin
						);

						$db_huawei->where('id_sn', $sn_exist_1st->id_sn);

						if($db_huawei->update('dt_sn', $upd)) {
							$sn_history = array(
								'sn' => $sn_exist_1st->id_sn,
								'id_doc' => $doc_id,
								'datetime_history' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn_history', $sn_history)){
								$return = array('stat' => true, 'det' => 'SN successfully saved');
							} else {
								$return = array('stat' => false, 'det' => 'SN cannot be updated');
							}
						} else {
							$return = array('stat' => false, 'det' => 'SN cannot be updated');
						}		
					} else {
						$return = array('stat' => false, 'det' => 'SN or PN not registered in this document');
					}
				} else {
					$return = array('stat' => false, 'det' => 'SN not registered in this PN');
				}
			}else{
				$return = array('stat' => false, 'det' => 'SN cannot be found');
			}
		}

		echo json_encode($return);
	}

	public function add_remark_sn(){
		$return = false;
		$db_huawei = $this->load->database('huawei', TRUE);
		$data_sn = array(
			'remark' => $this->input->post('sn_remark')
		);

		$db_huawei->where('id_sn', $this->input->post('sn_id'));
		if($db_huawei->update('dt_sn', $data_sn)){
			$sn_history = array(
				'sn' => $this->input->post('sn_id'),
				'id_doc' => $this->input->post('remark_doc_id'),
				'datetime_history' => date("Y-m-d H:i:s"),
				'remark' => $this->input->post('sn_remark')
			);
			if($db_huawei->insert('dt_sn_history', $sn_history)){
				$return = true;
			} else {
				$return = false;
			}
		}
		echo json_encode($return);
	}

	public function get_remark_sn_by_id(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc = $db_huawei->select('remark')
		->from('dt_sn')
		->where('id_sn', $this->input->post('id_sn'))
		->get();

		echo json_encode($doc->result());
	}

	public function outbound_good_doc_submit() {
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = false;
		$checkDocCount = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundfrom', $this->session->userdata('id_warehouse'))
		->where('id_boundtype', $this->input->post('id_boundtype'))
		->where('id_boundto', $this->input->post('id_boundto'))
		->like('doc_date', date('Y-m-d'))
		->get();
		$checkDocCount = $checkDocCount->num_rows() + 1;

		$boundCode = $db_huawei->select('io_boundtype_code')
		->from('dm_io_boundtype')
		->where('id_io_boundtype', $this->input->post('id_boundtype'))
		->get();
		$boundCode = $boundCode->row();

		$toName = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouse', $this->input->post('id_boundto'))
		->get();
		$toName = $toName->row();

		$warehouseName = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouse', $this->session->userdata('id_warehouse'))
		->get();
		$warehouseName = $warehouseName->row();

		$doc_no = $boundCode->io_boundtype_code.'/'.$warehouseName->code_warehouse.$toName->code_warehouse.'/'.date("ymd").'/'.sprintf("%03s", $checkDocCount);

		$dataDocInbound = 
		array(
			'doc_no' => $doc_no,
			'doc_date' => date('Y-m-d H:i:s'),
			'id_boundtype' => $this->input->post('id_boundtype'),
			'id_boundto' => $this->input->post('id_boundto'),
			'id_boundfrom' => $this->session->userdata('id_warehouse'),
			'id_stockstatus' => '1',
			'id_project' => $this->input->post('id_project'),
			'create_by' => $this->session->userdata('login_id')
		);

		if($db_huawei->insert('dt_io_bound', $dataDocInbound)){
			$return = array('doc_no' => $doc_no, 'doc_date' => $dataDocInbound['doc_date']);
		} else {
			$return = false;
		}
		echo json_encode($return);
	}

	public function get_outbound_fsl_doc_list(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc_list = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundfrom', $this->session->userdata('id_warehouse'))
		->where('id_stockstatus', '1')
		->order_by('doc_date', 'DESC')
		->get();

		$doc_list = $doc_list->result_array();

		for($i=0; $i<count($doc_list);$i++) {
			$qty = $db_huawei->select('qty')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_list[$i]['id'])
			->get();

			$qty = $qty->result_array();
			$qty_total = 0;

			for($k=0; $k<count($qty);$k++) {
				$qty_total = intval($qty_total+$qty[$k]['qty']);
			};

			$sn_added = $db_huawei->select('*')
			->from('dt_sn_history')
			->where('id_doc', $doc_list[$i]['id'])
			->where('id_request', null)
			->where('id_pod', null)
			->where('id_pickup', null)
			->group_by('sn')
			->get();

			$sn_added_check_putaway = $sn_added->result_array();
			$doc_list[$i]['for_close'] = 0;

			for($z=0; $z<count($sn_added_check_putaway);$z++) {
				$sn_ini = $db_huawei->select('*')
				->from('dt_sn')
				->where('id_sn', $sn_added_check_putaway[$z]['sn'])
				->where('id_stockaccess', '7')
				->get();
				$doc_list[$i]['for_close'] = $doc_list[$i]['for_close']+$sn_ini->num_rows();
			};

			$doc_list[$i]['qty'] = $qty_total;
			$doc_list[$i]['sn_added'] = $sn_added->num_rows();
		};

		// print_r($doc_list);

		echo json_encode($doc_list);
	}

	public function add_pn_temp_outbound() {
		$return = false;
		$db_huawei = $this->load->database('huawei', TRUE);

		$pn_exist = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $this->input->post('product_number'))
		->get();

		if($pn_exist->num_rows() > 0){
			$pn_exist = $pn_exist->row();
			$pn_exist_by_doc = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $this->input->post('doc_id_temp'))
			->where('product_number', $pn_exist->id_pn)
			->get();

			if($pn_exist_by_doc->num_rows() > 0){
				$pn_exist_by_doc = $pn_exist_by_doc->row();
				$total = intval($pn_exist_by_doc->qty) + intval($this->input->post('qty'));
				$data = array(
					'qty' => $total
				);

				$db_huawei->where('doc_id', $this->input->post('doc_id_temp'));
				$db_huawei->where('product_number', $pn_exist->id_pn);
				$db_huawei->update('dt_inbound_cwh_pn_temp', $data);

				$return = true;
			} else {
				$tempPN = array(
					'product_number' => $pn_exist->id_pn,
					'doc_id' => $this->input->post('doc_id_temp'),
					'qty' => $this->input->post('qty'),
					'sr' => $this->input->post('sr_temp'),
					'rf' => $this->input->post('rf_temp')
				);

				$db_huawei->insert('dt_inbound_cwh_pn_temp', $tempPN);
				$return = true;
			}
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function outbound_loading_update() {
		$return = false;
		$db_huawei = $this->load->database('huawei', TRUE);

		$data = array(
			'id_logistic' => $this->input->post('id_logistic'),
			'awb_no' => $this->input->post('awb_no'),
			'id_logistic_service' => $this->input->post('id_logisticservice')
		);

		$db_huawei->where('id', $this->input->post('doc_id_send'));

		if($db_huawei->update('dt_io_bound', $data)){
			$sn_added = $db_huawei->select('*')
			->from('dt_sn_history')
			->where('id_doc', $this->input->post('doc_id_send'))
			->where('id_request', null)
			->where('id_pod', null)
			->where('id_pickup', null)
			->group_by('sn')
			->get();

			$sn_added = $sn_added->result_array();

			for($i = 0;$i<count($sn_added);$i++){
				$sn_upd = array(
					'id_stockaccess' => '8'
				);

				$db_huawei->where('id_sn', $sn_added[$i]['sn']);

				if($db_huawei->update('dt_sn', $sn_upd)){
					$sn_history = array(
						'sn' => $sn_added[$i]['sn'],
						'id_doc' => $this->input->post('doc_id_send'),
						'datetime_history' => date("Y-m-d H:i:s")
					);
					if($db_huawei->insert('dt_sn_history', $sn_history)){
						$return = true;
					} else {
						$return = false;
					}
				}
			}
		}
		echo json_encode($return);
	}

	public function close_doc_outbound()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$data = array(
			'status' => '0',
			'close_date' => date('Y-m-d H:i:s'),
			'date_send' => date('Y-m-d H:i:s'),
			'consignee' => $this->session->userdata('login_id')
		);

		$db_huawei->where('id', $this->input->post('doc_id'));

		if($db_huawei->update('dt_io_bound', $data)){
			$doc_det = $db_huawei->select('*')
			->from('dt_io_bound')
			->where('id', $this->input->post('doc_id'))
			->get();
			$doc_det = $doc_det->row();
			if($doc_det->id_boundtype == '10'){
				$sn_hst = $db_huawei->select('*')
				->from('dt_sn_history')
				->where('id_doc', $this->input->post('doc_id'))
				->where('id_request', null)
				->where('id_pod', null)
				->where('id_pickup', null)
				->group_by('sn')
				->get();
				$sn_hst = $sn_hst->result_array();

				for($i=0;$i<count($sn_hst);$i++){
					$upd = array(
						'id_stockaccess' => '5',
						'id_stockposition' => '4',
						'id_warehouse' => $doc_det->id_boundto
					);
					$db_huawei->where('id_sn', $sn_hst[$i]['sn']);
					if($db_huawei->update('dt_sn', $upd)){
						$sn_history = array(
							'sn' => $sn_hst[$i]['sn'],
							'id_doc' => $this->input->post('doc_id'),
							'datetime_history' => date("Y-m-d H:i:s")
						);

						if($db_huawei->insert('dt_sn_history', $sn_history)){
							$return = array('stat' => true, 'det' => 'Document successfully closed');
						} else {
							$return = array('stat' => false, 'det' => 'Document cannot be closed');
						}
					}
				}
			} elseif($doc_det->id_boundtype == '7' || $doc_det->id_boundtype == '15'){
				$sn_hst = $db_huawei->select('*')
				->from('dt_sn_history')
				->where('id_doc', $this->input->post('doc_id'))
				->where('id_request', null)
				->where('id_pod', null)
				->where('id_pickup', null)
				->group_by('sn')
				->get();
				$sn_hst = $sn_hst->result_array();

				for($i=0;$i<count($sn_hst);$i++){
					$upd = array(
						'id_stockaccess' => '2',
						'id_stockposition' => '9',
						'id_warehouse' => $doc_det->id_boundto
					);
					$db_huawei->where('id_sn', $sn_hst[$i]['sn']);
					if($db_huawei->update('dt_sn', $upd)){
						$sn_history = array(
							'sn' => $sn_hst[$i]['sn'],
							'id_doc' => $this->input->post('doc_id'),
							'datetime_history' => date("Y-m-d H:i:s")
						);

						if($db_huawei->insert('dt_sn_history', $sn_history)){
							$return = array('stat' => true, 'det' => 'Document successfully closed');
						} else {
							$return = array('stat' => false, 'det' => 'Document cannot be closed');
						}
					}
				}
			} else {
				$sn_hst = $db_huawei->select('*')
				->from('dt_sn_history')
				->where('id_doc', $this->input->post('doc_id'))
				->where('id_request', null)
				->where('id_pod', null)
				->where('id_pickup', null)
				->group_by('sn')
				->get();
				$sn_hst = $sn_hst->result_array();

				for($i=0;$i<count($sn_hst);$i++){
					$upd = array(
						'id_stockaccess' => '2',
						'id_stockposition' => '8',
						'id_warehouse' => $doc_det->id_boundto
					);
					$db_huawei->where('id_sn', $sn_hst[$i]['sn']);
					if($db_huawei->update('dt_sn', $upd)){
						$sn_history = array(
							'sn' => $sn_hst[$i]['sn'],
							'id_doc' => $this->input->post('doc_id'),
							'datetime_history' => date("Y-m-d H:i:s")
						);

						if($db_huawei->insert('dt_sn_history', $sn_history)){
							$return = array('stat' => true, 'det' => 'Document successfully closed');
						} else {
							$return = array('stat' => false, 'det' => 'Document cannot be closed');
						}
					}
				}
			}
		}

		echo json_encode($return);
	}

	public function get_inbound_fsl_doc_list_faulty(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc_list = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundto', $this->session->userdata('id_warehouse'))
		->where('id_stockstatus', '2')
		->order_by('doc_date', 'DESC')
		->get();

		$doc_list = $doc_list->result_array();

		for($i=0; $i<count($doc_list);$i++) {
			$qty = $db_huawei->select('qty')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_list[$i]['id'])
			->get();

			$qty = $qty->result_array();
			$qty_total = 0;

			for($k=0; $k<count($qty);$k++) {
				$qty_total = intval($qty_total+$qty[$k]['qty']);
			};

			$sn_added = $db_huawei->select('*')
			->from('dt_sn_history')
			->where('id_doc', $doc_list[$i]['id'])
			->where('id_request', null)
			->where('id_pod', null)
			->where('id_pickup', null)
			->group_by('sn')
			->get();

			$sn_added_check_putaway = $sn_added->result_array();
			$doc_list[$i]['for_close'] = 0;

			for($z=0; $z<count($sn_added_check_putaway);$z++) {
				$sn_ini = $db_huawei->select('*')
				->from('dt_sn')
				->where('id_sn', $sn_added_check_putaway[$z]['sn'])
				->where('id_stockaccess', '7')
				->get();
				$doc_list[$i]['for_close'] = $doc_list[$i]['for_close']+$sn_ini->num_rows();
			};

			$doc_list[$i]['qty'] = $qty_total;

			if($doc_list[$i]['date_send'] != null){
				$sn_hst = $db_huawei->select('*')
				->from('dt_sn_history hst')
				->join('dt_sn sn', 'sn.id_sn = hst.sn')
				->where('hst.id_doc', $doc_list[$i]['id'])
				->where('hst.id_request', null)
				->where('hst.id_pod', null)
				->where('hst.id_pickup', null)
				->where('sn.id_stockaccess', '2')
				->group_by('hst.sn')
				->get();

				$doc_list[$i]['sn_added'] = $sn_hst->num_rows();
			}else{
				$doc_list[$i]['sn_added'] = $sn_added->num_rows();
			}
		};

		// print_r($doc_list);

		echo json_encode($doc_list);
	}

	public function inbound_faulty_doc_submit()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = false;
		$checkDocCount = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundto', $this->session->userdata('id_warehouse'))
		->where('id_boundtype', $this->input->post('id_boundtype'))
		->where('id_boundfrom', $this->input->post('id_boundfrom'))
		->like('doc_date', date('Y-m-d'))
		->get();
		$checkDocCount = $checkDocCount->num_rows() + 1;

		$boundCode = $db_huawei->select('io_boundtype_code')
		->from('dm_io_boundtype')
		->where('id_io_boundtype', $this->input->post('id_boundtype'))
		->get();
		$boundCode = $boundCode->row();

		$fromName = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouse', $this->input->post('id_boundfrom'))
		->get();
		$fromName = $fromName->row();

		$bondTo = $db_huawei->select('code_warehouse')
		->from('dm_warehouse')
		->where('id_warehouse', $this->session->userdata('id_warehouse'))
		->get();
		$bondTo = $bondTo->row();

		$doc_no = $boundCode->io_boundtype_code.'/'.$fromName->code_warehouse.$bondTo->code_warehouse.'/'.date("ymd").'/'.sprintf("%03s", $checkDocCount);

		$dataDocInbound = 
		array(
			'doc_no' => $doc_no,
			'doc_date' => date('Y-m-d H:i:s'),
			'id_boundtype' => $this->input->post('id_boundtype'),
			'id_boundfrom' => $this->input->post('id_boundfrom'),
			'id_boundto' => $this->session->userdata('id_warehouse'),
			'id_stockstatus' => '2',
			'id_logistic_service' => $this->input->post('id_logistic_service'),
			'id_project' => $this->input->post('id_project'),
			'id_logistic' => $this->input->post('id_logistic'),
			'awb_no' => $this->input->post('awb_no'),
			'create_by' => $this->session->userdata('login_id')
		);

		if($db_huawei->insert('dt_io_bound', $dataDocInbound)){
			$return = array('doc_no' => $doc_no, 'doc_date' => $dataDocInbound['doc_date']);
		} else {
			$return = false;
		}
		echo json_encode($return);
	}

	public function get_outbound_fsl_doc_list_faulty(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc_list = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundfrom', $this->session->userdata('id_warehouse'))
		->where('id_stockstatus', '2')
		->order_by('doc_date', 'DESC')
		->get();

		$doc_list = $doc_list->result_array();

		for($i=0; $i<count($doc_list);$i++) {
			$qty = $db_huawei->select('qty')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_list[$i]['id'])
			->get();

			$qty = $qty->result_array();
			$qty_total = 0;

			for($k=0; $k<count($qty);$k++) {
				$qty_total = intval($qty_total+$qty[$k]['qty']);
			};

			$sn_added = $db_huawei->select('*')
			->from('dt_sn_history')
			->where('id_doc', $doc_list[$i]['id'])
			->where('id_request', null)
			->where('id_pod', null)
			->where('id_pickup', null)
			->group_by('sn')
			->get();

			$sn_added_check_putaway = $sn_added->result_array();
			$doc_list[$i]['for_close'] = 0;

			for($z=0; $z<count($sn_added_check_putaway);$z++) {
				$sn_ini = $db_huawei->select('*')
				->from('dt_sn')
				->where('id_sn', $sn_added_check_putaway[$z]['sn'])
				->where('id_stockaccess', '7')
				->get();
				$doc_list[$i]['for_close'] = $doc_list[$i]['for_close']+$sn_ini->num_rows();
			};

			$doc_list[$i]['qty'] = $qty_total;
			$doc_list[$i]['sn_added'] = $sn_added->num_rows();
		};

		// print_r($doc_list);

		echo json_encode($doc_list);
	}

	public function outbound_good_faulty_submit() {
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = false;
		$checkDocCount = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id_boundfrom', $this->session->userdata('id_warehouse'))
		->where('id_boundtype', $this->input->post('id_boundtype'))
		->where('id_boundto', $this->input->post('id_boundto'))
		->like('doc_date', date('Y-m-d'))
		->get();
		$checkDocCount = $checkDocCount->num_rows() + 1;

		$boundCode = $db_huawei->select('io_boundtype_code')
		->from('dm_io_boundtype')
		->where('id_io_boundtype', $this->input->post('id_boundtype'))
		->get();
		$boundCode = $boundCode->row();

		$toName = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouse', $this->input->post('id_boundto'))
		->get();
		$toName = $toName->row();

		$bondFrom = $db_huawei->select('code_warehouse')
		->from('dm_warehouse')
		->where('id_warehouse', $this->session->userdata('id_warehouse'))
		->get();
		$bondFrom = $bondFrom->row();

		$doc_no = $boundCode->io_boundtype_code.'/'.$bondFrom->code_warehouse.$toName->code_warehouse.'/'.date("ymd").'/'.sprintf("%03s", $checkDocCount);

		$dataDocInbound = 
		array(
			'doc_no' => $doc_no,
			'doc_date' => date('Y-m-d H:i:s'),
			'id_boundtype' => $this->input->post('id_boundtype'),
			'id_boundto' => $this->input->post('id_boundto'),
			'id_boundfrom' => $this->session->userdata('id_warehouse'),
			'id_stockstatus' => '2',
			'id_project' => $this->input->post('id_project'),
			'create_by' => $this->session->userdata('login_id')
		);

		if($db_huawei->insert('dt_io_bound', $dataDocInbound)){
			$return = array('doc_no' => $doc_no, 'doc_date' => $dataDocInbound['doc_date']);
		} else {
			$return = false;
		}
		echo json_encode($return);
	}
}
