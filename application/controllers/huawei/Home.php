<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function index()
	{
		$data['userdata'] = $this->session->all_userdata();
		$this->load->view('huawei/layout');
		$this->load->view('huawei/dashboard', $data);
	}

	public function dash_tes()
	{
		$data['userdata'] = $this->session->all_userdata();
		$this->load->view('huawei/layout');
		$this->load->view('huawei/dashboard_2', $data);
	}

	public function get_request_by_day_this_month()
	{
		// $days = $this->input->post('days_this_month');
		for($d=0; $d<31; $d++)
		{
			$time=mktime(12, 0, 0, date('m'), $d+1, date('Y'));          
			if (date('m', $time)==date('m'))       
				$days[$d]=date('Y-m-d', $time);
		}


		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('id_customer') > 0){
			for($i = 0; $i < count($days); $i++){
				$req_today = $db_huawei->select('COUNT(id_request) as total')
				->from('dt_request')
				->like('time_request', $days[$i])
				->where('id_customer', $this->session->userdata('id_customer'))
				->where('id_status_request !=', '4')
				->where('id_status_request !=', '7')
				->get();
				// no inject_rmr to show
				$req_today = $req_today->row();
				$req_today = $req_today->total;
				$all_data[$i] = intval($req_today);
			}
		}else{
			$all_cust = $db_huawei->select('*')
			->from('dm_customer')
			->get();
			$all_cust = $all_cust->result_array();

			for($c = 0; $c < count($all_cust); $c++){
				for($i = 0; $i < count($days); $i++){
					$req_today = $db_huawei->select('COUNT(id_request) as total')
					->from('dt_request')
					->like('time_request', $days[$i])
					->where('id_customer', $all_cust[$c]['id_customer'])
					->where('id_status_request !=', '4')
					->where('id_status_request !=', '7')
					->get();
					// no inject_rmr to show
					$req_today = $req_today->row();
					$req_today = $req_today->total;
					$all_data[$all_cust[$c]['code_customer']][$i] = intval($req_today);
				}
			}
		}

		echo json_encode($all_data);
	}

	public function get_request_by_day_this_week()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

		if($this->session->userdata('id_customer') > 0){
			for($i = 0; $i < 7; $i++){
				$req_today = $db_huawei->select('COUNT(id_request) as total')
				->from('dt_request')
				->where('id_customer', $this->session->userdata('id_customer'))
				->where('id_status_request !=', '4')
				->where('id_status_request !=', '7')
				->get();
			}

			$req_today = $db_huawei->select('*')
			->from('dt_request')
			->where('id_customer', $this->session->userdata('id_customer'))
			->where('id_status_request !=', '4')
			->where('id_status_request !=', '7')
			->get();
			$req_today = $req_today->result_array();

			$all_data[0] = 0;
			$all_data[1] = 0;
			$all_data[2] = 0;
			$all_data[3] = 0;
			$all_data[4] = 0;
			$all_data[5] = 0;
			$all_data[6] = 0;

			for($i = 0; $i < count($req_today); $i++){
				$get_day_name = date('l', strtotime($req_today[$i]['time_request']));
				if($get_day_name == $days[0]){
					$all_data[0] = $all_data[0] + 1;
				}elseif($get_day_name == $days[1]){
					$all_data[1] = $all_data[1] + 1;
				}elseif($get_day_name == $days[2]){
					$all_data[2] = $all_data[2] + 1;
				}elseif($get_day_name == $days[3]){
					$all_data[3] = $all_data[3] + 1;
				}elseif($get_day_name == $days[4]){
					$all_data[4] = $all_data[4] + 1;
				}elseif($get_day_name == $days[5]){
					$all_data[5] = $all_data[5] + 1;
				}else{
					$all_data[6] = $all_data[6] + 1;
				}
			}
		}else{
			$all_cust = $db_huawei->select('*')
			->from('dm_customer')
			->get();
			$all_cust = $all_cust->result_array();

			for($c = 0; $c < count($all_cust); $c++){
				$req_today = $db_huawei->select('*')
				->from('dt_request')
				->where('id_customer', $all_cust[$c]['id_customer'])
				->where('id_status_request !=', '4')
				->where('id_status_request !=', '7')
				->get();
				$req_today = $req_today->result_array();

				$all_data[$all_cust[$c]['code_customer']][0] = 0;
				$all_data[$all_cust[$c]['code_customer']][1] = 0;
				$all_data[$all_cust[$c]['code_customer']][2] = 0;
				$all_data[$all_cust[$c]['code_customer']][3] = 0;
				$all_data[$all_cust[$c]['code_customer']][4] = 0;
				$all_data[$all_cust[$c]['code_customer']][5] = 0;
				$all_data[$all_cust[$c]['code_customer']][6] = 0;

				for($i = 0; $i < count($req_today); $i++){
					$get_day_name = date('l', strtotime($req_today[$i]['time_request']));
					if($get_day_name == $days[0]){
						$all_data[$all_cust[$c]['code_customer']][0] = $all_data[$all_cust[$c]['code_customer']][0] + 1;
					}elseif($get_day_name == $days[1]){
						$all_data[$all_cust[$c]['code_customer']][1] = $all_data[$all_cust[$c]['code_customer']][1] + 1;
					}elseif($get_day_name == $days[2]){
						$all_data[$all_cust[$c]['code_customer']][2] = $all_data[$all_cust[$c]['code_customer']][2] + 1;
					}elseif($get_day_name == $days[3]){
						$all_data[$all_cust[$c]['code_customer']][3] = $all_data[$all_cust[$c]['code_customer']][3] + 1;
					}elseif($get_day_name == $days[4]){
						$all_data[$all_cust[$c]['code_customer']][4] = $all_data[$all_cust[$c]['code_customer']][4] + 1;
					}elseif($get_day_name == $days[5]){
						$all_data[$all_cust[$c]['code_customer']][5] = $all_data[$all_cust[$c]['code_customer']][5] + 1;
					}else{
						$all_data[$all_cust[$c]['code_customer']][6] = $all_data[$all_cust[$c]['code_customer']][6] + 1;
					}
				}
			}
		}
		echo json_encode(array('data'=>$all_data, 'days'=>$days));
	}

	public function get_request_by_hour_this_day()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('id_customer') > 0){
			for($i = 0; $i < 24; $i++){
				$req_today = $db_huawei->select('COUNT(id_request) as total')
				->from('dt_request')
				->like('time_request', ' '.sprintf("%02s", $i).':')
				->where('id_customer', $this->session->userdata('id_customer'))
				->where('id_status_request !=', '4')
				->where('id_status_request !=', '7')
				->get();
				// no inject_rmr to show
				$req_today = $req_today->row();
				$req_today = $req_today->total;
				$all_data[$i] = intval($req_today);
				$hours[$i] = sprintf("%02s", $i);
			}
		}else{
			$all_cust = $db_huawei->select('*')
			->from('dm_customer')
			->get();
			$all_cust = $all_cust->result_array();

			for($c = 0; $c < count($all_cust); $c++){
				for($i = 0; $i < 24; $i++){
					$req_today = $db_huawei->select('COUNT(id_request) as total')
					->from('dt_request')
					->like('time_request', ' '.sprintf("%02s", $i).':')
					->where('id_customer', $all_cust[$c]['id_customer'])
					->where('id_status_request !=', '4')
					->where('id_status_request !=', '7')
					->get();
					// no inject_rmr to show
					$req_today = $req_today->row();
					$req_today = $req_today->total;
					$all_data[$all_cust[$c]['code_customer']][$i] = intval($req_today);
					$hours[$i] = sprintf("%02s", $i);
				}
			}
		}

		echo json_encode(array('data'=>$all_data, 'hours'=>$hours));
	}

	public function get_progress_stocktake()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$all_data = $db_huawei->select('ROUND((SUM(CASE WHEN status_cek = 1 THEN 1 ELSE 0 END)/COUNT(id_sn) * 100),2) AS percentage', FALSE)
		->from('dt_sn')
		->where('id_warehouse !=', '1')
		->where('id_stockaccess', '1')
		->where('id_stockposition', '1')
		->group_by('id_warehouse')
		->get();
		$all_data = $all_data->result_array();

		$all_data_send['hundred'] = 0;
		$all_data_send['seventy_five'] = 0;
		$all_data_send['fifty'] = 0;
		$all_data_send['twenty_five'] = 0;
		$all_data_send['less_twenty_five'] = 0;
		$all_data_send['not_yet'] = 0;

		for($i = 0; $i<count($all_data); $i++){
			if($all_data[$i]['percentage'] == 100){
				$all_data_send['hundred'] = $all_data_send['hundred'] + 1;
			}elseif($all_data[$i] < 100 && $all_data[$i] >= 75){
				$all_data_send['seventy_five'] = $all_data_send['seventy_five'] + 1;
			}elseif($all_data[$i] < 75 && $all_data[$i] >= 50){
				$all_data_send['fifty'] = $all_data_send['fifty'] + 1;
			}elseif($all_data[$i] < 50 && $all_data[$i] >= 25){
				$all_data_send['twenty_five'] = $all_data_send['twenty_five'] + 1;
			}elseif($all_data[$i] < 25 && $all_data[$i] > 0){
				$all_data_send['less_twenty_five'] = $all_data_send['less_twenty_five'] + 1;
			}else{
				$all_data_send['not_yet'] = $all_data_send['not_yet'] + 1;
			}
		}

		$progress_cwh = $db_huawei->select('SUM(CASE WHEN status_cek = 0 THEN 1 ELSE 0 END) AS total_uncheck, SUM(CASE WHEN status_cek = 1 THEN 1 ELSE 0 END) AS total_check', FALSE)
		->from('dt_sn')
		->where('id_warehouse', '1')
		->where('id_stockaccess', '1')
		->where('id_stockposition', '1')
		->get();

		echo json_encode(array('all_fsl' => $all_data_send, 'cwh' => $progress_cwh->row()));
	}

	public function get_request_data_all_card()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('id_customer') > 0){
			$all_data = $db_huawei->select('COUNT(id_request) as total_request, SUM(CASE WHEN(id_sn IS NULL AND (id_status_request = 0 OR id_status_request = 2 OR id_status_request = 1)) THEN 1 ELSE 0 END) AS total_process, SUM(CASE WHEN (id_pod IS NOT NULL AND id_sn IS NOT NULL AND url_photo_mdn IS NULL) THEN 1 ELSE 0 END) AS total_in_delivery, SUM(CASE WHEN id_status_request = 6 THEN 1 ELSE 0 END) AS total_success, SUM(CASE WHEN id_status_request = 5 THEN 1 ELSE 0 END) AS total_failed, SUM(CASE WHEN id_status_request = 3 THEN 1 ELSE 0 END) AS total_oos', FALSE)
			->from('dt_request')
			->where('id_customer', $this->session->userdata('id_customer'))
			->where('id_status_request !=', '4')
			->where('id_status_request !=', '7')
			->get();
			// no inject_rmr to show
		}else{
			$all_data = $db_huawei->select('COUNT(id_request) as total_request, SUM(CASE WHEN(id_sn IS NULL AND (id_status_request = 0 OR id_status_request = 2 OR id_status_request = 1)) THEN 1 ELSE 0 END) AS total_process, SUM(CASE WHEN (id_pod IS NOT NULL AND id_sn IS NOT NULL AND url_photo_mdn IS NULL) THEN 1 ELSE 0 END) AS total_in_delivery, SUM(CASE WHEN id_status_request = 6 THEN 1 ELSE 0 END) AS total_success, SUM(CASE WHEN id_status_request = 5 THEN 1 ELSE 0 END) AS total_failed, SUM(CASE WHEN id_status_request = 3 THEN 1 ELSE 0 END) AS total_oos', FALSE)
			->from('dt_request')
			->where('id_status_request !=', '7')
			->get();
			// no inject_rmr to show
		}

		echo json_encode($all_data->row());
	}

	public function get_request_IOP_stock()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$total_stock = $db_huawei->select('SUM(CASE WHEN id_stockstatus = 1 THEN 1 ELSE 0 END) AS total_good, SUM(CASE WHEN id_stockstatus = 2 THEN 1 ELSE 0 END) AS total_faulty', FALSE)
		->from('dt_sn')
		->where('id_stockaccess', '1')
		->get();

		if($this->session->userdata('id_customer') > 0){
			$all_data = $db_huawei->select('COUNT(req.id_request) as total_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS total_inner, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS total_outer, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS total_p2p', FALSE)
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->where('req.id_customer', $this->session->userdata('id_customer'))
			->where('id_status_request !=', '4')
			->where('req.id_status_request !=', '7')
			->get();
			$all_data_send = $all_data->row();
		}else{
			$all_cust = $db_huawei->select('*')
			->from('dm_customer')
			->get();
			$all_cust = $all_cust->result_array();

			for($c = 0; $c < count($all_cust); $c++){
				$all_data = $db_huawei->select('COUNT(req.id_request) as total_request, SUM(CASE WHEN dop.id_deliverytype = 1 THEN 1 ELSE 0 END) AS total_inner, SUM(CASE WHEN dop.id_deliverytype = 2 THEN 1 ELSE 0 END) AS total_outer, SUM(CASE WHEN req.id_warehouse != dop.id_warehouse THEN 1 ELSE 0 END) AS total_p2p', FALSE)
				->from('dt_request req')
				->join('dm_dop dop', 'dop.id_dop = req.id_dop')
				->where('req.id_customer', $all_cust[$c]['id_customer'])
				->where('id_status_request !=', '4')
				->where('req.id_status_request !=', '7')
				->get();
				// no inject_rmr to show
				$all_data_send[$all_cust[$c]['code_customer']] = $all_data->row();
			}
		}

		echo json_encode(array('iop'=> $all_data_send, 'stock'=>$total_stock->row()));
	}
}
