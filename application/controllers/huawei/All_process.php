<?php
defined('BASEPATH') or exit('No direct script access allowed');

class All_process extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if (! $this->session->userdata('logged')) {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function change_domain_user($domain)
	{
		if ($this->session->userdata('domain') == 'all') {
			$this->session->set_userdata('domain', $domain);
			redirect(base_url() . $domain);
		} else {
			redirect(base_url() . "login");
		}
		// $this->load->view('demo/home');
	}

	public function changeToMd5($pass)
	{
		print_r(md5($pass));
	}

	public function add_sn_good_faulty($id_doc) {
		$data['doc_id'] = $id_doc;
		$this->load->view('huawei/layout');
		$this->load->view('huawei/inventory/cwh_inbound/sn_good', $data);
	}

	public function change_select_by_boundtype(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_boundtype = $this->input->post('id_boundtype');
		$warehouse_class = '';

		if($id_boundtype == '3' || $id_boundtype == '9') {
			$warehouse_class = '2';
		} elseif($id_boundtype == '5' || $id_boundtype == '10'){
			$warehouse_class = '3';
		} elseif($id_boundtype == '15' || $id_boundtype == '7'){
			$warehouse_class = '1';
		}

		$doc = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouseclass', $warehouse_class)
		->where('id_warehouse !=', $this->session->userdata('id_warehouse'))
		->get();

		echo json_encode($doc->result_array());
	}

	public function makePdfSN($doc_id)
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$doc = $db_huawei->select('hst.*, d.doc_no')
		->from('dt_io_bound d')
		->join('dt_sn_history hst', 'hst.id_doc = d.id')
		->where('d.id', $doc_id)
		->group_by('hst.sn')
		->get();
		$doc = $doc->result_array();

		for($i=0; $i<count($doc);$i++) {
			$sn = $db_huawei->select('*')
			->from('dt_sn')
			->where('id_sn', $doc[$i]['sn'])
			->get();
			$sn = $sn->row();

			$pn = $db_huawei->select('name_pn')
			->from('dm_pn')
			->where('id_pn', $sn->id_pn)
			->get();
			$pn = $pn->row();

			$data_sn[$i] = array('loc_bin' => $sn->locbin, 'sn' => $sn->sn, 'pn' => $pn->name_pn);
		};

		require APPPATH.'libraries/fpdf_barcode.php';
		$pdf = new PDF_Code128('P', 'mm', 'A4');
		$pdf->AddPage();
		// $pdf->SetDisplayMode(real,'default');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(190,7,$doc[0]['doc_no'],0,1,'C');
		$pdf->Cell(10,7,'',0,1);
		$pdf->SetFont('Arial','B',9);

		$count_change_page = 0;

		$y = 40;
		$h_bar = 30;
		for($k = 0; $k < count($data_sn); $k++){
			$pdf->Cell(60,5,'Loc Bin',1,0);
			$pdf->Cell(60,5,'Part Number',1,0);
			$pdf->Cell(60,5,'Serial Number',1,1);
			$pdf->SetY($y);
			$pdf->SetFont('Arial','B',9);
			$pdf->Code128(18,$h_bar,$data_sn[$k]['loc_bin'],45,10);
			$pdf->Code128(78,$h_bar,$data_sn[$k]['pn'],45,10);
			$pdf->Code128(133,$h_bar,$data_sn[$k]['sn'],50,10);
			$pdf->Cell(60,6,$data_sn[$k]['loc_bin'],0,0,'C');
			$pdf->Cell(60,6,$data_sn[$k]['pn'],0,0,'C');
			$pdf->Cell(60,6,$data_sn[$k]['sn'],0,1,'C');

			$y = $y + 25;
			$h_bar = $h_bar + 25;
			$count_change_page = $count_change_page + 1;

			if($count_change_page == 10){
				$pdf->AddPage();
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(190,7,$doc[0]['doc_no'],0,1,'C');
				$pdf->Cell(10,7,'',0,1);
				$pdf->SetFont('Arial','B',9);
				$count_change_page = 0;
				$y = 40;
				$h_bar = 30;
			}
		}

		$pdf->Output();

		// $pdf->Output('D', $doc[0]['doc_no'].'-SN.pdf');
	}

	public function docPrint($doc_id){
		$db_huawei = $this->load->database('huawei', TRUE);

		$doc = $db_huawei->select('*')
		->from('dt_io_bound doc')
		->join('dm_io_boundtype bt', 'bt.id_io_boundtype = doc.id_boundtype', 'left')
		->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic', 'left')
		->where('doc.id', $doc_id)
		->get();
		$data['doc'] = $doc->result_array();

		$doc = $doc->row();

		$from_det = $db_huawei->select('*')
		->from('dt_io_bound doc')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom', 'left')
		->join('dm_city city', 'city.id_city = warehouse.id_city', 'left')
		->join('dm_country country', 'country.id_country = city.id_country', 'left')
		->where('doc.id', $doc_id)
		->get();
		$data['from_det'] = $from_det->result_array();

		$to_det = $db_huawei->select('*')
		->from('dt_io_bound doc')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundto', 'left')
		->join('dm_city city', 'city.id_city = warehouse.id_city', 'left')
		->join('dm_country country', 'country.id_country = city.id_country', 'left')
		->where('doc.id', $doc_id)
		->get();
		$data['to_det'] = $to_det->result_array();

		$sn_det = $db_huawei->select('proj.name_project, sn.sn, pn.name_pn, basecode.description, stock_stat.name_stockstatus, sn.remark')
		->from('dt_io_bound doc')
		->join('dm_project proj', 'proj.id_project = doc.id_project', 'left')
		->join('dt_sn_history sn_hst', 'sn_hst.id_doc = doc.id')
		->join('dt_sn sn', 'sn.id_sn = sn_hst.sn', 'left')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn', 'left')
		->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
		->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus', 'left')
		->where('doc.id', $doc_id)
		->group_by('sn_hst.sn')
		->order_by('sn_hst.sn', 'DESC')
		->get();

		$data['sn_det'] = $sn_det->result_array();

		// $view = $this->load->view('huawei/inventory/print_doc', $data, true);
		$this->load->view('huawei/inventory/print_doc_bu', $data);
		// $doc_no = explode('/', $doc->doc_no);
		// $pdf_name = $doc_no[0].'-'.$doc_no[1].'-'.$doc_no[2].'-'.$doc_no[3];

		// $this->load->library('pdf');

		// $this->pdf->loadHtml($view);
		// $this->pdf->render();
		// $this->pdf->stream($pdf_name.".pdf", array("Attachment"=>0));
	}

	public function docPrintRequest($id, $type){
		$db_huawei = $this->load->database('huawei', TRUE);
		if($type == 1){
			$doc = $db_huawei->select('basecode.description AS des_req, basecode2.description AS des_del, pn.name_pn AS pn_req, pn2.name_pn AS pn_del, req.rmr AS rmr, req.order AS sr, req.rma AS rma, req.site_id AS site, warehouse.name_warehouse AS pool_origin, dop.name_dop, dop.alamat_dop, region.name_region, requestor.name_requestor, requestor.email, requestor.phone, req.time_request, req.create_time, user.name_user, del_type.name_deliverytype, req.eta_received, req.sla, customer.name_customer, pod.pod_no, sn.sn, dop.id_warehouse AS dop_warehouse, req.id_warehouse AS req_warehouse')
			->from('dt_request req')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode', 'left')
			->join('dm_pn pn2', 'pn2.id_pn = req.id_pn', 'left')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
			->join('dm_basecode basecode2', 'basecode2.id_basecode = pn2.id_basecode', 'left')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_region region', 'region.id_region = dop.id_region', 'left')
			->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter', 'left')
			->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
			->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->where('req.id_pod', $id)
			->get();

			$data['all'] = $doc->result_array();
			$data['title'] = 'DELIVERY ORDER';
			$data['ria_pu'] = false;

			$this->load->view('huawei/inventory/print_doc_request/print_pod', $data);
		}elseif($type == 2){
			$doc = $db_huawei->select('basecode.description AS des_del, pn.name_pn AS pn_del, req.rmr AS rmr, req.order AS sr, req.rma AS rma, req.site_id AS site, dop.name_dop, dop.alamat_dop, region.name_region, requestor.name_requestor, requestor.email, requestor.phone, req.time_request, req.pickup_request_time, user.name_user, del_type.name_deliverytype, req.eta_received, req.sla, customer.name_customer, pickup.pickup_no, basecode2.description AS des_req, pn2.name_pn AS pn_req')
			->from('dt_request req')
			->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
			->join('dm_pn pn2', 'pn2.id_pn = req.id_basecode', 'left')
			->join('dm_basecode basecode2', 'basecode2.id_basecode = pn2.id_basecode', 'left')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_region region', 'region.id_region = dop.id_region', 'left')
			->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter', 'left')
			->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
			->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
			->where('req.id_pickup', $id)
			->get();

			$data['all'] = $doc->result_array();
			$data['title'] = 'PICKUP ORDER';
			$data['ria_pu'] = false;

			$this->load->view('huawei/inventory/print_doc_request/print_pickup', $data);
		}elseif($type == 3){
			$doc = $db_huawei->select('basecode.description AS des_req, pn.name_pn AS pn_req, req.rmr AS rmr, req.order AS sr, req.rma AS rma, req.site_id AS site, dop.name_dop, dop.alamat_dop, region.name_region, requestor.name_requestor, requestor.email, requestor.phone, req.time_request, req.pickup_request_time, user.name_user, del_type.name_deliverytype, req.eta_received, req.sla, customer.name_customer, pickup.pickup_no')
			->from('dt_request req')
			->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode', 'left')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_region region', 'region.id_region = dop.id_region', 'left')
			->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter', 'left')
			->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
			->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
			->where('req.id_pickup', $id)
			->get();

			$data['all'] = $doc->result_array();
			$data['title'] = 'PICKUP ORDER';
			$data['ria_pu'] = true;

			$this->load->view('huawei/inventory/print_doc_request/print_pickup', $data);
		}

		// $this->load->library('pdf');
		// $this->pdf->set_option('isHtml5ParserEnabled', true);

		// $this->pdf->loadHtml($view);
		// $this->pdf->render();
		// $this->pdf->stream("Document.pdf", array("Attachment"=>0));
	}

	public function add_sn_io()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);

		$loc_bin = $this->input->post('add-sn-bin');
		$pn = $this->input->post('add-sn-pn');
		$sn = $this->input->post('add-sn-sn');
		$doc_id = $this->input->post('add-sn-docid');

		$doc_dtl = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id', $doc_id)
		->get();
		$doc_dtl = $doc_dtl->row();

		$pn_exist = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $pn)
		->get();

		$sn_exist = $db_huawei->select('*')
		->from('dt_sn')
		->where('sn', $sn)
		->get();

		$stock_stat = '';
		if($doc_dtl->id_stockstatus == '1'){
			$stock_stat = '1';
		}else{
			$stock_stat = '2';
		}

		if($doc_dtl->id_boundtype == '1' || $doc_dtl->id_boundtype == '11'){
			$temp_exist = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_id)
			->get();
			if($temp_exist->num_rows() > 0){
				$pn_exist_in_temp = $db_huawei->select('*')
				->from('dm_pn pn')
				->join('dt_inbound_cwh_pn_temp temp', 'temp.product_number = pn.id_pn')
				->where('temp.doc_id', $doc_id)
				->where('pn.name_pn', $pn)
				->get();
				if($pn_exist_in_temp->num_rows() > 0){
					if($pn == $sn){
						if($pn_exist->num_rows() < 1){
							$return = array('stat' => false, 'det' => 'PN does not exist in database');
						} else {
							$format = 'NS'.$pn;
							$sn_num = $db_huawei->select('*')
							->from('no_sn_counter')
							->get();
							$sn_num = $sn_num->row();
							$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

							$upd_count = array(
								'number' => $sn_num->number+1
							);

							$db_huawei->where('id_sn_counter', '1');

							if($db_huawei->update('no_sn_counter', $upd_count)) {
								$pn_exist = $pn_exist->row();
								$sn_insert = array(
									'sn' => $generated_sn,
									'id_pn' => $pn_exist->id_pn,
									'id_stockstatus' => $stock_stat,
									'id_stockaccess' => '6',
									'id_stockposition' => '1',
									'id_warehouse' => $doc_dtl->id_boundto,
									'id_project' => $doc_dtl->id_project,
									'locbin' => $loc_bin,
									'stockTaker' => $this->session->userdata('login_id'),
									'stocktake_time' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn', $sn_insert)){
									$sn_id = $db_huawei->insert_id();

									$sn_history = array(
										'sn' => $sn_id,
										'id_doc' => $doc_id,
										'datetime_history' => date("Y-m-d H:i:s")
									);

									if($db_huawei->insert('dt_sn_history', $sn_history)){
										$return = array('stat' => true, 'det' => 'SN successfully saved');
									} else {
										$return = array('stat' => false, 'det' => 'SN cannot be saved');
									}
								}
							}
						}
					} else {
						if($pn_exist->num_rows() < 1){
							$return = array('stat' => false, 'det' => 'PN does not exist in database');
						} else {
							if($sn_exist->num_rows() > 0) {
								$sn_exist = $sn_exist->row();
								$pn_exist = $pn_exist->row();
								if($sn_exist->id_pn != $pn_exist->id_pn){
									$return = array('stat' => false, 'det' => 'SN has a different PN with your input!');
								}else{
									if($sn_exist->id_stockposition == '1' || $sn_exist->id_stockposition == '2' || $sn_exist->id_stockposition == '8' || $sn_exist->id_stockposition == '9'){
										$return = array('stat' => false, 'det' => 'SN cannot be saved');
									}else{
										$upd = array(
											'id_stockaccess' => '6',
											'stocktake_time' => date('Y-m-d H:i:s'),
											'stocktaker' => $this->session->userdata('login_id'),
											'id_stockposition' => '1',
											'id_stockstatus' => $stock_stat,
											'locbin' => $loc_bin,
											'id_warehouse' => $doc_dtl->id_boundto,
											'id_project' => $doc_dtl->id_project,
										);

										$db_huawei->where('id_sn', $sn_exist->id_sn);

										if($db_huawei->update('dt_sn', $upd)){
											$sn_history = array(
												'sn' => $sn_exist->id_sn,
												'id_doc' => $doc_id,
												'datetime_history' => date("Y-m-d H:i:s")
											);

											if($db_huawei->insert('dt_sn_history', $sn_history)){
												$return = array('stat' => true, 'det' => 'SN successfully saved');
											} else {
												$return = array('stat' => false, 'det' => 'SN cannot be saved');
											}
										}
									}
								}
							} else {
								$pn_exist = $pn_exist->row();
								$sn_insert = array(
									'sn' => $sn,
									'id_pn' => $pn_exist->id_pn,
									'id_stockstatus' => $stock_stat,
									'id_stockaccess' => '6',
									'id_stockposition' => '1',
									'id_warehouse' => $doc_dtl->id_boundto,
									'id_project' => $doc_dtl->id_project,
									'locbin' => $loc_bin,
									'stockTaker' => $this->session->userdata('login_id'),
									'stocktake_time' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn', $sn_insert)){
									$sn_id = $db_huawei->insert_id();

									$sn_history = array(
										'sn' => $sn_id,
										'id_doc' => $doc_id,
										'datetime_history' => date("Y-m-d H:i:s")
									);

									if($db_huawei->insert('dt_sn_history', $sn_history)){
										$return = array('stat' => true, 'det' => 'SN successfully saved');
									} else {
										$return = array('stat' => false, 'det' => 'SN cannot be saved');
									}
								}
							}
						}
					}
				} else {
					$return = array('stat' => false, 'det' => 'PN not registered in this document');
				}
			} else {
				if($pn == $sn){
					if($pn_exist->num_rows() < 1){
						$return = array('stat' => false, 'det' => 'PN does not exist in database');
					} else {
						$format = 'NS'.$pn;
						$sn_num = $db_huawei->select('*')
						->from('no_sn_counter')
						->get();
						$sn_num = $sn_num->row();
						$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

						$upd_count = array(
							'number' => $sn_num->number+1
						);

						$db_huawei->where('id_sn_counter', '1');

						if($db_huawei->update('no_sn_counter', $upd_count)) {
							$pn_exist = $pn_exist->row();
							$sn_insert = array(
								'sn' => $generated_sn,
								'id_pn' => $pn_exist->id_pn,
								'id_stockstatus' => $stock_stat,
								'id_stockaccess' => '6',
								'id_stockposition' => '1',
								'id_warehouse' => $doc_dtl->id_boundto,
								'id_project' => $doc_dtl->id_project,
								'locbin' => $loc_bin,
								'stockTaker' => $this->session->userdata('login_id'),
								'stocktake_time' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn', $sn_insert)){
								$sn_id = $db_huawei->insert_id();

								$sn_history = array(
									'sn' => $sn_id,
									'id_doc' => $doc_id,
									'datetime_history' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn_history', $sn_history)){
									$return = array('stat' => true, 'det' => 'SN successfully saved');
								} else {
									$return = array('stat' => false, 'det' => 'SN cannot be saved');
								}
							}
						}
					}
				} else {
					if($pn_exist->num_rows() < 1){
						$return = array('stat' => false, 'det' => 'PN does not exist in database');
					} else {
						if($sn_exist->num_rows() > 0) {
							$sn_exist = $sn_exist->row();
							$pn_exist = $pn_exist->row();
							if($sn_exist->id_pn != $pn_exist->id_pn){
								$return = array('stat' => false, 'det' => 'SN has a different PN with your input!');
							}else{
								if($sn_exist->id_stockposition == '1' || $sn_exist->id_stockposition == '2' || $sn_exist->id_stockposition == '8' || $sn_exist->id_stockposition == '9'){
									$return = array('stat' => false, 'det' => 'SN cannot be saved');
								}else{
									$upd = array(
										'id_stockaccess' => '6',
										'stocktake_time' => date('Y-m-d H:i:s'),
										'stocktaker' => $this->session->userdata('login_id'),
										'id_stockposition' => '1',
										'id_stockstatus' => $stock_stat,
										'locbin' => $loc_bin,
										'id_warehouse' => $doc_dtl->id_boundto,
										'id_project' => $doc_dtl->id_project,
									);

									$db_huawei->where('id_sn', $sn_exist->id_sn);

									if($db_huawei->update('dt_sn', $upd)){
										$sn_history = array(
											'sn' => $sn_exist->id_sn,
											'id_doc' => $doc_id,
											'datetime_history' => date("Y-m-d H:i:s")
										);

										if($db_huawei->insert('dt_sn_history', $sn_history)){
											$return = array('stat' => true, 'det' => 'SN successfully saved');
										} else {
											$return = array('stat' => false, 'det' => 'SN cannot be saved');
										}
									}
								}
							}
						} else {
							$pn_exist = $pn_exist->row();
							$sn_insert = array(
								'sn' => $sn,
								'id_pn' => $pn_exist->id_pn,
								'id_stockstatus' => $stock_stat,
								'id_stockaccess' => '6',
								'id_stockposition' => '1',
								'id_warehouse' => $doc_dtl->id_boundto,
								'id_project' => $doc_dtl->id_project,
								'locbin' => $loc_bin,
								'stockTaker' => $this->session->userdata('login_id'),
								'stocktake_time' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn', $sn_insert)){
								$sn_id = $db_huawei->insert_id();

								$sn_history = array(
									'sn' => $sn_id,
									'id_doc' => $doc_id,
									'datetime_history' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn_history', $sn_history)){
									$return = array('stat' => true, 'det' => 'SN successfully saved');
								} else {
									$return = array('stat' => false, 'det' => 'SN cannot be saved');
								}
							}
						}
					}
				}
			}
		} elseif($doc_dtl->id_boundtype == '2') {
			$temp_exist = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_id)
			->get();
			if($temp_exist->num_rows() > 0){
				$pn_exist_in_temp = $db_huawei->select('*')
				->from('dm_pn pn')
				->join('dt_inbound_cwh_pn_temp temp', 'temp.product_number = pn.id_pn')
				->where('temp.doc_id', $doc_id)
				->where('pn.name_pn', $pn)
				->get();

				if($pn_exist_in_temp->num_rows() > 0){
					if($pn == $sn){
						if($pn_exist->num_rows() < 1){
							$return = array('stat' => false, 'det' => 'PN does not exist in database');
						} else {
							$format = 'NS'.$pn;
							$sn_num = $db_huawei->select('*')
							->from('no_sn_counter')
							->get();
							$sn_num = $sn_num->row();
							$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

							$upd_count = array(
								'number' => $sn_num->number+1
							);

							$db_huawei->where('id_sn_counter', '1');

							if($db_huawei->update('no_sn_counter', $upd_count)) {
								$pn_exist = $pn_exist->row();
								$sn_insert = array(
									'sn' => $generated_sn,
									'id_pn' => $pn_exist->id_pn,
									'id_stockstatus' => '1',
									'id_stockaccess' => '6',
									'id_stockposition' => '1',
									'id_warehouse' => $doc_dtl->id_boundto,
									'id_project' => $doc_dtl->id_project,
									'locbin' => $loc_bin,
									'stockTaker' => $this->session->userdata('login_id'),
									'stocktake_time' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn', $sn_insert)){
									$sn_id = $db_huawei->insert_id();

									$sn_history = array(
										'sn' => $sn_id,
										'id_doc' => $doc_id,
										'datetime_history' => date("Y-m-d H:i:s")
									);

									if($db_huawei->insert('dt_sn_history', $sn_history)){
										$return = array('stat' => true, 'det' => 'SN successfully saved');
									} else {
										$return = array('stat' => false, 'det' => 'SN cannot be saved');
									}
								}
							}
						}
					} else {
						if($pn_exist->num_rows() < 1){
							$return = array('stat' => false, 'det' => 'PN does not exist in database');
						} else {
							if($sn_exist->num_rows() > 0) {
								$return = array('stat' => false, 'det' => 'SN already exist in database');
							} else {
								$pn_exist = $pn_exist->row();
								$sn_insert = array(
									'sn' => $sn,
									'id_pn' => $pn_exist->id_pn,
									'id_stockstatus' => '1',
									'id_stockaccess' => '6',
									'id_stockposition' => '1',
									'id_warehouse' => $doc_dtl->id_boundto,
									'id_project' => $doc_dtl->id_project,
									'locbin' => $loc_bin,
									'stockTaker' => $this->session->userdata('login_id'),
									'stocktake_time' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn', $sn_insert)){
									$sn_id = $db_huawei->insert_id();

									$sn_history = array(
										'sn' => $sn_id,
										'id_doc' => $doc_id,
										'datetime_history' => date("Y-m-d H:i:s")
									);

									if($db_huawei->insert('dt_sn_history', $sn_history)){
										$return = array('stat' => true, 'det' => 'SN successfully saved');
									} else {
										$return = array('stat' => false, 'det' => 'SN cannot be saved');
									}
								}
							}
						}
					}
				} else {
					$return = array('stat' => false, 'det' => 'PN not registered in this document');
				}
			} else {
				if($pn == $sn){
					if($pn_exist->num_rows() < 1){
						$return = array('stat' => false, 'det' => 'PN does not exist in database');
					} else {
						$format = 'NS'.$pn;
						$sn_num = $db_huawei->select('*')
						->from('no_sn_counter')
						->get();
						$sn_num = $sn_num->row();
						$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

						$upd_count = array(
							'number' => $sn_num->number+1
						);

						$db_huawei->where('id_sn_counter', '1');

						if($db_huawei->update('no_sn_counter', $upd_count)) {
							$pn_exist = $pn_exist->row();
							$sn_insert = array(
								'sn' => $generated_sn,
								'id_pn' => $pn_exist->id_pn,
								'id_stockstatus' => '1',
								'id_stockaccess' => '6',
								'id_stockposition' => '1',
								'id_warehouse' => $doc_dtl->id_boundto,
								'id_project' => $doc_dtl->id_project,
								'locbin' => $loc_bin,
								'stockTaker' => $this->session->userdata('login_id'),
								'stocktake_time' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn', $sn_insert)){
								$sn_id = $db_huawei->insert_id();

								$sn_history = array(
									'sn' => $sn_id,
									'id_doc' => $doc_id,
									'datetime_history' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn_history', $sn_history)){
									$return = array('stat' => true, 'det' => 'SN successfully saved');
								} else {
									$return = array('stat' => false, 'det' => 'SN cannot be saved');
								}
							}
						}
					}
				} else {
					if($pn_exist->num_rows() < 1){
						$return = array('stat' => false, 'det' => 'PN does not exist in database');
					} else {
						if($sn_exist->num_rows() > 0) {
							$return = array('stat' => false, 'det' => 'SN already exist in database');
						} else {
							$pn_exist = $pn_exist->row();
							$sn_insert = array(
								'sn' => $sn,
								'id_pn' => $pn_exist->id_pn,
								'id_stockstatus' => '1',
								'id_stockaccess' => '6',
								'id_stockposition' => '1',
								'id_warehouse' => $doc_dtl->id_boundto,
								'id_project' => $doc_dtl->id_project,
								'locbin' => $loc_bin,
								'stockTaker' => $this->session->userdata('login_id'),
								'stocktake_time' => date("Y-m-d H:i:s")
							);

							if($db_huawei->insert('dt_sn', $sn_insert)){
								$sn_id = $db_huawei->insert_id();

								$sn_history = array(
									'sn' => $sn_id,
									'id_doc' => $doc_id,
									'datetime_history' => date("Y-m-d H:i:s")
								);

								if($db_huawei->insert('dt_sn_history', $sn_history)){
									$return = array('stat' => true, 'det' => 'SN successfully saved');
								} else {
									$return = array('stat' => false, 'det' => 'SN cannot be saved');
								}
							}
						}
					}
				}
			}
		} elseif($doc_dtl->id_boundtype == '3' || $doc_dtl->id_boundtype == '4' || $doc_dtl->id_boundtype == '5' || $doc_dtl->id_boundtype == '6' || $doc_dtl->id_boundtype == '7' || $doc_dtl->id_boundtype == '8' || $doc_dtl->id_boundtype == '9' || $doc_dtl->id_boundtype == '10' || $doc_dtl->id_boundtype == '15') {
			$pn_exist_out = $db_huawei->select('*')
			->from('dt_inbound_cwh_pn_temp temp')
			->join('dm_pn pn', 'pn.id_pn = temp.product_number')
			->where('temp.doc_id', $doc_id)
			->where('pn.name_pn', $pn)
			->get();

			$sn_exist_exit = $db_huawei->select('*')
			->from('dt_sn')
			->where('sn', $sn)
			->where('id_stockaccess', '1')
			->where('status_cek', '1')
			->where('id_warehouse', $this->session->userdata('id_warehouse'))
			->get();

			$sn_added = $db_huawei->select('id_doc')
			->from('dt_sn_history')
			->where('id_doc', $doc_id)
			->where('id_request', null)
			->where('id_pod', null)
			->where('id_pickup', null)
			->group_by('sn')
			->get();

			$temp_count = 0;

			$temp_added = $db_huawei->select('qty')
			->from('dt_inbound_cwh_pn_temp')
			->where('doc_id', $doc_id)
			->get();
			$temp_added = $temp_added->result_array();
			for($z=0;$z<count($temp_added);$z++){
				$temp_count = $temp_count + intval($temp_added[$z]['qty']);
			}

			if($sn_added->num_rows() < $temp_count){
				if($pn == $sn){
					$return = array('stat' => false, 'det' => 'PN & SN cannot be same');
				} else {
					if($pn_exist_out->num_rows() < 1){
						$return = array('stat' => false, 'det' => 'PN does not exist in this document');
					} else {
						if($sn_exist_exit->num_rows() < 1) {
							$return = array('stat' => false, 'det' => 'SN cannot be use');
						} else {
							$sn_exist_exit = $sn_exist_exit->row();
							$pn_exist_out = $pn_exist_out->row();
							if($sn_exist_exit->id_pn !== $pn_exist_out->id_pn){
								$return = array('stat' => false, 'det' => 'SN & PN does not match');
							} else {
								if($sn_exist_exit->id_stockaccess !== '3'){
									$upd = array(
										'id_stockaccess' => '3',
										'id_stockstatus' => $stock_stat,
										'stocktake_time' => date('Y-m-d H:i:s'),
										'stocktaker' => $this->session->userdata('login_id'),
										'locbin' => $loc_bin
									);

									$db_huawei->where('id_sn', $sn_exist_exit->id_sn);

									if($db_huawei->update('dt_sn', $upd)){
										$sn_history = array(
											'sn' => $sn_exist_exit->id_sn,
											'id_doc' => $doc_id,
											'datetime_history' => date("Y-m-d H:i:s")
										);

										if($db_huawei->insert('dt_sn_history', $sn_history)){
											$return = array('stat' => true, 'det' => 'SN successfully saved');
										} else {
											$return = array('stat' => false, 'det' => 'SN cannot be saved');
										}
									}
								}else{
									$return = array('stat' => false, 'det' => 'SN not available');
								}
							}
						}
					}
				}
			}else{
				$return = array('stat' => false, 'det' => 'Maximum quantity has been reach!');
			}		
		}

		echo json_encode($return);
	}

	public function get_sn_by_doc_id_inbound(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$doc_id = $this->input->post('doc_id');
		// $doc_id = 166;

		$sn_hst = $db_huawei->select('*')
		->from('dt_sn_history')
		->where('id_doc', $doc_id)
		->where('id_request', null)
		->where('id_pod', null)
		->where('id_pickup', null)
		->group_by('sn')
		->get();
		$sn_hst = $sn_hst->result_array();

		if(count($sn_hst) > 0){
			$doc = $db_huawei->select('date_send')
			->from('dt_io_bound')
			->where('id', $doc_id)
			->get();
			$doc = $doc->row();

			for($i=0; $i<count($sn_hst);$i++) {
				$sn = $db_huawei->select('*')
				->from('dt_sn sn')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
				->where('sn.id_sn', $sn_hst[$i]['sn'])
				->get();
				$sn = $sn->row();

				// $pn = $db_huawei->select('*')
				// ->from('dm_pn')
				// ->where('id_pn', $sn->id_pn)
				// ->get();
				// $pn = $pn->row();

				$all_data[$i] = array('id_sn' => $sn->id_sn,'pn' => $sn->name_pn,'loc_bin' => $sn->locbin, 'sn' => $sn->sn, 'access' => $sn->id_stockaccess, 'date_send' => $doc->date_send);
			};
		}else{
			$all_data = [];
		}

		echo json_encode($all_data);
	}

	public function delete_sn_add_sn()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$doc_type = $db_huawei->select('doc.id_boundtype, doc.id')
		->from('dt_sn_history hst')
		->join('dt_io_bound doc', 'doc.id = hst.id_doc')
		->where('hst.sn', $this->input->post('id_sn'))
		->where('doc.id', $this->input->post('id_doc'))
		->where('hst.id_request', null)
		->where('hst.id_pod', null)
		->where('hst.id_pickup', null)
		->get();

		$doc_type = $doc_type->row();
		$return = '';

		if($doc_type->id_boundtype == '1' || $doc_type->id_boundtype == '2' || $doc_type->id_boundtype == '11'){
			$data = array(
				'id_stockaccess' => '11',
				'id_stockposition' => '7'
			);
			$db_huawei->where('id_sn', $this->input->post('id_sn'));
			if($db_huawei->update('dt_sn', $data)){
				if($db_huawei->delete('dt_sn_history', array('sn' => $this->input->post('id_sn'), 'id_doc' => $doc_type->id))){
				$return = true;
				}
			}
		//	 if($db_huawei->delete('dt_sn_history', array('sn' => $this->input->post('id_sn'), 'id_doc' => $doc_type->id))){
		//	 	if($db_huawei->delete('dt_sn', array('id_sn' => $this->input->post('id_sn')))){
		//	 		$return = true;
		//	 	}
		//	 }
		}else{
			$data = array(
				'id_stockaccess' => '1',
				'id_stockposition' => '1'
			);
			$db_huawei->where('id_sn', $this->input->post('id_sn'));
			if($db_huawei->update('dt_sn', $data)){
				if($db_huawei->delete('dt_sn_history', array('sn' => $this->input->post('id_sn'), 'id_doc' => $doc_type->id))){
					$return = true;
				}
			}
		}

		echo json_encode($return);
	}

	public function change_session_admin()
	{
		$this->load->view('huawei/layout');
		$this->load->view('huawei/change_session');
	}

	public function log_as()
	{
		$id_domain = $this->db->select('*')
		->from('dm_domain')
		->where('name_domain', $this->input->post('id_domain'))
		->get();
		$id_domain = $id_domain->row();

		if($this->session->userdata('level') == 4){
			$log_as = $this->db->select('*, d.name_domain AS domain')
			->from('dt_user u')
			->join('dm_domain d', 'd.id_domain = u.id_domain')
			->where('u.id_domain', $id_domain->id_domain)
			->where('u.id_userlevel', '10')
			// ->or_where('u.id_domain', '0')
			->get();
		}else{
			$log_as = $this->db->select('*, d.name_domain AS domain')
			->from('dt_user u')
			->join('dm_domain d', 'd.id_domain = u.id_domain')
			->where('u.id_domain', $id_domain->id_domain)
			->or_where('u.id_domain', '0')
			->get();
		}
		echo json_encode($log_as->result_array());
	}

	public function log_as_proceed()
	{
		$id_warehouse = $this->input->post('id_warehouse');

		$this->session->set_userdata('id_warehouse', $id_warehouse);

		echo json_encode(true);
	}

	public function print_awb($from, $to, $qty, $weight, $doc)
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$doc = $db_huawei->select('*')
		->from('dt_io_bound')
		->where('id', $doc)
		->get();
		$data['doc'] = $doc->result_array();

		if($from == 1){
			$from = $db_huawei->select('*, warehouse.address AS warehouse_address, warehouse.*')
			->from('dm_warehouse warehouse')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_warehouse = warehouse.id_warehouse')
			->where('warehouse.id_warehouse', $from)
			->where('user.id_userlevel', '5')
			->get();
			$data['from'] = $from->result_array();
		}else{
			$from = $db_huawei->select('*, warehouse.address AS warehouse_address, warehouse.*')
			->from('dm_warehouse warehouse')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_warehouse = warehouse.id_warehouse', 'left')
			->where('warehouse.id_warehouse', $from)
			->get();
			$data['from'] = $from->result_array();
		}

		if($to == 1){
			$to = $db_huawei->select('*, warehouse.address AS warehouse_address, warehouse.*')
			->from('dm_warehouse warehouse')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_warehouse = warehouse.id_warehouse')
			->where('warehouse.id_warehouse', $to)
			->where('user.id_userlevel', '5')
			->get();
			$data['to'] = $to->result_array();
		}else{
			$to = $db_huawei->select('*, warehouse.address AS warehouse_address, warehouse.*')
			->from('dm_warehouse warehouse')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_warehouse = warehouse.id_warehouse', 'left')
			->where('warehouse.id_warehouse', $to)
			->get();
			$data['to'] = $to->result_array();
		}

		$data['qty'] = $qty;
		$data['weight'] = $weight;

		$this->load->view('huawei/finance/print/print_awb', $data);
	}

	public function print_time_sheet($dop, $pod_cmn, $pic, $telp)
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$pod_cmn = explode(',', urldecode($pod_cmn));

		$data['doc_no'] = $pod_cmn;

		$warehouse_pool = $db_huawei->select('*, warehouse.address AS warehouse_address')
		->from('dm_dop dop')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_warehouse = warehouse.id_warehouse')
		->where('id_dop', $dop)
		->get();
		$data['pic_pool'] = $warehouse_pool->result_array();

		$data['pic_dop'] = str_replace('%20', ' ', $pic);
		$data['pic_dop_telp'] = $telp;

		$this->load->view('huawei/finance/print/print_timesheet', $data);
	}

	public function excel_inventory_stock()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$input = $this->input->post();
		$doc_name = ['Available Stock', 'All Stock', 'All Faulty', 'Distribution', 'Return to CWH', 'Transfer Stock', 'Inbound new or handover', 'Outbound handover'];

		$this->load->library("excel");

		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		if($input['type'] == 1 || $input['type'] == 2 || $input['type'] == 3){
			$table_columns = array("No", "Warehouse", "Project", "Description", "Basecode", "Serial Number", "Locator", "Stock Status", "Stock Position", "Stock Access", "Bound Date", "Bound Doc", "Last Stocktake", "Stocktaker");
		}elseif($input['type'] == 4 || $input['type'] == 6){
			$table_columns = array("No", "From", "Document", "SR", "PN", "SN", "To", "Locator", "Status", "Send Date", "Logistics", "AWB Number", "Receive Date");
		}elseif($input['type'] == 5){
			$table_columns = array("No", "From", "Document", "SR", "PN", "SN", "To", "Locator", "Status", "Send Date", "Logistics", "AWB Number", "Receive Date");
		}elseif($input['type'] == 7 || $input['type'] == 8){
			$table_columns = array("No", "Document", "PN", "SN", "Stock Status", "From", "To", "Locator", "Receive Date", "Status");
		}

		$column = 0;

		foreach($table_columns as $field){

			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);

			$column++;

		}

		$excel_row = 2;

		if($input['type'] == 1 || $input['type'] == 2 || $input['type'] == 3){
			if($input['type'] == 1){
				$db_huawei->select('warehouse.name_warehouse, project.name_project, basecode.description, basecode.name_basecode, sn.sn, sn.locbin, stock_stat.name_stockstatus, stock_pos.name_stockposition, stock_acc.name_stockaccess, doc.doc_date, doc.doc_no, sn.stocktake_time, user.name_user')
				->from('dt_sn_history sn_hst')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn', 'left')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse', 'left')
				->join('dm_project project', 'project.id_project = sn.id_project', 'left')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn', 'left')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus', 'left')
				->join('dm_stockposition stock_pos', 'stock_pos.id_stockposition = sn.id_stockposition', 'left')
				->join('dm_stockaccess stock_acc', 'stock_acc.id_stockaccess = sn.id_stockaccess', 'left')
				->join('wmshilco_user_mgm.dt_user user', 'user.id_user = sn.stocktaker', 'left')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc', 'left')
				->where('sn.id_stockposition', '1')
				->where_in('sn.id_stockaccess', array('1', '10', '2', '3', '6', '7', '8'))
				->order_by('warehouse.name_warehouse', 'ASC');

				if($input['id_project']){
					$db_huawei->like('sn.id_project', $input['id_project']);
				}

				if($input['id_warehouse']){
					$db_huawei->like('sn.id_warehouse', $input['id_warehouse']);
				}

				if($input['id_stockstatus']){
					$db_huawei->like('sn.id_stockstatus', $input['id_stockstatus']);
				}

				if($input['date']){
					$date = explode(' - ', $input['date']);
					if($date[0] == $date[1]){
						$db_huawei->like('sn.stocktake_time', $date[0]);
					}else{
						$db_huawei->where('sn.stocktake_time >=', $date[0]);
						$db_huawei->where('sn.stocktake_time <=', $date[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data = $db_huawei->get();
				$data = $data->result();

			}elseif($input['type'] == 2){
				$db_huawei->select('warehouse.name_warehouse, project.name_project, basecode.description, basecode.name_basecode, sn.sn, sn.locbin, stock_stat.name_stockstatus, stock_pos.name_stockposition, stock_acc.name_stockaccess, doc.doc_date, doc.doc_no, sn.stocktake_time, user.name_user')
				->from('dt_sn sn')
				->join('dt_sn_history sn_hst', 'sn_hst.sn = sn.id_sn')
				// ->from('dt_sn_history sn_hst')
				// ->join('dt_sn sn', 'sn.id_sn = sn_hst.sn', 'left')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse', 'left')
				->join('dm_project project', 'project.id_project = sn.id_project', 'left')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn', 'left')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus', 'left')
				->join('dm_stockposition stock_pos', 'stock_pos.id_stockposition = sn.id_stockposition', 'left')
				->join('dm_stockaccess stock_acc', 'stock_acc.id_stockaccess = sn.id_stockaccess', 'left')
				->join('wmshilco_user_mgm.dt_user user', 'user.id_user = sn.stocktaker', 'left')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc', 'left')
				->where_not_in('sn.id_stockaccess', '11')
				->order_by('warehouse.name_warehouse', 'ASC');

				if($input['id_project']){
					$db_huawei->like('sn.id_project', $input['id_project']);
				}

				if($input['id_warehouse']){
					$db_huawei->like('sn.id_warehouse', $input['id_warehouse']);
				}

				if($input['id_stockstatus']){
					$db_huawei->like('sn.id_stockstatus', $input['id_stockstatus']);
				}

				if($input['date']){
					$date = explode(' - ', $input['date']);
					if($date[0] == $date[1]){
						$db_huawei->like('sn.stocktake_time', $date[0]);
					}else{
						$db_huawei->where('sn.stocktake_time >=', $date[0]);
						$db_huawei->where('sn.stocktake_time <=', $date[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data = $db_huawei->get();
				$data = $data->result();

			}else{
				$data = $db_huawei->select('warehouse.name_warehouse, project.name_project, basecode.description, basecode.name_basecode, sn.sn, sn.locbin, stock_stat.name_stockstatus, stock_pos.name_stockposition, stock_acc.name_stockaccess, doc.doc_date, doc.doc_no, sn.stocktake_time, user.name_user')
				->from('dt_sn_history sn_hst')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn', 'left')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse', 'left')
				->join('dm_project project', 'project.id_project = sn.id_project', 'left')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn', 'left')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus', 'left')
				->join('dm_stockposition stock_pos', 'stock_pos.id_stockposition = sn.id_stockposition', 'left')
				->join('dm_stockaccess stock_acc', 'stock_acc.id_stockaccess = sn.id_stockaccess', 'left')
				->join('wmshilco_user_mgm.dt_user user', 'user.id_user = sn.stocktaker', 'left')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc', 'left')
				->where('sn.id_stockposition', '1')
				->where('sn.id_stockstatus', '2')
				->order_by('warehouse.name_warehouse', 'ASC');

				if($input['id_project']){
					$db_huawei->like('sn.id_project', $input['id_project']);
				}

				if($input['id_warehouse']){
					$db_huawei->like('sn.id_warehouse', $input['id_warehouse']);
				}

				if($input['date']){
					$date = explode(' - ', $input['date']);
					if($date[0] == $date[1]){
						$db_huawei->like('sn.stocktake_time', $date[0]);
					}else{
						$db_huawei->where('sn.stocktake_time >=', $date[0]);
						$db_huawei->where('sn.stocktake_time <=', $date[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data = $db_huawei->get();
				$data = $data->result();
			}

			$num = 1;
			foreach($data as $row){
				$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->name_warehouse,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_project,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->description,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->name_basecode,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->sn,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->locbin,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->name_stockstatus,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->name_stockposition,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->name_stockaccess,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->doc_date,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->doc_no,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->stocktake_time,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row, $row->name_user,PHPExcel_Cell_DataType::TYPE_STRING);

				$excel_row++;
				$num++;

			}
		}elseif($input['type'] == 4 || $input['type'] == 6){
			if($input['type'] == 4){
				$db_huawei->select('warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to, sn.sn, sn.locbin, stock_stat.name_stockstatus, doc.date_send, doc.date_received, doc.doc_no, pn.name_pn, doc.doc_no, bound_type.name_io_boundtype, doc.status, logistic.name_logistic, doc.awb_no, temp.sr')
				->from('dt_sn_history sn_hst')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
				->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
				->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
				->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
				->join('dt_inbound_cwh_pn_temp temp', 'temp.doc_id = doc.id')
				->where_in('doc.id_boundtype', array('3', '9'))
				->order_by('doc.date_send', 'ASC');

				if($input['id_project']){
					$db_huawei->like('sn.id_project', $input['id_project']);
				}

				if($input['id_warehouse']){
					$db_huawei->like('doc.id_boundto', $input['id_warehouse']);
				}

				if($input['id_stockstatus']){
					$db_huawei->like('sn.id_stockstatus', $input['id_stockstatus']);
				}

				if($input['date']){
					$date = explode(' - ', $input['date']);
					if($date[0] == $date[1]){
						$db_huawei->like('doc.doc_date', $date[0]);
					}else{
						$db_huawei->where('doc.doc_date >=', $date[0]);
						$db_huawei->where('doc.doc_date <=', $date[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data = $db_huawei->get();
				$data = $data->result();
			}else{
				$data = $db_huawei->select('warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to, sn.sn, sn.locbin, stock_stat.name_stockstatus, doc.date_send, doc.date_received, doc.doc_no, pn.name_pn, doc.doc_no, bound_type.name_io_boundtype, doc.status, logistic.name_logistic, doc.awb_no, temp.sr')
				->from('dt_sn_history sn_hst')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
				->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
				->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
				->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
				->join('dt_inbound_cwh_pn_temp temp', 'temp.doc_id = doc.id')
				->where('warehouse.id_warehouseclass', '2')
				->where('warehouse2.id_warehouseclass', '2')
				->order_by('doc.date_send', 'ASC');

				if($input['id_project']){
					$db_huawei->like('sn.id_project', $input['id_project']);
				}

				if($input['id_warehouse']){
					$db_huawei->where('doc.id_boundfrom = '.$input['id_warehouse'].' or doc.id_boundto ='.$input['id_warehouse']);
				}

				if($input['id_stockstatus']){
					$db_huawei->like('sn.id_stockstatus', $input['id_stockstatus']);
				}

				if($input['date']){
					$date = explode(' - ', $input['date']);
					if($date[0] == $date[1]){
						$db_huawei->like('doc.doc_date', $date[0]);
					}else{
						$db_huawei->where('doc.doc_date >=', $date[0]);
						$db_huawei->where('doc.doc_date <=', $date[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data = $db_huawei->get();
				$data = $data->result();
			}

			// array("No", "Document", "PN", "SN", "From", "To", "Send Date", "Receive Date", "Status");

			$num = 1;
			foreach($data as $row){
			//	$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->doc_no,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_pn,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->sn,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->from,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->to,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->locbin,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->date_send,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->date_received,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->name_logistic,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->awb_no,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->sr,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->name_stockstatus,PHPExcel_Cell_DataType::TYPE_STRING);
            	$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->from,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->doc_no,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->sr,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->name_pn,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->sn,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->to,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->locbin,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->name_stockstatus,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->date_send,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->name_logistic,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->awb_no,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->date_received,PHPExcel_Cell_DataType::TYPE_STRING);    
			
				$excel_row++;
				$num++;

			}
		}elseif($input['type'] == 5){
			$data = $db_huawei->select('warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to, sn.sn, sn.locbin, stock_stat.name_stockstatus, doc.date_send, doc.date_received, doc.doc_no, pn.name_pn, bound_type.name_io_boundtype, doc.status, doc.awb_no, temp.sr, logistic.name_logistic')
			->from('dt_sn_history sn_hst')
			->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
			->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
			->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
			->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
			->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
			->join('dm_logistic logistic', 'logistic.id_logistic = doc.id_logistic')
			->join('dt_inbound_cwh_pn_temp temp', 'temp.doc_id = doc.id')
			->where_in('doc.id_boundtype', array('7', '15'))
			->order_by('doc.date_send', 'ASC');

			if($input['id_project']){
				$db_huawei->like('sn.id_project', $input['id_project']);
			}

			if($input['id_warehouse']){
				$db_huawei->where('doc.id_boundfrom', $input['id_warehouse']);
			}

			if($input['id_stockstatus']){
				$db_huawei->like('sn.id_stockstatus', $input['id_stockstatus']);
			}

			if($input['date']){
				$date = explode(' - ', $input['date']);
				if($date[0] == $date[1]){
					$db_huawei->like('doc.doc_date', $date[0]);
				}else{
					$db_huawei->where('doc.doc_date >=', $date[0]);
					$db_huawei->where('doc.doc_date <=', $date[1].' 23:59:59');
				}
			}

			$db_huawei->group_by('sn_hst.sn');
			$data = $db_huawei->get();
			$data = $data->result();

			$num = 1;
			foreach($data as $row){
			//	$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->doc_no,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_pn,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->sn,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->name_stockstatus,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->name_io_boundtype,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->from,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->to,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->locbin,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->date_send,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->date_received,PHPExcel_Cell_DataType::TYPE_STRING);
			//	$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->status == 1 ? 'Progress' : 'Closed',PHPExcel_Cell_DataType::TYPE_STRING);
            	$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->from,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->doc_no,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->sr,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->name_pn,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->sn,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->to,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->locbin,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->status,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->date_send,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->name_logistic,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->awb_no,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->date_received,PHPExcel_Cell_DataType::TYPE_STRING);
				$excel_row++;
				$num++;

			}
		}elseif($input['type'] == 7 || $input['type'] == 8){
			if($input['type'] == 7){
				$data = $db_huawei->select('warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to, sn.sn, sn.locbin, stock_stat.name_stockstatus, doc.date_received, doc.doc_no, pn.name_pn, doc.doc_no, doc.status')
				->from('dt_sn_history sn_hst')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
				->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
				->where_in('doc.id_boundtype', array('1', '2', '11'))
				->order_by('doc.date_received', 'ASC');

				if($input['id_project']){
					$db_huawei->like('sn.id_project', $input['id_project']);
				}

				if($input['id_warehouse']){
					$db_huawei->where('doc.id_boundto', $input['id_warehouse']);
				}

				if($input['id_stockstatus']){
					$db_huawei->like('sn.id_stockstatus', $input['id_stockstatus']);
				}

				if($input['date']){
					$date = explode(' - ', $input['date']);
					if($date[0] == $date[1]){
						$db_huawei->like('doc.doc_date', $date[0]);
					}else{
						$db_huawei->where('doc.doc_date >=', $date[0]);
						$db_huawei->where('doc.doc_date <=', $date[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data = $db_huawei->get();
				$data = $data->result();
			}else{
				$data = $db_huawei->select('warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to, sn.sn, sn.locbin, stock_stat.name_stockstatus, doc.date_received, doc.doc_no, pn.name_pn, doc.doc_no, doc.status')
				->from('dt_sn_history sn_hst')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
				->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
				->where_in('doc.id_boundtype', array('4', '5', '10'))
				->order_by('doc.date_received', 'ASC');

				if($input['id_project']){
					$db_huawei->like('sn.id_project', $input['id_project']);
				}

				if($input['id_warehouse']){
					$db_huawei->where('doc.id_boundfrom', $input['id_warehouse']);
				}

				if($input['id_stockstatus']){
					$db_huawei->like('sn.id_stockstatus', $input['id_stockstatus']);
				}

				if($input['date']){
					$date = explode(' - ', $input['date']);
					if($date[0] == $date[1]){
						$db_huawei->like('doc.doc_date', $date[0]);
					}else{
						$db_huawei->where('doc.doc_date >=', $date[0]);
						$db_huawei->where('doc.doc_date <=', $date[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data = $db_huawei->get();
				$data = $data->result();
			}

			$num = 1;
			foreach($data as $row){
				$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->doc_no,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_pn,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->sn,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->name_stockstatus,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->from,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->to,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->locbin,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->date_received,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->status == 1 ? 'Progress' : 'Closed',PHPExcel_Cell_DataType::TYPE_STRING);

				$excel_row++;
				$num++;

			}
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');

		$file_name = $doc_name[$input['type'] - 1] . '-' . time() . '-Data.xls';

		$object_writer->save(getcwd() . '/public/upload/' . $file_name);
		if (filesize(getcwd() . '/public/upload/' . $file_name) > 10000000) {
			$file_content = file_get_contents(getcwd() . '/public/upload/' . $file_name);
			if (file_exists(getcwd() . '/public/upload/' . $file_name)) {
				unlink(getcwd() . '/public/upload/' . $file_name);
			}
			$this->zip->add_data($file_name, $file_content);
			$this->zip->download($file_name . '.zip');
		} else {
			if (file_exists(getcwd() . '/public/upload/' . $file_name)) {
				unlink(getcwd() . '/public/upload/' . $file_name);
			}
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $file_name);
			$object_writer->save('php://output');
		}
		redirect(base_url('huawei/download/inventory_stock'));
	}

	public function excel_transaction()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$input = $this->input->post();
		$doc_name = ['All Transaction', 'Unreturn', 'Out of Stock', 'In Delivery'];

		$this->load->library("excel");

		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		if($input['type'] == 1){
			$table_columns = array("No", "Application Type", "Callcenter", "Project", "Customer Name", "Order Creation Date", "Order Receipt Date", "RMR No.", "SR/PR No.", "RMA/PR No", "TT No.", "Site ID", "Site Name", "BOM Code", "Description", "Applicant", "DOP Area", "DOP Address", "Region", "Responsible Warehouse", "Outbound Warehouse", "Delivery Status", "Confirm Item", "Outbound SN", "Loc Bin Delivery", "SLA", "Save Time", "Outbound Date", "Outbound No", "ETA", "Consignee Name", "Customer Signing Date", "TAT Delivery", "Delivery Status", "iCare Outbound Status", "Delivery Logistic", "Delivery Ship Method", "Delivery AWB No.", "Remark Delivery", "Request Pickup Date", "Pickup Creation Date", "CMN No.", "Actual Pick Up Date", "Faulty Item", "Faulty SN", "Faulty Loc Bin", "TAT Pick Up", "Pickup Status", "iCare Inbound Status", "Remark Pickup", "Pick up Logistic", "Pickup Ship Method", "Pickup AWB No.", "Bound Type", "Last Inbound Date");
		}elseif($input['type'] == 2){
			$table_columns = array("No", "Project", "Customer", "Request Time", "RMR", "SR Order / Pr No", "RMA / Pr Code", "Site", "Site ID", "Basecode", "Description", "Requestor", "DOP Area", "DOP Address", "District", "Region", "Warehouse Area", "Warehouse Origin", "Delivery Status", "PN Delivery", "SN Delivery", "Loc Bin Delivery", "SLA", "Delivery Order", "ETA", "Consignee", "Receive Time", "Outbound Order");
		}elseif($input['type'] == 3){
			$table_columns = array("No", "Project", "Customer", "Request Time", "RMR", "SR Order / Pr No", "RMA / Pr Code", "Site", "Site ID", "Basecode", "Description", "Requestor", "DOP Area", "DOP Address", "District", "Region", "Warehouse Area", "Status");
		}else{
			$table_columns = array("No", "Project", "Customer", "Request Time", "RMR", "SR Order / Pr No", "RMA / Pr Code", "Site", "Site ID", "Basecode", "Description", "Requestor", "DOP Area", "DOP Address", "District", "Region", "Warehouse Area", "Warehouse Origin", "Delivery Status", "PN Delivery", "SN Delivery", "Loc Bin Delivery", "SLA", "Delivery Order Time", "Delivery Order", "ETA", "Status");
		}

		$column = 0;

		foreach($table_columns as $field){

			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);

			$column++;

		}

		$excel_row = 2;

		if($input['type'] == 1){
			$db_huawei->select('user.name_user, project.name_project, app.name_application_type, customer.name_customer, req.create_time, req.time_request, req.rmr, req.order, req.rma, req.tiket, req.site_id, req.site_name, pn.name_pn AS pn_req, basecode.description, requestor.name_requestor, warehouse.name_warehouse, dop.alamat_dop, region.name_region, dop.name_dop, warehouse2.name_warehouse AS origin_name, del_type.name_deliverytype, pn2.name_pn AS pn_del, sn.sn AS sn_del, req.sla, req.save_request_time, pod.time_created AS pod_create_time, pod.pod_no, req.eta_received, pod.consignee, pod.actual_received, stat_req.status_request, req.outbound_order, logistic.name_logistic AS logistic_del, log_service.name_logisticservice AS log_service_del, pod.awb_no AS awb_del, req.remark, req.pickup_request_time, pickup.time_created AS pickup_create_time, pickup.pickup_no, pickup.actual_pickup, pn3.name_pn AS pn_pickup, sn2.sn AS sn_pickup, stat_pickup.pickup_status, req.inbound_order, req.remark_pickup, logistic2.name_logistic AS logistic_pickup, log_service2.name_logisticservice AS log_service_pickup, pickup.awb_no AS awb_pickup, doc.doc_no, doc.doc_date, sn.locbin AS locbin_del, sn2.locbin AS locbin_pu')
			->from('dt_request req')
			->join('dm_application_type app', 'app.id_application_type = req.id_application_type', 'left')
			->join('dm_project project', 'project.id_project = req.id_project', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode', 'left')
			->join('dm_pn pn2', 'pn2.id_pn = req.id_pn', 'left')
			->join('dm_pn pn3', 'pn3.id_pn = req.id_pn_pickup', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_sn sn2', 'sn2.id_sn = req.id_sn_pickup', 'left')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
			->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter', 'left')
			->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
			->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse', 'left')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = req.id_warehouse', 'left')
			->join('dm_region region', 'region.id_region = dop.id_region', 'left')
			->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
			->join('dm_logistic logistic', 'logistic.id_logistic = pod.id_logistic', 'left')
			->join('dm_logisticservice log_service', 'log_service.id_logisticservice = pod.id_logisticservice', 'left')
			->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
			->join('dt_status_pickup stat_pickup', 'stat_pickup.id_statuspickup = req.id_statuspickup', 'left')
			->join('dm_logistic logistic2', 'logistic2.id_logistic = pickup.id_logistic', 'left')
			->join('dm_logisticservice log_service2', 'log_service2.id_logisticservice = pickup.id_logisticservice', 'left')
			->join('dt_sn_history sn_hst', 'sn_hst.sn = sn.id_sn', 'left')
			->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc', 'left')
			->order_by('req.time_request', 'ASC');
			// ->join('dt_sn_history sn_hst2', 'sn_hst2.sn = sn2.id_sn', 'left')
			// ->join('dt_io_bound doc2', 'doc2.id = sn_hst2.id_doc', 'left');

			if($input['id_project']){
				$db_huawei->like('req.id_project', $input['id_project']);
			}

			if($input['id_customer']){
				$db_huawei->like('req.id_customer', $input['id_customer']);
			}

			if($input['date']){
				$date = explode(' - ', $input['date']);
				if($date[0] == $date[1]){
					$db_huawei->like('req.time_request', $date[0]);
				}else{
					$db_huawei->where('req.time_request >=', $date[0]);
					$db_huawei->where('req.time_request <=', $date[1].' 23:59:59');
				}
			}

			$db_huawei->group_by('req.id_request');
			$db_huawei->group_by('sn_hst.sn');
			// $db_huawei->group_by('sn_hst2.sn');
			$data = $db_huawei->get();
			$data = $data->result();

			$num = 1;
			foreach($data as $row){
				$date_diff1 = new DateTime($row->actual_received);
				$date_diff2 = new DateTime($row->time_request);
				$date_diff3 = new DateTime($row->pickup_request_time);
				$date_diff4 = new DateTime($row->actual_pickup);
				$date_diff = $date_diff1->diff($date_diff2);
				$hours = $date_diff->days * 24 + $date_diff->h;
				$minute = $date_diff->i;
				$second = $date_diff->s;
				$total = (''.$hours.':'.$minute.':'.$second.'');
			//	$date_diff = $date_diff->format('%a Days, %h Hour, %i Minute');
				$date_diff5 = $date_diff3->diff($date_diff4);
				$hours1 = $date_diff5->days * 24 + $date_diff5->h;
				$minute1 = $date_diff5->i;
				$second1 = $date_diff5->s;
				$total1 = (''.$hours1.':'.$minute1.':'.$second1.'');
			//	$date_diff5 = $date_diff5->format('%a Days, %h Hour, %i Minute');

				$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->name_application_type ? $row->name_application_type : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_user ? $row->name_user : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->name_project ? $row->name_project : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->name_customer ? $row->name_customer : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->create_time ? $row->create_time : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->time_request ? $row->time_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->rmr ? $row->rmr : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->order ? $row->order : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->rma ? $row->rma : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->tiket ? $row->tiket : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->site_id ? $row->site_id : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->site_name ? $row->site_name : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row, $row->pn_req ? $row->pn_req : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('O'.$excel_row, $row->description ? $row->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('P'.$excel_row, $row->name_requestor ? $row->name_requestor : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Q'.$excel_row, $row->name_dop ? $row->name_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('R'.$excel_row, $row->alamat_dop ? $row->alamat_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('S'.$excel_row, $row->name_region ? $row->name_region : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('T'.$excel_row, $row->name_warehouse ? $row->name_warehouse : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('U'.$excel_row, $row->origin_name ? $row->origin_name : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('V'.$excel_row, $row->origin_name != $row->name_warehouse ? 'Pool to Pool' : $row->name_deliverytype,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('W'.$excel_row, $row->pn_del ? $row->pn_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('X'.$excel_row, $row->sn_del ? $row->sn_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Y'.$excel_row, $row->locbin_del ? $row->locbin_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Z'.$excel_row, $row->sla != ' Hours  Minutes' ? $row->sla : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AA'.$excel_row, $row->save_request_time ? $row->save_request_time : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AB'.$excel_row, $row->pod_create_time ? $row->pod_create_time : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AC'.$excel_row, $row->pod_no ? $row->pod_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AD'.$excel_row, $row->eta_received ? $row->eta_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AE'.$excel_row, $row->consignee ? $row->consignee : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AF'.$excel_row, $row->actual_received ? $row->actual_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AG'.$excel_row, $row->pod_create_time ? $total : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AH'.$excel_row, $row->status_request ? $row->status_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AI'.$excel_row, $row->outbound_order ? $row->outbound_order : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AJ'.$excel_row, $row->logistic_del ? $row->logistic_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AK'.$excel_row, $row->log_service_del ? $row->log_service_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AL'.$excel_row, $row->awb_del ? $row->awb_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AM'.$excel_row, $row->remark ? $row->remark : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AN'.$excel_row, $row->pickup_request_time ? $row->pickup_request_time : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AO'.$excel_row, $row->pickup_create_time ? $row->pickup_create_time : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AP'.$excel_row, $row->pickup_no ? $row->pickup_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AQ'.$excel_row, $row->actual_pickup ? $row->actual_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AR'.$excel_row, $row->pn_pickup ? $row->pn_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AS'.$excel_row, $row->sn_pickup ? $row->sn_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AT'.$excel_row, $row->locbin_pu ? $row->locbin_pu : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AU'.$excel_row, $row->pickup_create_time ? $total1 : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AV'.$excel_row, $row->pickup_status ? $row->pickup_status : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AW'.$excel_row, $row->inbound_order ? $row->inbound_order : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AX'.$excel_row, $row->remark_pickup ? $row->remark_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AY'.$excel_row, $row->logistic_pickup ? $row->logistic_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AZ'.$excel_row, $row->log_service_pickup ? $row->log_service_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('BA'.$excel_row, $row->awb_pickup ? $row->awb_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('BB'.$excel_row, $row->doc_no ? $row->doc_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('BC'.$excel_row, $row->doc_date ? $row->doc_date : '',PHPExcel_Cell_DataType::TYPE_STRING);

				$excel_row++;
				$num++;

			}
		}elseif($input['type'] == 2){
			$db_huawei->select('project.name_project, customer.name_customer, req.time_request, req.rmr, req.order, req.rma, req.site_id, pn.name_pn AS pn_req, basecode.description, requestor.name_requestor, warehouse.name_warehouse, dop.alamat_dop, region.name_region, dop.name_dop, district.name_district, warehouse2.name_warehouse AS origin_name, del_type.name_deliverytype, pn2.name_pn AS pn_del, sn.sn AS sn_del, req.sla, pod.pod_no, req.eta_received, pod.consignee, pod.actual_received, req.outbound_order, sn.locbin AS locbin_del')
			->from('dt_request req')
			->join('dm_project project', 'project.id_project = req.id_project', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode', 'left')
			->join('dm_pn pn2', 'pn2.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
			->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
			->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse', 'left')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = req.id_warehouse', 'left')
			->join('dm_region region', 'region.id_region = dop.id_region', 'left')
			->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->join('dm_district district', 'district.id_district = dop.id_district', 'left')
			->where('req.id_pickup', null)
			->where('req.url_photo_mdn !=', null)
			->order_by('req.time_request', 'ASC');

			if($input['id_project']){
				$db_huawei->like('req.id_project', $input['id_project']);
			}

			if($input['id_customer']){
				$db_huawei->like('req.id_customer', $input['id_customer']);
			}

			if($input['date']){
				$date = explode(' - ', $input['date']);
				if($date[0] == $date[1]){
					$db_huawei->like('req.time_request', $date[0]);
				}else{
					$db_huawei->where('req.time_request >=', $date[0]);
					$db_huawei->where('req.time_request <=', $date[1].' 23:59:59');
				}
			}

			$data = $db_huawei->get();
			$data = $data->result();

			$num = 1;
			foreach($data as $row){
				$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->name_project ? $row->name_project : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_customer ? $row->name_customer : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->time_request ? $row->time_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->rmr ? $row->rmr : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->order ? $row->order : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->rma ? $row->rma : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->site_id ? $row->site_id : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->pn_req ? $row->pn_req : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->description ? $row->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->name_requestor ? $row->name_requestor : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->name_dop ? $row->name_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->alamat_dop ? $row->alamat_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row, $row->name_district ? $row->name_district : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('O'.$excel_row, $row->name_region ? $row->name_region : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('P'.$excel_row, $row->name_warehouse ? $row->name_warehouse : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Q'.$excel_row, $row->origin_name ? $row->origin_name : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('R'.$excel_row, $row->origin_name != $row->name_warehouse ? 'Pool to Pool' : $row->name_deliverytype,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('S'.$excel_row, $row->pn_del ? $row->pn_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('T'.$excel_row, $row->sn_del ? $row->sn_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('U'.$excel_row, $row->locbin_del ? $row->locbin_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('V'.$excel_row, $row->sla != ' Hours  Minutes' ? $row->sla : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('W'.$excel_row, $row->pod_no ? $row->pod_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('X'.$excel_row, $row->eta_received ? $row->eta_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Y'.$excel_row, $row->consignee ? $row->consignee : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Z'.$excel_row, $row->actual_received ? $row->actual_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('AA'.$excel_row, $row->outbound_order ? $row->outbound_order : '',PHPExcel_Cell_DataType::TYPE_STRING);

				$excel_row++;
				$num++;

			}
		}elseif($input['type'] == 3){
			$db_huawei->select('project.name_project, customer.name_customer, req.time_request, req.rmr, req.order, req.rma, req.site_id, pn.name_pn AS pn_req, basecode.description, requestor.name_requestor, warehouse.name_warehouse, dop.alamat_dop, region.name_region, dop.name_dop, district.name_district')
			->from('dt_request req')
			->join('dm_project project', 'project.id_project = req.id_project', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode', 'left')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
			->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
			->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse', 'left')
			->join('dm_region region', 'region.id_region = dop.id_region', 'left')
			->join('dm_district district', 'district.id_district = dop.id_district', 'left')
			->where('req.id_status_request', '3')
			->order_by('req.time_request', 'ASC');

			if($input['id_project']){
				$db_huawei->like('req.id_project', $input['id_project']);
			}

			if($input['id_customer']){
				$db_huawei->like('req.id_customer', $input['id_customer']);
			}

			if($input['date']){
				$date = explode(' - ', $input['date']);
				if($date[0] == $date[1]){
					$db_huawei->like('req.time_request', $date[0]);
				}else{
					$db_huawei->where('req.time_request >=', $date[0]);
					$db_huawei->where('req.time_request <=', $date[1].' 23:59:59');
				}
			}

			$data = $db_huawei->get();
			$data = $data->result();

			$num = 1;
			foreach($data as $row){
				$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->name_project ? $row->name_project : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_customer ? $row->name_customer : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->time_request ? $row->time_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->rmr ? $row->rmr : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->order ? $row->order : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->rma ? $row->rma : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->site_id ? $row->site_id : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->pn_req ? $row->pn_req : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->description ? $row->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->name_requestor ? $row->name_requestor : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->name_dop ? $row->name_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->alamat_dop ? $row->alamat_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row, $row->name_district ? $row->name_district : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('O'.$excel_row, $row->name_region ? $row->name_region : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('P'.$excel_row, $row->name_warehouse ? $row->name_warehouse : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Q'.$excel_row, 'OOS',PHPExcel_Cell_DataType::TYPE_STRING);

				$excel_row++;
				$num++;

			}
		}else{
			$db_huawei->select('project.name_project, customer.name_customer, req.time_request, req.rmr, req.order, req.rma, req.site_id, pn.name_pn AS pn_req, basecode.description, requestor.name_requestor, warehouse.name_warehouse, dop.alamat_dop, region.name_region, dop.name_dop, district.name_district, warehouse2.name_warehouse AS origin_name, del_type.name_deliverytype, pn2.name_pn AS pn_del, sn.sn AS sn_del, req.sla, pod.time_created AS pod_create_time, pod.pod_no, req.eta_received, sn.locbin AS locbin_del')
			->from('dt_request req')
			->join('dm_project project', 'project.id_project = req.id_project', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode', 'left')
			->join('dm_pn pn2', 'pn2.id_pn = req.id_pn', 'left')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
			->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
			->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse', 'left')
			->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = req.id_warehouse', 'left')
			->join('dm_region region', 'region.id_region = dop.id_region', 'left')
			->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->join('dm_district district', 'district.id_district = dop.id_district', 'left')
			->where('req.id_pod !=', null)
			->where('req.url_photo_mdn', null)
			->order_by('req.time_request', 'ASC');

			if($input['id_project']){
				$db_huawei->like('req.id_project', $input['id_project']);
			}

			if($input['id_customer']){
				$db_huawei->like('req.id_customer', $input['id_customer']);
			}

			if($input['date']){
				$date = explode(' - ', $input['date']);
				if($date[0] == $date[1]){
					$db_huawei->like('req.time_request', $date[0]);
				}else{
					$db_huawei->where('req.time_request >=', $date[0]);
					$db_huawei->where('req.time_request <=', $date[1].' 23:59:59');
				}
			}

			$data = $db_huawei->get();
			$data = $data->result();

			$num = 1;
			foreach($data as $row){
				$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->name_project ? $row->name_project : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, $row->name_customer ? $row->name_customer : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->time_request ? $row->time_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->rmr ? $row->rmr : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->order ? $row->order : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->rma ? $row->rma : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->site_id ? $row->site_id : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->pn_req ? $row->pn_req : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->description ? $row->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->name_requestor ? $row->name_requestor : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row, $row->name_dop ? $row->name_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row, $row->alamat_dop ? $row->alamat_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row, $row->name_district ? $row->name_district : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('O'.$excel_row, $row->name_region ? $row->name_region : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('P'.$excel_row, $row->name_warehouse ? $row->name_warehouse : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Q'.$excel_row, $row->origin_name ? $row->origin_name : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('R'.$excel_row, $row->name_deliverytype ? $row->name_deliverytype : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('S'.$excel_row, $row->pn_del ? $row->pn_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('T'.$excel_row, $row->sn_del ? $row->sn_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('U'.$excel_row, $row->locbin_del ? $row->locbin_del : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('V'.$excel_row, $row_loop->sla != ' Hours  Minutes' ? $row_loop->sla : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('W'.$excel_row, $row->pod_create_time ? $row->pod_create_time : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('X'.$excel_row, $row->pod_no ? $row->pod_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Y'.$excel_row, $row->eta_received ? $row->eta_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
				$object->getActiveSheet()->setCellValueExplicit('Z'.$excel_row, 'In Delivery',PHPExcel_Cell_DataType::TYPE_STRING);

				$excel_row++;
				$num++;

			}
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');

		$file_name = $doc_name[$input['type'] - 1] . '-' . time() . '-Data.xls';

		$object_writer->save(getcwd() . '/public/upload/' . $file_name);
		if (filesize(getcwd() . '/public/upload/' . $file_name) > 10000000) {
			$file_content = file_get_contents(getcwd() . '/public/upload/' . $file_name);
			if (file_exists(getcwd() . '/public/upload/' . $file_name)) {
				unlink(getcwd() . '/public/upload/' . $file_name);
			}
			$this->zip->add_data($file_name, $file_content);
			$this->zip->download($file_name . '.zip');
		} else {
			if (file_exists(getcwd() . '/public/upload/' . $file_name)) {
				unlink(getcwd() . '/public/upload/' . $file_name);
			}
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $file_name);
			$object_writer->save('php://output');
		}
		redirect(base_url('huawei/download/download_transaction'));
	}

	public function excel_finance()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$input = $this->input->post();
		$doc_name = ['INBOUND', 'REPLENISHMENT', 'DELIVER', 'RETURN', 'OUTBOUND'];

		$columns = [
			array("No", "Project", "Customer", "Description", "PN", "SN", "Status", "Doc", "Bound Type", "From", "Inbound Date"),
			array("No", "Project", "Customer", "Description", "PN", "SN", "SR Order", "Ref Number", "Distribution Date", "Received Date", "SLA", "Status", "Pool Initial", "Pool Destination", "Dist Year", "Dist Month", "Bound Type", "Doc"),
			array("No", "Customer", "RMR", "SR Number / PR Number", "RMA / PR Code", "Delivery Order", "TT / Site", "Requestor", "Pool Initial", "Pool Destination", "Delivery Type", "DOP", "DOP Address", "Module Name", "PN", "SN", "Delivery Year", "Quart", "Delivery Month", "Request Date", "SLA", "Delivered", "Delivery Date & Time", "Status", "Remark"),
			array("No", "Customer", "RMR", "SR Number / PR Number", "RMA / PR Code", "Status Request", "Pickup Order", "Requestor", "City", "DOP", "DOP Address", "Pickup Request Date", "Pickup Date", "SLA Pick Up", "Status Pick Up", "Return Date", "SLA Return", "Received CWH", "Status Return", "Module Name", "PN", "SN", "Return Status", "TT / Site", "Remark"),
			array("No", "Project", "Customer", "PN", "SN", "Description", "SR Order", "Ref Number", "Doc", "Sent To", "Bound Type", "Send Date", "Status", "AWB")
		];

		$this->load->library("excel");

		$object = new PHPExcel();

		$tes = $object->getActiveSheet();
		$tes->setTitle($doc_name[0]);

		$first_column = 0;

		foreach($columns[0] as $field){

			$object->getActiveSheet()->setCellValueByColumnAndRow($first_column, 1, $field);

			$first_column++;
		}

		$excel_row = 2;

		$db_huawei->select('project.name_project, basecode.description, pn.name_pn, sn.sn, stock_stat.name_stockstatus, doc.doc_no, bound_type.name_io_boundtype, warehouse.name_warehouse, doc.date_received')
		->from('dt_sn_history sn_hst')
		->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
		->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
		->join('dm_project project', 'project.id_project = doc.id_project')
		->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
		->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
		->where_in('doc.id_boundtype', array('1', '2', '11'))
		->order_by('doc.date_received', 'ASC');

		if($input['date']){
			$date = explode(' - ', $input['date']);
			if($date[0] == $date[1]){
				$db_huawei->like('doc.doc_date', $date[0]);
			}else{
				$db_huawei->where('doc.doc_date >=', $date[0]);
				$db_huawei->where('doc.doc_date <=', $date[1].' 23:59:59');
			}
		}

		$db_huawei->group_by('sn_hst.sn');
		$data = $db_huawei->get();
		$data = $data->result();

		$num = 1;
		foreach($data as $row){
			$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row, $num,PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row, $row->name_project ? $row->name_project : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row, '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row, $row->description ? $row->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row, $row->name_pn ? $row->name_pn : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row, $row->sn ? $row->sn : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row, $row->name_stockstatus ? $row->name_stockstatus : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row, $row->doc_no ? $row->doc_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row, $row->name_io_boundtype ? $row->name_io_boundtype : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row, $row->name_warehouse ? $row->name_warehouse : '',PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row, $row->date_received ? $row->date_received : '',PHPExcel_Cell_DataType::TYPE_STRING);

			$excel_row++;
			$num++;

		}

		for($i = 1; $i < count($doc_name); $i++){
			$excel_row_loop = 2;
			$sheet = $object->createSheet($i);
			$object->setActiveSheetIndex($i);

			$first_column_loop = 0;

			foreach($columns[$i] as $field_loop){

				$object->getActiveSheet()->setCellValueByColumnAndRow($first_column_loop, 1, $field_loop);

				$first_column_loop++;
			}

			if($i == 1){
				$db_huawei->select('project.name_project, basecode.description, pn.name_pn, sn.sn, temp.sr, temp.rf, stock_stat.name_stockstatus, doc.doc_no, bound_type.name_io_boundtype, warehouse.name_warehouse, doc.date_send, doc.awb_no, doc.date_received, warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to, doc.doc_date')
				->from('dt_inbound_cwh_pn_temp temp')
				->join('dt_sn_history sn_hst', 'sn_hst.id_doc = temp.doc_id')
				->join('dt_io_bound doc', 'doc.id = temp.doc_id')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
				->join('dm_project project', 'project.id_project = doc.id_project')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
				->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
				->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
				->where_in('doc.id_boundtype', array('3', '9'))
				->order_by('doc.date_send', 'ASC');

				if($input['date']){
					$date_loop = explode(' - ', $input['date']);
					if($date_loop[0] == $date_loop[1]){
						$db_huawei->like('doc.doc_date', $date_loop[0]);
					}else{
						$db_huawei->where('doc.doc_date >=', $date_loop[0]);
						$db_huawei->where('doc.doc_date <=', $date_loop[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data_loop = $db_huawei->get();
				$data_loop = $data_loop->result();

				$num_loop = 1;
				foreach($data_loop as $row_loop){
					$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row_loop, $num_loop,PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row_loop, $row_loop->name_project ? $row_loop->name_project : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row_loop, '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row_loop, $row_loop->description ? $row_loop->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row_loop, $row_loop->name_pn ? $row_loop->name_pn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row_loop, $row_loop->sn ? $row_loop->sn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row_loop, $row_loop->sr ? $row_loop->sr : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row_loop, $row_loop->rf ? $row_loop->rf : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row_loop, $row_loop->date_send ? $row_loop->date_send : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row_loop, $row_loop->date_received ? $row_loop->date_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row_loop, '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row_loop, '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row_loop, $row_loop->from ? $row_loop->from : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row_loop, $row_loop->to ? $row_loop->to : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('O'.$excel_row_loop, date('Y', strtotime($row_loop->doc_date)),PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('P'.$excel_row_loop, date('F', strtotime($row_loop->doc_date)),PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('Q'.$excel_row_loop, $row_loop->name_io_boundtype ? $row_loop->name_io_boundtype : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('R'.$excel_row_loop, $row_loop->doc_no ? $row_loop->doc_no : '',PHPExcel_Cell_DataType::TYPE_STRING);

					$excel_row_loop++;
					$num_loop++;

				}
			}elseif($i == 2){
				$db_huawei->select('project.name_project, customer.name_customer, req.time_request, req.rmr, req.order, req.rma, req.site_id, pn.name_pn, basecode.description, requestor.name_requestor, dop.alamat_dop, region.name_region, dop.name_dop, district.name_district, city2.name_city AS city_destination, city.name_city AS city_initial, sn.sn, req.sla, pod.time_created, pod.pod_no, pod.actual_received, stat_req.status_request, req.remark, del_type.name_deliverytype')
				->from('dt_request req')
				->join('dm_project project', 'project.id_project = req.id_project', 'left')
				->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
				->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
				->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
				->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
				->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse', 'left')
				->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = req.id_warehouse', 'left')
				->join('dm_region region', 'region.id_region = dop.id_region', 'left')
				->join('dm_district district', 'district.id_district = dop.id_district', 'left')
				->join('dt_pod pod', 'pod.id_pod = req.id_pod')
				->join('dm_city city', 'city.id_city = warehouse2.id_city', 'left')
				->join('dm_city city2', 'city2.id_city = warehouse.id_city', 'left')
				->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
				->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
			//	->where('req.id_pickup', null)
				->where_not_in('req.id_status_request', array('4', '7'))
				->order_by('req.time_request', 'ASC');

				if($input['date']){
					$date_loop = explode(' - ', $input['date']);
					if($date_loop[0] == $date_loop[1]){
						$db_huawei->like('req.time_request', $date_loop[0]);
					}else{
						$db_huawei->where('req.time_request >=', $date_loop[0]);
						$db_huawei->where('req.time_request <=', $date_loop[1].' 23:59:59');
					}
				}

				$data_loop = $db_huawei->get();
				$data_loop = $data_loop->result();

				$num_loop = 1;
				foreach($data_loop as $row_loop){
					$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row_loop, $num_loop,PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row_loop, $row_loop->name_customer ? $row_loop->name_customer : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row_loop, $row_loop->rmr ? $row_loop->rmr : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row_loop, $row_loop->order ? $row_loop->order : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row_loop, $row_loop->rma ? $row_loop->rma : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row_loop, $row_loop->pod_no ? $row_loop->pod_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row_loop, $row_loop->site_id ? $row_loop->site_id : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row_loop, $row_loop->name_requestor ? $row_loop->name_requestor : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row_loop, $row_loop->city_initial ? $row_loop->city_initial : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row_loop, $row_loop->city_destination ? $row_loop->city_destination : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row_loop, $row_loop->city_initial != $row_loop->city_destination ? 'Pool to Pool' : $row_loop->name_deliverytype,PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row_loop, $row_loop->name_dop ? $row_loop->name_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row_loop, $row_loop->alamat_dop ? $row_loop->alamat_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row_loop, $row_loop->description ? $row_loop->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('O'.$excel_row_loop, $row_loop->name_pn ? $row_loop->name_pn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('P'.$excel_row_loop, $row_loop->sn ? $row_loop->sn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('Q'.$excel_row_loop, date('Y', strtotime($row_loop->time_request)),PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('R'.$excel_row_loop, '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('S'.$excel_row_loop, date('F', strtotime($row_loop->time_request)),PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('T'.$excel_row_loop, $row_loop->time_request ? $row_loop->time_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('U'.$excel_row_loop, $row_loop->sla != ' Hours  Minutes' ? $row_loop->sla : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('V'.$excel_row_loop, $row_loop->time_created ? $row_loop->time_created : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('W'.$excel_row_loop, $row_loop->actual_received ? $row_loop->actual_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('X'.$excel_row_loop, $row_loop->status_request ? $row_loop->status_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('Y'.$excel_row_loop, $row_loop->remark ? $row_loop->remark : '',PHPExcel_Cell_DataType::TYPE_STRING);

					$excel_row_loop++;
					$num_loop++;

				}
			}elseif($i == 3){
				$db_huawei->select('project.name_project, customer.name_customer, req.time_request, req.rmr, req.order, req.rma, req.site_id, pn.name_pn, basecode.description, requestor.name_requestor, dop.alamat_dop, region.name_region, dop.name_dop, district.name_district, city2.name_city AS city_destination, city.name_city AS city_initial, sn.sn, req.sla, stat_req.status_request, pickup.time_created, pickup.pickup_no, pickup.actual_pickup, stat_req.status_request, req.remark, del_type.name_deliverytype, req.pickup_request_time, dop.sla AS dop_sla, stat_pickup.pickup_status, doc.date_send AS doc_date_send, doc.date_received AS doc_date_received, req.remark_pickup')
				->from('dt_request req')
				->join('dm_project project', 'project.id_project = req.id_project', 'left')
				->join('dm_pn pn', 'pn.id_pn = req.id_pn_pickup', 'left')
				->join('dt_sn sn', 'sn.id_sn = req.id_sn_pickup', 'left')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode', 'left')
				->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left')
				->join('dt_requestor requestor', 'requestor.id_requestor = req.id_requestor', 'left')
				->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse', 'left')
				->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = req.id_warehouse', 'left')
				->join('dm_region region', 'region.id_region = dop.id_region', 'left')
				->join('dm_district district', 'district.id_district = dop.id_district', 'left')
				->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup')
				->join('dm_city city', 'city.id_city = warehouse2.id_city', 'left')
				->join('dm_city city2', 'city2.id_city = warehouse.id_city', 'left')
				->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
				->join('dm_deliverytype del_type', 'del_type.id_deliverytype = dop.id_deliverytype', 'left')
				->join('dt_status_pickup stat_pickup', 'stat_pickup.id_statuspickup = req.id_statuspickup', 'left')
				->join('dt_sn_history sn_hst', 'sn_hst.sn = req.id_sn_pickup', 'left')
				->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc', 'left')
				->where('req.id_pickup !=', null)
				->group_by('req.rmr')
				->order_by('req.time_request', 'ASC');

				if($input['date']){
					$date_loop = explode(' - ', $input['date']);
					if($date_loop[0] == $date_loop[1]){
						$db_huawei->like('pickup.actual_pickup', $date_loop[0]);
					}else{
						$db_huawei->where('pickup.actual_pickup >=', $date_loop[0]);
						$db_huawei->where('pickup.actual_pickup <=', $date_loop[1].' 23:59:59');
					}
				}

				$db_huawei->or_where_in('doc.id_boundtype', array('15', '7'));

				$data_loop = $db_huawei->get();
				$data_loop = $data_loop->result();

				$num_loop = 1;
				foreach($data_loop as $row_loop){
					$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row_loop, $num_loop,PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row_loop, $row_loop->name_customer ? $row_loop->name_customer : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row_loop, $row_loop->rmr ? $row_loop->rmr : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row_loop, $row_loop->order ? $row_loop->order : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row_loop, $row_loop->rma ? $row_loop->rma : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row_loop, $row_loop->status_request ? $row_loop->status_request : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row_loop, $row_loop->pickup_no ? $row_loop->pickup_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row_loop, $row_loop->name_requestor ? $row_loop->name_requestor : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row_loop, $row_loop->city_destination ? $row_loop->city_destination : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row_loop, $row_loop->name_dop ? $row_loop->name_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row_loop, $row_loop->alamat_dop ? $row_loop->alamat_dop : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row_loop, $row_loop->pickup_request_time ? $row_loop->pickup_request_time : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row_loop, $row_loop->actual_pickup ? $row_loop->actual_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row_loop, $row_loop->dop_sla ? $row_loop->dop_sla : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('O'.$excel_row_loop, round(strtotime($row_loop->actual_pickup) - strtotime($row_loop->pickup_request_time), 1) > $row_loop->dop_sla ? 'Failed' : 'In SLA',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('P'.$excel_row_loop, $row_loop->doc_date_send ? $row_loop->doc_date_send : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('Q'.$excel_row_loop, '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('R'.$excel_row_loop, $row_loop->doc_date_received ? $row_loop->doc_date_received : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('S'.$excel_row_loop, '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('T'.$excel_row_loop, $row_loop->description ? $row_loop->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('U'.$excel_row_loop, $row_loop->name_pn ? $row_loop->name_pn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('V'.$excel_row_loop, $row_loop->sn ? $row_loop->sn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('W'.$excel_row_loop, $row_loop->pickup_status ? $row_loop->pickup_status : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('X'.$excel_row_loop, $row_loop->site_id ? $row_loop->site_id : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('Y'.$excel_row_loop, $row_loop->remark_pickup ? $row_loop->remark_pickup : '',PHPExcel_Cell_DataType::TYPE_STRING);

					$excel_row_loop++;
					$num_loop++;

				}
			}else{
				$db_huawei->select('project.name_project, basecode.description, pn.name_pn, sn.sn, temp.sr, temp.rf, stock_stat.name_stockstatus, doc.doc_no, bound_type.name_io_boundtype, warehouse.name_warehouse, doc.date_send, doc.awb_no')
				->from('dt_inbound_cwh_pn_temp temp')
				->join('dt_sn_history sn_hst', 'sn_hst.id_doc = temp.doc_id')
				->join('dt_io_bound doc', 'doc.id = temp.doc_id')
				->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
				->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
				->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
				->join('dm_project project', 'project.id_project = doc.id_project')
				->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
				->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
				->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundto')
				->where_in('doc.id_boundtype', array('4', '5', '10'))
				->order_by('doc.date_send', 'ASC');

				if($input['date']){
					$date_loop = explode(' - ', $input['date']);
					if($date_loop[0] == $date_loop[1]){
						$db_huawei->like('doc.doc_date', $date_loop[0]);
					}else{
						$db_huawei->where('doc.doc_date >=', $date_loop[0]);
						$db_huawei->where('doc.doc_date <=', $date_loop[1].' 23:59:59');
					}
				}

				$db_huawei->group_by('sn_hst.sn');
				$data_loop = $db_huawei->get();
				$data_loop = $data_loop->result();

				$num_loop = 1;
				foreach($data_loop as $row_loop){
					$object->getActiveSheet()->setCellValueExplicit('A'.$excel_row_loop, $num_loop,PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('B'.$excel_row_loop, $row_loop->name_project ? $row_loop->name_project : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('C'.$excel_row_loop, '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('D'.$excel_row_loop, $row_loop->name_pn ? $row_loop->name_pn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('E'.$excel_row_loop, $row_loop->sn ? $row_loop->sn : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('F'.$excel_row_loop, $row_loop->description ? $row_loop->description : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('G'.$excel_row_loop, $row_loop->sr ? $row_loop->sr : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('H'.$excel_row_loop, $row_loop->rf ? $row_loop->rf : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('I'.$excel_row_loop, $row_loop->doc_no ? $row_loop->doc_no : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('J'.$excel_row_loop, $row_loop->name_warehouse ? $row_loop->name_warehouse : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('K'.$excel_row_loop, $row_loop->name_io_boundtype ? $row_loop->name_io_boundtype : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('L'.$excel_row_loop, $row_loop->date_send ? $row_loop->date_send : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('M'.$excel_row_loop, $row_loop->name_stockstatus ? $row_loop->name_stockstatus : '',PHPExcel_Cell_DataType::TYPE_STRING);
					$object->getActiveSheet()->setCellValueExplicit('N'.$excel_row_loop, $row_loop->awb_no ? $row_loop->awb_no : '',PHPExcel_Cell_DataType::TYPE_STRING);

					$excel_row_loop++;
					$num_loop++;

				}
			}

			$sheet->setTitle($doc_name[$i]);
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');

		$file_name = time() . '-Data.xls';

		$object_writer->save(getcwd() . '/public/upload/' . $file_name);
		if (filesize(getcwd() . '/public/upload/' . $file_name) > 10000000) {
			$file_content = file_get_contents(getcwd() . '/public/upload/' . $file_name);
			if (file_exists(getcwd() . '/public/upload/' . $file_name)) {
				unlink(getcwd() . '/public/upload/' . $file_name);
			}
			$this->zip->add_data($file_name, $file_content);
			$this->zip->download($file_name . '.zip');
		} else {
			if (file_exists(getcwd() . '/public/upload/' . $file_name)) {
				unlink(getcwd() . '/public/upload/' . $file_name);
			}
			header('Content-Type: application/vnd.ms-excel');

			header('Content-Disposition: attachment;filename="' . time() . '-Data.xls"');
			$object_writer->save('php://output');
		}
		redirect(base_url('huawei/download/download_finance'));
	}

	public function get_notif_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$level = $this->session->userdata('level');

		if($level == 10 || $level == 5 || $level == 6 || $level == 7){
			$i_good = $db_huawei->select('doc_no')
			->from('dt_io_bound')
			->where('id_boundto', $this->session->userdata('id_warehouse'))
			->where('date_received', null)
			->where('id_stockstatus', '1')
			->get();

			$i_faulty = $db_huawei->select('doc_no')
			->from('dt_io_bound')
			->where('id_boundto', $this->session->userdata('id_warehouse'))
			->where('date_received', null)
			->where('id_stockstatus', '2')
			->get();

			$return = array('i_good' => $i_good->result_array(), 'i_faulty' => $i_faulty->result_array());

			if($level == 10){
				$to_deliv = $db_huawei->select('req.rmr')
				->from('dt_request req')
				->join('dm_dop dop', 'dop.id_dop = req.id_dop')
				->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
				->where('req.id_pod', null)
				->where('req.url_photo_cmn', null)
				->get();

				$to_deliv_ria = $db_huawei->select('req.rmr')
				->from('dt_request req')
				->join('dm_dop dop', 'dop.id_dop = req.id_dop')
				->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
				->where('req.id_pod', null)
				->where('req.url_photo_cmn !=', null)
				->get();

				$to_pickup = $db_huawei->select('req.rmr')
				->from('dt_request req')
				->join('dm_dop dop', 'dop.id_dop = req.id_dop')
				->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
				->where('req.pickup_request_time !=', null)
				->where('req.id_status_request !=', '1')
				->where('req.url_photo_mdn !=', null)
				->get();

				$to_pickup_ria = $db_huawei->select('req.rmr')
				->from('dt_request req')
				->join('dm_dop dop', 'dop.id_dop = req.id_dop')
				->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
				->where('req.pickup_request_time !=', null)
				->where('req.id_status_request', '1')
				->get();

				$on_deliv = $db_huawei->select('req.rmr')
				->from('dt_request req')
				->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
				// ->where('req.url_photo_cmn !=', null)
				->where('req.id_status_request !=', '2')
				->get();
				
				$on_deliv_ria = $db_huawei->select('req.rmr')
				->from('dt_request req')
				->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
				->where('req.id_status_request', '2')
				// ->where('req.url_photo_cmn !=', null)
				->get();

				$return = array('i_good' => $i_good->result_array(), 'i_faulty' => $i_faulty->result_array(), 'to_deliv' => $to_deliv->result_array(), 'to_deliv_ria' => $to_deliv_ria->result_array(), 'to_pickup' => $to_pickup->result_array(), 'to_pickup_ria' => $to_pickup_ria->result_array(), 'request_on_deliv' => $on_deliv->result_array(), 'request_on_deliv_ria' => $on_deliv_ria->result_array());
			}
		} elseif ($level == 8){
			$request = $db_huawei->select('SUM(CASE WHEN (req.url_photo_mdn IS NOT NULL AND req.outbound_order IS NULL) THEN 1 ELSE 0 END) AS total_delivered, SUM(CASE WHEN (req.url_photo_cmn IS NOT NULL AND req.inbound_order IS NULL) THEN 1 ELSE 0 END) AS total_returned', FALSE)
			->from('dt_request req')
			->get();

			$i_order = $db_huawei->select('req.rmr')
			->from('dt_request req')
			->where('req.url_photo_mdn !=', null)
			->where('req.outbound_order', null)
			->get();

			$o_order = $db_huawei->select('req.rmr')
			->from('dt_request req')
			->where('req.url_photo_cmn !=', null)
	    	->where('req.inbound_order', null)
			->get();

			$return = array('i_order' => $i_order->result_array(), 'o_order' => $o_order->result_array());
		} elseif($level == 4){
			$on_deliv = $db_huawei->select('req.rmr')
			->from('dt_request req')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod')
			->where('req.eta_received <', date('Y-m-d H:i:s'))
			->where('pod.actual_received', null)
			->get();

			$return = array('request_on_deliv' => $on_deliv->result_array());
		} elseif($level == 69){
			$count_wrong_pass = $this->db->select('user.name_user, user_log.log_wrong_pass')
			->from('dt_user_log user_log')
			->join('dt_user user', 'user.id_user = user_log.id_user')
			->where('user_log.log_wrong_pass >=', 3)
			->like('user_log.timestamp', date('Y-m-d'))
			->get();

			$return = array('user_wrong_pass' => $count_wrong_pass->result_array());
		}

		echo json_encode($return);
	}

	public function testing()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$db_huawei->select('warehouse.name_warehouse as to, warehouse2.name_warehouse as from')
		// $db_huawei->select('project.name_project, basecode.description, pn.name_pn, sn.sn, temp.sr, temp.rf, stock_stat.name_stockstatus, doc.doc_no, bound_type.name_io_boundtype, warehouse.name_warehouse, doc.date_send, doc.awb_no, doc.date_received, warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to, doc.doc_date')
		->from('dt_io_bound doc')
		// ->from('dt_inbound_cwh_pn_temp temp')
		// ->join('dt_sn_history sn_hst', 'sn_hst.id_doc = doc.id')
		// ->join('dt_inbound_cwh_pn_temp temp', 'temp.doc_id = doc.id')
		// ->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
		// ->join('dt_sn sn', 'sn.id_sn = sn_hst.sn', 'left')
		// ->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		// ->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
		->join('dm_project project', 'project.id_project = doc.id_project')
		// ->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
		->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom', 'left')
		->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto', 'left')
		->where_in('doc.id_boundtype', array('3', '9'));

		// if($input['date']){
		// 	$date_loop = explode(' - ', $input['date']);
		// 	$db_huawei->where('doc.doc_date >=', $date_loop[0]);
		// 	$db_huawei->where('doc.doc_date <=', $date_loop[1]);
		// }

		// $db_huawei->group_by('sn_hst.id_doc');
		// $db_huawei->group_by('sn_hst.sn');
		$data_loop = $db_huawei->get();
		$data_loop = $data_loop->result();

		echo json_encode($data_loop);
	}

	public function tes_again()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$db_huawei->select('*')
		->from('dt_io_bound doc')
		// ->from('dt_inbound_cwh_pn_temp temp')
		// ->join('dt_sn_history sn_hst', 'sn_hst.id_doc = doc.id')
		// ->join('dt_inbound_cwh_pn_temp temp', 'temp.doc_id = doc.id')
		// // ->join('dt_io_bound doc', 'doc.id = sn_hst.id_doc')
		// ->join('dt_sn sn', 'sn.id_sn = sn_hst.sn')
		// ->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		// ->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
		// ->join('dm_project project', 'project.id_project = doc.id_project')
		// ->join('dm_stockstatus stock_stat', 'stock_stat.id_stockstatus = sn.id_stockstatus')
		// ->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
		// ->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
		// ->join('dm_warehouse warehouse2', 'warehouse.id_warehouse = doc.id_boundto')
		->where_in('doc.id_boundtype', array('3', '9'));

		// if($input['date']){
		// 	$date_loop = explode(' - ', $input['date']);
		// 	$db_huawei->where('doc.doc_date >=', $date_loop[0]);
		// 	$db_huawei->where('doc.doc_date <=', $date_loop[1]);
		// }

		// $db_huawei->group_by('sn_hst.id_doc');
		// $db_huawei->group_by('sn_hst.sn');
		$data_loop = $db_huawei->get();
		$data_loop = $data_loop->result();

		echo json_encode($data_loop);
	}
}