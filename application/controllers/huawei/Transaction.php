<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function request()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$proj = $db_huawei->select('*')
		->from('dm_project')
		->get();

		$data['proj'] = $proj->result_array();
		
		$check_list = $db_huawei->select('*')
		->from('dm_status_request')
		->where_not_in('id_status_request', array('1', '5', '6'))
		->get();

		$data['check_list'] = $check_list->result_array();
		$data['check_list_edit'] = $check_list->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/transaction/request', $data);
	}

	public function icare()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$this->load->view('huawei/layout');
		$this->load->view('huawei/transaction/icare');
	}

	public function rmr_report()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$this->load->view('huawei/layout');
		$this->load->view('huawei/transaction/rmr_report');
	}

	public function ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$proj = $db_huawei->select('*')
		->from('dm_project')
		->get();

		$data['proj'] = $proj->result_array();

		$check_list = $db_huawei->select('*')
		->from('dm_status_request')
		->where_not_in('id_status_request', array('1', '5', '6'))
		->get();

		$data['check_list'] = $check_list->result_array();
		$data['check_list_edit'] = $check_list->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/transaction/ria', $data);
	}

	public function get_project_type_by_project()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$proj = $db_huawei->select('*')
		->from('dm_projecttype')
		->where('id_project', $this->input->post('id_project'))
		->get();

		echo json_encode($proj->result_array());
	}

    public function get_application_type()
    {
        $db_huawei = $this->load->database('huawei', TRUE);
        $proj = $db_huawei->select('*')
        ->from('dm_application_type')
        ->where('id_project', $this->input->post('id_project'))
        ->get();
        
        echo json_encode($proj->result_array());
    }
    
	public function get_customer_by_project_type()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$proj = $db_huawei->select('*')
		->from('dm_customer')
		->where('id_projecttype', $this->input->post('id_projecttype'))
		->get();

		echo json_encode($proj->result_array());
	}

	public function get_requestor_and_dop_by_customer()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$proj = $db_huawei->select('*')
		->from('dt_requestor')
		->where('activated', '1')
		->where('id_customer', $this->input->post('id_customer'))
		->get();
		$proj2 = $db_huawei->select('*')
		->from('dm_dop')
		->where('activated','1')
		->where('id_customer', $this->input->post('id_customer'))
		->get();

		echo json_encode(array('requestor' => $proj->result_array(), 'dop' => $proj2->result_array()));
	}

	public function create_RMR()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$ret_rmr = '';

		$rmr = $db_huawei->select('customer.code_customer, warehouse.code_warehouse')
		->from('dm_customer customer')
		->join('dm_dop dop', 'dop.id_customer = customer.id_customer')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
		->where('customer.id_customer', $this->input->post('id_customer'))
		->where('dop.id_dop', $this->input->post('id_dop'))
		->get();
		$rmr = $rmr->row();
		$ret_rmr = $rmr->code_customer.$rmr->code_warehouse.'-'.date('ymd').'-'.$this->input->post('rma');

		echo json_encode($ret_rmr);
	}

	public function check_pn_exist_in_warehouse()
	{
		$return = '';
		$pn = $this->input->post('pn');

		$db_huawei = $this->load->database('huawei', TRUE);

		$pn_exist = $db_huawei->select('pn.name_pn, pn.id_pn, warehouse.id_warehouse, warehouse.name_warehouse')
		->from('dt_sn sn')
		->join('dm_dop dop', 'dop.id_warehouse = sn.id_warehouse')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->where('pn.name_pn', $pn)
		->where('dop.id_dop', $this->input->post('id_dop'))
		->where('sn.id_stockaccess', '1')
		->group_by('pn.name_pn')
		->get();

		if($pn_exist->num_rows() < 1) {
			$pn_exist_another = $db_huawei->select('warehouse.name_warehouse, warehouse.id_warehouse, pn.id_pn, pn.name_pn')
			->from('dt_sn sn')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
			->where('pn.name_pn', $pn)
			->where('sn.id_stockaccess', '1')
			->get();

			if($pn_exist_another->num_rows() < 1){
				$pn_compatible = $db_huawei->select('comp.pn')
				->from('dm_pn pn')
				->join('dt_compatibility comp', 'comp.id_pn = pn.id_pn')
				->where('pn.name_pn', $pn)
				->get();

				if($pn_compatible->num_rows() < 1){
					$return = array('pool' => [], 'pn' => []);
				} else {
					$pn_compatible = $pn_compatible->result_array();
					$return_comp = [];

					for($i = 0; $i < count($pn_compatible); $i++) {
						$comp_exist = $db_huawei->select('pn.id_pn, pn.name_pn, warehouse.name_warehouse, warehouse.id_warehouse')
						->from('dm_pn pn')
						->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
						->join('dm_dop dop', 'dop.id_warehouse = sn.id_warehouse')
						->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
						->where('pn.name_pn', $pn_compatible[$i]['pn'])
						->where('dop.id_dop', $this->input->post('id_dop'))
						->group_by('warehouse.id_warehouse')
						->get();

						if($comp_exist->num_rows() > 0) {
							$return_comp[$i] = $comp_exist->row();
						}
					}

					$return = array('pool' => $return_comp[0], 'pn' => $return_comp);
				}
			} else {
				$return = array('pool' => $pn_exist_another->result_array(), 'pn' => $pn_exist_another->result_array());
			}
		} else {
			$return = array('pool' => $pn_exist->result_array(), 'pn' => $pn_exist->result_array());
		}

		echo json_encode($return);
	}

	public function request_input_process()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$now = date('Y-m-d H:i:s');

		for($i = 0; $i < count($this->input->post('form_checklist')); $i++){
			$id_pn = $db_huawei->select('id_pn')
			->from('dm_pn')
			->where('name_pn', $this->input->post('form_pn_req')[$i])
			->get();
			$id_pn = $id_pn->row();

			$sla = (intval($this->input->post('form_sla_hour')[$i]) * 60 * 60) + (intval($this->input->post('form_sla_minute')[$i]) * 60);

			$eta = strtotime($this->input->post('time_request')) + $sla;

			$data = 
			array(
				'id_project' => $this->input->post('id_project'),
				'id_projecttype' => $this->input->post('id_projecttype'),
		//		'id_application_type' => $this->input->post('id_application_type'),
				'id_customer' => $this->input->post('id_customer'),
				'id_dop' => $this->input->post('id_dop'),
				'create_time' => $this->input->post('input_create_time'),
				'time_request' => $this->input->post('time_request'),
				'id_requestor' => $this->input->post('id_requestor'),
				'id_callcenter' => $this->session->userdata('login_id'),
				'rmr' => $this->input->post('form_created_rmr')[$i],
				'order' => $this->input->post('form_sr_code')[$i],
				'rma' => $this->input->post('form_rma')[$i],
				'site_id' => $this->input->post('form_site')[$i],
				'id_basecode' => $id_pn->id_pn,
				'id_warehouse' => $this->input->post('form_pool')[$i],
				'id_pn' => $this->input->post('form_pn_del')[$i],
				'sla' => $this->input->post('form_sla_hour')[$i].' Hours '.$this->input->post('form_sla_minute')[$i].' Minutes',
				'id_status_request' => $this->input->post('form_checklist')[$i],
				'eta_received' => date('Y-m-d H:i:s', $eta),
				'remark' => $this->input->post('form_remark')[$i],
				'save_request_time' => $now
			);

			if($db_huawei->insert('dt_request', $data)){
				$return = true;
			} else {
				$return = false;
			}
		}

		echo json_encode($return);
	}

	public function get_all_pn_comp_in_warehouse()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pn = $this->input->post('pn');
		$id_dop = $this->input->post('id_dop');
		$id_project = $this->input->post('id_project');

		// $pn = '02100054';
		// $id_dop = '1';

		$return_all = array();

		$pn_exist_in_warehouse = $db_huawei->select('pn.id_pn, pn.name_pn, warehouse.name_warehouse, warehouse.id_warehouse')
		->from('dt_sn sn')
		->join('dm_dop dop', 'dop.id_warehouse = sn.id_warehouse')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->where('pn.name_pn', $pn)
		->where('dop.id_dop', $id_dop)
		->where('sn.id_stockaccess', '1')
		->where('sn.id_stockstatus', '1')
		->where('sn.id_project', $id_project)
		->group_by('warehouse.id_warehouse')
		->get();
		$pn_exist_in_warehouse = $pn_exist_in_warehouse->result_array();

		if(count($pn_exist_in_warehouse) > 0){
			$return_all['data'][0]['id_pn'] = $pn_exist_in_warehouse[0]['id_pn'];
			$return_all['data'][0]['name_pn'] = $pn_exist_in_warehouse[0]['name_pn'];
			$return_all['id_warehouse'] = $pn_exist_in_warehouse[0]['id_warehouse'];
			$return_all['name_warehouse'] = $pn_exist_in_warehouse[0]['name_warehouse'];
		}

		$pn_compatible_list = $db_huawei->select('comp.pn')
		->from('dm_pn pn')
		->join('dt_compatibility comp', 'comp.id_pn = pn.id_pn')
		->where('pn.name_pn', $pn)
		->get();
		$pn_compatible_list = $pn_compatible_list->result_array();

		for($i = 0; $i < count($pn_compatible_list); $i++) {
			$comp_exist_in_warehouse = $db_huawei->select('pn.id_pn, pn.name_pn')
			->from('dm_pn pn')
			->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
			->join('dm_dop dop', 'dop.id_warehouse = sn.id_warehouse')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
			->where('pn.name_pn', $pn_compatible_list[$i]['pn'])
			->where('dop.id_dop', $id_dop)
			->where('sn.id_stockaccess', '1')
			->where('sn.id_stockstatus', '1')
			->where('sn.id_project', $id_project)
			->get();

			if($comp_exist_in_warehouse->num_rows() > 0) {
				$return_all['data'][$i+1] = $comp_exist_in_warehouse->row();
			}
		}
		echo json_encode($return_all);
	}

	public function get_all_pn_comp_in_all_warehouse()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pn = $this->input->post('pn');

		$return_all = array();

		$pn_exist_in_warehouse = $db_huawei->select('warehouse.name_warehouse, warehouse.id_warehouse')
		->from('dt_sn sn')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->where('pn.name_pn', $pn)
		->where('sn.id_stockaccess', '1')
		->where('sn.id_stockstatus', '1')
		->get();

		$pn_exist_in_warehouse = $pn_exist_in_warehouse->result_array();

		for($i = 0; $i < count($pn_exist_in_warehouse); $i++) {
			$return_all[$i] = $pn_exist_in_warehouse[$i];
		}

		$pn_compatible_list = $db_huawei->select('comp.pn')
		->from('dm_pn pn')
		->join('dt_compatibility comp', 'comp.id_pn = pn.id_pn')
		->where('pn.name_pn', $pn)
		->get();
		$pn_compatible_list = $pn_compatible_list->result_array();

		for($a = 0; $a < count($pn_compatible_list); $a++) {
			$comp_exist_in_warehouse = $db_huawei->select('warehouse.id_warehouse, warehouse.name_warehouse')
			->from('dt_sn sn')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
			->where('pn.name_pn', $pn_compatible_list[$a]['pn'])
			->where('sn.id_stockaccess', '1')
			->where('sn.id_stockstatus', '1')
			->get();

			$comp_exist_in_warehouse = $comp_exist_in_warehouse->result_array();

			for($i = 0; $i < count($comp_exist_in_warehouse); $i++){
				$return_all[count($return_all)] = $comp_exist_in_warehouse[$i];
			}
		}

		$unique = array();

		foreach ($return_all as $value)
		{
			$unique[$value['id_warehouse']] = $value;
		}

		echo json_encode(array_values($unique));
	}

	public function get_pn_comp_by_warehouse()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pn = $this->input->post('pn');
		$id_warehouse = $this->input->post('id_warehouse');

		$return_all = array();

		$pn_exist_in_warehouse = $db_huawei->select('pn.id_pn, pn.name_pn')
		->from('dt_sn sn')
		->join('dm_pn pn', 'pn.id_pn = sn.id_pn')
		->where('pn.name_pn', $pn)
		->where('sn.id_warehouse', $id_warehouse)
		->where('sn.id_stockaccess', '1')
		->where('sn.id_stockstatus', '1')
		->group_by('sn.id_warehouse')
		->get();
		if($pn_exist_in_warehouse->num_rows() > 0) {
			$return_all[0] = $pn_exist_in_warehouse->row();
		}

		$pn_compatible_list = $db_huawei->select('comp.pn')
		->from('dm_pn pn')
		->join('dt_compatibility comp', 'comp.id_pn = pn.id_pn')
		->where('pn.name_pn', $pn)
		->get();
		$pn_compatible_list = $pn_compatible_list->result_array();

		for($i = 0; $i < count($pn_compatible_list); $i++) {
			$comp_exist_in_warehouse = $db_huawei->select('pn.id_pn, pn.name_pn')
			->from('dm_pn pn')
			->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
			->where('pn.name_pn', $pn_compatible_list[$i]['pn'])
			->where('sn.id_warehouse', $id_warehouse)
			->where('sn.id_stockaccess', '1')
			->where('sn.id_stockstatus', '1')
			->get();

			if($comp_exist_in_warehouse->num_rows() > 0) {
				$return_all[count($return_all)] = $comp_exist_in_warehouse->row();
			}
		}
		echo json_encode($return_all);
	}

	public function get_all_request_progress()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 69) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, stat_req.*')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
			// ->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			// ->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_sn', null)
			->where('req.id_status_request !=', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, stat_req.*')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
			// ->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			// ->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.id_sn', null)
			->where_not_in('req.id_status_request', array('3', '4', '5', '1'))
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}

	public function get_all_request_in_delivery()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4 || $this->session->userdata('level') == 69) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_pod !=', null)
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
			->where('req.id_status_request !=', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			// ->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.id_pod !=', null)
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
			->where('req.id_status_request !=', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}

	public function get_all_request_to_pickup()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4 || $this->session->userdata('level') == 69) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.url_photo_mdn !=', null)
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
			->where('req.id_status_request !=', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.url_photo_mdn !=', null)
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
			->where('req.id_status_request !=', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}

	public function get_request_doc_detail()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data = $db_huawei->select('req.*, pod.*, pickup.*, warehouse.*, pn.name_pn AS pn_req, pn2.name_pn AS pn_del')
		->from('dt_request req')
		->join('dm_pn pn', 'pn.id_pn = req.id_basecode')
		->join('dm_pn pn2', 'pn2.id_pn = req.id_pn')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
		->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
		->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
		->where('req.id_request', $this->input->post('id_request'))
		->get();

		echo json_encode($data->result_array());
	}

	public function edit_process_save()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_pn = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $this->input->post('edit_pn_req'))
		->get();
		$id_pn = $id_pn->row();
		$return ='';

		if($this->input->post('edit_receive_time_request')){
			$id_pod = $db_huawei->select('id_pod')
			->from('dt_request')
			->where('id_request', $this->input->post('edit_id_doc'))
			->get();
			$id_pod = $id_pod->row();

			$edit_pod = array(
				'actual_received' => $this->input->post('edit_receive_time_request')
			);
			$db_huawei->where('id_pod', $id_pod->id_pod);
			$db_huawei->update('dt_pod', $edit_pod);
		}
		if($this->input->post('edit_pickup_time_request')){
			$id_pickup = $db_huawei->select('id_pickup')
			->from('dt_request')
			->where('id_request', $this->input->post('edit_id_doc'))
			->get();
			$id_pickup = $id_pickup->row();

			$edit_pickup = array(
				'actual_pickup' => $this->input->post('edit_pickup_time_request')
			);
			$db_huawei->where('id_pickup', $id_pickup->id_pickup);
			$db_huawei->update('dt_pickup', $edit_pickup);
		}

		// $stat_req = '';

		$stat_req = $this->input->post('edit_checklist_real');

		// $c_ria = $db_huawei->select('*')
		// ->from('dt_request')
		// ->where('id_request', $this->input->post('edit_id_doc'))
		// ->get();
		// $c_ria = $c_ria->row();

		// if(!$this->input->post('edit_checklist_real') && $this->input->post('edit_checklist_real') != 0){

		// 	if($c_ria->url_photo_cmn != null){
		// 		$stat_req = '2';
		// 	}else{
		// 		$stat_req = '1';
		// 	}
		// }else{

		// }

		$sla = (intval($this->input->post('edit_sla_hour')) * 60 * 60) + (intval($this->input->post('edit_sla_minute')) * 60);

		$eta = strtotime($this->input->post('edit_time_request')) + $sla;

		$data = array(
			'time_request' => $this->input->post('edit_time_request'),
			'id_basecode' => $id_pn->id_pn,
			'id_warehouse' => $this->input->post('edit_pool'),
			'site_id' => $this->input->post('edit_site'),
			'id_pn' => $this->input->post('edit_pn_del'),
			'sla' => $this->input->post('edit_sla_hour').' Hours '.$this->input->post('edit_sla_minute').' Minutes',
			'id_status_request' => $stat_req,
			'eta_received' => date('Y-m-d H:i:s', $eta),
			'remark' => $this->input->post('edit_remark')
		);

		$db_huawei->where('id_request', $this->input->post('edit_id_doc'));
		
		if($db_huawei->update('dt_request', $data)){
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($this->input->post());
	}

	public function add_sn_by_rmr()
	{
		$sn = $this->input->post('add_sn_rmr');
		$id_request = $this->input->post('add_sn_id_doc');
		$return = '';

		// $sn = '2';
		// $id_request = '1';

		$db_huawei = $this->load->database('huawei', TRUE);
		$check_sn = $db_huawei->select('sn.id_sn')
		->from('dt_sn sn')
		->join('dt_request req', 'req.id_warehouse = sn.id_warehouse')
		->where('sn.sn', $sn)
		->where('req.id_request', $id_request)
		->where('sn.id_stockaccess', '1')
		->where('sn.id_stockstatus', '1')
		->get();

		if($check_sn->num_rows() > 0) {
			$check_sn = $check_sn->row();
			$data = array(
				'id_sn' => $check_sn->id_sn
			);

			$db_huawei->where('id_request', $id_request);

			if($db_huawei->update('dt_request', $data)){
				$upd = array(
					'id_stockaccess' => '3'
				);
				$db_huawei->where('id_sn', $check_sn->id_sn);
				if($db_huawei->update('dt_sn', $upd)){
					$return = true;
				} else {
					$return = false;
				}
			} else {
				$return = false;
			}
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function get_create_mdn_data()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$log_list = $db_huawei->select('*')
		->from('dm_logistic')
		->where('activated', '1')
		->get();

		$log_service_list = $db_huawei->select('*')
		->from('dm_logisticservice')
		->get();

		$rmr_list = $db_huawei->select('*')
		->from('dt_request req')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->where('req.id_sn !=', null)
		->where('req.id_pod', null)
		->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
		->get();

		echo json_encode(array('logistic' => $log_list->result_array(), 'logistic_service' => $log_service_list->result_array(), 'rmr_list' => $rmr_list->result_array()));
	}

	public function create_mdn_process()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$code_warehouse = $db_huawei->select('warehouse.code_warehouse')
		->from('dt_request req')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
		->where('req.id_request', $this->input->post('create_mdn_rmr_list')[0])
		->get();
		$code_warehouse = $code_warehouse->row();


		$doc_no_temp = 'REQ/'.$code_warehouse->code_warehouse.'DOP/'.date("ymd");

		$checkDocCount = $db_huawei->select('*')
		->from('dt_pod')
		->like('pod_no', $doc_no_temp)
		->get();
		$checkDocCount = $checkDocCount->num_rows() + 1;
		$doc_no = 'REQ/'.$code_warehouse->code_warehouse.'DOP/'.date("ymd").'/'.sprintf("%03s", $checkDocCount);

		$data_pod = 
		array(
			'time_created' => date('Y-m-d H:i:s'),
			'pod_no' => $doc_no,
			'id_logistic' => $this->input->post('create_mdn_logistic'),
			'id_logisticservice' => $this->input->post('create_mdn_logistic_service'),
			'awb_no' => $this->input->post('create_mdn_awb'),
			'created_by' => $this->session->userdata('login_id')
		);

		if($db_huawei->insert('dt_pod', $data_pod)){
			$id_pod = $db_huawei->insert_id();
			for($i = 0; $i < count($this->input->post('create_mdn_rmr_list')); $i++){
				$upd_req = array(
					'id_status_request' => '8',
					'id_pod' => $id_pod
				);
				$db_huawei->where('id_request', $this->input->post('create_mdn_rmr_list')[$i]);
				$db_huawei->update('dt_request', $upd_req);

				$id_sn = $db_huawei->select('id_sn')
				->from('dt_request')
				->where('id_request', $this->input->post('create_mdn_rmr_list')[$i])
				->get();
				$id_sn = $id_sn->row();

				$upd_sn = array(
					'id_stockaccess' => '2',
					'id_stockposition' => '5'
				);
				$db_huawei->where('id_sn', $id_sn->id_sn);
				if($db_huawei->update('dt_sn', $upd_sn)){
					$sn_history = array(
						'sn' => $id_sn->id_sn,
						'id_pod' => $id_pod,
						'datetime_history' => date("Y-m-d H:i:s")
					);
					if($db_huawei->insert('dt_sn_history', $sn_history)){
						$return = true;
					} else {
						$return = false;
					}
				}else{
					$return = false;
				}
			}
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function get_pod_list()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('*')
		->from('dt_pod pod')
		->join('dt_request req', 'req.id_pod = pod.id_pod')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->where('pod.consignee', null)
		// ->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('pod.actual_received', null)
		->get();

		echo json_encode($pod_list->result_array());
	}

	public function get_cmn_list_reupload()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('*')
		->from('dt_pickup pu')
		->join('dt_request req', 'req.id_pickup = pu.id_pickup')
		->where('pu.actual_pickup !=', null)
		->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('req.id_status_request !=', '1')
		->group_by('req.id_pickup')
		->get();

		echo json_encode($pod_list->result_array());
	}

	public function get_pod_list_reupload()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('*')
		->from('dt_pod pod')
		->join('dt_request req', 'req.id_pod = pod.id_pod')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->where('pod.consignee !=', null)
		->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
		// ->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('pod.actual_received !=', null)
		->group_by('req.id_pod')
		->get();

		echo json_encode($pod_list->result_array());
	}

	public function get_image_mdn_reupload()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('url_photo_mdn')
		->from('dt_request')
		->where('id_pod', $this->input->post('doc_id'))
		->group_by('id_pod')
		->get();

		echo json_encode($pod_list->row());
	}

	public function get_image_cmn_reupload()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('url_photo_cmn')
		->from('dt_request')
		->where('id_pickup', $this->input->post('doc_id'))
		->group_by('id_pickup')
		->get();

		echo json_encode($pod_list->row());
	}

	public function reupload_mdn_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';
		$config['upload_path']   = realpath(APPPATH . '../public/upload/file_mdn'); 
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$new_name = time().'-'.$_FILES["reupload_mdn"]['name'];
		$config['file_name'] = $new_name;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('reupload_mdn')) {
			$return = array('stat' => false, 'det' => $this->upload->display_errors());
		}else { 
			$uploadedImage = $this->upload->data();

			$source_path = realpath(APPPATH . '../public/upload/file_mdn') .'/'. $uploadedImage['file_name'];
			$target_path = realpath(APPPATH . '../public/upload/file_mdn') .'/'. $uploadedImage['file_name'];
			$config_manip = array(
				'image_library' => 'gd2',
				'source_image' => $source_path,
				'new_image' => $target_path,
				'maintain_ratio' => true,
				'width' => 640,
				'overwrite' => true
			);

			$this->load->library('image_lib', $config_manip);
			if (!$this->image_lib->resize()) {
				$return = array('stat' => false, 'det' => $this->image_lib->display_errors());
			} else {
				$this->image_lib->clear();
				$rmr_list = $db_huawei->select('*')
				->from('dt_request')
				->where('id_pod', $this->input->post('reupload_mdn_doc_id'))
				->get();

				$old_mdn_photo = $db_huawei->select('*')
				->from('dt_request')
				->where('id_pod', $this->input->post('reupload_mdn_doc_id'))
				->group_by('id_pod')
				->get();
				$old_mdn_photo = $old_mdn_photo->row();

				$old_mdn_photo = explode("/", $old_mdn_photo->url_photo_mdn);
				$old_mdn_photo = $old_mdn_photo[4];

				if(is_readable(realpath(APPPATH . '../public/upload/file_mdn').'/'.$old_mdn_photo) && unlink(realpath(APPPATH . '../public/upload/file_mdn').'/'.$old_mdn_photo)){
					$rmr_list = $rmr_list->result_array();

					for($i = 0; $i < count($rmr_list); $i++){
						$upd_rmr = array(
							'url_photo_mdn' => '/public/upload/file_mdn' .'/'. $uploadedImage['file_name']
						);
						$db_huawei->where('id_request', $rmr_list[$i]['id_request']);
						$db_huawei->update('dt_request', $upd_rmr);
					}

					$return = array('stat' => true, 'det' => 'Data successfully saved');
				}else{
					$return = array('stat' => false, 'det' => 'Data cannot be saved');
				}
			}
		}

		echo json_encode($return);
	}

	public function reupload_cmn_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';
		$config['upload_path']   = realpath(APPPATH . '../public/upload/file_cmn'); 
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$new_name = time().'-'.$_FILES["reupload_cmn"]['name'];
		$config['file_name'] = $new_name;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('reupload_cmn')) {
			$return = array('stat' => false, 'det' => $this->upload->display_errors());
		}else { 
			$uploadedImage = $this->upload->data();

			$source_path = realpath(APPPATH . '../public/upload/file_cmn') .'/'. $uploadedImage['file_name'];
			$target_path = realpath(APPPATH . '../public/upload/file_cmn') .'/'. $uploadedImage['file_name'];
			$config_manip = array(
				'image_library' => 'gd2',
				'source_image' => $source_path,
				'new_image' => $target_path,
				'maintain_ratio' => true,
				'width' => 640,
				'overwrite' => true
			);

			$this->load->library('image_lib', $config_manip);
			if (!$this->image_lib->resize()) {
				$return = array('stat' => false, 'det' => $this->image_lib->display_errors());
			} else {
				$this->image_lib->clear();
				$rmr_list = $db_huawei->select('*')
				->from('dt_request')
				->where('id_pickup', $this->input->post('reupload_cmn_doc_id'))
				->get();

				$old_mdn_photo = $db_huawei->select('*')
				->from('dt_request')
				->where('id_pickup', $this->input->post('reupload_cmn_doc_id'))
				->group_by('id_pickup')
				->get();
				$old_mdn_photo = $old_mdn_photo->row();

				$old_mdn_photo = explode("/", $old_mdn_photo->url_photo_cmn);
				$old_mdn_photo = $old_mdn_photo[4];

				if(is_readable(realpath(APPPATH . '../public/upload/file_cmn').'/'.$old_mdn_photo) && unlink(realpath(APPPATH . '../public/upload/file_cmn').'/'.$old_mdn_photo)){
					$rmr_list = $rmr_list->result_array();

					for($i = 0; $i < count($rmr_list); $i++){
						$upd_rmr = array(
							'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name']
						);
						$db_huawei->where('id_request', $rmr_list[$i]['id_request']);
						$db_huawei->update('dt_request', $upd_rmr);
					}

					$return = array('stat' => true, 'det' => 'Data successfully saved');
				}else{
					$return = array('stat' => false, 'det' => 'Data cannot be saved');
				}
			}
		}

		echo json_encode($return);
	}

	public function get_rmr_list_cmn()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$log_list = $db_huawei->select('*')
		->from('dm_logistic')
		->where('activated', '1')
		->get();

		$log_service_list = $db_huawei->select('*')
		->from('dm_logisticservice')
		->get();

		$rmr_list = $db_huawei->select('*')
		->from('dt_request')
		->where('pickup_request_time !=', null)
		->where('id_pickup', null)
		->where('id_warehouse', $this->session->userdata('id_warehouse'))
		->where('id_status_request !=', '1')
		->get();

		echo json_encode(array('logistic' => $log_list->result_array(), 'logistic_service' => $log_service_list->result_array(), 'rmr_list' => $rmr_list->result_array()));
	}

	public function upload_mdn_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';
		$config['upload_path']   = realpath(APPPATH . '../public/upload/file_mdn'); 
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$new_name = time().'-'.$_FILES["upload_mdn_file"]['name'];
		$config['file_name'] = $new_name;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('upload_mdn_file')) {
			$return = array('stat' => false, 'det' => $this->upload->display_errors());
		}else { 
			$uploadedImage = $this->upload->data();

			$source_path = realpath(APPPATH . '../public/upload/file_mdn') .'/'. $uploadedImage['file_name'];
			$target_path = realpath(APPPATH . '../public/upload/file_mdn') .'/'. $uploadedImage['file_name'];
			$config_manip = array(
				'image_library' => 'gd2',
				'source_image' => $source_path,
				'new_image' => $target_path,
				'maintain_ratio' => true,
				'width' => 640,
				'overwrite' => true
			);

			$this->load->library('image_lib', $config_manip);
			if (!$this->image_lib->resize()) {
				$return = array('stat' => false, 'det' => $this->image_lib->display_errors());
			} else {
				$this->image_lib->clear();
				$upd_pod = array(
					'actual_received' => $this->input->post('upload_mdn_date'),
					'consignee' => $this->input->post('upload_mdn_consignee')
				);
				$db_huawei->where('id_pod', $this->input->post('upload_mdn_pod'));
				if($db_huawei->update('dt_pod', $upd_pod)){
					$rmr_list = $db_huawei->select('*')
					->from('dt_request')
					->where('id_pod', $this->input->post('upload_mdn_pod'))
					->get();
					$rmr_list = $rmr_list->result_array();

					$real_pod = $db_huawei->select('actual_received')
					->from('dt_pod')
					->where('id_pod', $this->input->post('upload_mdn_pod'))
					->get();
					$real_pod = $real_pod->row();

					for($i = 0; $i < count($rmr_list); $i++){
						if($real_pod->actual_received <= $rmr_list[$i]['eta_received']){
							$id_status_request = 6;
						}else{
							$id_status_request = 5;
						}
						$upd_rmr = array(
							'url_photo_mdn' => '/public/upload/file_mdn' .'/'. $uploadedImage['file_name'],
							'id_status_request' => $id_status_request
						);
						$db_huawei->where('id_request', $rmr_list[$i]['id_request']);
						if($db_huawei->update('dt_request', $upd_rmr)){
							$upd_sn = array(
								'id_stockposition' => '5',
								'id_stockaccess' => '4'
							);
							$db_huawei->where('id_sn', $rmr_list[$i]['id_sn']);
							if($db_huawei->update('dt_sn', $upd_sn)){
								$sn_history = array(
									'sn' => $rmr_list[$i]['id_sn'],
									'id_pod' => $this->input->post('upload_mdn_pod'),
									'datetime_history' => date("Y-m-d H:i:s")
								);
								if($db_huawei->insert('dt_sn_history', $sn_history)){
									$return = array('stat' => true, 'det' => 'Data successfully updated');
								} else {
									$return = array('stat' => false, 'det' => 'Data cannot be updated');
								}
							} else {
								$return = array('stat' => false, 'det' => 'Data cannot be updated');
							}
						} else {
							$return = array('stat' => false, 'det' => 'Data cannot be updated');
						}
					}
				} else {
					$return = array('stat' => false, 'det' => 'Data cannot be updated');
				}
			}
		}

		echo json_encode($return);
	}

	public function pickup_rmr()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$upd_req = array(
			'pickup_request_time' => $this->input->post('pickup_time')
		);
		$db_huawei->where('id_request', $this->input->post('pickup_id_request'));
		if($db_huawei->update('dt_request', $upd_req)){
			$return = true;
		}else{
			$return = false;
		}

		echo json_encode($return);
	}

	public function create_cmn_process()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$code_warehouse = $db_huawei->select('code_warehouse')
		->from('dm_warehouse')
		->where('id_warehouse', $this->session->userdata('id_warehouse'))
		->get();
		$code_warehouse = $code_warehouse->row();

		$doc_no_temp = 'CMN/DOP'.$code_warehouse->code_warehouse.'/'.date("ymd");

		$checkDocCount = $db_huawei->select('*')
		->from('dt_pickup')
		->like('pickup_no', $doc_no_temp)
		->get();
		$checkDocCount = $checkDocCount->num_rows() + 1;
		$doc_no = 'CMN/DOP'.$code_warehouse->code_warehouse.'/'.date("ymd").'/'.sprintf("%03s", $checkDocCount);

		$data_pod = 
		array(
			'time_created' => date('Y-m-d H:i:s'),
			'pickup_no' => $doc_no,
			'id_logistic' => $this->input->post('create_cmn_logistic'),
			'id_logisticservice' => $this->input->post('create_cmn_logistic_service'),
			'awb_no' => $this->input->post('create_cmn_awb'),
			'created_by' => $this->session->userdata('login_id')
		);

		if($db_huawei->insert('dt_pickup', $data_pod)){
			$id_pod = $db_huawei->insert_id();
			for($i = 0; $i < count($this->input->post('create_cmn_rmr_list')); $i++){
				$upd_req = array(
					'id_pickup' => $id_pod
				);
				$db_huawei->where('id_request', $this->input->post('create_cmn_rmr_list')[$i]);
				if($db_huawei->update('dt_request', $upd_req)){
					$return = true;
				} else {
					$return = false;
				}
			}
		} else {
			$return = false;
		}

		echo json_encode($return);
	}

	public function get_cmn_list()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('*')
		->from('dt_pickup pu')
		->join('dt_request req', 'req.id_pickup = pu.id_pickup')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('pu.actual_pickup', null)
		->where('req.id_status_request !=', '1')
		->get();

		echo json_encode($pod_list->result_array());
	}

	public function get_cmn_list_ria(){
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('*')
		->from('dt_pickup pu')
		->join('dt_request req', 'req.id_pickup = pu.id_pickup')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('pu.actual_pickup', null)
		->where('req.id_status_request', '1')
		->get();

		echo json_encode($pod_list->result_array());
	}

	public function get_rmr_list_by_pickup()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('*')
		->from('dt_request')
		->where('id_pickup', $this->input->post('id_pickup'))
		->get();

		$status = $db_huawei->select('*')
		->from('dt_status_pickup')
		->where('id_statuspickup !=', '5')
		->where('id_statuspickup !=', '6')
		->get();

		echo json_encode(array('rmr_list' => $pod_list->result_array(), 'status' => $status->result_array()));
	}

	public function upload_cmn_check_pn()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';
		$rmr_list = $this->input->post('rmr_list');
		$pn_list = $this->input->post('pn_pickup');
		$sn_list = $this->input->post('sn_pickup');
		$loc_bin_list = $this->input->post('loc_bin');
		$status_list = $this->input->post('pickup_status');

		$return = '';
		$count1 = 0;

		for($a = 0; $a < count($rmr_list); $a++){
			if($status_list[$a] == '2'){
				$count1++;
			}else{
				$req_pn = $db_huawei->select('pn.id_pn')
				->from('dt_request req')
				->join('dm_pn pn', 'pn.id_pn = req.id_pn')
				->where('req.id_request', $rmr_list[$a])
				->where('pn.name_pn', $pn_list[$a])
				->get();

				if($req_pn->num_rows() < 1){
					$id_pn_req = $db_huawei->select('*')
					->from('dt_request')
					->where('id_request', $rmr_list[$a])
					->get();
					$id_pn_req = $id_pn_req->row();

					$req_pn_comp = $db_huawei->select('*')
					->from('dt_compatibility')
					->where('id_pn', $id_pn_req->id_pn)
					->where('pn', $pn_list[$a])
					->get();
					if($req_pn_comp->num_rows() < 1){
						break;
					}
				}
				$count1++;
			}
		};

		if($count1 == count($rmr_list)){
			$return = array('stat' => true, 'det' => 'All Good');
		}else{
			$return = array('stat' => false, 'det' => $pn_list[$a].' cannot be use!');
		}

		echo json_encode($return);
		// echo json_encode($this->input->post());
		// echo json_encode($count1);
	}

	public function upload_cmn_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';
		$rmr_list = $this->input->post('rmr_list');
		$pn_list = $this->input->post('pn_pickup');
		$sn_list = $this->input->post('sn_pickup');
		$loc_bin_list = $this->input->post('loc_bin');
		$status_list = $this->input->post('pickup_status');

		$return = '';

		$config['upload_path']   = realpath(APPPATH . '../public/upload/file_cmn'); 
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$new_name = time().'-'.$_FILES["upload_cmn_file"]['name'];
		$config['file_name'] = $new_name;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('upload_cmn_file')) {
			$return = array('stat' => false, 'det' => $this->upload->display_errors());
		}else { 
			$uploadedImage = $this->upload->data();

			$source_path = realpath(APPPATH . '../public/upload/file_cmn') .'/'. $uploadedImage['file_name'];
			$target_path = realpath(APPPATH . '../public/upload/file_cmn') .'/'. $uploadedImage['file_name'];
			$config_manip = array(
				'image_library' => 'gd2',
				'source_image' => $source_path,
				'new_image' => $target_path,
				'maintain_ratio' => true,
				'width' => 640,
				'overwrite' => true
			);

			$this->load->library('image_lib', $config_manip);
			if (!$this->image_lib->resize()) {
				$return = array('stat' => false, 'det' => $this->image_lib->display_errors());
			} else {
				$this->image_lib->clear();
				$upd_pickup = array(
					'actual_pickup' => $this->input->post('upload_cmn_date')
				);

				$db_huawei->where('id_pickup', $this->input->post('upload_cmn_doc_id'));
				if($db_huawei->update('dt_pickup', $upd_pickup)){
					for($b = 0; $b < count($rmr_list); $b++){
						$id_stockstatus = $db_huawei->select('id_stockstatus')
						->from('dt_status_pickup')
						->where('id_statuspickup', $status_list[$b])
						->get();
						$id_stockstatus = $id_stockstatus->row();

						$req_det_id = $db_huawei->select('*')
						->from('dt_request')
						->where('id_request', $rmr_list[$b])
						->get();
						$req_det_id = $req_det_id->row();

						if($status_list[$b] == '2'){
							$upd_sn_exist = array(
								'id_warehouse' => $this->session->userdata('id_warehouse'),
								'locbin' => $loc_bin_list[$b],
								'id_stockstatus' => $id_stockstatus->id_stockstatus,
								'id_stockposition' => '1',
								'id_stockaccess' => '1',
								'status_cek' => '0',
								'id_project' => $req_det_id->id_project,
								'id_pn' => $req_det_id->id_pn
							);

							$db_huawei->where('id_sn', $req_det_id->id_sn);
							if($db_huawei->update('dt_sn', $upd_sn_exist)){
								$insert_sn_hst = 
								array(
									'sn' => $req_det_id->id_sn,
									'datetime_history' => date('Y-m-d H:i:s'),
									'id_pickup' => $this->input->post('upload_cmn_doc_id')
								);
								if($db_huawei->insert('dt_sn_history', $insert_sn_hst)){
									$upd_rmr = array(
										'id_pn_pickup' => $req_det_id->id_pn,
										'aging_stop' => $this->input->post('upload_cmn_date'),
										'id_statuspickup' => $status_list[$b],
										'id_sn_pickup' => $req_det_id->id_sn,
										'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name'],
										'remark_pickup' => $this->input->post('remark_pickup')[$b] ? $this->input->post('remark_pickup')[$b] : ''
									);

									$db_huawei->where('id_request', $rmr_list[$b]);
									if($db_huawei->update('dt_request', $upd_rmr)){
										$return = array('stat' => true, 'det' => 'Data successfully saved');
									}else{
										$return = array('stat' => false, 'det' => 'Data cannot be saved');
									}
								}
							}
						}else{
							$pn_pickup_id = $db_huawei->select('id_pn')
							->from('dm_pn')
							->where('name_pn', $pn_list[$b])
							->get();
							$pn_pickup_id = $pn_pickup_id->row();

							$check_sn_exist = $db_huawei->select('id_sn')
							->from('dt_sn')
							->where('sn', $sn_list[$b])
							->get();
							if($check_sn_exist->num_rows() > 0){
								$check_sn_exist = $check_sn_exist->row();
								$upd_sn_exist = array(
									'id_warehouse' => $this->session->userdata('id_warehouse'),
									'locbin' => $loc_bin_list[$b],
									'id_stockstatus' => $id_stockstatus->id_stockstatus,
									'id_stockposition' => '1',
									'id_stockaccess' => '1',
									'status_cek' => '0',
									'id_project' => $req_det_id->id_project,
									'id_pn' => $pn_pickup_id->id_pn
								);

								$db_huawei->where('id_sn', $check_sn_exist->id_sn);
								if($db_huawei->update('dt_sn', $upd_sn_exist)){
									$insert_sn_hst = 
									array(
										'sn' => $check_sn_exist->id_sn,
										'datetime_history' => date('Y-m-d H:i:s'),
										'id_pickup' => $this->input->post('upload_cmn_doc_id')
									);
									if($db_huawei->insert('dt_sn_history', $insert_sn_hst)){
										$upd_rmr = array(
											'id_pn_pickup' => $pn_pickup_id->id_pn,
											'aging_stop' => $this->input->post('upload_cmn_date'),
											'id_statuspickup' => $status_list[$b],
											'id_sn_pickup' => $check_sn_exist->id_sn,
											'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name'],
											'remark_pickup' => $this->input->post('remark_pickup')[$b] ? $this->input->post('remark_pickup')[$b] : ''
										);

										$db_huawei->where('id_request', $rmr_list[$b]);
										if($db_huawei->update('dt_request', $upd_rmr)){
											$return = array('stat' => true, 'det' => 'Data successfully saved');
										}else{
											$return = array('stat' => false, 'det' => 'Data cannot be saved');
										}
									}
								}
							}else{
								if($pn_list[$b] == $sn_list[$b]){
									$format = 'NS'.$pn_list[$b];
									$sn_num = $db_huawei->select('*')
									->from('no_sn_counter')
									->get();
									$sn_num = $sn_num->row();
									$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

									$upd_count = array(
										'number' => $sn_num->number+1
									);

									$db_huawei->where('id_sn_counter', '1');
									if($db_huawei->update('no_sn_counter', $upd_count)){
										$insert_sn = 
										array(
											'sn' => $generated_sn,
											'id_warehouse' => $this->session->userdata('id_warehouse'),
											'locbin' => $loc_bin_list[$b],
											'id_stockstatus' => $id_stockstatus->id_stockstatus,
											'id_stockposition' => '1',
											'id_stockaccess' => '1',
											'status_cek' => '0',
											'id_project' => $req_det_id->id_project,
											'id_pn' => $pn_pickup_id->id_pn,
											'stocktake_time' => date('Y-m-d H:i:s'),
											'stocktaker' => $this->session->userdata('login_id'),
											'remark' => ''
										);
										if($db_huawei->insert('dt_sn', $insert_sn)){
											$id_sn_inserted = $db_huawei->insert_id();

											$insert_sn_hst = 
											array(
												'sn' => $id_sn_inserted,
												'datetime_history' => date('Y-m-d H:i:s'),
												'id_pickup' => $this->input->post('upload_cmn_doc_id')
											);

											if($db_huawei->insert('dt_sn_history', $insert_sn_hst)){
												$upd_rmr = array(
													'id_pn_pickup' => $pn_pickup_id->id_pn,
													'aging_stop' => $this->input->post('upload_cmn_date'),
													'id_statuspickup' => $status_list[$b],
													'id_sn_pickup' => $id_sn_inserted,
													'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name'],
													'remark_pickup' => $this->input->post('remark_pickup')[$b] ? $this->input->post('remark_pickup')[$b] : ''
												);

												$db_huawei->where('id_request', $rmr_list[$b]);
												if($db_huawei->update('dt_request', $upd_rmr)){
													$return = array('stat' => true, 'det' => 'Data successfully saved');
												}else{
													$return = array('stat' => false, 'det' => 'Data cannot be saved');
												}
											}
										}
									}
								} else {
									$insert_sn = 
									array(
										'sn' => $sn_list[$b],
										'id_warehouse' => $this->session->userdata('id_warehouse'),
										'locbin' => $loc_bin_list[$b],
										'id_stockstatus' => $id_stockstatus->id_stockstatus,
										'id_stockposition' => '1',
										'id_stockaccess' => '1',
										'status_cek' => '0',
										'id_project' => $req_det_id->id_project,
										'id_pn' => $pn_pickup_id->id_pn,
										'stocktake_time' => date('Y-m-d H:i:s'),
										'stocktaker' => $this->session->userdata('login_id'),
										'remark' => ''
									);
									if($db_huawei->insert('dt_sn', $insert_sn)){
										$id_sn_inserted = $db_huawei->insert_id();

										$insert_sn_hst = 
										array(
											'sn' => $id_sn_inserted,
											'datetime_history' => date('Y-m-d H:i:s'),
											'id_pickup' => $this->input->post('upload_cmn_doc_id')
										);

										if($db_huawei->insert('dt_sn_history', $insert_sn_hst)){
											$upd_rmr = array(
												'id_pn_pickup' => $pn_pickup_id->id_pn,
												'aging_stop' => $this->input->post('upload_cmn_date'),
												'id_statuspickup' => $status_list[$b],
												'id_sn_pickup' => $id_sn_inserted,
												'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name'],
												'remark_pickup' => $this->input->post('remark_pickup')[$b] ? $this->input->post('remark_pickup')[$b] : ''
											);

											$db_huawei->where('id_request', $rmr_list[$b]);
											if($db_huawei->update('dt_request', $upd_rmr)){
												$return = array('stat' => true, 'det' => 'Data successfully saved');
											}else{
												$return = array('stat' => false, 'det' => 'Data cannot be saved');
											}
										}
									}
								}
							}
						}
					}
				} else {
					$return = array('stat' => false, 'det' => 'Data cannot be saved');
				}
			}
		}

		echo json_encode($return);
	}

	public function close_pickup()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';

		$upd_rmr = array(
			'id_statuspickup' => $this->input->post('close_pickup_status')
		);

		$db_huawei->where('id_request', $this->input->post('close_pickup_id_request'));
		if($db_huawei->update('dt_request', $upd_rmr)){
			$return = true;
		}else{
			$return = false;
		}

		echo json_encode($return);
	}

	public function icare_inbound_list()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, statReq.*, pickup.*, statPick.*, user.*')
		->from('dt_request req')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
		->join('dm_pn pn', 'pn.id_pn = req.id_pn_pickup')
		->join('dt_sn sn', 'sn.id_sn = req.id_sn_pickup')
		->join('dm_status_request statReq', 'statReq.id_status_request = req.id_status_request')
		->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup')
		->join('dt_status_pickup statPick', 'statPick.id_statuspickup = req.id_statuspickup')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter')
		// ->where('req.url_photo_mdn !=', null)
		->where('req.url_photo_cmn !=', null)
		->where('req.inbound_order', null)
		->get();

		echo json_encode($list->result_array());
	}

	public function icare_outbound_list()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, statReq.*, pod.*, user.*')
		->from('dt_request req')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
		->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
		->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
		->join('dm_status_request statReq', 'statReq.id_status_request = req.id_status_request')
		->join('dt_pod pod', 'pod.id_pod = req.id_pod')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter')
		->where('req.url_photo_mdn !=', null)
		// ->where('req.url_photo_cmn', null)
		->where('req.outbound_order', null)
		->get();

		echo json_encode($list->result_array());
	}

	public function inbound_order_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';

		$upd_rmr = array(
			'inbound_order' => $this->input->post('inbound_order')
		);

		$db_huawei->where('id_request', $this->input->post('id_request_inbound'));

		if($db_huawei->update('dt_request', $upd_rmr)){
			$return=true;
		}else{
			$return=false;
		}

		echo json_encode($return);
	}

	public function outbound_order_process()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';

		$upd_rmr = array(
			'outbound_order' => $this->input->post('outbound_order')
		);

		$db_huawei->where('id_request', $this->input->post('id_request_outbound'));

		if($db_huawei->update('dt_request', $upd_rmr)){
			$return=true;
		}else{
			$return=false;
		}

		echo json_encode($return);
	}

	public function get_rmr_list_by_rmr()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$customer_list = $db_huawei->select('*')
		->from('dt_request req')
		->join('dm_customer customer', 'customer.id_customer = req.id_customer')
		->join('dm_pn pn', 'pn.id_pn = req.id_pn')
		->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
		->join ('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
		->like('req.rmr', $this->input->post('rmr'))
		->get();

		echo json_encode($customer_list->result_array());
	}

	public function request_input_process_ria()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$now = date('Y-m-d H:i:s');

		for($i = 0; $i < count($this->input->post('form_pn_req')); $i++){
			$id_pn = $db_huawei->select('id_pn')
			->from('dm_pn')
			->where('name_pn', $this->input->post('form_pn_req')[$i])
			->get();
			$id_pn = $id_pn->row();

			// $sla = (intval($this->input->post('form_sla_hour')[$i]) * 60 * 60) + (intval($this->input->post('form_sla_minute')[$i]) * 60);

			// $eta = strtotime($this->input->post('time_request')) + $sla;

			$data = 
			array(
				'id_project' => $this->input->post('id_project'),
				'id_projecttype' => $this->input->post('id_projecttype'),
				'id_customer' => $this->input->post('id_customer'),
				'id_dop' => $this->input->post('id_dop'),
				'create_time' => $this->input->post('input_create_time'),
				'time_request' => $this->input->post('time_request'),
				'id_requestor' => $this->input->post('id_requestor'),
				'id_callcenter' => $this->session->userdata('login_id'),
				'rmr' => $this->input->post('form_created_rmr')[$i],
				'order' => $this->input->post('form_sr_code')[$i],
				'rma' => $this->input->post('form_rma')[$i],
				'site_id' => $this->input->post('form_site')[$i],
				'id_basecode' => $id_pn->id_pn,
				// 'sla' => $this->input->post('form_sla_hour')[$i].' Hours '.$this->input->post('form_sla_minute')[$i].' Minutes',
				'id_status_request' => '1',
				// 'eta_received' => date('Y-m-d H:i:s', $eta),
				// 'remark_pickup' => $this->input->post('form_remark')[$i],
				'save_request_time' => $now
			);

			if($db_huawei->insert('dt_request', $data)){
				$return = true;
			} else {
				$return = false;
			}
		}

		echo json_encode($return);
	}

	public function create_RMR_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$ret_rmr = '';

		$check_pn = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $this->input->post('pn_req'))
		->get();

		if($check_pn->num_rows() > 0){
			$rmr = $db_huawei->select('customer.code_customer, warehouse.code_warehouse')
			->from('dm_customer customer')
			->join('dm_dop dop', 'dop.id_customer = customer.id_customer')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
			->where('customer.id_customer', $this->input->post('id_customer'))
			->where('dop.id_dop', $this->input->post('id_dop'))
			->get();
			$rmr = $rmr->row();
			$ret_rmr = $rmr->code_customer.$rmr->code_warehouse.'-'.date('ymd').'-'.$this->input->post('rma');
		}else{
			$ret_rmr = false;
		}

		echo json_encode($ret_rmr);
	}

	public function get_all_request_to_pickup_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4 || $this->session->userdata('level') == 69) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
			->where('req.id_status_request', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.url_photo_cmn', null)
			->where('req.id_statuspickup', null)
			->where('req.id_status_request', '1')
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}

	public function get_rmr_list_cmn_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$log_list = $db_huawei->select('*')
		->from('dm_logistic')
		->where('activated', '1')
		->get();

		$log_service_list = $db_huawei->select('*')
		->from('dm_logisticservice')
		->get();

		$rmr_list = $db_huawei->select('*')
		->from('dt_request req')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('req.pickup_request_time !=', null)
		->where('req.id_pickup', null)
		->where('req.id_status_request', '1')
		->get();

		echo json_encode(array('logistic' => $log_list->result_array(), 'logistic_service' => $log_service_list->result_array(), 'rmr_list' => $rmr_list->result_array()));
	}

	public function get_all_request_in_delivery_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4 || $this->session->userdata('level') == 69) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_pod !=', null)
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
			->where('req.id_pickup !=', null)
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, sn.sn AS name_sn, pod.consignee')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			// ->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.id_pod !=', null)
			->where('req.id_sn !=', null)
			->where('req.url_photo_mdn', null)
			->where('req.id_pickup !=', null)
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}

	public function get_all_request_progress_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		if($this->session->userdata('level') == 3 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 69) {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, stat_req.*')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
			->join('dm_pn pn', 'pn.id_pn = req.id_basecode')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
			// ->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			// ->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('req.id_sn', null)
			// ->where('req.id_status_request', '2')
			->order_by('req.time_request', 'DESC')
			->get();
		} else {
			$list = $db_huawei->select('req.*, dop.name_dop, warehouse.name_warehouse, pn.name_pn, stat_req.*')
			->from('dt_request req')
			->join('dm_dop dop', 'dop.id_dop = req.id_dop')
			->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
			->join('dm_pn pn', 'pn.id_pn = req.id_pn')
			->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request', 'left')
			// ->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
			// ->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
			->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
			->where('req.id_sn', null)
			->where_not_in('req.id_status_request', array('3', '4', '5'))
			// ->where('req.id_status_request', '2')
			->order_by('req.time_request', 'DESC')
			->get();
		}
		echo json_encode($list->result_array());
	}

	public function get_request_doc_detail_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$data = $db_huawei->select('req.*, pod.*, pickup.*, warehouse.*, pn.name_pn AS pn_req, pn2.name_pn AS pn_del')
		->from('dt_request req')
		->join('dm_pn pn', 'pn.id_pn = req.id_basecode')
		->join('dm_pn pn2', 'pn2.id_pn = req.id_pn', 'left')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
		->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
		->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
		->where('req.id_request', $this->input->post('id_request'))
		->get();

		echo json_encode($data->result_array());
	}

	public function upload_cmn_process_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';
		$rmr_list = $this->input->post('rmr_list');
		$pn_list = $this->input->post('pn_pickup');
		$sn_list = $this->input->post('sn_pickup');
		$loc_bin_list = $this->input->post('loc_bin');

		$return = '';

		$config['upload_path']   = realpath(APPPATH . '../public/upload/file_cmn'); 
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$new_name = time().'-'.$_FILES["upload_cmn_file"]['name'];
		$config['file_name'] = $new_name;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('upload_cmn_file')) {
			$return = array('stat' => false, 'det' => $this->upload->display_errors());
		}else { 
			$uploadedImage = $this->upload->data();

			$source_path = realpath(APPPATH . '../public/upload/file_cmn') .'/'. $uploadedImage['file_name'];
			$target_path = realpath(APPPATH . '../public/upload/file_cmn') .'/'. $uploadedImage['file_name'];
			$config_manip = array(
				'image_library' => 'gd2',
				'source_image' => $source_path,
				'new_image' => $target_path,
				'maintain_ratio' => true,
				'width' => 640,
				'overwrite' => true
			);

			$this->load->library('image_lib', $config_manip);
			if (!$this->image_lib->resize()) {
				$return = array('stat' => false, 'det' => $this->image_lib->display_errors());
			} else {
				$this->image_lib->clear();
				$upd_pickup = array(
					'actual_pickup' => $this->input->post('upload_cmn_date')
				);

				$db_huawei->where('id_pickup', $this->input->post('upload_cmn_doc_id'));
				if($db_huawei->update('dt_pickup', $upd_pickup)){
					for($b = 0; $b < count($rmr_list); $b++){
						$id_stockstatus = $db_huawei->select('id_stockstatus')
						->from('dt_status_pickup')
						->where('id_statuspickup', $this->input->post('checklist_pickup')[$b])
						->get();
						$id_stockstatus = $id_stockstatus->row();

						$req_det_id = $db_huawei->select('*')
						->from('dt_request')
						->where('id_request', $rmr_list[$b])
						->get();
						$req_det_id = $req_det_id->row();

						$pn_pickup_id = $db_huawei->select('id_pn')
						->from('dm_pn')
						->where('name_pn', $pn_list[$b])
						->get();
						$pn_pickup_id = $pn_pickup_id->row();

						$check_sn_exist = $db_huawei->select('id_sn')
						->from('dt_sn')
						->where('sn', $sn_list[$b])
						->get();

						if($check_sn_exist->num_rows() > 0){
							$check_sn_exist = $check_sn_exist->row();
							$upd_sn_exist = array(
								'id_warehouse' => $this->session->userdata('id_warehouse'),
								'locbin' => $loc_bin_list[$b],
								'id_stockstatus' => $id_stockstatus->id_stockstatus,
								'id_stockposition' => '1',
								'id_stockaccess' => '1',
								'status_cek' => '0',
								'id_project' => $req_det_id->id_project,
								'id_pn' => $pn_pickup_id->id_pn
							);

							$db_huawei->where('id_sn', $check_sn_exist->id_sn);
							if($db_huawei->update('dt_sn', $upd_sn_exist)){
								$insert_sn_hst = 
								array(
									'sn' => $check_sn_exist->id_sn,
									'datetime_history' => date('Y-m-d H:i:s'),
									'id_pickup' => $this->input->post('upload_cmn_doc_id')
								);
								if($db_huawei->insert('dt_sn_history', $insert_sn_hst)){
									$upd_rmr = array(
										'id_pn_pickup' => $pn_pickup_id->id_pn,
										'aging_stop' => $this->input->post('upload_cmn_date'),
										'id_statuspickup' => '1',
										'id_sn_pickup' => $check_sn_exist->id_sn,
										'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name'],
										'remark_pickup' => $this->input->post('remark_pickup')[$b] ? $this->input->post('remark_pickup')[$b] : ''
									);

									$db_huawei->where('id_request', $rmr_list[$b]);
									if($db_huawei->update('dt_request', $upd_rmr)){
										$return = array('stat' => true, 'det' => 'Data successfully saved');
									}else{
										$return = array('stat' => false, 'det' => 'Data cannot be saved');
									}
								}
							}
						}else{
							if($pn_list[$b] == $sn_list[$b]){
								$format = 'NS'.$pn_list[$b];
								$sn_num = $db_huawei->select('*')
								->from('no_sn_counter')
								->get();
								$sn_num = $sn_num->row();
								$generated_sn = $format.sprintf("%0".intval(20-strlen($format))."s", $sn_num->number+1);

								$upd_count = array(
									'number' => $sn_num->number+1
								);

								$db_huawei->where('id_sn_counter', '1');
								if($db_huawei->update('no_sn_counter', $upd_count)){
									$insert_sn = 
									array(
										'sn' => $generated_sn,
										'id_warehouse' => $this->session->userdata('id_warehouse'),
										'locbin' => $loc_bin_list[$b],
										'id_stockstatus' => $id_stockstatus->id_stockstatus,
										'id_stockposition' => '1',
										'id_stockaccess' => '1',
										'status_cek' => '0',
										'id_project' => $req_det_id->id_project,
										'id_pn' => $pn_pickup_id->id_pn,
										'stocktake_time' => date('Y-m-d H:i:s'),
										'stocktaker' => $this->session->userdata('login_id'),
										'remark' => ''
									);
									if($db_huawei->insert('dt_sn', $insert_sn)){
										$id_sn_inserted = $db_huawei->insert_id();

										$insert_sn_hst = 
										array(
											'sn' => $id_sn_inserted,
											'datetime_history' => date('Y-m-d H:i:s'),
											'id_pickup' => $this->input->post('upload_cmn_doc_id')
										);

										if($db_huawei->insert('dt_sn_history', $insert_sn_hst)){
											$upd_rmr = array(
												'id_pn_pickup' => $pn_pickup_id->id_pn,
												'aging_stop' => $this->input->post('upload_cmn_date'),
												'id_statuspickup' => '1',
												'id_sn_pickup' => $id_sn_inserted,
												'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name'],
												'remark_pickup' => $this->input->post('remark_pickup')[$b] ? $this->input->post('remark_pickup')[$b] : ''
											);

											$db_huawei->where('id_request', $rmr_list[$b]);
											if($db_huawei->update('dt_request', $upd_rmr)){
												$return = array('stat' => true, 'det' => 'Data successfully saved');
											}else{
												$return = array('stat' => false, 'det' => 'Data cannot be saved');
											}
										}
									}
								}
							} else {
								$insert_sn = 
								array(
									'sn' => $sn_list[$b],
									'id_warehouse' => $this->session->userdata('id_warehouse'),
									'locbin' => $loc_bin_list[$b],
									'id_stockstatus' => $id_stockstatus->id_stockstatus,
									'id_stockposition' => '1',
									'id_stockaccess' => '1',
									'status_cek' => '0',
									'id_project' => $req_det_id->id_project,
									'id_pn' => $pn_pickup_id->id_pn,
									'stocktake_time' => date('Y-m-d H:i:s'),
									'stocktaker' => $this->session->userdata('login_id'),
									'remark' => ''
								);
								if($db_huawei->insert('dt_sn', $insert_sn)){
									$id_sn_inserted = $db_huawei->insert_id();

									$insert_sn_hst = 
									array(
										'sn' => $id_sn_inserted,
										'datetime_history' => date('Y-m-d H:i:s'),
										'id_pickup' => $this->input->post('upload_cmn_doc_id')
									);

									if($db_huawei->insert('dt_sn_history', $insert_sn_hst)){
										$upd_rmr = array(
											'id_pn_pickup' => $pn_pickup_id->id_pn,
											'aging_stop' => $this->input->post('upload_cmn_date'),
											'id_statuspickup' => '1',
											'id_sn_pickup' => $id_sn_inserted,
											'url_photo_cmn' => '/public/upload/file_cmn' .'/'. $uploadedImage['file_name'],
											'remark_pickup' => $this->input->post('remark_pickup')[$b] ? $this->input->post('remark_pickup')[$b] : ''
										);

										$db_huawei->where('id_request', $rmr_list[$b]);
										if($db_huawei->update('dt_request', $upd_rmr)){
											$return = array('stat' => true, 'det' => 'Data successfully saved');
										}else{
											$return = array('stat' => false, 'det' => 'Data cannot be saved');
										}
									}
								}
							}
						}
					}
				} else {
					$return = array('stat' => false, 'det' => 'Data cannot be saved');
				}
			}
		}

		echo json_encode($return);
	}

	public function upload_cmn_check_pn_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$return = '';
		$rmr_list = $this->input->post('rmr_list');
		$pn_list = $this->input->post('pn_pickup');
		$sn_list = $this->input->post('sn_pickup');
		$loc_bin_list = $this->input->post('loc_bin');
		$status_list = $this->input->post('pickup_status');

		$return = '';
		$count1 = 0;

		for($a = 0; $a < count($rmr_list); $a++){
			if($status_list[$a] == '2'){
				$count1++;
			}else{
				$req_pn = $db_huawei->select('pn.id_pn')
				->from('dt_request req')
				->join('dm_pn pn', 'pn.id_pn = req.id_basecode')
				->where('req.id_request', $rmr_list[$a])
				->where('pn.name_pn', $pn_list[$a])
				->get();

				if($req_pn->num_rows() < 1){
					$id_pn_req = $db_huawei->select('*')
					->from('dt_request')
					->where('id_request', $rmr_list[$a])
					->get();
					$id_pn_req = $id_pn_req->row();

					$req_pn_comp = $db_huawei->select('*')
					->from('dt_compatibility')
					->where('id_pn', $id_pn_req->id_basecode)
					->where('pn', $pn_list[$a])
					->get();
					if($req_pn_comp->num_rows() < 1){
						break;
					}
				}
				$count1++;
			}
		};

		if($count1 == count($rmr_list)){
			$return = array('stat' => true, 'det' => 'All Good');
		}else{
			$return = array('stat' => false, 'det' => $pn_list[$a].' cannot be use!');
		}

		echo json_encode($return);
	}

	public function get_cmn_list_reupload_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$pod_list = $db_huawei->select('*')
		->from('dt_pickup pu')
		->join('dt_request req', 'req.id_pickup = pu.id_pickup')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->where('pu.actual_pickup !=', null)
		->where('dop.id_warehouse', $this->session->userdata('id_warehouse'))
		->where('req.id_status_request', '1')
		->get();

		echo json_encode($pod_list->result_array());
	}

	public function edit_process_save_ria()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$id_pn = $db_huawei->select('*')
		->from('dm_pn')
		->where('name_pn', $this->input->post('edit_pn_req'))
		->get();
		$id_pn = $id_pn->row();
		$return ='';

		if($this->input->post('edit_receive_time_request')){
			$id_pod = $db_huawei->select('id_pod')
			->from('dt_request')
			->where('id_request', $this->input->post('edit_id_doc'))
			->get();
			$id_pod = $id_pod->row();

			$edit_pod = array(
				'actual_received' => $this->input->post('edit_receive_time_request')
			);
			$db_huawei->where('id_pod', $id_pod->id_pod);
			$db_huawei->update('dt_pod', $edit_pod);
		}

		if($this->input->post('edit_pickup_time_request')){
			$id_pickup = $db_huawei->select('id_pickup')
			->from('dt_request')
			->where('id_request', $this->input->post('edit_id_doc'))
			->get();
			$id_pickup = $id_pickup->row();

			$edit_pickup = array(
				'actual_pickup' => $this->input->post('edit_pickup_time_request')
			);
			$db_huawei->where('id_pickup', $id_pickup->id_pickup);
			$db_huawei->update('dt_pickup', $edit_pickup);
		}

		$stat_req = '1';

		$c_ria = $db_huawei->select('*')
		->from('dt_request')
		->where('id_request', $this->input->post('edit_id_doc'))
		->get();
		$c_ria = $c_ria->row();

		if($c_ria->url_photo_cmn != null){
			$stat_req = '2';
		}else{
			$stat_req = '1';
		}

		$sla = (intval($this->input->post('edit_sla_hour')) * 60 * 60) + (intval($this->input->post('edit_sla_minute')) * 60);

		$eta = strtotime($this->input->post('edit_time_request')) + $sla;

		$data = array(
			'time_request' => $this->input->post('edit_time_request'),
			'site_id' => $this->input->post('edit_site'),
			'id_basecode' => $id_pn->id_pn,
			'id_warehouse' => $this->input->post('edit_pool'),
			'id_pn' => $this->input->post('edit_pn_del'),
			'sla' => $this->input->post('edit_sla_hour').' Hours '.$this->input->post('edit_sla_minute').' Minutes',
			'id_status_request' => $stat_req,
			'eta_received' => date('Y-m-d H:i:s', $eta),
			'remark_pickup' => $this->input->post('edit_remark')
		);

		$db_huawei->where('id_request', $this->input->post('edit_id_doc'));
		
		if($db_huawei->update('dt_request', $data)){
			$return = true;
		} else {
			$return = false;
		}

		echo json_encode($this->input->post());
	}

	public function create_mdn_process_ria()
	{
		$return = '';
		$db_huawei = $this->load->database('huawei', TRUE);
		$code_warehouse = $db_huawei->select('warehouse.code_warehouse')
		->from('dt_request req')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = dop.id_warehouse')
		->where('req.id_request', $this->input->post('create_mdn_rmr_list')[0])
		->get();
		$code_warehouse = $code_warehouse->row();

		$doc_no_temp = 'REQ/'.$code_warehouse->code_warehouse.'DOP/'.date("ymd");

		$checkDocCount = $db_huawei->select('*')
		->from('dt_pod')
		->like('pod_no', $doc_no_temp)
		->get();
		$checkDocCount = $checkDocCount->num_rows() + 1;
		$doc_no = 'REQ/'.$code_warehouse->code_warehouse.'DOP/'.date("ymd").'/'.sprintf("%03s", $checkDocCount);

		$data_pod = 
		array(
			'time_created' => date('Y-m-d H:i:s'),
			'pod_no' => $doc_no,
			'id_logistic' => $this->input->post('create_mdn_logistic'),
			'id_logisticservice' => $this->input->post('create_mdn_logistic_service'),
			'awb_no' => $this->input->post('create_mdn_awb'),
			'created_by' => $this->session->userdata('login_id')
		);

		if($db_huawei->insert('dt_pod', $data_pod)){
			$id_pod = $db_huawei->select('*')
			->from('dt_pod')
			->where('id_pod', $db_huawei->insert_id())
			->get();
			$id_pod = $id_pod->row();
			
			for($i = 0; $i < count($this->input->post('create_mdn_rmr_list')); $i++){
				$id_sn = $db_huawei->select('*')
				->from('dt_request')
				->where('id_request', $this->input->post('create_mdn_rmr_list')[$i])
				->get();
				$id_sn = $id_sn->row();
				$sla_real = explode(' ', $id_sn->sla);

				$sla = (intval($sla_real[0]) * 60 * 60) + (intval($sla_real[2]) * 60);

				$eta = strtotime($id_sn->time_request) + $sla;

				$upd_req = array(
					'id_pod' => $id_pod->id_pod,
					'eta_received' => date('Y-m-d H:i:s', $eta)
				);
				$db_huawei->where('id_request', $this->input->post('create_mdn_rmr_list')[$i]);
				$db_huawei->update('dt_request', $upd_req);

				$upd_sn = array(
					'id_stockaccess' => '2',
					'id_stockposition' => '5'
				);
				$db_huawei->where('id_sn', $id_sn->id_sn);
				if($db_huawei->update('dt_sn', $upd_sn)){
					$sn_history = array(
						'sn' => $id_sn->id_sn,
						'id_pod' => $id_pod->id_pod,
						'datetime_history' => date("Y-m-d H:i:s")
					);
					if($db_huawei->insert('dt_sn_history', $sn_history)){
						$return = true;
					} else {
						$return = false;
					}
				}else{
					$return = false;
				}
			}
		} else {
			$return = false;
		}

		echo json_encode($return);
	}


	public function testing()
	{
		$sla = (intval(47) * 60 * 60) + (intval(30) * 60);

		$eta = strtotime(date('Y-m-d H:i:s')) + $sla;

		print(date('Y-m-d H:i:s', $eta));
	}
}