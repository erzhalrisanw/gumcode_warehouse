<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function search_inventory()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$warehouse = $db_huawei->select('*')
		->from('dm_warehouse')
		->where('id_warehouseclass !=', '3')
		->get();

		$data['from'] = $warehouse->result_array();
		$data['to'] = $warehouse->result_array();

		$dop = $db_huawei->select('*')
		->from('dm_dop')
		->where('activated', '1')
		->get();

		$data['dop'] = $dop->result_array();

		$customer = $db_huawei->select('*')
		->from('dm_customer')
		->get();
		$data['customer'] = $customer->result_array();

		$stock_stat = $db_huawei->select('*')
		->from('dm_stockstatus')
		->get();
		$data['stock_stat'] = $stock_stat->result_array();

		$stock_acc = $db_huawei->select('*')
		->from('dm_stockaccess')
		->where( 'id_stockaccess !=', '9', '11')
		->get();
		$data['stock_acc'] = $stock_acc->result_array();

		$stock_post = $db_huawei->select('*')
		->from('dm_stockposition')
		->where_in('id_stockposition', array('1', '2', '4', '5', '8', '9'))
		->get();
		$data['stock_pos'] = $stock_post->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/report/search_inventory', $data);
	}

	public function search_sn_history()
	{
		$sn = $this->input->post('input_search_hst');

		$db_huawei = $this->load->database('huawei', TRUE);

		// $result = $db_huawei->select('sn.sn, doc.doc_no, req.rmr, doc.doc_date, req.time_request, req2.time_request AS tr_pickup, req2.rmr AS rmr_pickup, pod.pod_no, pickup.pickup_no')
		// ->from('dt_sn_history hst')
		// ->join('dt_sn sn', 'sn.id_sn = hst.sn')
		// ->join('dt_io_bound doc', 'doc.id = hst.id_doc', 'left')
		// ->join('dt_request req', 'req.id_pod = hst.id_pod', 'left')
		// ->join('dt_request req2', 'req2.id_pickup = hst.id_pickup', 'left')
		// ->join('dt_pod pod', 'pod.id_pod = hst.id_pod', 'left')
		// ->join('dt_pickup pickup', 'pickup.id_pickup = hst.id_pickup', 'left')
		// ->like('sn.sn', $sn)
		// // ->group_by('CASE WHEN hst.id_doc IS NOT NULL THEN hst.id_doc END', FALSE)
		// ->group_by('CASE WHEN hst.id_pod IS NOT NULL THEN hst.id_pod END', FALSE)
		// ->group_by('CASE WHEN hst.id_pickup IS NOT NULL THEN hst.id_pickup END', FALSE)
		// // ->group_by(array('hst.id_pod', 'hst.id_pickup'))
		// ->get();

		$result = array();

		$check_doc = $db_huawei->select('sn.sn, doc.doc_no, doc.doc_date')
		->from('dt_sn_history hst')
		->join('dt_sn sn', 'sn.id_sn = hst.sn')
		->join('dt_io_bound doc', 'doc.id = hst.id_doc')
		->where('sn.sn', $sn)
		->group_by('doc.id')
		->get();
		$check_doc = $check_doc->result_array();

		if(count($check_doc) > 0){
			for($i = 0; $i < count($check_doc); $i++){
				$result[$i] = $check_doc[$i];
			}
		}

		$check_pod = $db_huawei->select('sn.sn, req.rmr, req.time_request, pod.pod_no')
		->from('dt_sn_history hst')
		->join('dt_sn sn', 'sn.id_sn = hst.sn')
		->join('dt_pod pod', 'pod.id_pod = hst.id_pod')
		->join('dt_request req', 'req.id_pod = hst.id_pod')
		->where('sn.sn', $sn)
		->group_by('pod.id_pod')
		->get();
		$check_pod = $check_pod->result_array();

		if(count($check_pod) > 0){
			for($i = 0; $i < count($check_pod); $i++){
				$result[count($result) + $i] = $check_pod[$i];
			}
		}

		$check_pu = $db_huawei->select('sn.sn, req.rmr, req.time_request, pickup.pickup_no')
		->from('dt_sn_history hst')
		->join('dt_sn sn', 'sn.id_sn = hst.sn')
		->join('dt_pickup pickup', 'pickup.id_pickup = hst.id_pickup')
		->join('dt_request req', 'req.id_pickup = hst.id_pickup')
		->where('sn.sn', $sn)
		->group_by('pickup.id_pickup')
		->get();
		$check_pu = $check_pu->result_array();

		if(count($check_pu) > 0){
			for($i = 0; $i < count($check_pu); $i++){
				$result[count($result) + $i] = $check_pu[$i];
			}
		}

		$result_one = $db_huawei->select('sn.locbin AS locator, sn.sn, warehouse.name_warehouse AS warehouse, status.name_stockstatus AS status, access.name_stockaccess AS access, sn.stocktake_time AS stocktake_date, user.name_user AS stocktaker')
		->from('dt_sn sn')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
		->join('dm_stockstatus status', 'status.id_stockstatus = sn.id_stockstatus')
		->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_user = sn.stocktaker')
		->where('sn.sn', $sn)
		->get();
		$result_one = $result_one->row();

		echo json_encode(array('many' => $result, 'one' => $result_one));
	}

	public function search_inventory_process()
	{
		$return = '';
		$input = $this->input->post();

		$db_huawei = $this->load->database('huawei', TRUE);

		$db_huawei->select('sn.id_sn, warehouse.name_warehouse, SUM(CASE WHEN sn.id_stockstatus = 1 THEN 1 ELSE 0 END) AS total_good, SUM(CASE WHEN sn.id_stockstatus = 2 THEN 1 ELSE 0 END) AS total_faulty, name_stockaccess AS access', FALSE)
		->from('dm_pn pn')
		->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
		->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
		->join('dm_stockstatus status', 'status.id_stockstatus = sn.id_stockstatus')
		->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
		->where('sn.id_stockaccess !=', '11');

		if($input['i_pn']){
			$db_huawei->like('pn.name_pn', $input['i_pn']);
		}

		if($input['i_sn']){
			$sn_col = explode(', ', $input['i_sn']);
			$db_huawei->where_in('sn.sn', $sn_col);
		}

		if($input['id_warehouse']){
			$db_huawei->like('sn.id_warehouse', $input['id_warehouse']);
		}

		if($input['i_desc']){
			$db_huawei->like('basecode.description', $input['i_desc']);
		}

		if($input['i_bin']){
			$db_huawei->like('sn.locbin', $input['i_bin']);
		}

		if($input['i_stock_status']){
			$db_huawei->like('sn.id_stockstatus', $input['i_stock_status']);
		}

		if($input['i_stock_position']){
			$db_huawei->like('sn.id_stockposition', $input['i_stock_position']);
		}

		if($input['i_stock_access']){
			$db_huawei->like('sn.id_stockaccess', $input['i_stock_access']);
		}

		if($input['i_stock_date']){
			$db_huawei->like('sn.stocktake_time', $input['i_stock_date']);
		}

		$db_huawei->group_by('sn.id_warehouse');
		$db_huawei->group_by('sn.id_stockaccess');
		$stock = $db_huawei->get();

		$db_huawei->select('warehouse.name_warehouse, project.name_project, basecode.name_basecode, basecode.description, pn.name_pn, sn.sn, status.name_stockstatus, position.name_stockposition, access.name_stockaccess, sn.stocktake_time, user.name_user, sn.locbin')
		->from('dm_pn pn')
		->join('dt_sn sn', 'sn.id_pn = pn.id_pn')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = sn.id_warehouse')
		->join('dm_stockaccess access', 'access.id_stockaccess = sn.id_stockaccess')
		->join('dm_stockstatus status', 'status.id_stockstatus = sn.id_stockstatus')
		->join('dm_stockposition position', 'position.id_stockposition = sn.id_stockposition')
		->join('dm_project project', 'project.id_project = sn.id_project')
		->join('dm_basecode basecode', 'basecode.id_basecode = pn.id_basecode')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_user = sn.stocktaker')
		->where('sn.id_stockaccess !=', '11');

		if($input['i_pn']){
			$db_huawei->like('pn.name_pn', $input['i_pn']);
		}

		if($input['i_sn']){
			$sn_col = explode(', ', $input['i_sn']);
			$db_huawei->where_in('sn.sn', $sn_col);
		}

		if($input['id_warehouse']){
			$db_huawei->like('sn.id_warehouse', $input['id_warehouse']);
		}

		if($input['i_desc']){
			$db_huawei->like('basecode.description', $input['i_desc']);
		}

		if($input['i_bin']){
			$db_huawei->like('sn.locbin', $input['i_bin']);
		}

		if($input['i_stock_status']){
			$db_huawei->like('sn.id_stockstatus', $input['i_stock_status']);
		}

		if($input['i_stock_position']){
			$db_huawei->like('sn.id_stockposition', $input['i_stock_position']);
		}

		if($input['i_stock_access']){
			$db_huawei->like('sn.id_stockaccess', $input['i_stock_access']);
		}

		if($input['i_stock_date']){
			$db_huawei->like('sn.stocktake_time', $input['i_stock_date']);
		}
		
		$one_by_one = $db_huawei->get();

		echo json_encode(array('stock' => $stock->result_array(), 'one_by_one' => $one_by_one->result_array()));
	}

	public function search_request_process()
	{
		$input = $this->input->post();
		$db_huawei = $this->load->database('huawei', TRUE);

		$db_huawei->select('req.*, req.time_request, req.order, req.rma, pod.pod_no, pod.awb_no, pod.actual_received, pod.consignee, dop.name_dop, warehouse.name_warehouse, pn.name_pn AS name_pn_del, sn.sn AS name_sn_del, statReq.status_request, pickup.pickup_no, pickup.actual_pickup, statPick.pickup_status, pn2.name_pn AS name_pn_pickup, sn2.sn AS name_sn_pickup, customer.name_customer, pn3.name_pn AS pn_request')
		->from('dt_request req')
		->join('dm_dop dop', 'dop.id_dop = req.id_dop', 'left')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse', 'left')
		->join('dm_pn pn2', 'pn2.id_pn = req.id_pn_pickup', 'left')
		->join('dt_sn sn2', 'sn2.id_sn = req.id_sn_pickup', 'left')
		->join('dm_pn pn', 'pn.id_pn = req.id_pn', 'left')
		->join('dt_sn sn', 'sn.id_sn = req.id_sn', 'left')
		->join('dm_pn pn3', 'pn3.id_pn = req.id_basecode', 'left')
		->join('dt_pod pod', 'pod.id_pod = req.id_pod', 'left')
		->join('wmshilco_user_mgm.dt_user user', 'user.id_user = req.id_callcenter', 'left')
		->join('dm_status_request statReq', 'statReq.id_status_request = req.id_status_request', 'left')
		->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup', 'left')
		->join('dt_status_pickup statPick', 'statPick.id_statuspickup = req.id_statuspickup', 'left')
		->join('dm_customer customer', 'customer.id_customer = req.id_customer', 'left');

		if($input['req_pn_req']){
			$db_huawei->like('pn3.name_pn', $input['req_pn_req']);
		}

		if($input['req_pn_pu']){
			$db_huawei->like('pn2.name_pn', $input['req_pn_pu']);
		}

		if($input['req_pn_del']){
			$db_huawei->like('pn.name_pn', $input['req_pn_del']);
		}

		if($input['req_sn_del']){
			$sn_col_del = explode(', ', $input['req_sn_del']);
			$db_huawei->or_where_in('sn.sn', $sn_col_del);
		}

		if($input['req_sn_pu']){
			$sn_col_pu = explode(', ', $input['req_sn_pu']);
			$db_huawei->or_where_in('sn2.sn', $sn_col_pu);
		}

		if($input['req_rmr']){
			$sn_col_rmr = explode(', ', $input['req_rmr']);
			$db_huawei->or_where_in('req.rmr', $sn_col_rmr);
		}

		if($input['req_mdn']){
			$sn_col_mdn = explode(', ', $input['req_mdn']);
			$db_huawei->or_where_in('pod.pod_no', $sn_col_mdn);
		}

		if($input['req_cmn']){
			$sn_col_cmn = explode(', ', $input['req_cmn']);
			$db_huawei->or_where_in('pickup.pickup_no', $sn_col_cmn);
		}

		if($input['req_date']){
			$db_huawei->like('req.time_request', $input['req_date']);
		}

		if($input['req_dop']){
			$db_huawei->like('req.id_dop', $input['req_dop']);
		}

		if($input['req_customer']){
			$db_huawei->like('req.id_customer', $input['req_customer']);
		}

		if($input['req_site']){
			$db_huawei->like('req.site', $input['req_site']);
		}
		$list = $db_huawei->get();

		echo json_encode($list->result_array());
	}
}