<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Download extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('domain') != 'huawei') {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
	}

	public function document_printout()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$warehouse = $db_huawei->select('*')
		->from('dm_warehouse')
		->get();
		$data['warehouse'] = $warehouse->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/download/document_printout', $data);
	}

	public function inventory_stock()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$project = $db_huawei->select('*')
		->from('dm_project')
		->get();
		$data['project'] = $project->result_array();

		$stock_stat = $db_huawei->select('*')
		->from('dm_stockstatus')
		->get();
		$data['stock_status'] = $stock_stat->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/download/inventory_stock', $data);
	}

	public function download_transaction()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$project = $db_huawei->select('*')
		->from('dm_project')
		->get();
		$data['project'] = $project->result_array();

		$customer = $db_huawei->select('*')
		->from('dm_customer')
		->get();
		$data['customer'] = $customer->result_array();

		$this->load->view('huawei/layout');
		$this->load->view('huawei/download/download_transaction', $data);
	}

	public function download_finance()
	{
		$db_huawei = $this->load->database('huawei', TRUE);
		$this->load->view('huawei/layout');
		$this->load->view('huawei/download/download_finance');
	}

	public function search_inbound_doc()
	{
		$input = $this->input->post();

		$db_huawei = $this->load->database('huawei', TRUE);

		$db_huawei->select('doc.id, doc.doc_no, doc.doc_date, warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to')
		->from('dt_io_bound doc')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
		->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
		->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
		->like('bound_type.name_io_boundtype', 'inbound');

		if($input['ib_doc_no']){
			$db_huawei->like('doc.doc_no', $input['ib_doc_no']);
		}

		if($input['ib_created_date']){
			$db_huawei->like('doc.doc_date', $input['ib_created_date']);
		}

		if($input['ib_from']){
			$db_huawei->like('doc.id_boundfrom', $input['ib_from']);
		}

		if($input['ib_to']){
			$db_huawei->like('doc.id_boundto', $input['ib_to']);
		}

		$stock = $db_huawei->get();
		echo json_encode($stock->result_array());
	}

	public function search_outbound_doc()
	{
		$input = $this->input->post();

		$db_huawei = $this->load->database('huawei', TRUE);

		$db_huawei->select('doc.id, doc.doc_no, doc.doc_date, warehouse.name_warehouse AS from, warehouse2.name_warehouse AS to')
		->from('dt_io_bound doc')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = doc.id_boundfrom')
		->join('dm_warehouse warehouse2', 'warehouse2.id_warehouse = doc.id_boundto')
		->join('dm_io_boundtype bound_type', 'bound_type.id_io_boundtype = doc.id_boundtype')
		->like('bound_type.name_io_boundtype', 'outbound');

		if($input['ob_doc_no']){
			$db_huawei->like('doc.doc_no', $input['ob_doc_no']);
		}

		if($input['ob_created_date']){
			$db_huawei->like('doc.doc_date', $input['ob_created_date']);
		}

		if($input['ob_from']){
			$db_huawei->like('doc.id_boundfrom', $input['ob_from']);
		}

		if($input['ob_to']){
			$db_huawei->like('doc.id_boundto', $input['ob_to']);
		}
		
		$stock = $db_huawei->get();
		echo json_encode($stock->result_array());
	}

	public function search_request_doc()
	{
		$input = $this->input->post();

		$db_huawei = $this->load->database('huawei', TRUE);

		$db_huawei->select('req.url_photo_mdn, pod.pod_no, req.time_request, warehouse.name_warehouse, stat_req.status_request')
		->from('dt_request req')
		->join('dt_pod pod', 'pod.id_pod = req.id_pod')
		->join('dm_status_request stat_req', 'stat_req.id_status_request = req.id_status_request')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse')
		->where('req.id_status_request !=', '7');

		if($input['req_date_range']){
			$date = explode(' - ', $input['req_date_range']);
			$db_huawei->where('req.time_request >=', $date[0]);
			$db_huawei->where('req.time_request <=', $date[1]);
		}

		if($input['req_warehouse']){
			$db_huawei->like('req.id_warehouse', $input['req_warehouse']);
		}
		
		$stock = $db_huawei->get();
		echo json_encode($stock->result_array());
	}

	public function search_pickup_doc()
	{
		$input = $this->input->post();

		$db_huawei = $this->load->database('huawei', TRUE);

		$db_huawei->select('req.url_photo_cmn, pickup.pickup_no, req.time_request, warehouse.name_warehouse, stat_pickup.pickup_status')
		->from('dt_request req')
		->join('dt_pickup pickup', 'pickup.id_pickup = req.id_pickup')
		->join('dt_status_pickup stat_pickup', 'stat_pickup.id_statuspickup = req.id_statuspickup')
		->join('dm_warehouse warehouse', 'warehouse.id_warehouse = req.id_warehouse');
		

		if($input['pickup_date_range']){
			$date = explode(' - ', $input['pickup_date_range']);
			$db_huawei->where('req.time_request >=', $date[0]);
			$db_huawei->where('req.time_request <=', $date[1]);
		}

		if($input['pickup_warehouse']){
			$db_huawei->like('req.id_warehouse', $input['pickup_warehouse']);
		}
		
		$stock = $db_huawei->get();
		echo json_encode($stock->result_array());
	}

	public function get_warehouse_by_download_inventory_type()
	{
		$db_huawei = $this->load->database('huawei', TRUE);

		$type = $this->input->post('type');

		$db_huawei->select('*')
		->from('dm_warehouse');

		if($type == 1 || $type == 3){
			$db_huawei->where('id_warehouseclass !=', '3');
		}elseif($type == 4 || $type == 5 || $type == 6){
			$db_huawei->where_not_in('id_warehouseclass', array('1', '3'));
		}elseif($type == 7 || $type == 8){
			$db_huawei->where('id_warehouseclass !=', '3');
		}

		$data = $db_huawei->get();

		echo json_encode($data->result_array());
	}
}