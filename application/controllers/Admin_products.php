<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_products extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('admin_model'));
    }

    public function index()
    {
        $this->db->select('*');
        $this->db->from('category');
        $cat = $this->db->get();

        $data = array(
            'cat' => $cat->result_array()
        );

        if ($this->session->userdata('logged')) {
            $this->load->view('admin/layout');
            $this->load->view('admin/products', $data);
        } else {
            redirect(base_url() . "index.php/admin/login");
        }
    }

    public function getListProd()
    {
        $this->db
            ->select('*, product.name AS prod_name,product.id AS prod_id, product.url AS prod_url, category.name AS cat_name')
            ->from('product')
            ->join('category', 'product.category_id = category.id');
        $prod = $this->db->get();

        echo json_encode($prod->result_array());
    }

    public function getListCat()
    {
        $this->db
            ->select('*')
            ->from('category');
        $cat = $this->db->get();

        echo json_encode($cat->result_array());
    }

    public function getOneProd()
    {
        $prod = $this->db
            ->select('*, product.name AS prod_name, product.url AS prod_url, category.name AS cat_name')
            ->from('product')
            ->join('category', 'product.category_id = category.id')
            ->where('product.id', $this->input->post('id'))
            ->get();

        echo json_encode($prod->result_array());
    }

    public function getOneCat()
    {
        $cat = $this->db
            ->select('*')
            ->from('category')
            ->where('id', $this->input->post('id'))
            ->get();

        echo json_encode($cat->result_array());
    }

    public function editProd()
    {
        $status = '';
        $data = array('name' => $this->input->post('name-upd'));
        $update = $this->db->update('product', $data, array('id' => $this->input->post('prod-id-upd')));

        if ($update) {
            $status = array('status' => TRUE, 'data' => 'Product successfully updated.');
        } else {
            $status = array('status' => FALSE, 'data' => 'Product cannot be updated.');
        }

        if ($this->input->post('file-exist')) {
            $product = $this->db->get_where('product', array('id' => $this->input->post('prod-id-upd')));
            $product = $product->row_array();

            $new_name = time() . '-' . $_FILES['file']['name'];
            $config['file_name'] = $new_name;
            $config['upload_path']          = realpath(APPPATH . '../public/img_prod');
            $config['allowed_types']        = 'jpg|png|jpeg';
            $config['max_size']             = 1024;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $status = array('status' => FALSE, 'data' => 'Product picture cannot be updated.', 'err' => $this->upload->display_errors());
            } else {
                unlink(realpath(APPPATH . '../public/img_prod/' . $product['url']));
                $rec = $this->upload->data();
                $this->db->update('product', array('url' => $rec['file_name']), array('id' => $this->input->post('prod-id-upd')));

                $status = array('status' => TRUE, 'data' => 'Product picture successfully updated.');
            }
        }

        echo json_encode($status);
    }

    public function addCat()
    {
        if ($this->db->insert('category', array('name' => $this->input->post('cat')))) {
            echo json_encode(array('status' => TRUE, 'data' => 'Category successfully added.'));
        } else {
            echo json_encode(array('status' => FALSE, 'data' => 'Category cannot be added.'));
        }
    }

    public function addProd()
    {
        $model = $this->admin_model;
        $new_name = time() . '-' . $_FILES['file']['name'];
        $config['file_name'] = $new_name;
        $config['upload_path']          = realpath(APPPATH . '../public/img_prod');
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['max_size']             = 1024;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            $data = array(
                'name' => $this->input->post('name'),
                'category_id' => $this->input->post('cat'), 'url' => $this->upload->data('file_name')
            );
            $prod = $model->addProd($data);
            if ($prod) {
                echo json_encode(array('status' => TRUE, 'data' => 'Image successfully uploaded.'));
            } else {
                echo json_encode(array('status' => FALSE, 'data' => 'Data cannot be submitted.'));
            }
        } else {
            echo json_encode(array('status' => FALSE, 'data' => 'Image cannot be uploaded.', 'err' => $this->upload->display_errors()));
        }
    }

    public function addImage()
    {
        $model = $this->admin_model;
        $config['upload_path']          = realpath(APPPATH . '../public/img_slide');
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 2048;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('picture')) {
            $img = $model->addImage($this->upload->data('file_name'));
            if ($img) {
                echo json_encode(array('status' => TRUE, 'data' => 'Image successfully uploaded.'));
            } else {
                echo json_encode(array('status' => FALSE, 'data' => 'Image cannot be uploaded.', 'err' => $this->upload->display_errors()));
            }
        } else {
            echo json_encode(array('status' => FALSE, 'data' => 'Image cannot be uploaded.', 'err' => $this->upload->display_errors()));
        }
    }

    public function dltProd()
    {
        $product = $this->db->get_where('product', array('id' => $this->input->post('id')));
        $product = $product->row_array();
        if (unlink(APPPATH . '../public/img_prod' . '/' . $product['url'])) {
            $this->db->delete('product', array('id' => $this->input->post('id')));
            echo json_encode(array('status' => TRUE, 'data' => 'Image successfully deleted.'));
        } else {
            echo json_encode(array('status' => FALSE, 'data' => 'Image cannot be deleted!.'));
        }
    }

    public function dltCat()
    {
        if($this->db->delete('category', array('id' => $this->input->post('id')))){
            echo json_encode(array('status' => TRUE, 'data' => 'Image successfully deleted.'));
        } else {
            echo json_encode(array('status' => FALSE, 'data' => 'Image cannot be deleted!.'));
        }
    }

    public function editCat()
    {
        $status = '';
        $data = array('name' => $this->input->post('cat-name-upd'));
        $update = $this->db->update('category', $data, array('id' => $this->input->post('cat-id-upd')));

        if ($update) {
            $status = array('status' => TRUE, 'data' => 'Category successfully updated.');
        } else {
            $status = array('status' => FALSE, 'data' => 'Category cannot be updated.');
        }

        echo json_encode($status);
    }
}
