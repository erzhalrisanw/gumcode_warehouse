<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// $db_huawei = $this->load->database('huawei', TRUE);
		if ($this->session->userdata('domain') == 'all') {
			$data_menu = $this->db->select('*')
			->from('dm_domain')
			->where('id_domain !=', '0')
			->get();

			$data['menu'] = $data_menu->result_array();
			$data['userdata'] = $this->session->all_userdata();
			$this->load->view('menu_all/home', $data);
		} else {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
		// $this->load->view('demo/home');
	}

	public function change_domain_proceed($name_domain)
	{
		$this->session->set_userdata('domain', $name_domain);
		redirect(base_url() . $name_domain.'/home');
	}
}
