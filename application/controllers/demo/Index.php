<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->userdata('domain') == 'demo') {
			$data['userdata'] = $this->session->all_userdata();
			$this->load->view('demo/home', $data);
		} else {
			$this->session->sess_destroy();
			redirect(base_url() . "login");
		}
		// $this->load->view('demo/home');
	}
}
