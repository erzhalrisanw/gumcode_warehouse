<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <p class="jdl-big">Input</p>
                <div class="input-div">
                    <div class="my-form-group">
                        <p class="my-label-input">Project</p>
                        <input type="text" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Customer Name</p>
                        <input type="text" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Customer Request Date</p>
                        <input id="reqDate" type="text" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Request Creation Date</p>
                        <input id="createDate" type="text" class="form-control" disabled>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Name of Requestor</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Coverage Area (City)</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Person Responsibility</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Source of Info</p>
                        <input type="text" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Order Number</p>
                        <input type="text" class="form-control" placeholder="Determine Later" disabled>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Basecode (Part Number)</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Desc. Part</p>
                        <input type="text" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Trouble Ticket (TT/RMA)</p>
                        <input type="text" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Site Name Allocation</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Drop of Point (DOP) Address</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">SWH Origin</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Part Number Deliver</p>
                        <input type="text" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Service Level Agreement (SLA)</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Status</p>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Notes/Remarks</p>
                        <input type="text" class="form-control">
                    </div>
                    <button class="my-btn save">SAVE</button>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $("#reqDate").daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        startDate: moment(),
        timePicker24Hour: true,
        locale: {
            format: 'DD/MM/YYYY HH:mm:ss'
        }
    });
    $("#createDate").daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        startDate: moment(),
        timePicker24Hour: true,
        locale: {
            format: 'DD/MM/YYYY HH:mm:ss'
        }
    });
</script>
<style>
    .my-row {
        margin: 10px !important;
        border-radius: 5px;
        /* box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.3); */
        height: 80%;
    }

    .head-btn {
        padding: 20px;
        margin-bottom: 20px;
        width: 100%;
        border-bottom: 1px solid rgba(0, 0, 0, 0.3);
    }

    .my-btn {
        font-size: 14px;
        padding: 6px 20px !important;
        margin-bottom: 20px !important;
        background: transparent;
        border-color: rgba(0, 0, 0, 0.3) !important;
        color: black;
    }

    .my-btn-add {
        font-size: 14px;
        padding: 6px 20px !important;
        position: absolute;
        left: 20px;
        top: 0;
    }

    .my-btn.active {
        background: #007bff !important;
        border-color: #007bff !important;
    }

    .my-btn.switch {
        margin-bottom: 0 !important;
    }

    .my-btn.cat {
        margin-right: 15px;
    }

    .tab-prod-pic {
        height: 90px !important;
        width: 60px !important;
        display: block;
        margin: 0 auto;
    }

    .data-table {
        margin: 0 auto;
        margin-top: 45px;
        width: 100%;
        background: white;
        box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);
    }

    table thead th {
        border-bottom: 1px solid black !important;
    }

    table tbody {
        background: #DFDFDF !important;
    }

    .sec-div {
        width: 100%;
        position: relative;
    }

    .jdl-div {
        background: #dc3545;
        padding: 10px;
        margin-bottom: 20px;
    }

    .jdl-big {
        font-size: 24px;
        font-weight: 400;
        width: 100%;
        margin-bottom: 0;
    }

    .jdl-date {
        font-size: 13px;
        font-weight: 300;
        width: 100%;
        margin-bottom: 20px;
    }

    .graph {
        width: 1000px !important;
        border-radius: 5px;
        box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);
        margin: 20px auto;
        /* padding: 0 5px; */
    }

    .my-tab-th {
        font-size: 13px;
    }

    #graph1,
    #graph2 {
        margin: 0 auto !important;
        margin-top: 30px !important;
        margin-left: 10px !important;
        margin-bottom: 10px !important;
    }

    .graph-div-ttl {
        background: #DFDFDF;
        width: 100%;
        height: 40px;
        padding: 10px 20px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }

    .graph-ttl {
        color: black;
        font-size: 15px;
        font-weight: 300;
    }

    #trace-tab {
        width: 95%;
        margin: 0 auto;
        margin-top: 20px !important;
        margin-bottom: 40px !important;
    }

    .my-input-group {
        display: flex;
        margin-left: 10px;
    }

    .ttl-input {
        font-size: 14px;
        font-weight: 700;
        margin-right: 20px;
        margin-top: 3px;
    }

    .search-input {
        border-radius: 4px;
        border: .5px solid rgba(0, 0, 0, 0.3);
        height: 30px;
        padding-right: 25px;
    }

    .my-input-icon {
        font-size: 14px;
        margin-top: 8px;
        margin-left: -20px;
    }

    .my-table {
        border: 1px solid black;
    }

    .my-table-head {
        border-bottom: 1px solid red !important;
    }

    .input-div {
        margin-left: 20px;
        margin-top: 30px;
        width: 60%;
    }

    .my-label-input {
        font-size: 12px;
        font-weight: 300;
        /* margin-top: 7px; */
        margin-bottom: 20px;
    }

    .form-control {
        height: 30px;
        font-size: 12px;
        font-weight: 300;
        margin-bottom: 10px;
        width: 50%;
    }

    .my-form-group {
        display: flex;
        justify-content: space-between;
    }

    .my-btn.save {
        border: 1px solid #dc3545 !important;
        background: #dc3545;
        color: white;
        font-size: 12px;
        font-weight: 300;
        padding: 5px 30px !important;
        float: right;
        margin-top: 20px;
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.3);
    }

    .my-btn.save:hover{
        background: white;
        color: #dc3545;
    }

    @media only screen and (max-width: 600px) {
        .my-row {
            box-shadow: none;
            margin: 0 !important;
        }

        .head-btn {
            margin-bottom: 5px;
        }

        .content {
            padding: 0 !important;
        }

        .container-fluid {
            padding: 0 !important;
        }

        .data-table {
            width: 100%;
            margin: 0 auto !important;
            font-size: 10px !important;
            margin-top: 20px !important;
            table-layout: fixed;
        }

        .btn {
            font-size: 12px !important;
        }

        .tab-prod-pic {
            height: 70px !important;
            width: 40px !important;
        }

        .sec-div {
            padding: 0 20px;
        }

        .my-btn-add {
            position: relative !important;
            width: 100%;
            margin: 0 !important;
            left: 0 !important;
            margin-top: 10px !important;
        }

        /* .dataTables_filter input {
            width: 100px !important;
        } */
    }
</style>