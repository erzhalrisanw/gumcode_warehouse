<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/daterangepicker/daterangepicker.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <script src="<?php echo base_url(''); ?>/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script>
    $(document).ready(function() {
      let level = "<?= $this->session->userdata('level'); ?>";
      if (level == 1) {
        $('#call-center').show();
        $('#swh').hide();
        $('#cwh').hide();
      } else if (level == 2) {
        $('#call-center').hide();
        $('#swh').show();
        $('#cwh').hide();
      } else {
        $('#call-center').hide();
        $('#swh').hide();
        $('#cwh').show();
      }
    });
  </script>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light my-head-nav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link my-nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item item-logout">
          <a href="<?php echo base_url(''); ?>index.php/login/do_logout" class="nav-link nav-side-link">Log Out
            <i class="nav-icon fas fa-sign-out-alt"></i>
          </a>
        </li>
      </ul>
    </nav>
    <aside class="main-sidebar elevation-4 nav-side">
      <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex user-panel1">
          <div class="image ttl">
            <h5 class="ttl-big">SYS</h5>
          </div>
          <div class="info ttl">
            <h5 class="subttl-big">Admin</h5>
          </div>
        </div>
        <div class="user-panel mt-3 pb-3 mb-3 d-flex user-panel2">
          <div class="image">
            <img src="<?= base_url('') ?>/dist/img/user2-160x160.jpg" class="img-circle elevation-2 my-user-img" alt="User Image">
          </div>
          <div class="info">
            <p class="user-info-p"><?= $this->session->userdata('username'); ?></p>
          </div>
          <!-- <div class="image">
            <img src="<?php echo base_url(''); ?>/dist/img/logo-gasplus-symbol.png" class="nav-image symb">
          </div>
          <div class="info">
            <img src="<?php echo base_url(''); ?>/dist/img/logo-gasplus-text.png" class="nav-image text">
          </div> -->
        </div>
        <nav id="call-center" class="mt-2 nav-side-list">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'dashboard' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/dashboard" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'input' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/input" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-file-medical"></i>
                <p>
                  Input
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'report' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/report" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-file"></i>
                <p>
                  Report
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'trace_track' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/trace_track" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-map-marker-alt"></i>
                <p>
                  Trace and Track
                </p>
              </a>
            </li>
          </ul>
        </nav>

        <nav id="swh" class="mt-2 nav-side-list">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'dashboard' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/dashboard" class="nav-link nav-side-link">
                <i class="nav-icon fa fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'delivery_transaction' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/delivery_transaction" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-truck-loading"></i>
                <p>
                  Delivery & Transaction
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'stocktake' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/stocktake" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-boxes"></i>
                <p>
                  Stocktake
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'trace_track' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/trace_track" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-map-marker-alt"></i>
                <p>
                  Trace and Track
                </p>
              </a>
            </li>
          </ul>
        </nav>

        <nav id="cwh" class="mt-2 nav-side-list">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'dashboard' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/dashboard" class="nav-link nav-side-link">
                <i class="nav-icon fa fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'report' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/report" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-file"></i>
                <p>
                  Report
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'stocktake' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/stocktake" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-truck-loading"></i>
                <p>
                  Stocktake
                </p>
              </a>
            </li>
            <li class="nav-item my-nav-item <?php echo $this->uri->segment(1) == 'trace_track' ? 'my-active' : '' ?>">
              <a href="<?php echo base_url(''); ?>index.php/trace_track" class="nav-link nav-side-link">
                <i class="nav-icon fas fa-map-marker-alt"></i>
                <p>
                  Track
                </p>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </aside>
  </div>
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <script src="<?php echo base_url(''); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/moment/moment.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <script src="<?php echo base_url(''); ?>/dist/js/adminlte.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/series-label.js"></script>
</body>

</html>
<style>
  .ttl-big, .subttl-big {
    color: white;
    margin: 0 !important;
  }

  .ttl-big {
    margin-left: 10px !important;
    margin-right: 10px !important;
  }

  .subttl-big {
    font-weight: 300;
  }

  .info.ttl, .img.ttl {
    padding: 0 !important;
  }

  .content-wrapper, .wrapper, .layout-fixed {
    background: #F4F6F8;
  }

  .layout-fixed {
    font-family: 'Poppins' !important;
  }

  .my-head-nav {
    background: #dc3545 !important;
  }

  .nav-image.text {
    width: 110px !important;
    margin-left: -5px;
  }

  .nav-image.symb {
    width: 40px !important;
    margin-top: -5px;
  }

  .item-logout {
    position: absolute;
    right: 20px;
    font-size: 14px;
    margin-top: 8px !important;
  }

  div.image {
    padding: 0 !important;
    padding-left: 0 !important;
  }

  div.info {
    text-decoration: none !important;
    /* padding: 0 !important; */
  }

  .main-sidebar {
    background: white;
  }

  .nav-side {
    background: #343A40;
  }

  .sidebar {
    padding: 0 15px !important;
    position: relative !important;
  }

  .my-nav-link {
    color: white !important;
  }

  .nav-side-link {
    padding: 0 !important;
  }

  .user-panel1 {
    background: #DC3D45 !important;
    position: absolute;
    left: 0;
    right: 0;
  }

  .user-panel2 {
    border-bottom: 1px solid white;
    position: absolute;
    padding-left: 2px !important;
    top: 60px;
    left: 15px;
    right: 15px;
    height: 80px !important;
  }

  .my-user-img {
    width: 40px !important;
  }

  .user-panel {
    background: transparent;
    margin-top: -20px;
    margin: 0 !important;
    padding: 15px 0 0 8px;
    padding-bottom: 0 !important;
    height: 56px;
  }

  .nav-side-list {
    margin-top: 170px !important;
  }

  .nav-side-link {
    color: white !important;
  }

  .user-info-p {
    color: white;
    margin: 0;
    font-size: 20px;
    margin-left: 10px;
  }

  .my-nav-item {
    margin-bottom: 20px !important;
    border-radius: 5px;
    padding: 5px 5px;
    padding-left: 8px;
    /* text-align: center !important; */
  }

  .my-nav-item>a>p {
    font-weight: 300 !important;
  }

  .my-nav-item:hover {
    background: #dc3545;
  }

  .my-active {
    background: #dc3545;
  }

  .btn {
    box-shadow: 0px 3px 5px 1px rgba(0, 0, 0, 0.3) !important;
  }
</style>