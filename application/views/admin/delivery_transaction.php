<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <p class="jdl-real-big">Delivery & Transaction</p>
                <div class="jdl-div">
                    <button id="tab1" class="my-btn-tab">Monitoring</button>
                    <button id="tab2" class="my-btn-tab">Request</button>
                    <button id="tab3" class="my-btn-tab">In Delivery</button>
                    <button id="tab4" class="my-btn-tab">Pickup</button>
                </div>
                <div class="data-table">
                    <div id="table1" class="div-table">
                        <button class="my-btn-export">Export PDF</button>
                        <table id="trace-tab3" class="table table-bordered table-striped">
                            <thead class="my-table-head">
                                <tr>
                                    <th class="my-tab-th">Request</th>
                                    <th class="my-tab-th">DOP</th>
                                    <th class="my-tab-th">Origin</th>
                                    <th class="my-tab-th">Module Delivery</th>
                                    <th class="my-tab-th">MDD</th>
                                    <th class="my-tab-th">Model Faulty</th>
                                    <th class="my-tab-th">FRF</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="table2" class="div-table">
                        <table id="trace-tab2" class="table table-bordered table-striped">
                            <thead class="my-table-head">
                                <tr>
                                    <th class="my-tab-th">Request</th>
                                    <th class="my-tab-th">RMR</th>
                                    <th class="my-tab-th">Owner</th>
                                    <th class="my-tab-th">DOP</th>
                                    <th class="my-tab-th">PN Req</th>
                                    <th class="my-tab-th">PN Delivery</th>
                                    <th class="my-tab-th">Origin</th>
                                    <th class="my-tab-th">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td colspan="8" style="text-align: center;"><button class="btn-add-tab"><i class="fa fa-plus"></i> Add Data</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="table3" class="div-table">
                        <table id="trace-tab1" class="table table-bordered table-striped">
                            <thead class="my-table-head">
                                <tr>
                                    <th class="my-tab-th">Request</th>
                                    <th class="my-tab-th">Indelivery</th>
                                    <th class="my-tab-th">MDD Number</th>
                                    <th class="my-tab-th">RMR</th>
                                    <th class="my-tab-th">Module Delivery</th>
                                    <th class="my-tab-th">Origin</th>
                                    <th class="my-tab-th">DOP</th>
                                    <th class="my-tab-th">Site</th>
                                    <th class="my-tab-th">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td colspan="9" style="text-align: center;"><button class="btn-add-tab"><i class="fa fa-plus"></i> Add Data</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="table4" class="div-table">
                        <table id="trace-tab4" class="table table-bordered table-striped">
                            <thead class="my-table-head">
                                <tr>
                                    <th class="my-tab-th">RMR</th>
                                    <th class="my-tab-th">Consignee</th>
                                    <th class="my-tab-th">DOP</th>
                                    <th class="my-tab-th">PN</th>
                                    <th class="my-tab-th">Req.Pickup</th>
                                    <th class="my-tab-th">Req.By</th>
                                    <th class="my-tab-th">TT.Number</th>
                                    <th class="my-tab-th">FRF</th>
                                    <th class="my-tab-th">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button class="btn btn-default btn-xs">Edit</button>
                                        <button class="btn btn-danger btn-xs">Delete</button></td>
                                </tr>
                                <tr>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td colspan="9" style="text-align: center;"><button class="btn-add-tab"><i class="fa fa-plus"></i> Add Data</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $('#trace-tab1,#trace-tab3,#trace-tab4').DataTable({
        "responsive": true,
        // "lengthChange": false,
        // "bInfo": false,
        "sort": false,
        "searching": false,
        // "paging": false
    });

    let tab2 = $('#trace-tab2').DataTable({
        "responsive": true,
        // "lengthChange": false,
        // "bInfo": false,
        "sort": false,
        "searching": false,
        // "paging": false
    });

    $('#tab1').click(function() {
        $('#table1').show();
        $('#table2, #table3, #table4').hide();
        $('#tab1').addClass('active');
        $('#tab2, #tab3, #tab4').removeClass('active');
    });

    $('#tab2').click(function() {
        $('#table2').show();
        $('#table1, #table3, #table4').hide();
        $('#tab2').addClass('active');
        $('#tab1, #tab3, #tab4').removeClass('active');
    });

    $('#tab3').click(function() {
        $('#table3').show();
        $('#table1, #table2, #table4').hide();
        $('#tab3').addClass('active');
        $('#tab1, #tab2, #tab4').removeClass('active');
    });

    $('#tab4').click(function() {
        $('#table4').show();
        $('#table1, #table2, #table3').hide();
        $('#tab4').addClass('active');
        $('#tab1, #tab2, #tab3').removeClass('active');
    });

    $("#tab1").trigger('click');
</script>
<style>
    .my-row {
        margin: 10px !important;
        border-radius: 5px;
        /* box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.3); */
        height: 80%;
    }

    .head-btn {
        padding: 20px;
        margin-bottom: 20px;
        width: 100%;
        border-bottom: 1px solid rgba(0, 0, 0, 0.3);
    }

    .btn-add-tab {
        font-size: 12px;
    }

    .my-btn {
        font-size: 14px;
        padding: 6px 20px !important;
        margin-bottom: 20px !important;
        background: transparent;
        border-color: rgba(0, 0, 0, 0.3) !important;
        color: black;
    }

    .jdl-real-big {
        font-size: 24px;
        font-weight: 400;
        width: 100%;
        margin-bottom: 0;
    }

    .my-btn-export {
        border: 1px solid #0069D9;
        background: #0069D9;
        color: white;
        font-size: 13px;
        padding: 5px 20px;
        box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.3);
        float: right;
    }

    .my-btn-tab {
        border: 1px solid #dc3545;
        background: transparent;
        margin-right: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom: 0;
        font-size: 13px;
        padding: 5px 20px;
    }

    .my-btn-tab:focus {
        outline: 0;
    }

    .my-btn-tab:hover {
        background: #dc3545;
        color: white;
    }

    .my-btn-tab.active {
        background: #dc3545;
        color: white;
    }

    .my-btn-add {
        font-size: 14px;
        padding: 6px 20px !important;
        position: absolute;
        left: 20px;
        top: 0;
    }

    .my-btn.active {
        background: #007bff !important;
        border-color: #007bff !important;
    }

    .my-btn.switch {
        margin-bottom: 0 !important;
    }

    .my-btn.cat {
        margin-right: 15px;
    }

    .tab-prod-pic {
        height: 90px !important;
        width: 60px !important;
        display: block;
        margin: 0 auto;
    }

    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        font-size: 12px !important;
    }

    .data-table {
        margin: 0 auto;
        width: 100%;
        background: transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.3);
        padding-bottom: 30px;
        padding-top: 30px;
    }

    .sec-div {
        width: 100%;
        position: relative;
    }

    .jdl-div {
        margin-top: 20px;
        background: transparent;
        padding: 10px;
        display: flex;
        width: 100%;
        padding-bottom: 0;
        border-bottom: 1px solid #dc3545;
    }

    .text-notice {
        font-size: 12px;
        font-weight: 300;
        color: #989898;
        margin-left: 10px;
        margin-bottom: 0;
    }

    .jdl-date {
        font-size: 13px;
        font-weight: 300;
        width: 100%;
        margin-bottom: 20px;
    }

    .graph {
        width: 1000px !important;
        border-radius: 5px;
        box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);
        margin: 20px auto;
        /* padding: 0 5px; */
    }

    .my-tab-th {
        font-size: 13px;
    }

    #graph1,
    #graph2 {
        margin: 0 auto !important;
        margin-top: 30px !important;
        margin-left: 10px !important;
        margin-bottom: 10px !important;
    }

    .graph-div-ttl {
        background: #DFDFDF;
        width: 100%;
        height: 40px;
        padding: 10px 20px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }

    .graph-ttl {
        color: black;
        font-size: 15px;
        font-weight: 300;
    }

    .page-item.active .page-link {
        background: #dc3545;
        border-color: #dc3545;
    }

    #trace-tab {
        /* width: 95%; */
        margin: 0 auto;
        margin-top: 20px !important;
        /* margin-bottom: 40px !important; */
    }

    div.dataTables_wrapper div.dataTables_info {
        font-size: 12px;
    }

    .my-input-group {
        display: flex;
        margin-left: 10px;
    }

    .ttl-input {
        font-size: 14px;
        font-weight: 700;
        margin-right: 20px;
        margin-top: 3px;
    }

    .search-input {
        border-radius: 4px;
        border: .5px solid rgba(0, 0, 0, 0.3);
        height: 30px;
        padding-right: 25px;
    }

    .my-input-icon {
        font-size: 14px;
        margin-top: 8px;
        margin-left: -20px;
    }

    .my-table-head {
        border-bottom: 1px solid red !important;
    }

    .div-table {
        padding: 0 10px;
    }

    @media only screen and (max-width: 600px) {
        .my-row {
            box-shadow: none;
            margin: 0 !important;
        }

        .head-btn {
            margin-bottom: 5px;
        }

        .content {
            padding: 0 !important;
        }

        .container-fluid {
            padding: 0 !important;
        }

        .data-table {
            width: 100%;
            margin: 0 auto !important;
            font-size: 10px !important;
            margin-top: 0 !important;
            table-layout: fixed;
        }

        .btn {
            font-size: 12px !important;
        }

        .tab-prod-pic {
            height: 70px !important;
            width: 40px !important;
        }

        .sec-div {
            padding: 0 20px;
        }

        .my-btn-add {
            position: relative !important;
            width: 100%;
            margin: 0 !important;
            left: 0 !important;
            margin-top: 10px !important;
        }

        /* .dataTables_filter input {
            width: 100px !important;
        } */
    }
</style>