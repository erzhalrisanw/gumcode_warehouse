<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <p class="jdl-big">Monthly Performance</p>
                <p class="jdl-date">Updated on November 2019</p>
                <img style="display: block; margin: 0 auto; margin-bottom: 30px;" src="<?= base_url('')?>/dist/img/icon-set.png" width="850px">
                <div class="graph">
                    <div class="graph-div-ttl">
                        <p class="graph-ttl">Day of Month Traffic November 2019</p>
                    </div>
                    <div id="graph1">
                    </div>
                </div>
                <div class="graph">
                    <div class="graph-div-ttl">
                        <p class="graph-ttl">Monthly Requests</p>
                    </div>
                    <div id="graph2">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    let categories1 = [];
    for (let i = 0; i < 30; i++) {
        categories1.push(i + 1);
    }
    var chart1 = Highcharts.chart('graph1', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories1,
            crosshair: true
        },
        credits: {
            enabled: false
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Requests'
            }
        },
        plotOptions: {
            column: {
                pointWidth: 20,
                pointPadding: 0.2,
                borderWidth: 0,
                color: '#dc3545'
            }
        },
        series: [{
            showInLegend: false,
            name: 'Requests',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 95.6]

        }]
    });

    var chart2 = Highcharts.chart('graph2', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        },
        credits: {
            enabled: false
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Requests'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Not Delivered',
            data: [5, 3, 4, 7, 2, 5, 3, 4, 7, 2, 7, 2]
        }, {
            name: 'Succeed',
            data: [2, 2, 3, 2, 1, 2, 2, 3, 2, 1, 2, 2]
        }, {
            name: 'On Process',
            data: [3, 4, 4, 2, 5, 3, 4, 4, 2, 5, 3, 4]
        }]
    });

    chart1
        .setSize(960);
    chart2
        .setSize(960);
</script>
