<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="data-table">
                    <div class="jdl-div">
                        <p class="jdl-big">Monitoring Transaction</p>
                    </div>
                    <div class="my-btn-group">
                        <button class="my-btn-rpt">Export PDF</button>
                        <button class="my-btn-rpt">See Detail</button>
                    </div>
                    <p class="last-updated">Last Updated November 2019</p>
                </div>
                <div class="data-table">
                    <div class="jdl-div">
                        <p class="jdl-big">Transaction Data</p>
                    </div>
                    <div class="my-btn-group">
                        <button class="my-btn-rpt">Export PDF</button>
                        <button class="my-btn-rpt">See Detail</button>
                    </div>
                    <p class="last-updated">Last Updated November 2019</p>
                </div>
                <div class="data-table">
                    <div class="jdl-div">
                        <p class="jdl-big">Inventory Data</p>
                    </div>
                    <div class="my-btn-group">
                        <button class="my-btn-rpt">Export PDF</button>
                        <button class="my-btn-rpt">See Detail</button>
                    </div>
                    <p class="last-updated">Last Updated November 2019</p>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
</script>
<style>
    .my-row {
        margin: 10px !important;
        border-radius: 5px;
        /* box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.3); */
        height: 80%;
    }

    .head-btn {
        padding: 20px;
        margin-bottom: 20px;
        width: 100%;
        border-bottom: 1px solid rgba(0, 0, 0, 0.3);
    }

    .my-btn {
        font-size: 14px;
        padding: 6px 20px !important;
        margin-bottom: 20px !important;
        background: transparent;
        border-color: rgba(0, 0, 0, 0.3) !important;
        color: black;
    }

    .my-btn-add {
        font-size: 14px;
        padding: 6px 20px !important;
        position: absolute;
        left: 20px;
        top: 0;
    }

    .my-btn.active {
        background: #007bff !important;
        border-color: #007bff !important;
    }

    .my-btn.switch {
        margin-bottom: 0 !important;
    }

    .my-btn.cat {
        margin-right: 15px;
    }
    
    .my-btn-group {
        display: flex;
        margin-left: 30px;
    }

    .my-btn-rpt {
        border: 1px solid #dc3545 !important;
        background: #dc3545;
        color: white;
        font-size: 12px;
        font-weight: 300;
        padding: 5px 20px !important;
        float: right;
        margin-top: 20px;
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.3);
        margin-right: 20px;
    }

    .my-btn-rpt:hover {
        background: white;
        color: #dc3545;
    }

    .tab-prod-pic {
        height: 90px !important;
        width: 60px !important;
        display: block;
        margin: 0 auto;
    }

    .data-table {
        margin: 0 auto;
        margin-top: 45px;
        width: 100%;
        background: white;
        box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);
    }

    table thead th {
        border-bottom: 1px solid black !important;
    }
    
    table tbody {
        background: #DFDFDF !important;
    }

    .sec-div {
        width: 100%;
        position: relative;
    }

    .jdl-div {
        background: #dc3545;
        padding: 10px;
        margin-bottom: 20px;
    }

    .jdl-big {
        font-size: 20px;
        font-weight: 300;
        width: 100%;
        margin-bottom: 0;
        color: white;
    }

    p.last-updated {
        font-size: 12px;
        font-weight: 700;
        margin: 0;
        margin-top: 10px;
        margin-left: 30px;
        margin-bottom: 30px;
    }

    .jdl-date {
        font-size: 13px;
        font-weight: 300;
        width: 100%;
        margin-bottom: 20px;
    }

    .graph {
        width: 1000px !important;
        border-radius: 5px;
        box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);
        margin: 20px auto;
        /* padding: 0 5px; */
    }

    .my-tab-th {
        font-size: 13px;
    }

    #graph1,
    #graph2 {
        margin: 0 auto !important;
        margin-top: 30px !important;
        margin-left: 10px !important;
        margin-bottom: 10px !important;
    }

    .graph-div-ttl {
        background: #DFDFDF;
        width: 100%;
        height: 40px;
        padding: 10px 20px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }

    .graph-ttl {
        color: black;
        font-size: 15px;
        font-weight: 300;
    }

    #trace-tab {
        width: 95%;
        margin: 0 auto;
        margin-top: 20px !important;
        margin-bottom: 40px !important;
    }

    .my-input-group {
        display: flex;
        margin-left: 10px;
    }

    .ttl-input {
        font-size: 14px;
        font-weight: 700;
        margin-right: 20px;
        margin-top: 3px;
    }

    .search-input {
        border-radius: 4px;
        border: .5px solid rgba(0, 0, 0, 0.3);
        height: 30px;
        padding-right: 25px;
    }

    .my-input-icon {
        font-size: 14px;
        margin-top: 8px;
        margin-left: -20px;
    }

    .my-table {
        border: 1px solid black;
    }

    .my-table-head {
        border-bottom: 1px solid red !important;
    }

    @media only screen and (max-width: 600px) {
        .my-row {
            box-shadow: none;
            margin: 0 !important;
        }

        .head-btn {
            margin-bottom: 5px;
        }

        .content {
            padding: 0 !important;
        }

        .container-fluid {
            padding: 0 !important;
        }

        .data-table {
            width: 100%;
            margin: 0 auto !important;
            font-size: 10px !important;
            margin-top: 20px !important;
            table-layout: fixed;
        }

        .btn {
            font-size: 12px !important;
        }

        .tab-prod-pic {
            height: 70px !important;
            width: 40px !important;
        }

        .sec-div {
            padding: 0 20px;
        }

        .my-btn-add {
            position: relative !important;
            width: 100%;
            margin: 0 !important;
            left: 0 !important;
            margin-top: 10px !important;
        }

        /* .dataTables_filter input {
            width: 100px !important;
        } */
    }
</style>