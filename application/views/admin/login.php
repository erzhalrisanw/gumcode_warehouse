<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/fontawesome-free/css/all.min.css"> -->
    <!-- Ionicons -->
    <script src="https://kit.fontawesome.com/15644815ae.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(''); ?>/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body class="hold-transition login-page">
    <?php
    if ($this->session->flashdata('error')) : ?>
        <script>
            swal({
                title: "Failed",
                text: "<?php echo $this->session->flashdata('error'); ?>",
                icon: 'error',
                button: false,
                timer: 2000
            });
        </script>
    <?php endif; ?>
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <img class="head-logo" src="<?php echo base_url(''); ?>/dist/img/harrasima-logo.png">
                <form action="<?php echo base_url(''); ?>index.php/login/auth" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Username" name="username">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="login-btn-div">
                        <button type="submit" class="btn btn-primary btn-block my-btn">Sign In</button>
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?php echo base_url(''); ?>/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(''); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(''); ?>/dist/js/adminlte.min.js"></script>

</body>

</html>
<style>
    .login-page {
        background: white !important;
        font-family: 'Poppins';
        background-image: url(<?= base_url('/dist/img/harrasima-bg-login.png');?>) !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .head-logo {
        display: block;
        margin: 0 auto;
        margin-bottom: 50px;
        width: 200px;
    }

    .login-btn-div {
        width: 100%;
        margin-top: 50px;
    }

    .login-card-body {
        box-shadow: 0px 0px 10px 4px rgba(0,0,0,0.3) !important;
        border-radius: 4px;
    }

    .my-btn {
        background: #DC3D45;
        border-color: #DC3D45;
    }

    .my-btn:hover {
        background: white;
        color: #DC3D45;
        border-color: #DC3D45;
    }
</style>