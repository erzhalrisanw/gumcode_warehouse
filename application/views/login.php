<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/fontawesome-free/css/all.min.css"> -->
    <!-- Ionicons -->
    <script src="https://kit.fontawesome.com/15644815ae.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(''); ?>/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url()?>/dist/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url()?>/dist/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url()?>/dist/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>/dist/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url()?>/dist/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url()?>/dist/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url()?>/dist/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url()?>/dist/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url()?>/dist/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=base_url()?>/dist/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url()?>/dist/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=base_url()?>/dist/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>/dist/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?=base_url()?>/dist/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=base_url()?>/dist/img/favicon/ms-icon-144x144.png">
</head>

<body class="hold-transition login-page">
    <?php
    if ($this->session->flashdata('error')) : ?>
        <script>
            // var fail = new Audio('<?php echo base_url(''); ?>/dist/tone/fail_tone.mp3')
            // fail.play();
            swal({
                title: "Failed",
                text: "<?php echo $this->session->flashdata('error'); ?>",
                icon: 'error',
                button: false,
                timer: 1500
            });
        </script>
    <?php endif; ?>
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <img class="head-logo" src="<?php echo base_url(''); ?>/dist/img/logo-blue.png">
                <label for="title-login" class="title-login">Gumcode Warehouse</label>
                <form action="<?php echo base_url(''); ?>index.php/login/auth" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Email" name="username" autocomplete="off">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="login-btn-div">
                        <button type="submit" class="btn btn-primary btn-block my-btn">Sign In</button>
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?php echo base_url(''); ?>/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(''); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(''); ?>/dist/js/adminlte.min.js"></script>

</body>

</html>
<style>
    .login-page {
        background: white !important;
        font-family: 'Poppins';
        background-image: url(<?= base_url('/dist/img/bg-login.jpg');?>) !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .head-logo {
        display: block;
        margin: 0 auto;
        width: 50px;
    }

    .title-login {
        color: #5588D1;
        margin: 10% 20%;
    }

    .login-btn-div {
        width: 100%;
        margin-top: 50px;
    }

    .login-card-body {
        box-shadow: 0px 0px 10px 4px rgba(0,0,0,0.3) !important;
        border-radius: 4px;
    }

    .my-btn {
        background: #5588D1;
        border-color: #5588D1;
    }

    .my-btn:hover {
        background: white;
        color: #5588D1;
        border-color: #5588D1;
    }

    @media only screen and (max-width: 600px) {
    }
</style>