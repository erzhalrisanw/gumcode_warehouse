<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Gumcode Warehouse</title>
	<script src="https://kit.fontawesome.com/15644815ae.js" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap" rel="stylesheet">

	<script src="<?php echo base_url(''); ?>/plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url(''); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

</head>
<body>
	<div class="container">
		<div class="real-div">
			<p class="page-ttl">Where do you want to go?</p>
			<a class="logout-btn" href="<?php echo base_url(''); ?>login/do_logout">Logout</a>
			<!-- <? print_r($userdata); ?> -->
			<div class="card-menu-col">
				<?php foreach ($menu as $menu) {
					echo '<a href="menu_all/index/change_domain_proceed/'.$menu['name_domain'].'/home" class="card-menu"><img width="120" src="'.base_url().'dist/img/logo-blue.png"></a>';
				} ?>
			</div>
		</div>
		<!-- <a href="" onclick="this.href='<?= base_url('menu_all/index/log_as_proceed')?>/'+document.getElementById('sel-as').value+'/'+document.getElementById('id-domain-redir').value" class="btn-proceed">Proceed</a> -->
	</div>
	<script type="text/javascript">
	</script>

	<style type="text/css">
		body {
			font-family: 'Montserrat';
			margin: 0;
			background-image: url(<?= base_url('/dist/img/bg-login.jpg');?>) !important;
			background-repeat: no-repeat !important;
			background-size: cover !important;
			position: relative;
		}

		.container {
			height: 100vh !important;
			width: 100vw !important;
		}

		.real-div {
			width: 60%;
			position: relative;
			top: 50%;
			transform: translateY(-50%);
			margin: 0 auto;
			background: white;
			border-radius: 8px;
			padding: 24px;
		}

		.card-menu-col {
			display: flex;
			/*width: 100%;*/
			padding: 24px;
			margin-top: 34px;
		}

		.card-menu {
			border: 1px solid rgba(0,0,0,.3);
			border-radius: 10px;
			margin-right: 24px;
			margin-bottom: 32px;
			padding: 12px 24px;
			cursor: pointer;
			text-decoration: none;
			box-shadow: 0px 25px 15px -20px #5588D1;
		}

		.card-menu:hover {
			transition: box-shadow .2s ease-in-out;
			box-shadow: 0px 35px 15px -30px #5588D1;
		}

		.card-menu > p {
			font-size: 20px;
			color: black;
			font-weight: 600;
			margin: 0;
			text-align: center;
			margin-top: 24px;
		}

		.page-ttl {
			font-size: 24px;
			margin: 0;
		}

		.logout-btn {
			border: 1px solid transparent;
			background: #5588D1;
			color: white;
			border-radius: 4px;
			padding: 6px 12px;
			text-decoration: none;
			position: absolute;
			top: 24px;
			right: 24px;
		}

		.logout-btn:hover {
			border: 1px solid #5588D1;
			background: white;
			color: #5588D1;
		}

		.my-form-group {
			width: 200px;
			display: block;
		}

		.select2 {
			width: 100%;
		}

		.btn-proceed {
			position: absolute;
			background: #03c100;
			color: white;
			border-radius: 4px;
			padding: 10px 30px 10px 30px;
			font-size: 14px;
			font-weight: 400;
			cursor: pointer;
			margin-left: 20px;
			bottom: 0;
			left: 230px;
			text-decoration: none;
		}

		@media only screen and (max-width: 600px) {
			.real-div {
				width: 100% !important;
			}
		}
	</style>
</body>
</html>