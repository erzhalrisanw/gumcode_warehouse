<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Warehouse List</p>
                </div>

                <div class="temp-div table-responsive">
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Country</th>
                                <th>Code</th>
                                <th>Nama</th>
                                <th>PIC</th>
                                <th>Telp</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Kota</th>
                                <th>Supervisor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($warehouse as $warehouse){ ?>
                                <?= '<tr>' ?>
                                <?= '<td>'.$warehouse['alpha2'].'</td>' ?>
                                <?= '<td>'.$warehouse['code_warehouse'].'</td>' ?>
                                <?= '<td>'.$warehouse['name_warehouse'].'</td>' ?>
                                <?= '<td>'.$fslpic[$warehouse['id_warehouse']].'</td>' ?>
                                <?= '<td>'.$fslphone[$warehouse['id_warehouse']].'</td>' ?>
                                <?= '<td>'.$fslemail[$warehouse['id_warehouse']].'</td>' ?>
                                <?= '<td>'.$warehouse['address'].'</td>' ?>
                                <?= '<td>'.$warehouse['name_city'].'</td>' ?>
                                <?= '<td>'.$spv[$warehouse['id_spv']].'</td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        tempList = $('#tempList').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "Loading...",
                zeroRecords: "Loading..."
            },
            dom: 'Bfrtip',
            buttons: [ {
                extend: 'excelHtml5',
                // customize: function( xlsx ) {
                //     var sheet = xlsx.xl.worksheets['sheet1.xml'];

                //     $('row c[r^="D"]', sheet).attr( 's', '0' );
                // },
                title: 'Warehouse List-'+new Date()
            } ]
        });
    });
</script>
