<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">List Inventory</p>
                </div>

                <div id="div-content">
                    <form id="this-form">
                        <div class="my-form-group">
                            <input type="text" name="input" id="input" class="form-control" placeholder="Masukkan PN List">
                        </div>
                    </form>
                    <button id="saveForm" type="button" class="btn btn-success"><i id="loading-upload-mdn" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
                </div>

                <div class="temp-div table-responsive">
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Brand</th>
                                <th>Basecode</th>
                                <th>PN</th>
                                <th>Description</th>
                                <th>Weight</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        tempList = $('#tempList').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data To Show",
                zeroRecords: "No Data To Show"
            }
        });

        $('.temp-div').hide();
        $('#loading-upload-mdn').hide();

        $('#saveForm').click(function(){
            if($('#input').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $('#loading-upload-mdn').show();
                tempList.clear().draw();
                $.ajax({
                    url: "<?php echo base_url('huawei/datalist/get_pn_by_input');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form').serialize(),
                    success : function(data){
                        $(data).each(function(k,v) {
                            tempList.row.add([
                                v.name_brand,
                                v.name_basecode,
                                v.name_pn,
                                v.description,
                                v.weight
                                ]).draw( false );
                        });
                        $('#loading-upload-mdn').hide();
                        $('.temp-div').show();
                    },
                });
            }
        });
    });
</script>
