<div class="content-wrapper">
	<section class="content">
		<div class="container-fluid">
			<div class="row my-row">
				<div class="header-jdl">
					<p class="jdl-big">Search Inventory</p>
				</div>
				<div class="all-menu-filter">
					<div style="padding: 24px 0;">
						<div class="my-form-group">
							<input class="form-control" type="text" name="input_search_hst" id="input_search_hst" placeholder="search...">
						</div>
						<div class="btn-process mt-4">
							<button id="search_sn_hst" type="button" class="btn btn-primary">Search SN History</button>
						</div>
					</div>
					<div style="padding: 24px 0;">
						<p>Search Inventory</p>
						<form id="form_search_inventory">
							<div class="my-form-group">
								<input class="form-control" type="text" name="i_pn" id="i_pn" placeholder="PN">
							</div>
							<div class="my-form-group" style="display: block;">
								<p style="margin: 0;" class="my-label-input">SN :</p>
								<textarea name="i_sn" id="i_sn" class="form-control" rows="3"></textarea>
								<!-- <input class="form-control" type="text" name="i_sn" id="i_sn" placeholder="SN"> -->
							</div>
							<div class="my-form-group">
								<input class="form-control" type="text" name="i_desc" id="i_desc" placeholder="Description">
							</div>
							<div class="my-form-group">
								<input class="form-control" type="text" name="i_bin" id="i_bin" placeholder="Loc Bin">
							</div>
							<div class="my-form-group">
								<input class="form-control input-datetime" type="text" name="i_stock_date" id="i_stock_date" placeholder="Stocktake time">
							</div>
							<div style="display: block; margin-top: 12px;" class="my-form-group">
								<p class="my-label-input">Stock Status :</p>
								<select id="i_stock_status" name="i_stock_status" class="form-control">
									<option value="">Empty</option>
									<?php foreach ($stock_stat as $stock_stat) { ?>
										<option value="<?= $stock_stat['id_stockstatus'] ?>"><?= $stock_stat['name_stockstatus'] ?></option>
									<?php } ?>
								</select>
							</div>
							<div style="display: block; margin-top: 12px;" class="my-form-group">
								<p class="my-label-input">Stock Position :</p>
								<select id="i_stock_position" name="i_stock_position" class="form-control">
									<option value="">Empty</option>
									<?php foreach ($stock_pos as $stock_pos) { ?>
										<option value="<?= $stock_pos['id_stockposition'] ?>"><?= $stock_pos['name_stockposition'] ?></option>
									<?php } ?>
								</select>
							</div>
							<div style="display: block; margin-top: 12px;" class="my-form-group">
								<p class="my-label-input">Stock Access :</p>
								<select id="i_stock_access" name="i_stock_access" class="form-control">
									<option value="">Empty</option>
									<?php foreach ($stock_acc as $stock_acc) { ?>
										<option value="<?= $stock_acc['id_stockaccess'] ?>"><?= $stock_acc['name_stockaccess'] ?></option>
									<?php } ?>
								</select>
							</div>
							<div style="display: block; margin-top: 12px;" class="my-form-group">
								<p class="my-label-input">Warehouse :</p>
								<select id="id_warehouse" name="id_warehouse" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach ($to as $to) { ?>
										<option value="<?= $to['id_warehouse'] ?>"><?= $to['name_warehouse'] ?></option>
									<?php } ?>
								</select>
							</div>
						</form>
						<div class="btn-process mt-4">
							<button id="search_inventory" type="button" class="btn btn-primary">Search</button>
						</div>
					</div>
					<div style="margin-top:20px;">
						<p>Search Request</p>
						<form id="form_search_request">
							<div class="my-form-group">
								<input class="form-control" type="text" name="req_pn_req" id="req_pn_req" placeholder="PN Request">
							</div>
							<div class="my-form-group">
								<input class="form-control" type="text" name="req_pn_del" id="req_pn_del" placeholder="PN Delivery">
							</div>
							<div class="my-form-group">
								<input class="form-control" type="text" name="req_pn_pu" id="req_pn_pu" placeholder="PN Pickup">
							</div>
							<div class="my-form-group" style="display: block;">
								<p style="margin: 0;" class="my-label-input">SN Delivery :</p>
								<textarea name="req_sn_del" id="req_sn_del" class="form-control" rows="3"></textarea>
							</div>
							<div class="my-form-group" style="display: block;">
								<p style="margin: 0;" class="my-label-input">SN Pickup :</p>
								<textarea name="req_sn_pu" id="req_sn_pu" class="form-control" rows="3"></textarea>
							</div>
							<div class="my-form-group" style="display: block;">
								<p style="margin: 0;" class="my-label-input">RMR :</p>
								<textarea name="req_rmr" id="req_rmr" class="form-control" rows="3"></textarea>
							</div>
							<div class="my-form-group" style="display: block;">
								<p style="margin: 0;" class="my-label-input">MDN :</p>
								<textarea name="req_mdn" id="req_mdn" class="form-control" rows="3"></textarea>
							</div>
							<div class="my-form-group" style="display: block;">
								<p style="margin: 0;" class="my-label-input">CMN :</p>
								<textarea name="req_cmn" id="req_cmn" class="form-control" rows="3"></textarea>
							</div>
							<div class="my-form-group">
								<input class="form-control" type="text" name="req_site" id="req_site" placeholder="TT/Site">
							</div>
							<div class="my-form-group">
								<input class="form-control input-datetime" type="text" name="req_date" id="req_date" placeholder="Request Date">
							</div>
							<div style="display: block;" class="my-form-group">
								<p class="my-label-input">DOP :</p>
								<select id="req_dop" name="req_dop" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach ($dop as $dop) { ?>
										<option value="<?= $dop['id_dop'] ?>"><?= $dop['name_dop'] ?>, address: <?= $dop['alamat_dop'] ?></option>
									<?php } ?>
								</select>
							</div>
							<div style="display: block; margin-top: 12px;" class="my-form-group">
								<p class="my-label-input">Customer :</p>
								<select id="req_customer" name="req_customer" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach ($customer as $customer) { ?>
										<option value="<?= $customer['id_customer'] ?>"><?= $customer['name_customer'] ?></option>
									<?php } ?>
								</select>
							</div>
						</form>
						<div class="btn-process mt-4">
							<button id="search_request" type="button" class="btn btn-primary">Search</button>
						</div>
					</div>
				</div>

				<div id="search-loading" class="loadingio-spinner-spinner-audzu8pl3mb">
					<div class="ldio-yh420ne86p">
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
						<div>
						</div>
					</div>
				</div>

				<div style="margin-left: 42px; width: 70%;">
					<div style="margin: 34px 0; width: 100%;" id="table-1-div" class="table-responsive">
						<h3>SN History</h3>
						<table id="table-0" class="table table-bordered">
							<thead>
								<tr>
									<th>SN</th>
									<th>Locator</th>
									<th>Status</th>
									<th>Access</th>
									<th>Warehouse</th>
									<th>Stocktaker</th>
									<th>Stocktake Date</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<table id="table-1" class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>SN</th>
									<th>Document Number</th>
									<th>Document Date</th>
									<th>RMR</th>
									<th>Request Time</th>
									<th>POD No</th>
									<th>Pickup No</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>

					<div style="margin: 34px 0; width: 100%;" id="table-2-div" class="table-responsive">
						<h3>Inventory</h3>
						<table id="table-2" class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Warehouse</th>
									<th>Stockaccess</th>
									<th>Good</th>
									<th>Faulty</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>

					<div style="margin: 34px 0; width: 100%;" id="table-3-div" class="table-responsive">
						<table id="table-3" class="table table-bordered">
							<thead>
								<tr>
									<th>Warehouse</th>
									<th>Owner</th>
									<th>Basecode</th>
									<th>Module Name</th>
									<th>PN</th>
									<th>SN</th>
									<th>Loc Bin</th>
									<th>Status</th>
									<th>Position</th>
									<th>Access</th>
									<th>Stocktake Date</th>
									<th>Stocktaker</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>

					<div style="margin: 34px 0; margin-top: 64px; width: 100%;" id="table-4-div" class="table-responsive">
						<h3>Request</h3>
						<table id="table-4" class="table table-bordered">
							<thead>
								<tr>
									<th>Customer</th>
									<th>Request</th>
									<th>DOP</th>
									<th>Pool Origin</th>
									<th>Ticket</th>
									<th>Module Delivery</th>
									<th>MDN</th>
									<th>Module Pickup</th>
									<th>CMN</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
</section>
</div>
<script>
	$(function() {
		$('#table-1-div').hide();
		$('#table-2-div').hide();
		$('#table-3-div').hide();
		$('#table-4-div').hide();
		$('#search-loading').hide();

		table_0 = $('#table-0').DataTable({
			paging: false,
			lengthChange: false,
			searching: false,
			ordering: false,
			info: false,
			autoWidth: true,
			responsive: true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			}
		});

		table_1 = $('#table-1').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			info: true,
			autoWidth: true,
			responsive: true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			}
		});

		table_2 = $('#table-2').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			info: true,
			autoWidth: true,
			responsive: true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			},
			rowsGroup: [1]
		});

		table_3 = $('#table-3').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			info: true,
			autoWidth: true,
			responsive: true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			},
			rowsGroup: [0]
		});

		table_4 = $('#table-4').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			info: true,
			autoWidth: true,
			responsive: true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			},
			rowsGroup: [0]
		});

		$('textarea').keypress(function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				$(this).val($(this).val() + ', ');
			}
		});

		$('.input-datetime').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			locale: {
				format: 'YYYY-MM-DD'
			},
			autoUpdateInput: false
		});

		$('.input-datetime').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD'));
		});

		$('#search_sn_hst').click(function() {
			if ($('#input_search_hst').val() == '') {
				var swal_data = {
					title: 'Failed',
					icon: 'error',
					text: 'Input SN history cannot be empty!',
					button: false,
					timer: 1000
				};
				swal(swal_data).then(function() {});
			}
			table_1.clear().draw();
			table_0.clear().draw();
			$('#search-loading').show();
			$.ajax({
				url: "<?php echo base_url('huawei/report/search_sn_history'); ?>",
				type: "POST",
				dataType: 'json',
				data: {
					'input_search_hst': $('#input_search_hst').val()
				},
				success: function(data) {

					table_0.row.add([
						data.one.sn,
						data.one.locator,
						data.one.status,
						data.one.access,
						data.one.warehouse,
						data.one.stocktaker,
						data.one.stocktake_date
					]).draw();
					$(data.many).each(function(k, v) {
						table_1.row.add([
							k + 1,
							typeof v.sn !== 'undefined' ? v.sn : '',
							typeof v.doc_no !== 'undefined' ? v.doc_no : '',
							typeof v.doc_date !== 'undefined' ? v.doc_date : '',
							typeof v.rmr !== 'undefined' ? v.rmr : '',
							typeof v.time_request !== 'undefined' ? v.time_request : '',
							typeof v.pod_no !== 'undefined' ? v.pod_no : '',
							typeof v.pickup_no !== 'undefined' ? v.pickup_no : ''
						]).draw();
					});
				},
				complete: function(data) {
					$('#table-1-div').show();
					$('#table-2-div').hide();
					$('#table-3-div').hide();
					$('#table-4-div').hide();
					$('#search-loading').hide();
				}
			});
		});

		$('#search_inventory').click(function() {
			table_2.clear().draw();
			table_3.clear().draw();
			$('#search-loading').show();
			$.ajax({
				url: "<?php echo base_url('huawei/report/search_inventory_process'); ?>",
				type: "POST",
				dataType: 'json',
				data: $('#form_search_inventory').serialize(),
				success: function(data) {
					// console.log(data);
					$(data.stock).each(function(k, v) {
						table_2.row.add([
							k + 1,
							v.name_warehouse,
							v.access,
							v.total_good,
							v.total_faulty
						]).draw();
					});

					$(data.one_by_one).each(function(a, b) {
						table_3.row.add([
							b.name_warehouse,
							b.name_project,
							b.name_basecode,
							b.description,
							b.name_pn,
							b.sn,
							b.locbin,
							b.name_stockstatus,
							b.name_stockposition,
							b.name_stockaccess,
							b.stocktake_time,
							b.name_user
						]).draw();
					});
				},
				complete: function(data) {
					$('#table-1-div').hide();
					$('#table-2-div').show();
					$('#table-3-div').show();
					$('#table-4-div').hide();
					$('#search-loading').hide();
				}
			});
		});

		$('#search_request').click(function() {
			table_4.clear().draw();
			$('#search-loading').show();
			$.ajax({
				url: "<?php echo base_url('huawei/report/search_request_process'); ?>",
				type: "POST",
				dataType: 'json',
				data: $('#form_search_request').serialize(),
				success: function(data) {
					// console.log(data);
					$(data).each(function(k, v) {
						table_4.row.add([
							v.name_customer,
							v.rmr + '</br>' + v.time_request + '</br>' + v.name_user + '</br>' + v.status_request,
							v.name_dop,
							v.name_warehouse,
							'SR Order: ' + v.order + '</br>RMA: ' + v.rma,
							'PN: ' + v.name_pn_del + '</br>SN: ' + (v.name_sn_del ? v.name_sn_del : ''),
							(v.pod_no ? v.pod_no : '') + '</br>' + (v.awb_no ? v.awb_no : '') + '</br>' + (v.actual_received ? v.actual_received : '') + '</br>' + (v.consignee ? v.consignee : ''),
							'PN: ' + (v.name_pn_pickup ? v.name_pn_pickup : '') + '</br>SN: ' + (v.name_sn_pickup ? v.name_sn_pickup : ''),
							(v.pickup_no ? v.pickup_no : '') + '</br>' + (v.actual_pickup ? v.actual_pickup : '') + '</br>' + (v.pickup_status ? v.pickup_status : '')
						]).draw(false);
					});
				},
				complete: function(data) {
					$('#table-1-div').hide();
					$('#table-2-div').hide();
					$('#table-3-div').hide();
					$('#table-4-div').show();
					$('#search-loading').hide();
					if (window.innerWidth > 768) {
						$(window).scrollTop(0);
					} else {
						$(window).scrollTop($(document).height());
					}
				}
			});
		});

	});
</script>
