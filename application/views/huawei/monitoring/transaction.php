<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Transaction</p>
                </div>
                <div class="tab-btn">
					<div class="btn-item active">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/monitor.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Monitoring</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/delivery.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">In Delivery</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/pickup.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">To Pickup</label>
						</div>	
					</div>
				</div>

                <div class="monitoring-div table-responsive" style="padding-top: 42px;">
                    <div class="request-count">
                        <p></p>
                        <p></p>
                        <p></p>
                    </div>
                    <table id="monitoring-list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Request</th>
                                <th>DOP</th>
                                <th>Pool Origin</th>
                                <th>Ticket</th>
                                <th>Module Delivery</th>
                                <th>MDN</th>
                                <th>Module Pickup</th>
                                <th>CMN</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="in-delivery-list table-responsive" style="margin-top: 42px;">
                    <table id="in-delivery-list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Request</th>
                                <th>Date Request</th>
                                <th>DOP</th>
                                <th>Origin</th>
                                <th>PN Delivery</th>
                                <th>Site</th>
                                <th>Running Time</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="to-pickup-list table-responsive" style="margin-top: 42px;">

                    <table id="to-pickup-list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Request</th>
                                <th>DOP</th>
                                <th>PN</th>
                                <th>Ticket</th>
                                <th>Consignee</th>
                                <th>Pickup Request</th>
                                <th>Aging</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="modal-edit-doc-request" class="modal fade" role="dialog">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Request Doc</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div id="body-edit-request" class="modal-body">
                        <form id="form-edit-doc">
                            <input type="hidden" id="edit_id_dop" name="edit_id_dop">
                            <input type="hidden" id="edit_id_doc" name="edit_id_doc">

                            <div class="my-form-group">
                                <p class="my-label-input">Request Time :</p>
                                <input id="edit_time_request" type="text" class="form-control input-datetime" name="edit_time_request">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Received Time :</p>
                                <input id="edit_receive_time_request" type="text" class="form-control input-datetime" name="edit_receive_time_request">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Pickup Time :</p>
                                <input id="edit_pickup_time_request" type="text" class="form-control input-datetime" name="edit_pickup_time_request">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Site :</p>
                                <input id="edit_site" name="edit_site" type="text" class="form-control" style="width:80%;">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">PN Request :</p>
                                <input id="edit_pn_req" name="edit_pn_req" type="text" class="form-control" style="width:80%;">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Pool Origin :</p>
                                <span id="lock-pool-edit" class="lock"></span>
                                <select id="edit_pool" name="edit_pool" class="form-control select2">
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">PN Delivery :</p>
                                <select id="edit_pn_del" name="edit_pn_del" class="form-control select2">
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">SLA :</p>
                                <div style="width: 50%; display: flex;">
                                    <input id="edit_sla_hour" name="edit_sla_hour" type="number" value="0" style="width: 45%;" class="form-control">&nbsp;<p style="font-size: 11px; font-weight: 400; margin-left: 5px; margin-right: 10px; margin-top: 5px;">Hour</p>&nbsp;
                                    <input id="edit_sla_minute" name="edit_sla_minute" type="number" value="0" style="width: 45%;" class="form-control">&nbsp;<p style="font-size: 11px; font-weight: 400; margin-left: 5px; margin-top: 5px;">Minutes</p>&nbsp;
                                </div>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Status Request :</p>
                                <span id="lock-stat-edit" class="lock"></span>
                                <select id="edit_checklist" name="edit_checklist" class="form-control select2">
                                    <?php foreach($check_list_edit as $check_list_edit) {?>
                                        <option value="<?=$check_list_edit['id_status_request']?>"><?=$check_list_edit['status_request']?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" id="edit_checklist_real" name="edit_checklist_real">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Remark :</p>
                                <input id="edit_remark" name="edit_remark" type="text" class="form-control" style="width:80%;">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="edit-process" type="button" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        $("#edit_checklist").attr('readonly', true);
        $("#edit_pool").attr('readonly', true);
        monitoringList = $('#monitoring-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        indeliveryList = $('#in-delivery-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        topickupList = $('#to-pickup-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('.input-datetime').daterangepicker(
        {
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            startDate: moment(),
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            }
        }
        );

        $('#edit-process').click(function(){
            if($('#edit_site').val() == '' || $('#edit_time_request').val() == '' || $('#edit_pn_req').val() == '' || $('#edit_pool').val() == '' || $('#edit_pn_del').val() == '' || $('#edit_sla_hour').val() == '' || $('#edit_sla_minute').val() == '' || $('#edit_checklist').val() == '') {
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $.ajax({
                    url: "<?php echo base_url('huawei/transaction/edit_process_save');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#form-edit-doc').serialize(),
                    success : function(data){
                        if(data){
                            var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully saved', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                                $('#modal-edit-doc-request').modal('hide');
                                // $('.tab-btn').children('button').eq(1).trigger('click');
                                if($('#lock-stat-edit').hasClass('unlocked')){
                                    $('#lock-stat-edit').trigger('click');
                                }

                                if($('#lock-pool-edit').hasClass('unlocked')){
                                    $('#lock-pool-edit').trigger('click');
                                }
                            });
                        }else{
                            var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be saved', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                        }
                    },
                });
            }
        });

        $('#edit_pn_req').on('change', function(){
            if($('#edit_pn_req').val() != ''){
                $('#edit_pn_del').html('');
                $('#edit_pool').html('');
                $.ajax({
                    url: "<?php echo base_url('huawei/transaction/get_all_pn_comp_in_warehouse');?>",
                    type : "POST",
                    dataType: 'json',
                    data: {'id_dop' : $('#edit_id_dop').val(), 'pn' : $(this).val()},
                    success : function(data){
                        if(data.length !== 0){
                            $('#edit_pool').append(new Option(data.name_warehouse, data.id_warehouse));
                            $('#edit_pool').trigger('change');
                            $(data.data).each(function(k,v) {
                                $('#edit_pn_del').append(new Option(v.name_pn, v.id_pn));
                                $('#edit_pn_del').trigger('change');
                            });
                        }
                    },
                });
            }
        });

        $( "#lock-stat-edit" ).click(function() {
			$(this).toggleClass('unlocked');
			if($(this).hasClass('unlocked')) {
				$('#edit_checklist').attr('readonly', false);
				// $('#edit_checklist').trigger('change');
			} else {
				$('#edit_checklist').attr('readonly', true);
				if($('#edit_checklist_real').val() == '0'){
					$('#edit_checklist_real').val('0');
				}
			}
		});

        $('#edit_checklist').on('change', function(){
			$('#edit_checklist_real').val($(this).val());
		});

        $( "#lock-pool-edit" ).click(function() {
            $(this).toggleClass('unlocked');
            if($(this).hasClass('unlocked')) {
                $('#edit_pool').attr('readonly', false);
                $('#edit_pool').html('');
                $('#edit_pn_del').html('');

                $.ajax({
                    url: "<?php echo base_url('huawei/transaction/get_all_pn_comp_in_all_warehouse');?>",
                    type : "POST",
                    dataType: 'json',
                    data: {'pn' : $('#edit_pn_req').val()},
                    success : function(data){
                        if(data.length !== 0){
                            $(data).each(function(k,v) {
                                $('#edit_pool').append(new Option(v.name_warehouse, v.id_warehouse));
                                $('#edit_pool').trigger('change');
                            });
                        }
                    },
                });
            } else {
                $('#edit_pool').attr('readonly', true);
                $('#edit_pn_req').trigger('change');
            }
        });

        $('#edit_pool').on('change', function(){
            $.ajax({
                url: "<?php echo base_url('huawei/transaction/get_pn_comp_by_warehouse');?>",
                type : "POST",
                dataType: 'json',
                data: {'pn' : $('#edit_pn_req').val(), 'id_warehouse' : $('#edit_pool').val()},
                success : function(data){
                    $('#edit_pn_del').html('');
                    if(data.length !== 0){
                        $(data).each(function(k,v) {
                            $('#edit_pn_del').append(new Option(v.name_pn, v.id_pn));
                            $('#edit_pn_del').trigger('change');
                        });
                    }
                },
            });
        });

        $('.tab-btn > .btn-item').click(function(){
            $('.tab-btn > .btn-item').removeClass('active');
            $(this).addClass('active');
        });

        $('.tab-btn').children('.btn-item').eq(2).click(function(){
            $('.monitoring-div').hide();
            $('.in-delivery-list').hide();
            $('.to-pickup-list').show();
            topickupList.rows().remove().draw();

            $.ajax({
                url: "<?php echo base_url('huawei/monitoring/get_all_request_to_pickup');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    let btn_print_cmn = '';

                    $(data).each(function(k,v) {

                        topickupList.row.add([
                            k+1,
                            v.rmr,
                            v.name_dop,
                            v.name_pn,
                            v.order+'</br>'+v.rma,
                            v.consignee,
                            v.pickup_request_time,
                            dhm(Math.abs(new Date(v.time_request) - new Date()))
                            ]).draw( false );
                    });
                },
            });
        });

        $('.tab-btn').children('.btn-item').eq(1).click(function(){
            $('.monitoring-div').hide();
            $('.in-delivery-list').show();
            $('.to-pickup-list').hide();
            indeliveryList.rows().remove().draw();
            let user_level = "<?= $this->session->userdata('level'); ?>";

            $.ajax({
                url: "<?php echo base_url('huawei/monitoring/get_all_request_in_delivery');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    let btn_print_dop = '';

                    $(data).each(function(k,v) {

                        indeliveryList.row.add([
                            k+1,
                            v.rmr,
                            v.time_request,
                            v.name_dop,
                            v.name_warehouse,
                            'PN: '+v.name_pn+'</br>SN: '+v.name_sn,
                            v.site_id,
                            dhm(Math.abs(new Date(v.eta_received) - new Date()))
                            ]).draw( false );
                    });
                },
            });
        });

        $('.tab-btn').children('.btn-item').eq(0).click(function(){
            $('.monitoring-div').show();
            $('.in-delivery-list').hide();
            $('.to-pickup-list').hide();

            $.ajax({
                url: "<?php echo base_url('huawei/monitoring/count_not_matter');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $('.request-count').children('p').eq(0).text('On Progress : '+data.progress);
                    $('.request-count').children('p').eq(1).text('In Delivery : '+data.deliver);
                    $('.request-count').children('p').eq(2).text('To Pickup : '+data.pickup);
                },
            });

            monitoringList.rows().remove().draw();
            let user_level = "<?= $this->session->userdata('level'); ?>";

            $.ajax({
                url: "<?php echo base_url('huawei/monitoring/transaction_io_bound_list');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $(data).each(function(k,v) {
                        var download_cmn = '';
                        var download_mdn = '';
                        var btn_edit = '';
                        var print_cmn_unsigned = '';
                        var print_mdn_unsigned = '';
                        if(v.url_photo_cmn != null){
                            download_cmn = '<a style="font-size: 10px; margin-top: 24px;" class="btn btn-success btn-sm" href="<?=base_url()?>'+v.url_photo_cmn+'" target="_blank">Download CMN</a>';
                        }

                        if(v.id_pickup != null){
                            print_cmn_unsigned = '<a style="color:white; margin-top: 24px; font-size: 10px;" href="<?=base_url()?>huawei/all_process/docPrintRequest/'+v.id_pickup+'/'+2+'" target="_blank" class="btn btn-primary btn-sm">Print CMN Unsigned</a>';
                        }

                        if(v.id_pod != null){
                            print_mdn_unsigned = '<a style="color: white; margin-top: 24px; font-size: 10px;" class="btn btn-primary btn-sm" href="<?=base_url()?>huawei/all_process/docPrintRequest/'+v.id_pod+'/'+1+'" target="_blank">Print MDN Unsigned</a>';
                        }

                        if(v.url_photo_mdn != null){
                            download_mdn = '<a style="font-size: 10px; margin-top: 24px;" class="btn btn-success btn-sm" href="<?=base_url()?>'+v.url_photo_mdn+'" target="_blank">Download MDN</a>';
                        }

                        if(user_level == 3 || user_level == 2 || user_level == 69){
                            btn_edit = '<button onclick="editDoc('+v.id_request+')" class="btn btn-warning btn-sm">Edit</button>';
                        }
                        monitoringList.row.add([
                            k+1,
                            v.rmr+'</br>'+v.time_request+'</br>'+v.name_user+'</br>'+v.status_request,
                            v.name_dop,
                            v.name_warehouse,
                            'SR Order: '+v.order+'</br>RMA: '+v.rma,
                            'PN: '+v.name_pn_del+'</br>SN: '+(v.name_sn_del ? v.name_sn_del : ''),
                            (v.pod_no ? v.pod_no : '')+'</br>'+(v.awb_no ? v.awb_no : '')+'</br>'+(v.actual_received ? v.actual_received : '')+'</br>'+(v.consignee ? v.consignee : '')+'</br>'+print_mdn_unsigned+'</br>'+download_mdn,
                            'PN: '+(v.name_pn_pickup ? v.name_pn_pickup : '')+'</br>SN: '+(v.name_sn_pickup ? v.name_sn_pickup : ''),
                            (v.pickup_no ? v.pickup_no : '')+'</br>'+(v.actual_pickup ? v.actual_pickup : '')+'</br>'+(v.pickup_status ? v.pickup_status : '')+'</br>'+print_cmn_unsigned+'</br>'+download_cmn,
                            btn_edit
                            ]).draw( false );
                    });
                },
            });
        });

        $('.tab-btn').children('.btn-item').eq(0).trigger('click');

    });

function dhm(t){
    var cd = 24 * 60 * 60 * 1000,
    ch = 60 * 60 * 1000,
    d = Math.floor(t / cd),
    h = Math.floor( (t - d * cd) / ch),
    m = Math.round( (t - d * cd - h * ch) / 60000),
    pad = function(n){ return n < 10 ? '0' + n : n; };
    if( m === 60 ){
        h++;
        m = 0;
    }
    if( h === 24 ){
        d++;
        h = 0;
    }

    var str = [d, pad(h), pad(m)].join(':');
    str = str.split(":");
    var rtn = str[0] + ' Day ' + str[1] + ' Hours ' + str[2] + ' Minutes';
    return rtn;
}

function editDoc(doc_id){
    $("#modal-edit-doc-request").modal('show');
    $('#edit_id_doc').val(doc_id);

    $.ajax({
        url: "<?php echo base_url('huawei/transaction/get_request_doc_detail');?>",
        type : "POST",
        dataType: 'json',
        data: {'id_request' : doc_id},
        success : function(data){
            $('#edit_time_request').val(data[0].time_request);
            $('#edit_site').val(data[0].site_id);
            $('#edit_pn_req').val(data[0].pn_req);
            $('#edit_id_dop').val(data[0].id_dop);

            $('#edit_pool').append(new Option(data[0].name_warehouse, data[0].id_warehouse));
            $('#edit_pool').val(data[0].id_warehouse);

            $('#edit_pn_del').append(new Option(data[0].pn_del, data[0].id_pn));
            $('#edit_pn_del').val(data[0].id_pn);

            if(data[0].actual_received == null){
                $('#edit_receive_time_request').val('');
                $('#edit_receive_time_request').prop('disabled', true);
            } else {
                $('#edit_receive_time_request').prop('disabled', false);
                $('#edit_receive_time_request').val(data[0].actual_received);
            }

            if(data[0].actual_pickup == null){
                $('#edit_pickup_time_request').val('');
                $('#edit_pickup_time_request').prop('disabled', true);
            } else {
                $('#edit_pickup_time_request').prop('disabled', false);
                $('#edit_pickup_time_request').val(data[0].actual_pickup);
            }

            if(data[0].id_status_request > 0){
                if(!$('#lock-stat-edit').hasClass('unlocked')){
                    $('#lock-stat-edit').trigger('click');
                    $('#edit_checklist').val(data[0].id_status_request);
                }
            } else {
                if($('#lock-stat-edit').hasClass('unlocked')){
                    $('#lock-stat-edit').trigger('click');
                    $('#edit_checklist').val(data[0].id_status_request);
                }
            }

            $('#edit_checklist_real').val(data[0].id_status_request);

            var sla = data[0].sla;
            sla = sla.split(" ");
            $('#edit_sla_hour').val(sla[0]);
            $('#edit_sla_minute').val(sla[2]);
            $('#edit_remark').val(data[0].remark);
        },
    });
}
</script>
