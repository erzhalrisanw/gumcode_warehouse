<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Performance</p>
                </div>
                <div class="my-form">
                    <form id="this-form">
                        <div class="my-form-group">
                            <p class="my-label-input">Project :</p>
                            <select id="id_project" name="id_project" class="form-control select2">
                                <?php foreach($proj as $proj) {?>
                                    <option value="<?=$proj['id_project']?>"><?=$proj['name_project']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div id="customer-list" class="my-form-group">
                            <p class="my-label-input">Customer :</p>
                            <select id="id_customer" name="id_customer" class="form-control select2">
                                <option value="all">Show All</option>
                                <?php foreach($customer as $customer) {?>
                                    <option value="<?=$customer['id_customer']?>"><?=$customer['name_customer']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">Date Range :</p>
                            <input type="text" id="input_daterange" class="form-control input-datetime" name="input_daterange">
                        </div>
                    </form>
                    <div class="btn-process mt-2">
					    <button id="saveForm" type="button" class="btn btn-primary">Proceed</button>
				    </div>
                </div>
                <div id="div-chart-col">
                    <div class="inbound-div-chart">
                        <div class="card-body">
                            <canvas id="pieChart"></canvas>
                        </div>
                        <div class="card-body">
                            <canvas id="pieChart2"></canvas>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="lineChart"></canvas>
                    </div>
                    <div class="inbound-div-form">
                        <div class="table-responsive">
                            <table id="inbound-list" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Inner city</th>
                                        <th>Outer City</th>
                                        <th>Pool to Pool</th>
                                        <th>Total</th>
                                        <th>Percentage</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        inboundList = $('#inbound-list').DataTable({
            paging      : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        let user_level = "<?= $this->session->userdata('level'); ?>";

        if(user_level == 13){
            $('#customer-list').show();
            $('#id_customer').prop('disabled', false);
        }else if(user_level == 11){
            $('#customer-list').hide();
            $('#id_customer').prop('disabled', true);
        }

        $('.input-datetime').daterangepicker(
        {
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }
        );

        $.ajax({
            url: "<?php echo base_url('huawei/monitoring/get_data_performance');?>",
            type : "POST",
            dataType: 'json',
            data: $('#this-form').serialize(),
            success : function(data){
                inboundList.row.add([
                    'In SLA',
                    Number(data.sla[0].inner_city),
                    Number(data.sla[0].outer_city),
                    Number(data.sla[0].pool_to_pool),
                    Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city),
                    ((Number((Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                    ]).draw( false );

                inboundList.row.add([
                    'Failed',
                    Number(data.failed[0].inner_city),
                    Number(data.failed[0].outer_city),
                    Number(data.failed[0].pool_to_pool),
                    Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city),
                    ((Number((Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                    ]).draw( false );

                inboundList.row.add([
                    'In Delivery',
                    Number(data.in_delivery[0].inner_city),
                    Number(data.in_delivery[0].outer_city),
                    Number(data.in_delivery[0].pool_to_pool),
                    Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city),
                    ((Number((Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                    ]).draw( false );

                inboundList.row.add([
                    'Out of Stock',
                    Number(data.oos[0].inner_city),
                    Number(data.oos[0].outer_city),
                    Number(data.oos[0].pool_to_pool),
                    Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city),
                    ((Number((Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                    ]).draw( false );

                var ctx = document.getElementById("pieChart").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'pie',
                    responsive: true,

                    maintainAspectRatio: false,
                    data: {
                        labels: ["In SLA", "In Delivery", "Failed", "OOS"],
                        datasets: [{
                            backgroundColor: [
                            "#3498db",
                            "#f1c40f",
                            "#e74c3c",
                            "black"
                            ],
                            data: [(Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city)), (Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city)), (Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city)), (Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city))]
                        }]
                    }
                });

                var ctx2 = document.getElementById("pieChart2").getContext('2d');
                var myChart2 = new Chart(ctx2, {
                    type: 'pie',
                    responsive: true,

                    maintainAspectRatio: false,
                    data: {
                        labels: ["Inner City", "Outer City", "Pool to Pool"],
                        datasets: [{
                            backgroundColor: [
                            "blue",
                            "green",
                            "yellow"
                            ],
                            data: [(Number(data.sla[0].inner_city) + Number(data.in_delivery[0].inner_city) + Number(data.failed[0].inner_city) + Number(data.oos[0].inner_city)), (Number(data.sla[0].outer_city) + Number(data.in_delivery[0].outer_city) + Number(data.failed[0].outer_city) + Number(data.oos[0].outer_city)), (Number(data.sla[0].pool_to_pool) + Number(data.in_delivery[0].pool_to_pool) + Number(data.failed[0].pool_to_pool) + Number(data.oos[0].pool_to_pool))]
                        }]
                    }
                });

                var bar_ctx = document.getElementById('lineChart').getContext('2d');
                var bar_chart = new Chart(bar_ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Inner City", "Outer City", "Pool to Pool"],
                        datasets: [
                        {
                            label: 'In SLA',
                            data: [Number(data.sla[0].inner_city), Number(data.sla[0].outer_city), Number(data.sla[0].pool_to_pool)],
                            backgroundColor: "green",
                            hoverBackgroundColor: "green",
                            hoverBorderWidth: 0
                        },
                        {
                            label: 'Failed',
                            data: [Number(data.failed[0].inner_city), Number(data.failed[0].outer_city), Number(data.failed[0].pool_to_pool)],
                            backgroundColor: "red",
                            hoverBackgroundColor: "red",
                            hoverBorderWidth: 0
                        },
                        {
                            label: 'In Delivery',
                            data: [Number(data.in_delivery[0].inner_city), Number(data.in_delivery[0].outer_city), Number(data.in_delivery[0].pool_to_pool)],
                            backgroundColor: "yellow",
                            hoverBackgroundColor: "yellow",
                            hoverBorderWidth: 0
                        },
                        {
                            label: 'OOS',
                            data: [Number(data.oos[0].inner_city), Number(data.oos[0].outer_city), Number(data.oos[0].pool_to_pool)],
                            backgroundColor: "black",
                            hoverBackgroundColor: "black",
                            hoverBorderWidth: 0
                        },
                        ]
                    },
                    options: {
                        animation: {
                            duration: 10,
                        },
                        scales: {
                          xAxes: [{ 
                            stacked: true, 
                            gridLines: { display: false },
                            barPercentage: 0.4
                        }]
                    },
                    legend: {display: true}
                }
            });
            },
        });

$('#saveForm').click(function(){
    inboundList.clear().draw();
    $.ajax({
        url: "<?php echo base_url('huawei/monitoring/get_data_performance');?>",
        type : "POST",
        dataType: 'json',
        data: $('#this-form').serialize(),
        success : function(data){
            inboundList.row.add([
                'In SLA',
                Number(data.sla[0].inner_city),
                Number(data.sla[0].outer_city),
                Number(data.sla[0].pool_to_pool),
                Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city),
                ((Number((Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                ]).draw( false );

            inboundList.row.add([
                'Failed',
                Number(data.failed[0].inner_city),
                Number(data.failed[0].outer_city),
                Number(data.failed[0].pool_to_pool),
                Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city),
                ((Number((Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                ]).draw( false );

            inboundList.row.add([
                'In Delivery',
                Number(data.in_delivery[0].inner_city),
                Number(data.in_delivery[0].outer_city),
                Number(data.in_delivery[0].pool_to_pool),
                Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city),
                ((Number((Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                ]).draw( false );

            inboundList.row.add([
                'Out of Stock',
                Number(data.oos[0].inner_city),
                Number(data.oos[0].outer_city),
                Number(data.oos[0].pool_to_pool),
                Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city),
                ((Number((Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city)) / Number(data.total_request[0].total)) * 100) ? (Number((Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city)) / Number(data.total_request[0].total)) * 100).toFixed(2) : 0) + ' %'
                ]).draw( false );

            var ctx = document.getElementById("pieChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                responsive: true,

                maintainAspectRatio: false,
                data: {
                    labels: ["In SLA", "In Delivery", "Failed", "OOS"],
                    datasets: [{
                        backgroundColor: [
                        "#3498db",
                        "#f1c40f",
                        "#e74c3c",
                        "black"
                        ],
                        data: [(Number(data.sla[0].inner_city) + Number(data.sla[0].pool_to_pool) + Number(data.sla[0].outer_city)), (Number(data.in_delivery[0].inner_city) + Number(data.in_delivery[0].pool_to_pool) + Number(data.in_delivery[0].outer_city)), (Number(data.failed[0].inner_city) + Number(data.failed[0].pool_to_pool) + Number(data.failed[0].outer_city)), (Number(data.oos[0].inner_city) + Number(data.oos[0].pool_to_pool) + Number(data.oos[0].outer_city))]
                    }]
                }
            });

            var ctx2 = document.getElementById("pieChart2").getContext('2d');
            var myChart2 = new Chart(ctx2, {
                type: 'pie',
                responsive: true,

                maintainAspectRatio: false,
                data: {
                    labels: ["Inner City", "Outer City", "Pool to Pool"],
                    datasets: [{
                        backgroundColor: [
                        "blue",
                        "green",
                        "yellow"
                        ],
                        data: [(Number(data.sla[0].inner_city) + Number(data.in_delivery[0].inner_city) + Number(data.failed[0].inner_city) + Number(data.oos[0].inner_city)), (Number(data.sla[0].outer_city) + Number(data.in_delivery[0].outer_city) + Number(data.failed[0].outer_city) + Number(data.oos[0].outer_city)), (Number(data.sla[0].pool_to_pool) + Number(data.in_delivery[0].pool_to_pool) + Number(data.failed[0].pool_to_pool) + Number(data.oos[0].pool_to_pool))]
                    }]
                }
            });

            var bar_ctx = document.getElementById('lineChart').getContext('2d');
            var bar_chart = new Chart(bar_ctx, {
                type: 'bar',
                data: {
                    labels: ["Inner City", "Outer City", "Pool to Pool"],
                    datasets: [
                    {
                        label: 'In SLA',
                        data: [Number(data.sla[0].inner_city), Number(data.sla[0].outer_city), Number(data.sla[0].pool_to_pool)],
                        backgroundColor: "green",
                        hoverBackgroundColor: "green",
                        hoverBorderWidth: 0
                    },
                    {
                        label: 'Failed',
                        data: [Number(data.failed[0].inner_city), Number(data.failed[0].outer_city), Number(data.failed[0].pool_to_pool)],
                        backgroundColor: "red",
                        hoverBackgroundColor: "red",
                        hoverBorderWidth: 0
                    },
                    {
                        label: 'In Delivery',
                        data: [Number(data.in_delivery[0].inner_city), Number(data.in_delivery[0].outer_city), Number(data.in_delivery[0].pool_to_pool)],
                        backgroundColor: "yellow",
                        hoverBackgroundColor: "yellow",
                        hoverBorderWidth: 0
                    },
                    {
                        label: 'OOS',
                        data: [Number(data.oos[0].inner_city), Number(data.oos[0].outer_city), Number(data.oos[0].pool_to_pool)],
                        backgroundColor: "black",
                        hoverBackgroundColor: "black",
                        hoverBorderWidth: 0
                    },
                    ]
                },
                options: {
                    animation: {
                        duration: 10,
                    },
                    scales: {
                      xAxes: [{ 
                        stacked: true, 
                        gridLines: { display: false },
                        barPercentage: 0.4
                    }],
                },
                legend: {display: true}
            }
        });
        },
    });
});
});
</script>
