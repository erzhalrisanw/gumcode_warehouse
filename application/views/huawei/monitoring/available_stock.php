<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Available Stock</p>
                </div>
                <div class="inbound-div">
                    <div style="display: flex; margin: 20px 0;">
                        <input style="height: 40px; width: 20%; margin: 0;" type="text" id="rmr-search" class="form-control" name="" placeholder="Masukkan stok">
                        <button style="margin-left: 24px;" id="rmr-search-process" class="btn btn-primary">Search</button>
                    </div>
                    <div id="rmr-list-div" class="table-responsive">
                        <table id="rmr-list" class="table table-bordered">
                        </table>

                        <br>
                        <br>
                        <br>

                        <table id="detail-list" class="table table-bordered">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        rmrList = $('#rmr-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            },
            columns: [
            {
                name: 'first',
                title: 'Project',
            },
            {
                title: 'Warehouse',
            },
            {
                title: 'Available',
            }, 
            {
                title: 'In Delivery',
            }
            ],
            rowsGroup: [0]
        });

        detail_list = $('#detail-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            },
            columns: [
            {
                name: 'first',
                title: 'Warehouse',
            },
            {
                title: 'Owner',
            },
            {
                title: 'Basecode',
            }, 
            {
                title: 'Module Name',
            },
            {
                title: 'PN',
            },
            {
                title: 'SN',
            },
            {
                title: 'Status',
            },
            {
                title: 'Position',
            },
            {
                title: 'Access',
            },
            {
                title: 'Stocktake Date',
            },
            {
                title: 'Stocktaker',
            }
            ],
            rowsGroup: [0]
        });

        $('#rmr-list-div').hide();

        $('#rmr-search-process').click(function(){
            rmrList.clear().draw();
            detail_list.clear().draw();
            if($('#rmr-search').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Search input cannot be empty!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $.ajax({
                    url: "<?php echo base_url('huawei/monitoring/search_available_stock');?>",
                    type : "POST",
                    dataType: 'json',
                    data: {'rmr' : $('#rmr-search').val()},
                    success : function(data){
                        $(data.overall).each(function(k,v) {
                            rmrList.row.add([
                                v.name_project,
                                v.name_warehouse,
                                v.total_available,
                                v.total_delivery
                                ]).draw();
                        });

                        $(data.detail).each(function(a,b) {
                            detail_list.row.add([
                                b.name_warehouse,
                                b.name_project,
                                b.name_basecode,
                                b.description,
                                b.name_pn,
                                b.sn,
                                b.name_stockstatus,
                                b.name_stockposition,
                                b.name_stockaccess,
                                b.stocktake_time,
                                b.name_user
                                ]).draw();
                        });
                        $('#rmr-list-div').show();
                    },
                });
            }
        });
    });

    function openModalInbound(id_request){
        $('#id_request_inbound').val(id_request);
        $('#modal-insert-inbound').modal('show');
    }

    function openModalOutbound(id_request){
        $('#id_request_outbound').val(id_request);
        $('#modal-insert-outbound').modal('show');
    }
</script>
