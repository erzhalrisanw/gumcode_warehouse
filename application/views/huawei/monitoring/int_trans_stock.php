<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Int. Transfer Stock</p>
                </div>
                <div class="inbound-div table-responsive">
                    <table id="inbound-list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Document</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Status</th>
                                <th>Logistic</th>
                                <th>AWB</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        inboundList = $('#inbound-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        $.ajax({
            url: "<?php echo base_url('huawei/monitoring/get_int_trans_stock_data');?>",
            type : "GET",
            dataType: 'json',
            success : function(data){
                $(data).each(function(k,v) {
                    inboundList.row.add([
                        k+1,
                        v.doc_no,
                        v.from,
                        v.to,
                        v.status,
                        v.name_logistic,
                        v.awb_no
                        ]).draw( false );
                });
            },
        });
    });
</script>
