<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Reset Stocktake</p>
                </div>
                <div id="manage-add">
                    <form id="this-form">
                        <div class="input-div">
                            <div class="my-form-group" style="margin-bottom: 24px;">
                                <p class="my-label-input" style="width: 50%;">Type :</p>
                                <span>
                                    <input type="radio" name="type_name" value="1" checked="checked" style="margin: 0 50%;"></<input>
                                    <p class="my-label-input" style="margin: 0; text-align: center;">By Warehouse Class</p>
                                </span>
                                <span>
                                    <input type="radio" name="type_name" value="2" style="margin: 0 50%;"></input>
                                    <p class="my-label-input" style="margin: 0; text-align: center;">One By One FSL</p>
                                </span>
                            </div>
                            <div id="warehouse-class-input" class="my-form-group">
                                <p class="my-label-input">Warehouse Class :</p>
                                <select id="id_warehouseclass" name="id_warehouseclass" class="form-control select2">
                                    <?php foreach($id_warehouseclass as $id_warehouseclass) {?>
                                        <option value="<?=$id_warehouseclass['id_warehouseclass']?>"><?=$id_warehouseclass['name_warehouseclass']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">FSL :</p>
                                <select id="id_fsl" name="id_fsl" class="form-control select2">
                                    <?php foreach($fsl as $fsl) {?>
                                        <option value="<?=$fsl['id_warehouse']?>"><?=$fsl['name_warehouse']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </form>
                    <div class="btn-process mt-4">
					    <button id="saveForm" type="button" class="btn btn-success mr-4">Reset</button>
				    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#warehouse-class-input').show();
        $('#fsl-input').hide();
        $('input[name="type_name"]').change(function(){
            if($(this).val() == 1){
                $('#warehouse-class-input').show();
                $('#fsl-input').hide();
            }else{
                $('#warehouse-class-input').hide();
                $('#fsl-input').show();
            }
        });

        $('#saveForm').click(function(){
            $.ajax({
                url: "<?php echo base_url('huawei/manage_data/reset_stocktake_process');?>",
                type : "POST",
                dataType: 'json',
                data: $('#this-form').serialize(),
                success : function(data){
                    if(data){
                        var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully updated', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                            location.reload();
                        });
                    }
                },
            });
        });
    });
</script>


