<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Add Requestor</p>
                </div>
                <div id="manage-add">
                    <form id="this-form">
                        <div class="input-div">
                            <div id="warehouse-class-input" class="my-form-group">
                                <p class="my-label-input">Project :</p>
                                <select id="id_project" name="id_project" class="form-control select2">
                                    <option value="">Please Select Project</option>
                                    <?php foreach($project as $project) {?>
                                        <option value="<?=$project['id_project']?>"><?=$project['name_project']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Customer :</p>
                                <select id="id_customer" name="id_customer" class="form-control select2">
                                </select>
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Region :</p>
                                <select id="id_region" name="id_region" class="form-control select2">
                                </select>
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Name :</p>
                                <input type="text" name="name" id="name" class="form-control" style="width: 70%;" onkeyup="this.value = this.value.toUpperCase();">
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Email :</p>
                                <input type="text" name="email" id="email" class="form-control" style="width: 70%;">
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Phone :</p>
                                <input type="number" name="phone" id="phone" class="form-control" style="width: 70%;">
                            </div>
                        </div>
                    </form>
                    <div class="btn-process mt-4">
					    <button id="saveForm" type="button" class="btn btn-success mr-4">Proceed</button>
				    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#id_project').on('change', function(){
            $('#id_customer').html('');
            $.ajax({
                url: "<?php echo base_url('huawei/manage_data/get_customer_list_requestor');?>",
                type : "POST",
                dataType: 'json',
                data: {'id_project' : $(this).val()},
                success : function(data){
                    $(data).each(function(k,v) {
                        $('#id_customer').append(new Option(v.name_customer, v.id_customer));
                        $('#id_customer').trigger('change');
                    });
                },
            });
        });

        $('#id_customer').on('change', function(){
            $('#id_region').html('');
            $.ajax({
                url: "<?php echo base_url('huawei/manage_data/get_region_list_requestor');?>",
                type : "POST",
                dataType: 'json',
                data: {'id_customer' : $(this).val()},
                success : function(data){
                    $(data).each(function(k,v) {
                        $('#id_region').append(new Option(v.name_region, v.id_region));
                        $('#id_region').trigger('change');
                    });
                },
            });
        });

        $('#saveForm').click(function(){
            if($('#id_project').val() == '' || $('#id_customer').val() == '' || $('#id_region').val() == '' || $('#name').val() == '' || $('#email').val() == '' || $('#phone').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $.ajax({
                    url: "<?php echo base_url('huawei/manage_data/add_requestor_process');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form').serialize(),
                    success : function(data){
                        if(data){
                            var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully updated', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        }
                    },
                });
            }
        });
    });
</script>



