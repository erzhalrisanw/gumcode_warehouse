<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Change Status</p>
                </div>
                <div id="manage-add">
                    <form id="this-form">
                        <div class="input-div">
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Serial Numbers :</p>
                                <textarea name="sn" id="sn" class="form-control" rows="10"></textarea>
                            </div>
                        </div>
                    </form>
                    <div class="btn-process mt-4">
					    <button id="saveForm" type="button" class="btn btn-success mr-4">Proceed</button>
				    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('textarea').keypress(function(e){
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).val($(this).val() + ', ');
            }
        });
        $('#saveForm').click(function(){
            if($('#sn').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $.ajax({
                    url: "<?php echo base_url('huawei/manage_data/change_status_process');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form').serialize(),
                    success : function(data){
                        if(data){
                            var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully updated', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        }else{
                            var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                        }
                    },
                });
            }
        });
    });
</script>

