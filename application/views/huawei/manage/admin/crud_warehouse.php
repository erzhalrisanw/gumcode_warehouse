<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD Warehouse</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create Warehouse</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($first as $first) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $first['name_warehouse'] . '</td>' ?>
                                <?= '<td>' . $first['code_warehouse'] . '</td>' ?>
                                <?= '<td>' . $first['address'] . '</td>' ?>
                                <?= '<td>' . $first['name_city'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $first['id_warehouse'] . ')">Delete Warehouse</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_city(' . $first['id_warehouse'] . ')">Edit Warehouse</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Warehouse</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_warehouse" name="name_warehouse" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_warehouse" name="code_warehouse" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Address :</p>
                        <textarea name="address_warehouse" class="form-control" style="width: 70%;"></textarea>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Warehouse Class :</p>
                        <select id="id_warehouseclass" name="id_warehouseclass" class="form-control select2">
                            <?php foreach ($dm_warehouseclass as $dm_warehouseclass) { ?>
                                <option value="<?= $dm_warehouseclass['id_warehouseclass'] ?>"><?= $dm_warehouseclass['name_warehouseclass'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">City :</p>
                        <select id="id_city" name="id_city" class="form-control select2">
                            <?php foreach ($dm_city as $dm_city) { ?>
                                <option value="<?= $dm_city['id_city'] ?>"><?= $dm_city['name_city'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">SPV :</p>
                        <select id="id_spv" name="id_spv" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($dt_user as $dt_user) { ?>
                                <option value="<?= $dt_user['id_user'] ?>"><?= $dt_user['name_user'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Warehouse</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form" enctype="multipart/form-data">
                    <input type="hidden" id="id_warehouse" name="id_warehouse">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_warehouse_edit" name="name_warehouse_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_warehouse_edit" name="code_warehouse_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Address :</p>
                        <textarea id="address_warehouse_edit" name="address_warehouse_edit" class="form-control" style="width: 70%;"></textarea>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Warehouse Class :</p>
                        <select id="id_warehouseclass_edit" name="id_warehouseclass_edit" class="form-control select2">
                            <?php foreach ($dm_warehouseclass_edit as $dm_warehouseclass_edit) { ?>
                                <option value="<?= $dm_warehouseclass_edit['id_warehouseclass'] ?>"><?= $dm_warehouseclass_edit['name_warehouseclass'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">City :</p>
                        <select id="id_city_edit" name="id_city_edit" class="form-control select2">
                            <?php foreach ($dm_city_edit as $dm_city_edit) { ?>
                                <option value="<?= $dm_city_edit['id_city'] ?>"><?= $dm_city_edit['name_city'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">SPV :</p>
                        <select id="id_spv_edit" name="id_spv_edit" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($dt_user_edit as $dt_user_edit) { ?>
                                <option value="<?= $dt_user_edit['id_user'] ?>"><?= $dt_user_edit['name_user'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_warehouse'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
        $('#edit_user_process').click(function() {
            $('#loading-edit-user').show();
            $.ajax({
                url: "<?php echo base_url('huawei/admin_manage/edit_warehouse_data'); ?>",
                type: "POST",
                dataType: 'json',
                data: $('#edit-user-form').serialize(),
                success: function(data) {
                    if (data) {
                        var swal_data = {
                            title: 'Success',
                            icon: 'success',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {
                            $('#loading-edit-user').hide();
                            location.reload();
                        });
                    } else {
                        var swal_data = {
                            title: 'Failed',
                            icon: 'error',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {});
                    }
                },
            });
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_warehouse'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_warehouse': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_city(id_warehouse) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_warehouse_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_warehouse': id_warehouse
            },
            success: function(data) {
                $('#id_warehouse').val(id_warehouse);
                $('#name_warehouse_edit').val(data.name_warehouse);
                $('#code_warehouse_edit').val(data.code_warehouse);
                $('#address_warehouse_edit').val(data.address);
                $('#id_warehouseclass_edit').val(data.id_warehouseclass);
                $('#id_warehouseclass_edit').trigger('change');
                $('#id_city_edit').val(data.id_city);
                $('#id_city_edit').trigger('change');
                $('#id_spv_edit').val(data.id_spv);
                $('#id_spv_edit').trigger('change');
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
