<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD Logistic</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create Logistic</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dm_city as $dm_city) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $dm_city['name_logistic'] . '</td>' ?>
                                <?= '<td>' . $dm_city['code_logistic'] . '</td>' ?>
                                <?= '<td>' . $dm_city['address'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $dm_city['id_logistic'] . ')">Delete Logistic</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_logistic(' . $dm_city['id_logistic'] . ')">Edit Logistic</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Logistic</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_logistic" name="name_logistic" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_logistic" name="code_logistic" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Address :</p>
                        <textarea name="address_logistic" class="form-control" style="width: 70%;"></textarea>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Phone :</p>
                        <input id="phone_logistic" name="phone_logistic" type="number" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">City :</p>
                        <select id="id_city" name="id_city" class="form-control select2">
                            <?php foreach ($dm_country as $dm_country) { ?>
                                <option value="<?= $dm_country['id_city'] ?>"><?= $dm_country['name_city'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Logistic</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form" enctype="multipart/form-data">
                    <input type="hidden" id="id_logistic" name="id_logistic">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_logistic_edit" name="name_logistic_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_logistic_edit" name="code_logistic_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Address :</p>
                        <textarea id="address_logistic_edit" name="address_logistic_edit" class="form-control" style="width: 70%;"></textarea>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Phone :</p>
                        <input id="phone_logistic_edit" name="phone_logistic_edit" type="number" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">City :</p>
                        <select id="id_city_edit" name="id_city_edit" class="form-control select2">
                            <?php foreach ($dm_country_edit as $dm_country_edit) { ?>
                                <option value="<?= $dm_country_edit['id_city'] ?>"><?= $dm_country_edit['name_city'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_logistic_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_logistic'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
        $('#edit_logistic_process').click(function() {
            $('#loading-edit-user').show();
            $.ajax({
                url: "<?php echo base_url('huawei/admin_manage/edit_logistic_data'); ?>",
                type: "POST",
                dataType: 'json',
                data: $('#edit-user-form').serialize(),
                success: function(data) {
                    if (data) {
                        var swal_data = {
                            title: 'Success',
                            icon: 'success',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {
                            $('#loading-edit-user').hide();
                            location.reload();
                        });
                    } else {
                        var swal_data = {
                            title: 'Failed',
                            icon: 'error',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {});
                    }
                },
            });
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_logistic'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_logistic': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_logistic(id_logistic) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_logistic_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_logistic': id_logistic
            },
            success: function(data) {
                $('#id_logistic').val(id_logistic);
                $('#id_city_edit').val(data.id_city);
                $('#id_city_edit').trigger('change');
                $('#name_logistic_edit').val(data.name_logistic);
                $('#code_logistic_edit').val(data.code_logistic);
                $('#address_logistic_edit').val(data.address);
                $('#phone_logistic_edit').val(data.phone);
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
