<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD Bound Type</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create Bound Type</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Warehouse Class</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dm_city as $dm_city) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $dm_city['name_io_boundtype'] . '</td>' ?>
                                <?= '<td>' . $dm_city['io_boundtype_code'] . '</td>' ?>
                                <?= '<td>' . $dm_city['name_warehouseclass'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $dm_city['id_io_boundtype'] . ')">Delete Bound Type</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_city(' . $dm_city['id_io_boundtype'] . ')">Edit Bound Type</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Bound Type</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_io_boundtype" name="name_io_boundtype" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="io_boundtype_code" name="io_boundtype_code" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">DOP :</p>
                        <select id="id_warehouse_class" name="id_warehouse_class" class="form-control select2">
                            <?php foreach ($dm_warehouseclass as $dm_warehouseclass) { ?>
                                <option value="<?= $dm_warehouseclass['id_warehouseclass'] ?>"><?= $dm_warehouseclass['name_warehouseclass'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Bound Type</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form" enctype="multipart/form-data">
                    <input type="hidden" id="id_io_boundtype" name="id_io_boundtype">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_io_boundtype_edit" name="name_io_boundtype_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="io_boundtype_code_edit" name="io_boundtype_code_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">DOP :</p>
                        <select id="id_warehouse_class_edit" name="id_warehouse_class_edit" class="form-control select2">
                            <?php foreach ($dm_warehouseclass_edit as $dm_warehouseclass_edit) { ?>
                                <option value="<?= $dm_warehouseclass_edit['id_warehouseclass'] ?>"><?= $dm_warehouseclass_edit['name_warehouseclass'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_bound_type'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
        $('#edit_user_process').click(function() {
            $('#loading-edit-user').show();
            $.ajax({
                url: "<?php echo base_url('huawei/admin_manage/edit_boundtype_data'); ?>",
                type: "POST",
                dataType: 'json',
                data: $('#edit-user-form').serialize(),
                success: function(data) {
                    if (data) {
                        var swal_data = {
                            title: 'Success',
                            icon: 'success',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {
                            $('#loading-edit-user').hide();
                            location.reload();
                        });
                    } else {
                        var swal_data = {
                            title: 'Failed',
                            icon: 'error',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {});
                    }
                },
            });
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_bound_type'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_io_boundtype': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_city(id_io_boundtype) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_boundtype_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_io_boundtype': id_io_boundtype
            },
            success: function(data) {
                $('#id_io_boundtype').val(id_io_boundtype);
                $('#name_io_boundtype_edit').val(data.name_io_boundtype);
                $('#io_boundtype_code_edit').val(data.io_boundtype_code);
                $('#id_warehouse_class_edit').val(data.id_warehouse_class);
                $('#id_warehouse_class_edit').trigger('change');
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
