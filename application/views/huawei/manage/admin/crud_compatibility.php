<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD Compatibility</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create Compatibility</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>PN</th>
                                <th>PN Compatible</th>
                                <th>Status</th>
                                <th>Type</th>
                                <th>Status Compatible</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($first as $first) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $first['pn'] . '</td>' ?>
                                <?= '<td>' . $first['name_pn'] . '</td>' ?>
                                <?= '<td>' . $first['status'] . '</td>' ?>
                                <?= '<td>' . $first['type'] . '</td>' ?>
                                <?= '<td>' . $first['status_compatible'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $first['id_compatibility'] . ')">Delete Compatibility</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_city(' . $first['id_compatibility'] . ')">Edit Compatibility</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Compatibility</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">PN :</p>
                        <input id="pn" name="pn" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">PN Compatible :</p>
                        <select id="id_pn" name="id_pn" class="form-control select2">
                            <?php foreach ($dm_pn as $dm_pn) { ?>
                                <option value="<?= $dm_pn['id_pn'] ?>"><?= $dm_pn['name_pn'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Status :</p>
                        <input id="status" name="status" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Type :</p>
                        <input id="type" name="type" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Status Compatible :</p>
                        <input id="status_compatible" name="status_compatible" type="text" class="form-control" style="width: 70%;">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Compatibility</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form" enctype="multipart/form-data">
                    <input type="hidden" id="id_compatibility" name="id_compatibility">
                    <div class="my-form-group">
                        <p class="my-label-input">PN :</p>
                        <input id="pn_edit" name="pn_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">PN Compatible :</p>
                        <select id="id_pn_edit" name="id_pn_edit" class="form-control select2">
                            <?php foreach ($dm_pn_edit as $dm_pn_edit) { ?>
                                <option value="<?= $dm_pn_edit['id_pn'] ?>"><?= $dm_pn_edit['name_pn'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Status :</p>
                        <input id="status_edit" name="status_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Type :</p>
                        <input id="type_edit" name="type_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Status Compatible :</p>
                        <input id="status_compatible_edit" name="status_compatible_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_compatible'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
        $('#edit_user_process').click(function() {
            $('#loading-edit-user').show();
            $.ajax({
                url: "<?php echo base_url('huawei/admin_manage/edit_compatibility_data'); ?>",
                type: "POST",
                dataType: 'json',
                data: $('#edit-user-form').serialize(),
                success: function(data) {
                    if (data) {
                        var swal_data = {
                            title: 'Success',
                            icon: 'success',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {
                            $('#loading-edit-user').hide();
                            location.reload();
                        });
                    } else {
                        var swal_data = {
                            title: 'Failed',
                            icon: 'error',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {});
                    }
                },
            });
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_compatible'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_compatibility': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_city(id_compatibility) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_compatibility_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_compatibility': id_compatibility
            },
            success: function(data) {
                $('#id_compatibility').val(id_compatibility);
                $('#pn_edit').val(data.pn);
                $('#status_edit').val(data.status);
                $('#type_edit').val(data.type);
                $('#status_compatible_edit').val(data.status_compatible);
                $('#id_pn_edit').val(data.id_pn);
                $('#id_pn_edit').trigger('change');
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
