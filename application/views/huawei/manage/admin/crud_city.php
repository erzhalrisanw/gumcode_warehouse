<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD City</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create City</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dm_city as $dm_city) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $dm_city['name_city'] . '</td>' ?>
                                <?= '<td>' . $dm_city['name_country'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $dm_city['id_city'] . ')">Delete City</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_city(' . $dm_city['id_city'] . ')">Edit City</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create City</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_city" name="name_city" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Country :</p>
                        <select id="id_country" name="id_country" class="form-control select2">
                            <?php foreach ($dm_country as $dm_country) { ?>
                                <option value="<?= $dm_country['id_country'] ?>"><?= $dm_country['name_country'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit City</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form" enctype="multipart/form-data">
                    <input type="hidden" id="id_city" name="id_city">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_city_edit" name="name_city_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Country :</p>
                        <select id="id_country_edit" name="id_country_edit" class="form-control select2">
                            <?php foreach ($dm_country_edit as $dm_country_edit) { ?>
                                <option value="<?= $dm_country_edit['id_country'] ?>"><?= $dm_country_edit['name_country'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();

        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_city'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
    });

    $('#edit_user_process').click(function() {
        $('#loading-edit-user').show();
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/edit_city_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: $('#edit-user-form').serialize(),
            success: function(data) {
                if (data) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        $('#loading-edit-user').hide();
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_city'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_city': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_city(id_city) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_city_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_city': id_city
            },
            success: function(data) {
                $('#id_city').val(id_city);
                $('#id_country_edit').val(data.id_country);
                $('#id_country_edit').trigger('change');
                $('#name_city_edit').val(data.name_city);
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
