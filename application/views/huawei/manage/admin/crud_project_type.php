<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD Project Type</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create Project Type</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Project</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dm_city as $dm_city) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $dm_city['name_projecttype'] . '</td>' ?>
                                <?= '<td>' . $dm_city['rmr_code'] . '</td>' ?>
                                <?= '<td>' . $dm_city['name_project'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $dm_city['id_projecttype'] . ')">Delete Project Type</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_city(' . $dm_city['id_projecttype'] . ')">Edit Project Type</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Project Type</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_projecttype" name="name_projecttype" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="rmr_code" name="rmr_code" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Project :</p>
                        <select id="id_project" name="id_project" class="form-control select2">
                            <?php foreach ($dm_project as $dm_project) { ?>
                                <option value="<?= $dm_project['id_project'] ?>"><?= $dm_project['name_project'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Project Type</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form" enctype="multipart/form-data">
                    <input type="hidden" id="id_projecttype" name="id_projecttype">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_projecttype_edit" name="name_projecttype_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="rmr_code_edit" name="rmr_code_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Project :</p>
                        <select id="id_project_edit" name="id_project_edit" class="form-control select2">
                            <?php foreach ($dm_project_edit as $dm_project_edit) { ?>
                                <option value="<?= $dm_project_edit['id_project'] ?>"><?= $dm_project_edit['name_project'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_projecttype'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
        $('#edit_user_process').click(function() {
            $('#loading-edit-user').show();
            $.ajax({
                url: "<?php echo base_url('huawei/admin_manage/edit_projecttype_data'); ?>",
                type: "POST",
                dataType: 'json',
                data: $('#edit-user-form').serialize(),
                success: function(data) {
                    if (data) {
                        var swal_data = {
                            title: 'Success',
                            icon: 'success',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {
                            $('#loading-edit-user').hide();
                            location.reload();
                        });
                    } else {
                        var swal_data = {
                            title: 'Failed',
                            icon: 'error',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {});
                    }
                },
            });
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_projecttype'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_projecttype': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_city(id_projecttype) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_projecttype_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_projecttype': id_projecttype
            },
            success: function(data) {
                $('#id_projecttype').val(id_projecttype);
                $('#name_projecttype_edit').val(data.name_projecttype);
                $('#rmr_code_edit').val(data.rmr_code);
                $('#id_project_edit').val(data.id_project);
                $('#id_project_edit').trigger('change');
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
