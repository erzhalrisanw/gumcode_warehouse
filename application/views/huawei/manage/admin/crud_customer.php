<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD Customer</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create Customer</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dm_city as $dm_city) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $dm_city['name_customer'] . '</td>' ?>
                                <?= '<td>' . $dm_city['code_customer'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $dm_city['id_customer'] . ')">Delete Customer</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_customer(' . $dm_city['id_customer'] . ')">Edit Customer</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Customer</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_customer" name="name_customer" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_customer" name="code_customer" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Tipe Customer :</p>
                        <input id="tipe_customer" name="tipe_customer" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Project Type :</p>
                        <select id="id_projecttype" name="id_projecttype" class="form-control select2">
                            <?php foreach ($dm_country as $dm_country) { ?>
                                <option value="<?= $dm_country['id_projecttype'] ?>"><?= $dm_country['name_projecttype'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Region :</p>
                        <select id="id_region" name="id_region" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($dm_region as $dm_region) { ?>
                                <option value="<?= $dm_region['id_region'] ?>"><?= $dm_region['name_region'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Customer</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form">
                    <input type="hidden" id="id_customer" name="id_customer">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_customer_edit" name="name_customer_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_customer_edit" name="code_customer_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Tipe Customer :</p>
                        <input id="tipe_customer_edit" name="tipe_customer_edit" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Project Type :</p>
                        <select id="id_projecttype_edit" name="id_projecttype_edit" class="form-control select2">
                            <?php foreach ($dm_country_edit as $dm_country_edit) { ?>
                                <option value="<?= $dm_country_edit['id_projecttype'] ?>"><?= $dm_country_edit['name_projecttype'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Region :</p>
                        <select id="id_region_edit" name="id_region_edit" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($dm_region_edit as $dm_region_edit) { ?>
                                <option value="<?= $dm_region_edit['id_region'] ?>"><?= $dm_region_edit['name_region'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_customer'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
    });
    $('#edit_user_process').click(function() {
        $('#loading-edit-user').show();
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/edit_customer_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: $('#edit-user-form').serialize(),
            success: function(data) {
                if (data) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        $('#loading-edit-user').hide();
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_customer'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_customer': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_customer(id_customer) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_customer_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_customer': id_customer
            },
            success: function(data) {
                $('#id_customer').val(id_customer);
                $('#id_projecttype_edit').val(data.id_projecttype);
                $('#id_projecttype_edit').trigger('change');
                $('#id_region_edit').val(data.id_region);
                $('#id_region_edit').trigger('change');
                $('#name_customer_edit').val(data.name_customer);
                $('#code_customer_edit').val(data.code_customer);
                $('#tipe_customer_edit').val(data.tipe_customer);
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
