<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CRUD Site</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_city">Create Site</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Name DOP</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dm_city as $dm_city) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $dm_city['name_site'] . '</td>' ?>
                                <?= '<td>' . $dm_city['code_site'] . '</td>' ?>
                                <?= '<td>' . $dm_city['name_dop'] . '</td>' ?>
                                <?= '<td><button class="btn btn-danger" onclick="delete_city(' . $dm_city['id_site'] . ')">Delete Site</button><br><button style="margin-top: 10px;" class="btn btn-info" onclick="edit_city(' . $dm_city['id_site'] . ')">Edit Site</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Site</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_site" name="name_site" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_site" name="code_site" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">DOP :</p>
                        <select id="id_dop" name="id_dop" class="form-control select2">
                            <?php foreach ($dm_dop as $dm_dop) { ?>
                                <option value="<?= $dm_dop['id_dop'] ?>"><?= $dm_dop['name_dop'] ?>, address: <?= $dm_dop['alamat_dop'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Site</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form" enctype="multipart/form-data">
                    <input type="hidden" id="id_site" name="id_site">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_site_site" name="name_site_site" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Code :</p>
                        <input id="code_site_site" name="code_site_site" type="text" class="form-control" style="width: 70%;"   >
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">DOP :</p>
                        <select id="id_dop_site" name="id_dop_site" class="form-control select2">
                            <?php foreach ($dm_dop_edit as $dm_dop_edit) { ?>
                                <option value="<?= $dm_dop_edit['id_dop'] ?>"><?= $dm_dop_edit['name_dop'] ?>, address: <?= $dm_dop_edit['alamat_dop'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_city').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if ($("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/admin_manage/force_create_site'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: $('#create-user-form').serialize(),
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });
        $('#edit_user_process').click(function() {
            $('#loading-edit-user').show();
            $.ajax({
                url: "<?php echo base_url('huawei/admin_manage/edit_site_data'); ?>",
                type: "POST",
                dataType: 'json',
                data: $('#edit-user-form').serialize(),
                success: function(data) {
                    if (data) {
                        var swal_data = {
                            title: 'Success',
                            icon: 'success',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {
                            $('#loading-edit-user').hide();
                            location.reload();
                        });
                    } else {
                        var swal_data = {
                            title: 'Failed',
                            icon: 'error',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {});
                    }
                },
            });
        });
    });

    function delete_city(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/force_delete_site'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_site': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_city(id_site) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_site_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_site': id_site
            },
            success: function(data) {
                $('#id_site').val(id_site);
                $('#name_site_site').val(data.name_site);
                $('#code_site_site').val(data.code_site);
                $('#id_dop_site').val(data.id_dop);
                $('#id_dop_site').trigger('change');
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
