<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">User List</p>
                </div>

                <div class="temp-div table-responsive">
                    <button style="margin-bottom: 34px;" class="btn btn-primary" id="create_user">Create User</button>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID User</th>
                                <th>Name User</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($user_list as $user_list) { ?>
                                <?= '<tr>' ?>
                                <?= '<td>' . $user_list['id_user'] . '</td>' ?>
                                <?= '<td>' . $user_list['name_user'] . '</td>' ?>
                                <?= '<td>' . $user_list['email'] . '</td>' ?>
                                <?= '<td><button style="margin-bottom : 10px;" class="btn btn-warning" onclick="change_password(' . $user_list['id_user'] . ')">Change Password</button></br><button class="btn btn-danger" style="margin-bottom : 10px;" onclick="delete_user(' . $user_list['id_user'] . ')">Delete User</button></br><button class="btn btn-info" onclick="edit_user(' . $user_list['id_user'] . ')">Edit User</button></td>' ?>
                                <?= '</tr>' ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>

<div id="modal-create-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create User</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="create-user-form" enctype="multipart/form-data">
                    <div class="my-form-group">
                        <p class="my-label-input">Name :</p>
                        <input id="name_user" name="name_user" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Email :</p>
                        <input id="email_user" name="email_user" type="text" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Phone :</p>
                        <input id="phone_user" name="phone_user" type="number" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Photo :</p>
                        <input id="photo_user" name="photo_user" type="file" class="form-control" style="width: 70%;">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Domain :</p>
                        <select id="id_domain" name="id_domain" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($domain as $domain) { ?>
                                <option value="<?= $domain['id_domain'] ?>"><?= $domain['name_domain'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Project :</p>
                        <select id="id_project" name="id_project" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($project as $project) { ?>
                                <option value="<?= $project['id_project'] ?>"><?= $project['name_project'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Customer :</p>
                        <select id="id_customer" name="id_customer" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($customer as $customer) { ?>
                                <option value="<?= $customer['id_customer'] ?>"><?= $customer['name_customer'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Warehouse :</p>
                        <select id="id_warehouse" name="id_warehouse" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($warehouse as $warehouse) { ?>
                                <option value="<?= $warehouse['id_warehouse'] ?>"><?= $warehouse['name_warehouse'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Level :</p>
                        <select id="id_userlevel" name="id_userlevel" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($level as $level) { ?>
                                <option value="<?= $level['id_userlevel'] ?>"><?= $level['name_userlevel'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Password :</p>
                        <input id="pass_user" name="pass_user" type="password" class="form-control" style="width: 70%;">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create_user_process" type="button" class="btn btn-success"><i id="loading-create-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-edit-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="edit-user-form">
                    <input type="hidden" name="id_user" id="id_user">
                    <div class="my-form-group">
                        <p class="my-label-input">Domain :</p>
                        <select id="id_domain_edit" name="id_domain_edit" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($domain_edit as $domain_edit) { ?>
                                <option value="<?= $domain_edit['id_domain'] ?>"><?= $domain_edit['name_domain'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Project :</p>
                        <select id="id_project_edit" name="id_project_edit" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($project_edit as $project_edit) { ?>
                                <option value="<?= $project_edit['id_project'] ?>"><?= $project_edit['name_project'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Customer :</p>
                        <select id="id_customer_edit" name="id_customer_edit" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($customer_edit as $customer_edit) { ?>
                                <option value="<?= $customer_edit['id_customer'] ?>"><?= $customer_edit['name_customer'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Warehouse :</p>
                        <select id="id_warehouse_edit" name="id_warehouse_edit" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($warehouse_edit as $warehouse_edit) { ?>
                                <option value="<?= $warehouse_edit['id_warehouse'] ?>"><?= $warehouse_edit['name_warehouse'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Level :</p>
                        <select id="id_userlevel_edit" name="id_userlevel_edit" class="form-control select2">
                            <option value="0">Empty</option>
                            <?php foreach ($level_edit as $level_edit) { ?>
                                <option value="<?= $level_edit['id_userlevel'] ?>"><?= $level_edit['name_userlevel'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Activated :</p>
                        <select id="activated_edit" name="activated_edit" class="form-control select2">
                            <option value="1">Active</option>
                            <option value="0">Non-Active</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="edit_user_process" type="button" class="btn btn-success"><i id="loading-edit-user" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script>
    $(function() {
        $('#loading-create-user').hide();
        $('#loading-edit-user').hide();
        tempList = $('#tempList').DataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: true,
            language: {
                emptyTable: "No data to show",
                zeroRecords: "No data to show"
            }
        });

        $('#create_user').click(function() {
            $('#modal-create-user').modal('show');
        });

        $('#create_user_process').click(function() {
            if (typeof $("#photo_user").prop('files')[0] == 'undefined' || $("#create-user-form").find('input').val() == '') {
                var swal_data = {
                    title: 'Failed',
                    icon: 'error',
                    text: 'Please check your data again!',
                    button: false,
                    timer: 1000
                };
                swal(swal_data).then(function() {});
            } else {
                var my_formData = new FormData($("#create-user-form")[0]);
                my_formData.append('file', $("#photo_user").prop('files')[0]);
                $('#loading-create-user').show();

                $.ajax({
                    url: "<?php echo base_url('login/force_create_user'); ?>",
                    type: "POST",
                    dataType: 'json',
                    data: my_formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        if (data.stat) {
                            var swal_data = {
                                title: 'Success',
                                icon: 'success',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {
                                $('#loading-create-user').hide();
                                location.reload();
                            });
                        } else {
                            var swal_data = {
                                title: 'Failed',
                                icon: 'error',
                                text: data.det,
                                button: false,
                                timer: 1000
                            };
                            swal(swal_data).then(function() {});
                        }
                    },
                });
            }
        });

        $('#edit_user_process').click(function() {
            $('#loading-edit-user').show();
            $.ajax({
                url: "<?php echo base_url('huawei/admin_manage/edit_user_data'); ?>",
                type: "POST",
                dataType: 'json',
                data: $('#edit-user-form').serialize(),
                success: function(data) {
                    if (data) {
                        var swal_data = {
                            title: 'Success',
                            icon: 'success',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {
                            $('#loading-edit-user').hide();
                            location.reload();
                        });
                    } else {
                        var swal_data = {
                            title: 'Failed',
                            icon: 'error',
                            text: data.det,
                            button: false,
                            timer: 1000
                        };
                        swal(swal_data).then(function() {});
                    }
                },
            });
        });
    });

    function change_password(id_user) {
        $.ajax({
            url: "<?php echo base_url('login/force_change_password'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_user': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        text: 'New Password : ' + data.data,
                        icon: 'success',
                        buttons: {
                            confirm: {
                                text: "Ok",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        }
                    };
                    swal(swal_data).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
    }

    function delete_user(id_user) {
        if (confirm('Apakah anda yakin menghapus ini?')) {
        $.ajax({
            url: "<?php echo base_url('login/force_delete_user'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_user': id_user
            },
            success: function(data) {
                if (data.stat) {
                    var swal_data = {
                        title: 'Success',
                        icon: 'success',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {
                        location.reload();
                    });
                } else {
                    var swal_data = {
                        title: 'Failed',
                        icon: 'error',
                        text: data.det,
                        button: false,
                        timer: 1000
                    };
                    swal(swal_data).then(function() {});
                }
            },
        });
        }
    }

    function edit_user(id_user) {
        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_user_data'); ?>",
            type: "POST",
            dataType: 'json',
            data: {
                'id_user': id_user
            },
            success: function(data) {
                $('#id_user').val(id_user);
                $('#id_domain_edit').val(data.id_domain);
                $('#id_domain_edit').trigger('change');
                $('#id_project_edit').val(data.id_project);
                $('#id_project_edit').trigger('change');
                $('#id_customer_edit').val(data.id_customer);
                $('#id_customer_edit').trigger('change');
                $('#id_warehouse_edit').val(data.id_warehouse);
                $('#id_warehouse_edit').trigger('change');
                $('#id_userlevel_edit').val(data.id_userlevel);
                $('#id_userlevel_edit').trigger('change');
                $('#activated_edit').val(data.activated);
                $('#activated_edit').trigger('change');
                $('#modal-edit-user').modal('show');
            },
        });
    }
</script>
