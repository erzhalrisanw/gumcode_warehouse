<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Add PN New</p>
                </div>
                <div id="manage-add">
                    <form id="this-form">
                        <div class="input-div">
                            <div class="my-form-group">
                                <p class="my-label-input">Basecode :</p>
                                <!-- <input type="text" name="basecode" id="basecode" class="form-control"> -->
                                <select id="id_basecode" name="id_basecode" class="form-control select2">
                                    <?php foreach($basecode as $basecode) {?>
                                        <option value="<?=$basecode['id_basecode']?>"><?=$basecode['name_basecode']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">PN :</p>
                                <input type="text" name="pn" id="pn" class="form-control select2">
                            </div>
                        </div>
                    </form>
                    <div class="btn-process mt-4">
					    <button id="saveForm" type="button" class="btn btn-success mr-4">Proceed</button>
				    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#saveForm').click(function(){
            if($('#pn').val() == '' || $('#id_basecode').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $.ajax({
                    url: "<?php echo base_url('huawei/manage_data/add_pn_process');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form').serialize(),
                    success : function(data){
                        if(data){
                            var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully updated', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        }
                    },
                });
            }
        });
    });
</script>
