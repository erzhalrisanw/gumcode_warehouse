<?php 
if(isset($_GET['bln'])) $setmonth=$_GET['bln']; else $setmonth=date('n');
if(isset($_GET['thn'])) $setyear=$_GET['thn']; else $setyear=date('Y');
$d=cal_days_in_month(CAL_GREGORIAN, $setmonth, $setyear);
for($i=0;$i<$d;$i++) {
    $days_this_month[]=$i+1;
}
?>

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">User Log</p>
                </div>
                <div id="user-log">
                    <form id="this-form" method=GET>
                        <div style="margin-bottom: 20px;" class="input-div">
                            <div class="my-form-group">
                                <p class="my-label-input">Month :</p>
                                <select id="id_project" name="bln" class="form-control select2">
                                    <? for($i=1;$i<=12;$i++) {?>
                                        <option <?php if($i==$setmonth) echo 'selected ';?> value="<?=$i?>"><?=date("F",mktime(0, 0, 0, $i, 1, 2019))?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div id="customer-list" class="my-form-group">
                                <p class="my-label-input">Year :</p>
                                <select id="id_customer" name="thn" class="form-control select2">
                                    <? for($i=2019;$i<=2050;$i++) {?>
                                        <option <?php if($i==$setyear) echo 'selected ';?>value="<?=$i?>"><?=$i?></option>
                                    <? } ?>
                                </select>
                            </div>
                         </div>
                         <div class="btn-process">
					        <button id="form-proceed" type="button" class="btn btn-primary mr-4">Go</button>
				        </div>
                    </form>
                    <div class="statistic mt-5 mb-4">Statistic <?=date("M Y",mktime(0, 0, 0, $setmonth, 1, $setyear))?></div>
                    <div class="div-chart">
                        <div class="chart">
                            <canvas id="lineChart-1" style="height:250px; min-height:250px;"></canvas>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="inbound-list" class="table table-bordered">
                            <thead>
                                <tr>
                                    <?php
                                    echo "<th>".date("M Y",mktime(0, 0, 0, $setmonth, 1, $setyear))."</th>";
                                    for($i=0;$i<$d;$i++) {
                                        echo "<th>";
                                        $chkdow=date("w",mktime(0, 0, 0, $setmonth, $i+1, $setyear));
                                        if($chkdow==0 || $chkdow==6) 
                                            echo "<font color=red>".($i+1)."</font>";
                                            else echo ($i+1);
                                            echo "</th>";
                                        }
                                        ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($log as $log){
                                    $viewlog[$log['id_user']]['name']=$log['name_user'];
                                    $viewlog[$log['id_user']][$log['userday']]=$log['usercount'];
                                }
                                if(isset($viewlog)) {
                                    foreach($viewlog as $id=>$row) {
                                        echo "<tr>";
                                        echo "<td>".$row['name']."</td>";
                                        for($i=0;$i<$d;$i++) {
                                            $chkdow=date("w",mktime(0, 0, 0, $setmonth, $i+1, $setyear));
                                            if(isset($row[$i+1])) {
                                                echo "<td bgcolor=#ffffe6>";
                                                if($chkdow==0 || $chkdow==6) 
                                                        echo "<font color=red>".$row[$i+1]."</font>";
                                                    else
                                                        echo "<font color=blue>".$row[$i+1]."</font>";
                                                } else echo "<td>";
                                                echo "</td>";
                                            }
                                        echo "</tr>";
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $(function () {
        all_data();
    });

    function all_data(){

        var 
        setMonth = <?php print $setmonth;?>,
        setYear = <?php print $setyear;?>,
        today = new Date(setYear, setMonth, 1),
        firstDay = new Date(setYear, setMonth, 1),
        lastDay = new Date(setYear, setMonth + 1, 1);

        var days_this_month = <?php echo json_encode($days_this_month); ?>;
        var linechart_1 = $('#lineChart-1').get(0).getContext('2d');

        $.ajax({
            url: "<?php echo base_url('huawei/admin_manage/get_user_log');?>",
            data: { bln: "<?php echo $setmonth;?>", thn : "<?php echo $setyear;?>"} ,
            type : "GET",
            dataType: 'json',
            // data: {'days_this_month' : days_this_month},
            success : function(data){
                // console.log(data);
                var dailyData = {
                    labels  : days_this_month,
                    datasets: [{
                        label               : 'Login',
                        backgroundColor     : '#0B79FA',
                        borderColor         : 'rgba(11,121,250,.7)',
                        pointRadius         : 4,
                        pointHoverRadius    : 4,
                        fill                : false,
                        pointColor          : '#0B79FA',
                        pointStrokeColor    : 'rgba(11,121,250,.7)',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(11,121,250,.7)',
                        data                : data
                    }]
                };

                var lineChart = new Chart(linechart_1, { 
                    type: 'line',
                    easing: 'linear',
                    data: dailyData, 
                    options: {
                        maintainAspectRatio : false,
                        responsive : true,
                        legend: { display: false },
                        scales: {
                            xAxes: [{
                                gridLines : {
                                    display : false,
                                }
                            }],
                            yAxes: [{
                                gridLines : {
                                    display : false,
                                }
                            }]
                        }
                    }
                });
            },
        });


        //setTimeout(function(){ all_data(); }, 300000);
}

</script>



