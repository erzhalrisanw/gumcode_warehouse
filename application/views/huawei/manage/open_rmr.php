<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Open RMR</p>
                </div>
                <div class="tab-btn">
					<div class="btn-item active">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/delivery.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Delivery</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/pickup.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Pick Up</label>
						</div>	
					</div>
				</div>

                <div id="delivery-list" class="table1-div table-responsive">
                    <table id="table1" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>RMR</th>
                                <th>Request Time</th>
                                <th>User</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div id="pickup-list" class="table2-div table-responsive">
                    <table id="table2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>RMR</th>
                                <th>Request Time</th>
                                <th>User</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        table1 = $('#table1').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        table2 = $('#table2').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        $('.tab-btn > .btn-item').click(function(){
            $('.tab-btn > .btn-item').removeClass('active');
            $(this).addClass('active');
        });

        $('.tab-btn').children('.btn-item').eq(1).click(function(){
            $('.table1-div').hide();
            table2.clear().draw();

            $.ajax({
                url: "<?php echo base_url('huawei/manage_data/get_all_request_pickup');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $(data).each(function(k,v) {
                        table2.row.add([
                            k+1,
                            v.rmr,
                            v.time_request,
                            v.name_user,
                            '<button class="btn btn-danger btn-sm" onclick="openRMR('+v.id_request+', 2)">Open</button>'
                            ]).draw( false );
                    });
                },
            });

            $('.table2-div').show();
        });

        $('.tab-btn').children('.btn-item').eq(0).click(function(){
            $('.table2-div').hide();
            table1.clear().draw();

            $.ajax({
                url: "<?php echo base_url('huawei/manage_data/get_all_request_deliver');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $(data).each(function(k,v) {
                        table1.row.add([
                            k+1,
                            v.rmr,
                            v.time_request,
                            v.name_user,
                            '<button class="btn btn-danger btn-sm" onclick="openRMR('+v.id_request+', 1)">Open</button>'
                            ]).draw( false );
                    });
                },
            });
            $('.table1-div').show();
        });

        $('.tab-btn').children('.btn-item').eq(0).trigger('click');
    });

    function openRMR(doc_id, type){
        $.ajax({
            url: "<?php echo base_url('huawei/manage_data/open_rmr_processs');?>",
            type : "POST",
            dataType: 'json',
            data: {'id_request' : doc_id, 'type' : type},
            success : function(data){
                if(data){
                    var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully updated', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                        $($('.tab-btn').children('.btn-item')).each(function(k,v){
                            if($(this).hasClass('active')){
                                $(this).trigger('click');
                            }
                        });
                    });
                }else{
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be updated', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                }
            },
        });
    }
</script>
