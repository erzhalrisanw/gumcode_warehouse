<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Add DOP</p>
                </div>
                <div id="manage-add">
                    <form id="this-form">
                        <div class="input-div">
                            <div class="my-form-group">
                                <p class="my-label-input">Delivery Type :</p>
                                <select id="id_deliverytype" name="id_deliverytype" class="form-control select2">
                                    <?php foreach($del_type as $del_type) {?>
                                        <option value="<?=$del_type['id_deliverytype']?>"><?=$del_type['name_deliverytype']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Customer :</p>
                                <select id="id_customer" name="id_customer" class="form-control select2">
                                    <?php foreach($customer as $customer) {?>
                                        <option value="<?=$customer['id_customer']?>"><?=$customer['name_customer']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Warehouse :</p>
                                <select id="id_warehouse" name="id_warehouse" class="form-control select2">
                                    <?php foreach($warehouse as $warehouse) {?>
                                        <option value="<?=$warehouse['id_warehouse']?>"><?=$warehouse['name_warehouse']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Region :</p>
                                <select id="id_region" name="id_region" class="form-control select2">
                                    <?php foreach($region as $region) {?>
                                        <option value="<?=$region['id_region']?>"><?=$region['name_region']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Name :</p>
                                <input type="text" name="name" id="name" class="form-control" style="width: 70%;" onkeyup="this.value = this.value.toUpperCase();">
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">Alamat DOP :</p>
                                <textarea name="alamat" class="form-control" style="width: 70%;" rows="5" onkeyup="this.value = this.value.toUpperCase();"></textarea>
                            </div>
                            <div id="fsl-input" class="my-form-group">
                                <p class="my-label-input">SLA Pickup :</p>
                                <input type="number" name="sla" id="sla" class="form-control" style="width: 70%;">
                            </div>
                        </div>`
                    </form>
                    <div class="btn-process mt-4">
					    <button id="saveForm" type="button" class="btn btn-success mr-4">Proceed</button>
				    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#saveForm').click(function(){
            if($('#id_deliverytype').val() == '' || $('#id_customer').val() == '' || $('#id_region').val() == '' || $('#id_warehouse').val() == '' || $('#sla').val() == '' || $('#alamat').val() == '' || $('#name').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $.ajax({
                    url: "<?php echo base_url('huawei/manage_data/add_dop_process');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form').serialize(),
                    success : function(data){
                        if(data){
                            var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully updated', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                                location.reload();
                            });
                        }
                    },
                });
            }
        });
    });
</script>