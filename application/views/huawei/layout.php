<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gumcode Warehouse</title>
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/dist/css/style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/daterangepicker/daterangepicker.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

  <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/dist/img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/dist/img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/dist/img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/dist/img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/dist/img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/dist/img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/dist/img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/dist/img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>/dist/img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>/dist/img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>/dist/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>/dist/img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/dist/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?= base_url() ?>/dist/img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?= base_url() ?>/dist/img/favicon/ms-icon-144x144.png">
  

  <!-- JS -->

  <script src="<?php echo base_url(''); ?>/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?= base_url() ?>dist/js/pace/pace.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
  <script src="<?= base_url('') ?>/plugins/jquery-knob/jquery.knob.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/moment/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data.min.js" integrity="sha256-6EFCRhQs5e10gzbTAKzcFFWcpDGNAzJjkQR3i1lvqYE=" crossorigin="anonymous"></script>
   <!-- <script src="<?= base_url('') ?>/plugins/jquery-knob/jquery.knob.min.js"></script> -->

  <!-- <meta name="theme-color" content="#ffffff"> -->
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light my-head-nav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link my-nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item dropdown item-logout notif">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span id="total-notif" class="badge badge-warning navbar-badge"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div id="div-inventory">
              <span style="margin: 15px 0;" class="dropdown-item dropdown-header">Inventory</span>
              <div class="dropdown-divider"></div>
              <div id="div-inventory-cwh">
              </div>
              <div id="div-inventory-fsl">
              </div>
            </div>
            <div id="div-transaction">
              <span style="margin: 15px 0;" class="dropdown-item dropdown-header">Transaction</span>
              <div class="dropdown-divider"></div>
            </div>
            <div id="div-wrong-pass">
              <span style="margin: 15px 0;" class="dropdown-item dropdown-header">Wrong Password</span>
              <div class="dropdown-divider"></div>
            </div>
          </div>
        </li>
        <li class="nav-item dropdown item-logout logout">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-th-large"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header"><?= $this->session->userdata('email') ?></span>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url(''); ?>login/change_profile" class="dropdown-item">Change Profile
              <i class="nav-icon fas fa-key"></i>
            </a>
            <a href="<?php echo base_url(''); ?>login/do_logout" class="dropdown-item">Log Out
              <i class="nav-icon fas fa-sign-out-alt"></i>
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <aside class="main-sidebar elevation-4 nav-side">
      <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex user-panel1">
          <div class="image">
            <img class="huawei-logo" src="<?= base_url('') ?>/dist/img/logo.png">
            <!-- <h5 class="ttl-big">SYS</h5> -->
          </div>
          <div class="info">
            <h5 class="subttl-big">Gumcode</h5>
          </div>
        </div>
        <div class="user-panel mt-3 pb-3 mb-3 d-flex user-panel2">
          <div class="image">
            <img src="<?php echo $this->session->userdata('photo_profile') !== NULL ? base_url('public/upload/file_photo_profile') . '/' . $this->session->userdata('photo_profile') : base_url('/public/upload/file_photo_profile/user2-160x160.jpg') ?>" class="img-circle elevation-2 my-user-img" alt="User Image">
          </div>
          <div class="info">
            <p class="user-info-p"><?= $this->session->userdata('email'); ?></p>
          </div>
        </div>
        <nav class="mt-2 nav-side-list">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li id="menu-1" class="nav-item my-nav-item <?php echo $this->uri->segment(2) == 'home' ? 'my-active' : '' ?>">
              <a href="<?= base_url() ?>huawei/home" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>
            <li id="menu-2" class="nav-item my-nav-item has-treeview my-treeview <?php echo $this->uri->segment(2) == 'transaction' ?  : '' ?>">
              <a href="<?= base_url() . 'huawei/transaction/request' ?>" class="nav-link">
                <i class="nav-icon fas fa-calculator"></i>
                <p>
                  Transaction
                </p>
              </a>
            </li>
            <li id="menu-3" class="nav-item my-nav-item has-treeview my-treeview <?php echo $this->uri->segment(2) == 'monitoring' ? 'menu-open' : '' ?>">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-desktop"></i>
                <p>
                  Monitoring
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li id="menu-3-1" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'monitoring' && $this->uri->segment(3) == 'available_stock' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/monitoring/available_stock' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Available Stock</p>
                  </a>
                </li>
                <li id="menu-3-2" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'monitoring' && $this->uri->segment(3) == 'transaction' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/monitoring/transaction' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Transaction</p>
                  </a>
                </li>
                <li id="menu-3-3" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'monitoring' && $this->uri->segment(3) == 'int_trans_stock' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/monitoring/int_trans_stock' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Int. Transfer Stock</p>
                  </a>
                </li>
                <li id="menu-3-4" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'monitoring' && $this->uri->segment(3) == 'performance' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/monitoring/performance' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Performance</p>
                  </a>
                </li>
              </ul>
            </li>

            <li id="menu-4" class="nav-item my-nav-item has-treeview my-treeview <?php echo $this->uri->segment(2) == 'cwh' || $this->uri->segment(2) == 'fsl' ? 'menu-open' : '' ?>">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-warehouse"></i>
                <p>
                  Inventory
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li id="menu-4-1" class="nav-item has-treeview tree-item-2 <?php echo $this->uri->segment(2) == 'cwh' && ($this->uri->segment(3) == 'inbound_good' || $this->uri->segment(3) == 'inbound_faulty') ? 'menu-open' : '' ?>">
                 <a href="<?= base_url() . 'huawei/cwh/inbound_good' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>
                      CWH Inbound
                    </p>
                  </a>
                </li>

                <li id="menu-4-2" class="nav-item has-treeview tree-item-2 <?php echo $this->uri->segment(2) == 'cwh' && ($this->uri->segment(3) == 'outbound_good' || $this->uri->segment(3) == 'outbound_faulty') ? 'menu-open' : '' ?>">
                  <a href="<?= base_url() . 'huawei/cwh/outbound_good' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>
                      CWH Outbound
                    </p>
                  </a>
                </li>

                <li id="menu-4-3" class="nav-item has-treeview tree-item-2 <?php echo $this->uri->segment(2) == 'fsl' && ($this->uri->segment(3) == 'inbound_good' || $this->uri->segment(3) == 'inbound_faulty') ? 'menu-open' : '' ?>">
                  <a href="<?= base_url() . 'huawei/fsl/inbound_good' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>
                      FSL Inbound
                    </p>
                  </a>
                </li>

                <li id="menu-4-4" class="nav-item has-treeview tree-item-2 <?php echo $this->uri->segment(2) == 'fsl' && ($this->uri->segment(3) == 'outbound_good' || $this->uri->segment(3) == 'outbound_faulty') ? 'menu-open' : '' ?>">
                  <a href="<?= base_url() . 'huawei/fsl/outbound_good' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>
                      FSL Outbound
                    </p>
                  </a>
                </li>
              </ul>
            </li>

            <li id="menu-5" class="nav-item my-nav-item has-treeview my-treeview <?php echo $this->uri->segment(2) == 'counting_stock' && ($this->uri->segment(3) == 'stocktake') ? 'menu-open' : '' ?>">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-boxes"></i>
                <p>
                  Counting Stock
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li id="menu-5-1" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'counting_stock' && $this->uri->segment(3) == 'stocktake' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/counting_stock/stocktake' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Stocktake</p>
                  </a>
                </li>
                <li id="menu-5-2" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'counting_stock' && $this->uri->segment(3) == 'progress_stocktake' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/counting_stock/progress_stocktake' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Progress Stocktake</p>
                  </a>
                </li>
              </ul>
            </li>
            <li id="menu-6" class="nav-item my-nav-item has-treeview my-treeview <?php echo $this->uri->segment(2) == 'datalist' || $this->uri->segment(2) == 'report' ? 'menu-open' : '' ?>">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-file-alt"></i>
                <p>
                  Report
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li id="menu-6-1" class="nav-item has-treeview tree-item-2 <?php echo $this->uri->segment(2) == 'datalist' ? 'menu-open' : '' ?>">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>
                      Data List
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li id="menu-6-1-1" class="nav-item tree-item-3 <?php echo $this->uri->segment(2) == 'datalist' && $this->uri->segment(3) == 'warehouse' ? 'my-active' : '' ?>">
                      <a href="<?= base_url() . 'huawei/datalist/warehouse' ?>" class="nav-link">
                        <i class="far fa-dot-circle nav-icon tree-icon-2"></i>
                        <p>Warehouse</p>
                      </a>
                    </li>
                    <li id="menu-6-1-2" class="nav-item tree-item-3 <?php echo $this->uri->segment(2) == 'datalist' && $this->uri->segment(3) == 'pnlist' ? 'my-active' : '' ?>">
                      <a href="<?= base_url() . 'huawei/datalist/pnlist' ?>" class="nav-link">
                        <i class="far fa-dot-circle nav-icon tree-icon-2"></i>
                        <p>List Investory</p>
                      </a>
                    </li>
                  </ul>
                </li>
                <li id="menu-6-2" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'report' && $this->uri->segment(3) == 'search_inventory' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/report/search_inventory' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Search Inventory</p>
                  </a>
                </li>
              </ul>
            </li>

            <li id="menu-9" class="nav-item my-nav-item has-treeview my-treeview <?php echo $this->uri->segment(2) == 'manage_data' || $this->uri->segment(2) == 'admin_manage' ? 'menu-open' : '' ?>">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-database"></i>
                <p>
                  Manage Data
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li id="menu-9-1" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'manage_data' && $this->uri->segment(3) == 'add_pn_new' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/manage_data/add_pn_new' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Add PN New</p>
                  </a>
                </li>
                <li id="menu-9-2" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'manage_data' && $this->uri->segment(3) == 'add_dop' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/manage_data/add_dop' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Add DOP</p>
                  </a>
                </li>
                <li id="menu-9-3" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'manage_data' && $this->uri->segment(3) == 'add_requestor' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/manage_data/add_requestor' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Add Requestor</p>
                  </a>
                </li>
                <li id="menu-9-6" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'manage_data' && $this->uri->segment(3) == 'reset_stocktake' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/manage_data/reset_stocktake' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>Reset Stocktake</p>
                  </a>
                </li>
                <li id="menu-9-7" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'user_list' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/user_list' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>User List</p>
                  </a>
                </li>
                <li id="menu-9-9" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_city' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_city' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD City</p>
                  </a>
                </li>
                <li id="menu-9-10" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_customer' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_customer' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Customer</p>
                  </a>
                </li>
                <li id="menu-9-11" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_logistic' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_logistic' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Logistic</p>
                  </a>
                </li>
                <li id="menu-9-12" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_logistic_service' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_logistic_service' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Logistic Service</p>
                  </a>
                </li>
                <li id="menu-9-13" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_project' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_project' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Project</p>
                  </a>
                </li>
                <li id="menu-9-14" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_project_type' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_project_type' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Project Type</p>
                  </a>
                </li>
                <li id="menu-9-15" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_warehouse' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_warehouse' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Warehouse</p>
                  </a>
                </li>
                <li id="menu-9-16" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_compatibility' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_compatibility' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Compatibility</p>
                  </a>
                </li>
                <li id="menu-9-17" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_region' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_region' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Region</p>
                  </a>
                </li>
                <li id="menu-9-18" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_site' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_site' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Site</p>
                  </a>
                </li>
                <li id="menu-9-19" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'crud_bound_type' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/crud_bound_type' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>CRUD Bound Type</p>
                  </a>
                </li>
                <li id="menu-11-1" class="nav-item tree-item-2 <?php echo $this->uri->segment(2) == 'admin_manage' && $this->uri->segment(3) == 'userlog' ? 'my-active' : '' ?>">
                  <a href="<?= base_url() . 'huawei/admin_manage/userlog' ?>" class="nav-link">
                    <i class="far fa-circle nav-icon tree-icon-2"></i>
                    <p>User Log</p>
                  </a>
                </li>
                </li>
              </ul>
            </li>

            <li id="menu-10" class="nav-item my-nav-item <?php echo $this->uri->segment(2) == 'change_session_admin' ? 'my-active' : '' ?>">
              <a href="<?= base_url() ?>huawei/all_process/change_session_admin" class="nav-link">
                <i class="nav-icon fas fa-user-alt"></i>
                <p>
                  Change Session
                </p>
              </a>
            </li>

          </ul>
        </nav>
      </div>
    </aside>
  </div>
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <script src="<?php echo base_url(''); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <script src="<?php echo base_url(''); ?>/dist/js/adminlte.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <!-- <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script> -->
  <!-- <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script> -->
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/chart.js/Chart.min.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/series-label.js"></script>
  <script src="https://cdn.rawgit.com/ashl1/datatables-rowsgroup/fbd569b8768155c7a9a62568e66a64115887d7d0/dataTables.rowsGroup.js"></script>
  <script>
    $(document).ready(function() {
      $('.select2').select2();
      moment.tz.setDefault("Asia/Jakarta");

      let level = "<?= $this->session->userdata('level'); ?>";
      if (level == 1) {
        $('#menu-2').hide();
        $('#menu-3-1').hide();
        $('#menu-3-2').hide();
        $('#menu-3-3').hide();
        $('#menu-3-4').hide();
        $('#menu-4').hide();
        $('#menu-5-1').hide();
        $('#menu-7-3').hide();
        $('#menu-8').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 2) {
        $('#menu-2-4').hide();
        $('#menu-3-3').hide();
        $('#menu-3-4').hide();
        $('#menu-4').hide();
        $('#menu-5-1').hide();
        $('#menu-7-3').hide();
        $('#menu-8').hide();
        $('#menu-9-1').hide();
        $('#menu-9-5').hide();
        $('#menu-9-6').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 3) {
        $('#menu-2-4').hide();
        $('#menu-3-3').hide();
        $('#menu-3-4').hide();
        $('#menu-4').hide();
        $('#menu-5-1').hide();
        $('#menu-7-3').hide();
        $('#menu-8').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 4) {
        $('#menu-2-3').hide();
        $('#menu-2-4').hide();
        $('#menu-4-1').hide();
        $('#menu-4-2').hide();
        $('#menu-5-1').hide();
        $('#menu-7-3').hide();
        $('#menu-8-2').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        // $('#menu-10').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 5) {
        $('#menu-2').hide();
        $('#menu-3-2').hide();
        $('#menu-3-5').hide();
        $('#menu-4-3').hide();
        $('#menu-4-4').hide();
        $('#menu-7-3').hide();
        $('#menu-8-2').hide();
        $('#menu-9-2').hide();
        $('#menu-9-3').hide();
        $('#menu-9-4').hide();
        $('#menu-9-6').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 6) {
        $('#menu-2').hide();
        $('#menu-3-2').hide();
        $('#menu-3-3').hide();
        $('#menu-3-5').hide();
        $('#menu-4-3').hide();
        $('#menu-4-4').hide();
        $('#menu-4-1-2').hide();
        $('#menu-4-2-2').hide();
        $('#menu-7-3').hide();
        $('#menu-8-2').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 7) {
        $('#menu-2').hide();
        $('#menu-3-2').hide();
        $('#menu-3-3').hide();
        $('#menu-3-4').hide();
        $('#menu-3-5').hide();
        $('#menu-4-3').hide();
        $('#menu-4-4').hide();
        $('#menu-4-1-1').hide();
        $('#menu-4-2-1').hide();
        $('#menu-7-3').hide();
        $('#menu-8-2').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 8) {
        $('#menu-2-1').hide();
        $('#menu-2-2').hide();
        $('#menu-2-3').hide();
        $('#menu-3').hide();
        $('#menu-4').hide();
        $('#menu-5-1').hide();
        $('#menu-7-3').hide();
        $('#menu-8').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 9) {
        $('#menu-2').hide();
        $('#menu-3').hide();
        $('#menu-4').hide();
        $('#menu-5').hide();
        $('#menu-6-1-2').hide();
        $('#menu-6-1-3').hide();
        $('#menu-8-1').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 10) {
        $('#menu-2-3').hide();
        $('#menu-2-4').hide();
        $('#menu-3-1').hide();
        $('#menu-3-5').hide();
        $('#menu-4-1').hide();
        $('#menu-4-2').hide();
        $('#menu-6-2').hide();
        $('#menu-7').hide();
        $('#menu-8-2').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 11) {
        $('#menu-2').hide();
        $('#menu-3-2').hide();
        $('#menu-3-3').hide();
        $('#menu-3-4').hide();
        $('#menu-4').hide();
        $('#menu-5-1').hide();
        $('#menu-6-1-2').hide();
        $('#menu-6-1-3').hide();
        $('#menu-6-2').hide();
        $('#menu-7').hide();
        $('#menu-8').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 12) {
        $('#menu-2').hide();
        $('#menu-4-3').hide();
        $('#menu-4-4').hide();
        $('#menu-8-2').hide();
        $('#menu-7-3').hide();
        $('#menu-9-2').hide();
        $('#menu-9-3').hide();
        $('#menu-9-4').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      } else if (level == 13) {
        $('#menu-2').hide();
        $('#menu-3-2').hide();
        $('#menu-3-3').hide();
        $('#menu-3-4').hide();
        $('#menu-4').hide();
        $('#menu-5-1').hide();
        $('#menu-6-1-2').hide();
        $('#menu-6-1-3').hide();
        $('#menu-7-3').hide();
        $('#menu-8').hide();
        $('#menu-9').hide();
        $('#menu-9-7').hide();
        $('#menu-10').hide();
        $('#menu-5-3').hide();
        $('#menu-11').hide();
        $('#menu-12').hide();
      }

      if (level == 10 || level == 6 || level == 7 || level == 5 || level == 8 || level == 4 || level == 69) {
        $.ajax({
          url: "<?php echo base_url('huawei/all_process/get_notif_data'); ?>",
          type: "GET",
          dataType: 'json',
          success: function(data) {
            if (level == 10) {
              $('#div-wrong-pass').hide();
              $(data.i_good).each(function(k, v) {
                $('#div-inventory-fsl').append('<a href="<?= base_url() . 'huawei/fsl/inbound_good' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Good</i></a>');
              });

              $(data.i_faulty).each(function(k, v) {
                $('#div-inventory-fsl').append('<a href="<?= base_url() . 'huawei/fsl/inbound_faulty' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Faulty</i></a>');
              });

              $(data.to_deliv).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/request' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Delivery</i></a>');
              });

              $(data.to_deliv_ria).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/ria' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Delivery RIA</i></a>');
              });

              $(data.to_pickup).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/request' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Pickup</i></a>');
              });

              $(data.to_pickup_ria).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/ria' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Pickup RIA</i></a>');
              });

              $(data.request_on_deliv).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/request' ?>" class="dropdown-item"><i>' + v.rmr + ' - on Delivery</i></a>');
              });

              $(data.request_on_deliv_ria).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/ria' ?>" class="dropdown-item"><i>' + v.rmr + ' - on Delivery RIA</i></a>');
              });

              $('#total-notif').text(data.request_on_deliv.length + data.i_faulty.length + data.i_good.length + data.to_deliv.length + data.to_deliv_ria.length + data.to_pickup.length + data.to_pickup_ria.length);
            } else if (level == 6) {
              $('#div-inventory-fsl').hide();
              $('#div-transaction').hide();
              $('#div-wrong-pass').hide();
              $(data.i_good).each(function(k, v) {
                $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_good' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Good</i></a>');
              });

              $('#total-notif').text(data.i_good.length);
            } else if (level == 7) {
              $('#div-inventory-fsl').hide();
              $('#div-transaction').hide();
              $('#div-wrong-pass').hide();
              $(data.i_faulty).each(function(k, v) {
                $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_faulty' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Faulty</i></a>');
              });

              $('#total-notif').text(data.i_faulty.length);
            } else if (level == 5) {
              $('#div-inventory-fsl').hide();
              $('#div-transaction').hide();
              $('#div-wrong-pass').hide();

              $(data.i_good).each(function(k, v) {
                $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_good' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Good</i></a>');
              });
              $(data.i_faulty).each(function(k, v) {
                $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_faulty' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Faulty</i></a>');
              });

              $('#total-notif').text(data.i_faulty.length + data.i_good.length);
            } else if (level == 8) {
              $('#div-inventory').hide();
              $('#div-wrong-pass').hide();
              $(data.i_order).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/icare' ?>" class="dropdown-item"><i>' + v.rmr + ' - Inbound Order</i></a>');
              });
              $(data.o_order).each(function(k, v) {
                $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/icare' ?>" class="dropdown-item"><i>' + v.rmr + ' - Outbound Order</i></a>');
              });
              $('#total-notif').text(data.i_order.length + data.o_order.length);
            } else if (level == 4) {
              $('#div-inventory').hide();
              $('#div-wrong-pass').hide();
              $(data.request_on_deliv).each(function(k, v) {
                $('#div-transaction').append('<a href="#" class="dropdown-item"><i>' + v.rmr + ' - DO not updated, more than SLA</i></a>');
              });
              $('#total-notif').text(data.request_on_deliv.length);
            } else if (level == 69) {
              $('#div-inventory').hide();
              $('#div-transaction').hide();
              $(data.user_wrong_pass).each(function(k, v) {
                $('#div-wrong-pass').append('<a href="#" class="dropdown-item"><i>' + v.name_user + ' - ' + v.log_wrong_pass + ' times</i></a>');
              });

              $('#total-notif').text(data.user_wrong_pass.length);
            }
          },
        });
      } else {
        $('.notif').hide();
      }

      setTimeout(function() {

        $('#div-inventory-fsl, #div-transaction, #div-inventory-cwh, #div-wrong-pass').html('');
        $('#total-notif').text('');

        if (level == 10 || level == 6 || level == 7 || level == 5 || level == 8 || level == 4 || level == 69) {
          $.ajax({
            url: "<?php echo base_url('huawei/all_process/get_notif_data'); ?>",
            type: "GET",
            dataType: 'json',
            success: function(data) {
              if (level == 10) {
                $('#div-wrong-pass').hide();
                $(data.i_good).each(function(k, v) {
                  $('#div-inventory-fsl').append('<a href="<?= base_url() . 'huawei/fsl/inbound_good' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Good</i></a>');
                });

                $(data.i_faulty).each(function(k, v) {
                  $('#div-inventory-fsl').append('<a href="<?= base_url() . 'huawei/fsl/inbound_faulty' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Faulty</i></a>');
                });

                $(data.to_deliv).each(function(k, v) {
                  $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/request' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Delivery</i></a>');
                });

                $(data.to_deliv_ria).each(function(k, v) {
                  $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/ria' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Delivery RIA</i></a>');
                });

                $(data.to_pickup).each(function(k, v) {
                  $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/request' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Pickup</i></a>');
                });

                $(data.to_pickup_ria).each(function(k, v) {
                  $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/ria' ?>" class="dropdown-item"><i>' + v.rmr + ' - to Pickup RIA</i></a>');
                });

                $(data.request_on_deliv).each(function(k, v) {
                  $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/request' ?>" class="dropdown-item"><i>' + v.rmr + ' - DO not updated, more than SLA</i></a>');
                });

                $('#total-notif').text(data.request_on_deliv.length + data.i_faulty.length + data.i_good.length + data.to_deliv.length + data.to_deliv_ria.length + data.to_pickup.length + data.to_pickup_ria.length);
              } else if (level == 6) {
                $('#div-inventory-fsl').hide();
                $('#div-transaction').hide();
                $('#div-wrong-pass').hide();
                $(data.i_good).each(function(k, v) {
                  $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_good' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Good</i></a>');
                });

                $('#total-notif').text(data.i_good.length);
              } else if (level == 7) {
                $('#div-inventory-fsl').hide();
                $('#div-transaction').hide();
                $('#div-wrong-pass').hide();
                $(data.i_faulty).each(function(k, v) {
                  $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_faulty' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Faulty</i></a>');
                });

                $('#total-notif').text(data.i_faulty.length);
              } else if (level == 5) {
                $('#div-inventory-fsl').hide();
                $('#div-transaction').hide();
                $('#div-wrong-pass').hide();

                $(data.i_good).each(function(k, v) {
                  $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_good' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Good</i></a>');
                });
                $(data.i_faulty).each(function(k, v) {
                  $('#div-inventory-cwh').append('<a href="<?= base_url() . 'huawei/cwh/inbound_faulty' ?>" class="dropdown-item"><i>' + v.doc_no + ' - Inbound Faulty</i></a>');
                });

                $('#total-notif').text(data.i_faulty.length + data.i_good.length);
              } else if (level == 8) {
                $('#div-inventory').hide();
                $('#div-wrong-pass').hide();
                $(data.i_order).each(function(k, v) {
                  $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/icare' ?>" class="dropdown-item"><i>' + v.rmr + ' - Inbound Order</i></a>');
                });
                $(data.o_order).each(function(k, v) {
                  $('#div-transaction').append('<a href="<?= base_url() . 'huawei/transaction/icare' ?>" class="dropdown-item"><i>' + v.rmr + ' - Outbound Order</i></a>');
                });
                $('#total-notif').text(data.i_order.length + data.o_order.length);
              } else if (level == 4) {
                $('#div-inventory').hide();
                $('#div-wrong-pass').hide();
                $(data.request_on_deliv).each(function(k, v) {
                  $('#div-transaction').append('<a href="#" class="dropdown-item"><i>' + v.rmr + ' - DO not updated, more than SLA</i></a>');
                });
                $('#total-notif').text(data.request_on_deliv.length);
              } else if (level == 69) {
                $('#div-inventory').hide();
                $('#div-transaction').hide();
                $(data.user_wrong_pass).each(function(k, v) {
                  $('#div-wrong-pass').append('<a href="#" class="dropdown-item"><i>' + v.name_user + ' - ' + v.log_wrong_pass + ' times</i></a>');
                });

                $('#total-notif').text(data.user_wrong_pass.length);
              }
            },
          });
        } else {
          $('.notif').hide();
        }
      }, 60000);
    });
  </script>
</body>

</html>
