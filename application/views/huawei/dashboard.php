<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="card-col">
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-primary">
                            <i class="fas fa-users"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Request</span>
                                <span class="info-box-number card-top-count1">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-info">
                            <i class="fas fa-clock"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Process</span>
                                <span class="info-box-number card-top-count2">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-warning">
                            <i class="fas fa-truck"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">In Delivery</span>
                                <span class="info-box-number card-top-count3">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-success">
                            <i class="fas fa-thumbs-up"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Success</span>
                                <span class="info-box-number card-top-count4">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-danger">
                            <i class="fas fa-thumbs-down"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Failed</span>
                                <span class="info-box-number card-top-count5">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-secondary">
                            <i class="fas fa-bell"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">OOS</span>
                                <span class="info-box-number card-top-count6">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dashboard-bg">
                    <div class="div-chart">
                        <p>Daily Request</p>
                        <div class="border-kecil">a</div>
                        <div class="chart">
                            <canvas id="lineChart-1" style="height:250px; min-height:250px"></canvas>
                        </div>
                    </div>

                    <div class="iop-div">
                        <div class="div-chart col-sm-3">
                            <p>Inner City</p>
                            <div class="border-kecil">a</div>
                            <div class="chart">
                                <canvas id="chart_donat_1" style="height:150px; min-height:150px"></canvas>
                            </div>
                        </div>

                        <div class="div-chart col-sm-3">
                            <p>Outer City</p>
                            <div class="border-kecil">a</div>
                            <div class="chart">
                                <canvas id="chart_donat_2" style="height:150px; min-height:150px"></canvas>
                            </div>
                        </div>

                        <div class="div-chart col-sm-3">
                            <p>Pool to Pool</p>
                            <div class="border-kecil">a</div>
                            <div class="chart">
                                <canvas id="chart_donat_3" style="height:150px; min-height:150px"></canvas>
                            </div>
                        </div>
                    </div>

                    <div style="margin-top: 42px;" class="div-chart">
                        <p>Busy Days</p>
                        <div class="border-kecil">a</div>
                        <div class="chart">
                            <canvas id="lineChart-2" style="height:250px; min-height:250px"></canvas>
                        </div>
                    </div>

                    <div style="margin-top: 42px;" class="div-chart">
                        <p>Busy Hours</p>
                        <div class="border-kecil">a</div>
                        <div class="chart">
                            <canvas id="lineChart-3" style="height:250px; min-height:250px"></canvas>
                        </div>
                    </div>

                    <div class="stocktake-div">
                        <div style="margin-top: 42px;" class="div-chart col-sm-3">
                            <p>Total Stock</p>
                            <div class="border-kecil">a</div>
                            <div class="chart">
                                <canvas id="chart_pie_1" style="height:150px; min-height:150px"></canvas>
                            </div>
                        </div>
                        <div style="margin-top: 42px;" class="div-chart col-sm-3">
                            <p>Stocktake CWH</p>
                            <div class="border-kecil">a</div>
                            <div class="chart">
                                <canvas id="chart_pie_2" style="height:150px; min-height:150px"></canvas>
                            </div>
                        </div>
                        <div style="margin-top: 42px;" class="div-chart col-sm-6">
                            <p>Stocktake FSL</p>
                            <div class="border-kecil">a</div>
                            <div class="chart">
                                <canvas id="chart_bar_1" style="height:250px; min-height:250px"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        all_data();
    });

    function all_data(){
        var today = new Date(),
        firstDay = new Date(today.getFullYear(), today.getMonth(), 1),
        lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0),
        days_this_month = [];

        let level_user = '<?= $this->session->userdata('level')?>';
        let user_cust = '<?= $this->session->userdata('id_customer')?>';

        while(firstDay <= lastDay){
            days_this_month.push(firstDay.getDate());
            firstDay.setDate(firstDay.getDate() + 1);
        }

        var linechart_1 = $('#lineChart-1').get(0).getContext('2d');
        var linechart_2 = $('#lineChart-2').get(0).getContext('2d');
        var linechart_3 = $('#lineChart-3').get(0).getContext('2d');

        var chart_pie_1 = document.getElementById("chart_pie_1").getContext('2d');
        var chart_donat_1 = document.getElementById("chart_donat_1").getContext('2d');
        var chart_donat_2 = document.getElementById("chart_donat_2").getContext('2d');
        var chart_donat_3 = document.getElementById("chart_donat_3").getContext('2d');

        var chart_pie_2 = document.getElementById("chart_pie_2").getContext('2d');
        var chart_bar_1 = document.getElementById("chart_bar_1").getContext('2d');

        $.ajax({
            url: "<?php echo base_url('huawei/home/get_request_data_all_card');?>",
            type : "POST",
            dataType: 'json',
            data: {'days_this_month' : days_this_month},
            success : function(data){
                $('.card-top-count1').text(Number(data.total_request));
                $('.card-top-count2').text(Number(data.total_process));
                $('.card-top-count3').text(Number(data.total_in_delivery));
                $('.card-top-count4').text(Number(data.total_success));
                $('.card-top-count5').text(Number(data.total_failed));
                $('.card-top-count6').text(Number(data.total_oos));
            },
        });

        $.ajax({
            url: "<?php echo base_url('huawei/home/get_request_by_day_this_month');?>",
            type : "GET",
            dataType: 'json',
            // data: {'days_this_month' : days_this_month},
            success : function(data){
            	// console.log(data);
                if(user_cust > 0){
                    var dailyData = {
                        labels  : days_this_month,
                        datasets: [{
                            label               : 'Requests',
                            backgroundColor     : '#0B79FA',
                            borderColor         : 'rgba(11,121,250,.7)',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : '#0B79FA',
                            pointStrokeColor    : 'rgba(11,121,250,.7)',
                            pointHighlightFill  : '#fff',
                            pointHighlightStroke: 'rgba(11,121,250,.7)',
                            data                : data
                        }]
                    };
                }else{
                    var dailyData = {
                        labels  : days_this_month,
                        datasets: [
                        {
                            label               : 'TELKOMSEL',
                            backgroundColor     : 'red',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'red',
                            data                : data.TS
                        },
                        {
                            label               : 'INDOSAT',
                            backgroundColor     : 'yellow',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'yellow',
                            data                : data.IS
                        },
                        {
                            label               : 'TELKOM',
                            backgroundColor     : 'black',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'black',
                            data                : data.TK
                        },
                        {
                            label               : 'XL',
                            backgroundColor     : 'blue',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'blue',
                            data                : data.XL
                        },
                        {
                            label               : 'ENGINEER BORROW',
                            backgroundColor     : 'orange',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'orange',
                            data                : data.EB
                        },
                        {
                            label               : 'ENTERPRISE',
                            backgroundColor     : 'pink',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'pink',
                            data                : data.ET
                        },
                        {
                            label               : 'MORATELINDO',
                            backgroundColor     : 'deep purple',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'deep purple',
                            data                : data.MR
                        },
                        {
                            label               : 'HCPT',
                            backgroundColor     : 'teal',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'teal',
                            data                : data.HC
                        },
                        {
                            label               : 'HCPT MULTIVENDOR',
                            backgroundColor     : 'brown',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'brown',
                            data                : data.HM
                        },
                        {
                            label               : 'INDOSAT MEGA MEDIA',
                            backgroundColor     : 'blue grey',
                            pointRadius         : 4,
                            pointHoverRadius    : 4,
                            fill                : false,
                            pointColor          : 'blue grey',
                            data                : data.IM
                        }
                        ]
                    };
                }

                var lineChart = new Chart(linechart_1, { 
                    type: 'line',
                    easing: 'linear',
                    data: dailyData, 
                    options: {
                        maintainAspectRatio : false,
                        responsive : true,
                        legend: { display: false },
                        scales: {
                            xAxes: [{
                                gridLines : {
                                    display : false,
                                }
                            }],
                            yAxes: [{
                                gridLines : {
                                    display : false,
                                }
                            }]
                        }
                    }
                });
            },
        });

$.ajax({
    url: "<?php echo base_url('huawei/home/get_request_by_day_this_week');?>",
    type : "GET",
    dataType: 'json',
    success : function(data){
        if(user_cust > 0){
            var dailyData = {
                labels  : data.days,
                datasets: [{
                    label               : 'Requests',
                    backgroundColor     : '#0B79FA',
                    borderColor         : 'rgba(11,121,250,.7)',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : '#0B79FA',
                    pointStrokeColor    : 'rgba(11,121,250,.7)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(11,121,250,.7)',
                    data                : data.data
                }]
            };
        }else{
            var dailyData = {
                labels  : data.days,
                datasets: [
                {
                    label               : 'TELKOMSEL',
                    backgroundColor     : 'red',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'red',
                    data                : data.data.TS
                },
                {
                    label               : 'INDOSAT',
                    backgroundColor     : 'yellow',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'yellow',
                    data                : data.data.IS
                },
                {
                    label               : 'TELKOM',
                    backgroundColor     : 'black',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'black',
                    data                : data.data.TK
                },
                {
                    label               : 'XL',
                    backgroundColor     : 'blue',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'blue',
                    data                : data.data.XL
                },
                {
                    label               : 'ENGINEER BORROW',
                    backgroundColor     : 'orange',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'orange',
                    data                : data.data.EB
                },
                {
                    label               : 'ENTERPRISE',
                    backgroundColor     : 'pink',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'pink',
                    data                : data.data.ET
                },
                {
                    label               : 'MORATELINDO',
                    backgroundColor     : 'deep purple',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'deep purple',
                    data                : data.data.MR
                },
                {
                    label               : 'HCPT',
                    backgroundColor     : 'teal',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'teal',
                    data                : data.data.HC
                },
                {
                    label               : 'HCPT MULTIVENDOR',
                    backgroundColor     : 'brown',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'brown',
                    data                : data.data.HM
                },
                {
                    label               : 'INDOSAT MEGA MEDIA',
                    backgroundColor     : 'blue grey',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'blue grey',
                    data                : data.data.IM
                }
                ]
            };
        }

        var lineChart = new Chart(linechart_2, { 
            type: 'line',
            data: dailyData, 
            options: {
                maintainAspectRatio : false,
                responsive : true,
                legend: { display: false },
                scales: {
                    xAxes: [{
                        gridLines : {
                            display : false,
                        }
                    }],
                    yAxes: [{
                        gridLines : {
                            display : false,
                        }
                    }]
                }
            }
        });
    },
});

$.ajax({
    url: "<?php echo base_url('huawei/home/get_request_by_hour_this_day');?>",
    type : "GET",
    dataType: 'json',
    success : function(data){
        if(user_cust > 0){
            var dailyData = {
                labels  : data.hours,
                datasets: [{
                    label               : 'Requests',
                    backgroundColor     : '#0B79FA',
                    borderColor         : 'rgba(11,121,250,.7)',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : '#0B79FA',
                    pointStrokeColor    : 'rgba(11,121,250,.7)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(11,121,250,.7)',
                    data                : data.data
                }]
            };
        }else{
            var dailyData = {
                labels  : data.hours,
                datasets: [
                {
                    label               : 'TELKOMSEL',
                    backgroundColor     : 'red',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'red',
                    data                : data.data.TS
                },
                {
                    label               : 'INDOSAT',
                    backgroundColor     : 'yellow',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'yellow',
                    data                : data.data.IS
                },
                {
                    label               : 'TELKOM',
                    backgroundColor     : 'black',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'black',
                    data                : data.data.TK
                },
                {
                    label               : 'XL',
                    backgroundColor     : 'blue',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'blue',
                    data                : data.data.XL
                },
                {
                    label               : 'ENGINEER BORROW',
                    backgroundColor     : 'orange',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'orange',
                    data                : data.data.EB
                },
                {
                    label               : 'ENTERPRISE',
                    backgroundColor     : 'pink',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'pink',
                    data                : data.data.ET
                },
                {
                    label               : 'MORATELINDO',
                    backgroundColor     : 'deep purple',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'deep purple',
                    data                : data.data.MR
                },
                {
                    label               : 'HCPT',
                    backgroundColor     : 'teal',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'teal',
                    data                : data.data.HC
                },
                {
                    label               : 'HCPT MULTIVENDOR',
                    backgroundColor     : 'brown',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'brown',
                    data                : data.data.HM
                },
                {
                    label               : 'INDOSAT MEGA MEDIA',
                    backgroundColor     : 'blue grey',
                    pointRadius         : 4,
                    pointHoverRadius    : 4,
                    fill                : false,
                    pointColor          : 'blue grey',
                    data                : data.data.IM
                }
                ]
            };
        }

        var lineChart = new Chart(linechart_3, {
            type: 'line',
            data: dailyData, 
            options: {
                maintainAspectRatio : false,
                responsive : true,
                legend: { display: false },
                scales: {
                    xAxes: [{
                        gridLines : {
                            display : false,
                        }
                    }],
                    yAxes: [{
                        gridLines : {
                            display : false,
                        }
                    }]
                }
            }
        });
    },
});

$.ajax({
    url: "<?php echo base_url('huawei/home/get_progress_stocktake');?>",
    type : "GET",
    dataType: 'json',
    success : function(data){
        var donat_stock = new Chart(chart_pie_2, {
            type: 'pie',
            responsive: true,
            maintainAspectRatio: false,
            options : {
                legend : false
            },
            data: {
                labels: ["Checked", "Unchecked"],
                datasets: [{
                    backgroundColor: [
                    "#41A746",
                    '#DC3D45'
                    ],
                    data: [Number(data.cwh.total_check), Number(data.cwh.total_uncheck)]
                }]
            }
        });

        var progress_fsl = {
            labels  : ['100%', '75-100%', '50-75%', '25-50%', '< 25%', '0%'],
            datasets: [
            {
                label               : 'Total',
                backgroundColor     : ["#28a745", "#17a2b8", "#ff851b", "#ffc107", "#6610f2", "#dc3545"],
                data                : [Number(data.all_fsl.hundred), Number(data.all_fsl.seventy_five), Number(data.all_fsl.fifty), Number(data.all_fsl.twenty_five), Number(data.all_fsl.less_twenty_five), Number(data.all_fsl.not_yet)]
            }
            ]
        };

        var myBarChart = new Chart(chart_bar_1, {
            type: 'horizontalBar',
            data: progress_fsl,
            options: {
                maintainAspectRatio : false,
                responsive : true,
                legend: { display: false },
                scales: {
                    yAxes : [{
                        barPercentage: 0.4,
                        gridLines : {
                            display : false,
                        }
                    }],
                    xAxes : [{
                        gridLines : {
                            display : false,
                        }
                    }]
                }
            }
        });
    },
});

$.ajax({
    url: "<?php echo base_url('huawei/home/get_request_IOP_stock');?>",
    type : "GET",
    dataType: 'json',
    success : function(data){
        // console.log(data)
        var donat_stock = new Chart(chart_pie_1, {
            type: 'pie',
            responsive: true,
            maintainAspectRatio: false,
            options : {
                legend : false
            },
            data: {
                labels: ["Total Good", "Total Faulty"],
                datasets: [{
                    backgroundColor: [
                    "#41A746",
                    '#DC3D45'
                    ],
                    data: [Number(data.stock.total_good), Number(data.stock.total_faulty)]
                }]
            }
        });

        if(user_cust > 0){
            var donat_1 = new Chart(chart_donat_1, {
                type: 'doughnut',
                responsive: true,
                maintainAspectRatio: false,
                plugins: [{
                    beforeDraw: function(chart, options) {
                        if (chart.config.centerText.display !== null &&
                            typeof chart.config.centerText.display !== 'undefined' &&
                            chart.config.centerText.display) {
                            drawTotals(chart);
                    }
                }
            }],
            centerText: {
                display: true,
                text: Number(data.iop.total_inner)
            },
            options : {
                legend : false,
                tooltips : {
                    enabled: false
                }
            },
            data: {
                labels: ["Total Inner"],
                datasets: [{
                    backgroundColor: [
                    "#3498db",
                    '#6C757D'
                    ],
                    data: [Number(data.iop.total_inner), Number(data.iop.total_request - data.iop.total_inner)]
                }]
            }
        });

            var donat_2 = new Chart(chart_donat_2, {
                type: 'doughnut',
                responsive: true,
                maintainAspectRatio: false,
                plugins: [{
                    beforeDraw: function(chart, options) {
                        if (chart.config.centerText.display !== null &&
                            typeof chart.config.centerText.display !== 'undefined' &&
                            chart.config.centerText.display) {
                            drawTotals(chart);
                    }
                }
            }],
            centerText: {
                display: true,
                text: Number(data.iop.total_outer)
            },
            options : {
                legend : false,
                tooltips : {
                    enabled: false
                }
            },
            data: {
                labels: ["Total Outer"],
                datasets: [{
                    backgroundColor: [
                    "#f1c40f",
                    '#6C757D'
                    ],
                    data: [Number(data.iop.total_outer), Number(data.iop.total_request - data.iop.total_outer)]
                }]
            }
        });

            var donat_3 = new Chart(chart_donat_3, {
                type: 'doughnut',
                responsive: true,
                maintainAspectRatio: false,
                plugins: [{
                    beforeDraw: function(chart, options) {
                        if (chart.config.centerText.display !== null &&
                            typeof chart.config.centerText.display !== 'undefined' &&
                            chart.config.centerText.display) {
                            drawTotals(chart);
                    }
                }
            }],
            centerText: {
                display: true,
                text: Number(data.iop.total_p2p)
            },
            options : {
                legend : false,
                tooltips : {
                    enabled: false
                }
            },
            showTooltips: false,
            data: {
                labels: ["Total Pool to Pool"],
                datasets: [{
                    backgroundColor: [
                    "#e74c3c",
                    '#6C757D'
                    ],
                    data : [Number(data.iop.total_p2p), Number(data.iop.total_request - data.iop.total_p2p)]
                }]
            }
        });
        }else{
            var donat_1 = new Chart(chart_donat_1, {
                type: 'doughnut',
                responsive: true,
                maintainAspectRatio: false,
                options : {
                    legend : false,
                    tooltips : {
                        enabled: true
                    }
                },
                data: {
                    labels: ["TELKOMSEL", "INDOSAT", "TELKOM", "XL", "ENGINEER BORROW", "ENTERPRISE", "MORATELINDO", "HCPT", "HCPT MULTIVENDOR", "INDOSAT MEGA MEDIA"],
                    datasets: [{
                        backgroundColor: [
                        "red",
                        "yellow",
                        "black",
                        "blue",
                        "orange",
                        "pink",
                        "deep purple",
                        "teal",
                        "brown",
                        "blue grey"
                        ],
                        data: [Number(data.iop.TS.total_inner), Number(data.iop.IS.total_inner), Number(data.iop.TK.total_inner), Number(data.iop.XL.total_inner), Number(data.iop.EB.total_inner), Number(data.iop.ET.total_inner), Number(data.iop.MR.total_inner), Number(data.iop.HC.total_inner), Number(data.iop.HM.total_inner), Number(data.iop.IM.total_inner)]
                    }]
                }
            });

            var donat_2 = new Chart(chart_donat_2, {
                type: 'doughnut',
                responsive: true,
                maintainAspectRatio: false,
                options : {
                    legend : false,
                    tooltips : {
                        enabled: true
                    }
                },
                data: {
                    labels: ["TELKOMSEL", "INDOSAT", "TELKOM", "XL", "ENGINEER BORROW", "ENTERPRISE", "MORATELINDO", "HCPT", "HCPT MULTIVENDOR", "INDOSAT MEGA MEDIA"],
                    datasets: [{
                        backgroundColor: [
                        "red",
                        "yellow",
                        "black",
                        "blue",
                        "orange",
                        "pink",
                        "deep purple",
                        "teal",
                        "brown",
                        "blue grey"
                        ],
                        data: [Number(data.iop.TS.total_outer), Number(data.iop.IS.total_outer), Number(data.iop.TK.total_outer), Number(data.iop.XL.total_outer), Number(data.iop.EB.total_outer), Number(data.iop.ET.total_outer), Number(data.iop.MR.total_outer), Number(data.iop.HC.total_outer), Number(data.iop.HM.total_outer), Number(data.iop.IM.total_outer)]
                    }]
                }
            });

            var donat_3 = new Chart(chart_donat_3, {
                type: 'doughnut',
                responsive: true,
                maintainAspectRatio: false,
                options : {
                    legend : false,
                    tooltips : {
                        enabled: true
                    }
                },
                showTooltips: false,
                data: {
                    labels: ["TELKOMSEL", "INDOSAT", "TELKOM", "XL", "ENGINEER BORROW", "ENTERPRISE", "MORATELINDO", "HCPT", "HCPT MULTIVENDOR", "INDOSAT MEGA MEDIA"],
                    datasets: [{
                        backgroundColor: [
                        "red",
                        "yellow",
                        "black",
                        "blue",
                        "orange",
                        "pink",
                        "deep purple",
                        "teal",
                        "brown",
                        "blue grey"
                        ],
                        data: [Number(data.iop.TS.total_p2p), Number(data.iop.IS.total_p2p), Number(data.iop.TK.total_p2p), Number(data.iop.XL.total_p2p), Number(data.iop.EB.total_p2p), Number(data.iop.ET.total_p2p), Number(data.iop.MR.total_p2p), Number(data.iop.HC.total_p2p), Number(data.iop.HM.total_p2p), Number(data.iop.IM.total_p2p)]
                    }]
                }
            });
        }
    },
});

setTimeout(function(){ all_data(); }, 300000);
}

function drawTotals(chart) {

    var width = chart.chart.width,
    height = chart.chart.height,
    ctx = chart.chart.ctx;

    ctx.restore();
    var fontSize = (height / 114).toFixed(2);
    ctx.font = fontSize + "em sans-serif";
    ctx.textBaseline = "middle";

    var text = chart.config.centerText.text,
    textX = Math.round((width - ctx.measureText(text).width) / 2),
    textY = height / 2;

    ctx.fillText(text, textX, textY);
    ctx.save();
}
</script>
<style>
   
</style>