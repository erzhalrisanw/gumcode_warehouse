<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">RMR Report</p>
                </div>

                <div class="inbound-div">
                    <div style="display: flex; margin: 42px 0;">
                        <input style="height: 40px; width: 180px; margin: 0;" type="text" id="rmr-search" class="form-control" name="">
                        <button style="margin-left: 24px;" id="rmr-search-process" class="btn btn-primary">Search RMR</button>
                    </div>
                    <div id="rmr-list-div" class="table-responsive">
                        <table id="rmr-list" class="table table-bordered">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        rmrList = $('#rmr-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            },
            columns: [
            {
                name: 'first',
                title: 'Customer',
            },
            {
                title: 'Description Module',
            },
            {
                title: 'PN Delivery',
            }, 
            {
                title: 'RMR',
            },
            {
                title: 'SR Code',
            },
            {
                title: 'RMA',
            },
            {
                title: 'Site',
            },
            {   
               title: 'Origin'
            },
            {
                title: 'ETA',
            },
            {
                title: 'Remark',
            },
            ],
            rowsGroup: [0]
        });

        $('#rmr-list-div').hide();

        $('#rmr-search-process').click(function(){
            rmrList.clear().draw();
            $.ajax({
                url: "<?php echo base_url('huawei/transaction/get_rmr_list_by_rmr');?>",
                type : "POST",
                dataType: 'json',
                data: {'rmr' : $('#rmr-search').val()},
                success : function(data){
                    $(data).each(function(k,v) {
                        rmrList.row.add([
                            v.name_customer,
                            v.description,
                            v.name_pn,
                            v.rmr,
                            v.order,
                            v.rma,
                            v.site,
                            v.name_warehouse,
                            v.eta_received,
                            v.remark
                            ]).draw();
                    });
                    $('#rmr-list-div').show();
                },
            });
        });
    });
</script>
