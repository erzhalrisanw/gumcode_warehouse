<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">iCare</p>
                </div>
                <div class="tab-btn">
                    <button class="btn btn-primary">Inbound</button>
                    <button class="btn btn-primary">Outbound</button>
                </div>

                <div class="inbound-div table-responsive">
                    <table id="inbound-list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Request</th>
                                <th>DOP</th>
                                <th>Pool Origin</th>
                                <th>Ticket</th>
                                <th>Module Pickup</th>
                                <th>CMN</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="outbound-div table-responsive">
                    <table id="outbound-list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Request</th>
                                <th>DOP</th>
                                <th>Pool Origin</th>
                                <th>Ticket</th>
                                <th>Module Delivery</th>
                                <th>MDN</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="modal-insert-inbound" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Insert Inbound Order</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="form-inbound-order">
                            <input id="id_request_inbound" name="id_request_inbound" type="hidden" class="form-control">
                            <div class="my-form-group">
                                <p class="my-label-input">Inbound Order :</p>
                                <input id="inbound_order" name="inbound_order" type="text" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="inbound-order-process" type="button" class="btn btn-success">Proceed</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-insert-outbound" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Insert Outbound Order</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="form-outbound-order">
                            <input id="id_request_outbound" name="id_request_outbound" type="hidden" class="form-control">
                            <div class="my-form-group">
                                <p class="my-label-input">Outbound Order :</p>
                                <input id="outbound_order" name="outbound_order" type="text" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="outbound-order-process" type="button" class="btn btn-success">Proceed</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        inboundList = $('#inbound-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        outboundList = $('#outbound-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        $('.tab-btn > button').click(function(){
            $('.tab-btn > button').removeClass('active');
            $(this).addClass('active');
        });

        $('.tab-btn').children('button').eq(1).click(function(){
            $('.inbound-div').hide();
            $('.outbound-div').show();
            outboundList.rows().remove().draw();

            $.ajax({
                url: "<?php echo base_url('huawei/transaction/icare_outbound_list');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $(data).each(function(k,v) {
                        outboundList.row.add([
                            k+1,
                            v.rmr+'</br>'+v.time_request+'</br>'+v.name_user+'</br>'+v.status_request,
                            v.name_dop,
                            v.name_warehouse,
                            'SR Order: '+v.order+'</br>RMA: '+v.rma,
                            'PN: '+v.name_pn+'</br>SN: '+(v.name_sn != null ? v.name_sn : ''),
                            v.pod_no+'</br>'+v.awb_no+'</br>'+v.actual_received+'</br>'+'<a style="font-size: 10px; margin-top: 10px;" class="btn btn-primary btn-sm" href="<?=base_url()?>'+v.url_photo_mdn+'" target="_blank">Download MDN</a>',
                            '<button class="btn btn-warning" onclick="openModalOutbound('+v.id_request+')">INSERT</button>'
                            ]).draw( false );
                    });
                },
            });
        });

        $('.tab-btn').children('button').eq(0).click(function(){
            $('.inbound-div').show();
            $('.outbound-div').hide();
            inboundList.rows().remove().draw();

            $.ajax({
                url: "<?php echo base_url('huawei/transaction/icare_inbound_list');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $(data).each(function(k,v) {
                        inboundList.row.add([
                            k+1,
                            v.rmr+'</br>'+v.time_request+'</br>'+v.name_user+'</br>'+v.status_request,
                            v.name_dop,
                            v.name_warehouse,
                            'SR Order: '+v.order+'</br>RMA: '+v.rma,
                            'PN: '+v.name_pn+'</br>SN: '+(v.name_sn != null ? v.name_sn : ''),
                            v.pickup_no+'</br>'+v.actual_pickup+'</br>'+v.pickup_status+'</br>'+'<a style="font-size: 10px; margin-top: 10px;" class="btn btn-primary btn-sm" href="<?=base_url()?>'+v.url_photo_cmn+'" target="_blank">Download CMN</a>',
                            '<button class="btn btn-warning" onclick="openModalInbound('+v.id_request+')">INSERT</button>'
                            ]).draw( false );
                    });
                },
            });
        });

        $('.tab-btn').children('button').eq(0).trigger('click');

        $('#inbound-order-process').click(function(){
            $.ajax({
                url: "<?php echo base_url('huawei/transaction/inbound_order_process');?>",
                type : "POST",
                dataType: 'json',
                data: $('#form-inbound-order').serialize(),
                success : function(data){
                    if(data){
                        var swal_data = { title: 'Success', icon: 'success', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                            $('#modal-insert-inbound').modal('hide');
                            $('.tab-btn').children('button').eq(0).trigger('click');
                        });
                    }else{
                        var swal_data = { title: 'Failed', icon: 'error', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                        });
                    }
                },
            });
        });

        $('#outbound-order-process').click(function(){
            $.ajax({
                url: "<?php echo base_url('huawei/transaction/outbound_order_process');?>",
                type : "POST",
                dataType: 'json',
                data: $('#form-outbound-order').serialize(),
                success : function(data){
                    if(data){
                        var swal_data = { title: 'Success', icon: 'success', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                            $('#modal-insert-outbound').modal('hide');
                            $('.tab-btn').children('button').eq(1).trigger('click');
                        });
                    }else{
                        var swal_data = { title: 'Failed', icon: 'error', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                        });
                    }
                },
            });
        });

    });

    function openModalInbound(id_request){
        $('#id_request_inbound').val(id_request);
        $('#modal-insert-inbound').modal('show');
    }

    function openModalOutbound(id_request){
        $('#id_request_outbound').val(id_request);
        $('#modal-insert-outbound').modal('show');
    }
</script>
