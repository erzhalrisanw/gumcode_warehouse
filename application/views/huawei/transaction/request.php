<div class="content-wrapper">
	<section class="content">
		<div class="container-fluid">
			<div class="row my-row">
				<div class="header-jdl">
					<p class="jdl-big">Request</p>
				</div>
				<div class="tab-btn">
					<div class="btn-item active">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/request.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Create Request</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/list.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Request List</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/delivery.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">In Delivery List</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/pickup.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">To Pickup List</label>
						</div>	
					</div>
				</div>
				<div id="create-req">
					<form id="this-form">
						<input type="hidden" id="input_create_time" name="input_create_time">
						<div class="input-div">
							<div class="my-form-group">
								<p class="my-label-input">Project :</p>
								<select id="id_project" name="id_project" class="form-control select2">
									<option value="">Please select Project</option>
									<?php foreach($proj as $proj) {?>
										<option value="<?=$proj['id_project']?>"><?=$proj['name_project']?></option>
									<?php } ?>
								</select>
							</div>
							<div class="my-form-group">
								<p class="my-label-input">Project Type :</p>
								<select id="id_projecttype" name="id_projecttype" class="form-control select2">
									<option value="">Please select Project Type</option>
								</select>
							</div>
							<div class="my-form-group">
								<p class="my-label-input">Request Time :</p>
								<input type="text" id="time_request" class="form-control input-datetime" name="time_request">
							</div>
							<div class="my-form-group">
								<p class="my-label-input">Customer List :</p>
								<select id="id_customer" name="id_customer" class="form-control select2">
									<option value="">Please select Customer</option>
								</select>
							</div>
							<div class="my-form-group">
								<p class="my-label-input">Requestor :</p>
								<select id="id_requestor" name="id_requestor" class="form-control select2">
									<option value="">Please select Requestor</option>
								</select>
							</div>
							<div id="id_dop_select_div" style="width: 100% !important;" class="">
								<p style="" class="my-label-input">DOP :</p>
								<select id="id_dop" name="id_dop" class="form-control"> 
									<option value="">Please select DOP</option>
								</select>
							</div>
						</div>
							<div class="btn-add-pn-request">
								<button style="margin-top: 42px; margin-bottom: 24px;" type="button" class="btn btn-primary mr-4" id="add-pn-request">Add PN Request</button>
							</div>
						<div id="multi-input-doc" class="table-responsive">
							<table id="tab-pn-request" class="table table-bordered pl-3 pr-3">
								<thead>
									<tr>
										<th>PN Request</th>
										<th>SR Code</th>
										<th>RMA</th>
										<th>TT/Site</th>
										<th>Pool Origin</th>
										<th>PN Delivery</th>
										<th>SLA</th>
										<th>Status Request</th>
										<th>Remark</th>
										<th>Created RMR</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</form>
				<div class="btn-process mt-4">
					<button id="saveForm" type="button" class="btn btn-success mr-4">Proceed</button>
				</div>
				</div>

				<div id="req-list" class="table-responsive">
					<button style="margin-bottom: 42px;" id="btn-create-mdn" class="btn btn-warning mt-2">Create MDN</button>
					<table id="tab-req-list" class="table table-bordered pr-3 pl-3">
						<thead>
							<tr>
								<th>No</th>
								<th>Request</th>
								<th>Status</th>
								<th>Date Request</th>
								<th>DOP</th>
								<th>Origin</th>
								<th>PN</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

				<div id="in-delivery-list" class="table-responsive">
					<button style="margin-bottom: 42px; margin-right: 12px;" id="btn-upload-mdn" class="btn btn-warning mt-2">Upload File MDN</button>
					<button style="margin-bottom: 42px;" id="btn-reupload-mdn" class="btn btn-primary mt-2">Reupload File MDN</button>
					<table id="tab-in-delivery-list" class="table table-bordered pr-3 pl-3">
						<thead>
							<tr>
								<th>No</th>
								<th>Request</th>
								<th>Date Request</th>
								<th>DOP</th>
								<th>Origin</th>
								<th>PN Delivery</th>
								<th>TT/Site</th>
								<th>Running Time</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

				<div id="to-pickup-list" class="table-responsive">
					<button style="margin-bottom: 42px; margin-right: 12px;" id="btn-create-cmn" class="btn btn-warning mt-2">Create CMN</button>

					<button style="margin-bottom: 42px; margin-right: 12px;" id="btn-upload-cmn" class="btn btn-primary mt-2">Upload File CMN</button>
					<button style="margin-bottom: 42px;" id="btn-reupload-cmn" class="btn btn-success mt-2">Reupload File CMN</button>

					<table id="tab-to-pickup-list" class="table table-bordered pr-3 pl-3">
						<thead>
							<tr>
								<th>No</th>
								<th>Request</th>
								<th>DOP</th>
								<th>PN</th>
								<th>Ticket</th>
								<th>Consignee</th>
								<th>Pickup Request</th>
								<th>Aging</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
	</div>

	<div id="modal-add-pn-request" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add PN Request</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-add-pn" class="modal-body">
					<div class="my-form-group">
						<p class="my-label-input" style="width:30%;" >PN Request :</p>
						<input id="add_pn_pn_req" type="text" class="form-control">
					</div>
					<div class="my-form-group">
						<p class="my-label-input" style="width:30%;" >SR Code :</p>
						<input id="add_pn_sr" type="text" class="form-control">
					</div>
					<div class="my-form-group">
						<p class="my-label-input" style="width:30%;" >RMA :</p>
						<input id="add_pn_rma" type="text" class="form-control">
					</div>
					<div class="my-form-group">
						<p class="my-label-input" style="width:30%;" >TT/Site :</p>
						<input id="add_pn_site" type="text" class="form-control">
					</div>
					<div class="my-form-group">
						<p class="my-label-input">Pool Origin :</p>
						<span id="lock-pool" class="lock"></span>
						<select id="add_pn_pool" class="form-control select2">
						</select>
					</div>
					<div class="my-form-group">
						<p class="my-label-input">PN Delivery :</p>
						<select id="add_pn_pn_del" class="form-control select2">
						</select>
					</div>
					<div class="my-form-group">
						<p class="my-label-input">SLA :</p>
						<div style="width: 50%; display: flex;">
							<input id="add_pn_sla_hour" type="number" value="0" style="width: 45%;" class="form-control">&nbsp;<p style="font-size: 11px; font-weight: 400; margin-left: 5px; margin-right: 10px; margin-top: 5px;">Hour</p>&nbsp;
							<input id="add_pn_sla_minute" type="number" value="0" style="width: 45%;" class="form-control">&nbsp;<p style="font-size: 11px; font-weight: 400; margin-left: 5px; margin-top: 5px;">Minutes</p>&nbsp;
						</div>
						<!-- <input id="add_pn_sla" type="text" class="form-control"> -->
					</div>
					<div class="my-form-group">
						<p class="my-label-input">Status Request :</p>
						<span id="lock-stat" class="lock"></span>
						<select id="add_pn_checklist" class="form-control select2">
							<?php foreach($check_list as $check_list) {?>
								<option value="<?=$check_list['id_status_request']?>"><?=$check_list['status_request']?></option>
							<?php } ?>
						</select>
					</div>
					<div class="my-form-group">
						<p class="my-label-input" style="width:30%;" >Remark :</p>
						<input id="add_pn_remark" type="text" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button id="btn-add-pn-request" type="button" class="btn btn-success">Add</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-edit-doc-request" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Edit Request Doc</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-edit-request" class="modal-body">
					<form id="form-edit-doc">
						<input type="hidden" id="edit_id_dop" name="edit_id_dop">
						<input type="hidden" id="edit_id_doc" name="edit_id_doc">

						<div class="my-form-group">
							<p class="my-label-input">Request Time :</p>
							<input id="edit_time_request" type="text" class="form-control input-datetime" name="edit_time_request">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Received Time :</p>
							<input id="edit_receive_time_request" type="text" class="form-control input-datetime" name="edit_receive_time_request">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Pickup Time :</p>
							<input id="edit_pickup_time_request" type="text" class="form-control input-datetime" name="edit_pickup_time_request">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">TT/Site :</p>
							<input id="edit_site" name="edit_site" type="text" class="form-control" style="width:70%;">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">PN Request :</p>
							<input id="edit_pn_req" name="edit_pn_req" type="text" class="form-control" style="width:70%;">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Pool Origin :</p>
							<span id="lock-pool-edit" class="lock"></span>
							<select id="edit_pool" name="edit_pool" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">PN Delivery :</p>
							<select id="edit_pn_del" name="edit_pn_del" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">SLA :</p>
							<div style="width: 50%; display: flex;">
								<input id="edit_sla_hour" name="edit_sla_hour" type="number" value="0" style="width: 45%;" class="form-control">&nbsp;<p style="font-size: 11px; font-weight: 400; margin-left: 5px; margin-right: 10px; margin-top: 5px;">Hour</p>&nbsp;
								<input id="edit_sla_minute" name="edit_sla_minute" type="number" value="0" style="width: 45%;" class="form-control">&nbsp;<p style="font-size: 11px; font-weight: 400; margin-left: 5px; margin-top: 5px;">Minutes</p>&nbsp;
							</div>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Status Request :</p>
							<span id="lock-stat-edit" class="lock"></span>
							<select id="edit_checklist" name="edit_checklist" class="form-control select2">
								<?php foreach($check_list_edit as $check_list_edit) {?>
									<option value="<?=$check_list_edit['id_status_request']?>"><?=$check_list_edit['status_request']?></option>
								<?php } ?>
							</select>
							<input type="hidden" id="edit_checklist_real" name="edit_checklist_real">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Remark :</p>
							<input id="edit_remark" name="edit_remark" type="text" class="form-control" style="width:70%;">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="edit-process" type="button" class="btn btn-success">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-add-sn-rmr" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add SN by RMR</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<input type="hidden" id="add_sn_id_doc" name="add_sn_id_doc">

					<div class="my-form-group">
						<p class="my-label-input">Serial Number :</p>
						<input id="add_sn_rmr" type="text" class="form-control" name="add_sn_rmr">
					</div>
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>

	<div id="modal-create-mdn" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Create MDN</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-create-mdn" class="modal-body">
					<form id="form-create-mdn">
						<div class="my-form-group">
							<p class="my-label-input">Logistic :</p>
							<select id="create_mdn_logistic" name="create_mdn_logistic" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Logistic Service :</p>
							<select id="create_mdn_logistic_service" name="create_mdn_logistic_service" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">AWB :</p>
							<input id="create_mdn_awb" type="text" class="form-control" style="width:50%;" name="create_mdn_awb">
						</div>
						<div class="my-form-group">
							<p class="my-label-input" >RMR List :</p>
							<select id="create_mdn_rmr_list" name="create_mdn_rmr_list[]" class="select2" multiple="multiple">
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="create-mdn-process" type="button" class="btn btn-success">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-upload-file-mdn" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Upload File MDN</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-upload-mdn" class="modal-body">
					<form id="form-upload-mdn" enctype="multipart/form-data">
						<div class="my-form-group">
							<p class="my-label-input">Consignee :</p>
							<input id="upload_mdn_consignee" type="text" class="form-control" style="width: 50%;" name="upload_mdn_consignee">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Actual Received :</p>
							<input id="upload_mdn_date" type="text" class="form-control input-datetime" name="upload_mdn_date">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">POD List :</p>
							<select id="upload_mdn_pod" name="upload_mdn_pod" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">File Upload :</p>
							<input id="upload_mdn_file" type="file" class="form-control" name="upload_mdn_file">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="upload-mdn-process" type="button" class="btn btn-success"><i id="loading-upload-mdn" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-create-cmn" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Create CMN</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-create-cmn" class="modal-body">
					<form id="form-create-cmn">
						<div class="my-form-group">
							<p class="my-label-input">Logistic :</p>
							<select id="create_cmn_logistic" name="create_cmn_logistic" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Logistic Service :</p>
							<select id="create_cmn_logistic_service" name="create_cmn_logistic_service" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">AWB :</p>
							<input id="create_cmn_awb" type="text" class="form-control" style="width:50%;" name="create_cmn_awb">
						</div>
						<div class="my-form-group">
							<p class="my-label-input">RMR List :</p>
							<select id="create_cmn_rmr_list" name="create_cmn_rmr_list[]" class="select2" multiple="multiple">
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="create-cmn-process" type="button" class="btn btn-success">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-upload-cmn" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Upload CMN</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-upload-cmn" class="modal-body">
					<form id="form-upload-cmn" enctype="multipart/form-data">
						<div class="my-form-group">
							<p class="my-label-input">CMN List :</p>
							<select id="upload_cmn_doc_id" name="upload_cmn_doc_id" class="form-control select2">
								<option value="">Please Select CMN</option>
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Actual Pickup :</p>
							<input id="upload_cmn_date" type="text" class="form-control input-datetime" name="upload_cmn_date">
						</div>
						<div id="upload-cmn-table-div" class="my-form-group">
							<p class="my-label-input">PN & SN Pickup :</p>
							<div class="upload-cmn-table-div-isi table-responsive" style="width:80%;">
								<table id="tab-add-pn-sn-pickup" class="table table-bordered">
									<thead>
										<tr>
											<th>RMR</th>
											<th>Locator</th>
											<th>PN Pickup</th>
											<th>SN Pickup</th>
											<th>Status</th>
											<th>Remark</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								<button id="upload-cmn-check-pn" type="button" class="btn btn-warning">Check</button>
							</div>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">File Upload :</p>
							<input id="upload_cmn_file" type="file" class="form-control" name="upload_cmn_file">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="upload-cmn-process" type="button" class="btn btn-success"><i id="loading-upload-cmn" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-pickup-add" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Pickup</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-pickup-add" class="modal-body">
					<form id="form-pickup-add">
						<input type="hidden" id="pickup_id_request" name="pickup_id_request">
						<div class="my-form-group">
							<p class="my-label-input">Pickup Request Time :</p>
							<input id="pickup_time" type="text" class="form-control input-datetime" name="pickup_time">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="pickup_add_process" type="button" class="btn btn-success">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-pickup-close" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Close Pickup</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-pickup-close" class="modal-body">
					<form id="form-pickup-close">
						<input type="hidden" id="close_pickup_id_request" name="close_pickup_id_request">
						<div class="my-form-group">
							<p class="my-label-input">Status List :</p>
							<select id="close_pickup_status" name="close_pickup_status" class="form-control select2">
								<option value="5">Consumable</option>
								<option value="6">Force Majeur</option>
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="close_pickup_process" type="button" class="btn btn-success">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-reupload-file-cmn" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Reupload File CMN</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-reupload-cmn" class="modal-body">
					<form id="form-reupload-cmn" enctype="multipart/form-data">
						<div class="my-form-group">
							<p class="my-label-input">CMN List :</p>
							<select id="reupload_cmn_doc_id" name="reupload_cmn_doc_id" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Preview :</p>
						</div>
						<img style="margin-bottom: 24px;" id="preview_reupload_cmn" src="">
						<div class="my-form-group">
							<p class="my-label-input">File Upload :</p>
							<input id="reupload_cmn" type="file" class="form-control" name="reupload_cmn">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="reupload-cmn-process" type="button" class="btn btn-success"><i id="loading-reupload-cmn" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-reupload-file-mdn" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Reupload File MDN</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="body-reupload-mdn" class="modal-body">
					<form id="form-reupload-mdn" enctype="multipart/form-data">
						<div class="my-form-group">
							<p class="my-label-input">MDN List :</p>
							<select id="reupload_mdn_doc_id" name="reupload_mdn_doc_id" class="form-control select2">
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Preview :</p>
						</div>
						<img style="margin-bottom: 24px; width:500px;" id="preview_reupload_mdn" src="">
						<div class="my-form-group">
							<p class="my-label-input">File Upload :</p>
							<input id="reupload_mdn" type="file" class="form-control" name="reupload_mdn">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button id="reupload-mdn-process" type="button" class="btn btn-success"><i id="loading-reupload-mdn" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Save</button>
				</div>
			</div>
		</div>
	</div>

</section>
</div>
<script>
	$(function () {
		$('#add_pn_checklist').val('0');
		$('#add_pn_pool').val('');
		$('#add_pn_pn_del').val('');
		$("#add_pn_checklist, #edit_checklist").attr('readonly', true);
		$("#add_pn_pool, #edit_pool").attr('readonly', true);
		$('#id_projecttype, #id_customer, #id_requestor, #id_dop, #saveForm').prop('disabled', 'disabled');
		$('#multi-input-doc').hide();
		$('#add-pn-request').hide();
		$('#saveForm').hide();

		$('#id_dop').select2({
			width: 'resolve',
			templateResult: formatOutput
		});

		$('#time_request, #edit_time_request, #edit_receive_time_request, #edit_pickup_time_request, #upload_mdn_date, #upload_cmn_date').daterangepicker(
		{
			singleDatePicker: true,
			showDropdowns: true,
			timePicker: true,
			timePicker24Hour: true,
			startDate: moment(),
			locale: {
				format: 'YYYY-MM-DD HH:mm:ss'
			}
		}
		);

		addPnTable = $('#tab-pn-request').DataTable({
			paging      : false,
			lengthChange: false,
			searching   : false,
			ordering    : false,
			info        : false,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "",
				zeroRecords: ""
			}
		});

		requestList = $('#tab-req-list').DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			info        : true,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			}
		});

		indeliveryList = $('#tab-in-delivery-list').DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			info        : true,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			}
		});

		topickupList = $('#tab-to-pickup-list').DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			info        : true,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "No data to show...",
				zeroRecords: "No data to show..."
			}
		});

		tabAddPNSNPickup = $('#tab-add-pn-sn-pickup').DataTable({
			paging      : false,
			lengthChange: false,
			searching   : false,
			ordering    : false,
			info        : false,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "",
				zeroRecords: ""
			}
		});

		$('#id_project').on('change', function(){
			if($(this).val() == ''){
				$('#id_projecttype, #id_customer, #id_requestor, #id_dop').prop('disabled', 'disabled');
				$('#id_projecttype').find('option').not(':first').remove();
				$('#id_customer').find('option').not(':first').remove();
				$('#id_requestor').find('option').not(':first').remove();
				$('#id_dop').find('option').not(':first').remove();
			} else {
				$('#id_projecttype').removeAttr('disabled');
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/get_project_type_by_project');?>",
					type : "POST",
					dataType: 'json',
					data: {'id_project' : $(this).val()},
					success : function(data){
						$('#id_projecttype').find('option').not(':first').remove();
						$(data).each(function(k,v) {
							$('#id_projecttype').append(new Option(v.name_projecttype, v.id_projecttype));
						});
					},
				});
			}
		});

		$('#btn-create-cmn').click(function(){
			$('#body-create-cmn * > input').val('');
			$('#create_cmn_logistic, #create_cmn_logistic_service, #create_cmn_rmr_list').html('');
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_rmr_list_cmn');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					$(data.logistic).each(function(k,v) {
						$('#create_cmn_logistic').append(new Option(v.name_logistic, v.id_logistic));
						$('#create_cmn_logistic').trigger('change');
					});
					$(data.logistic_service).each(function(k,v) {
						$('#create_cmn_logistic_service').append(new Option(v.name_logisticservice, v.id_logisticservice));
						$('#create_cmn_logistic_service').trigger('change');
					});
					$(data.rmr_list).each(function(k,v) {
						$('#create_cmn_rmr_list').append(new Option(v.rmr, v.id_request));
						$('#create_cmn_rmr_list').trigger('change');
					});
					$('#modal-create-cmn').modal('show');
				},
			});
		});

		$('#create-cmn-process').click(function(){
			if($("#form-create-cmn").find('input').val() == '' || $('#create_cmn_rmr_list').val().length < 1) {
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}else{
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/create_cmn_process');?>",
					type : "POST",
					dataType: 'json',
					data: $('#form-create-cmn').serialize(),
					success : function(data){
						if(data){
							var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully saved', button:false, timer: 1000 };
							swal(swal_data).then(function() {
								$('#modal-create-cmn').modal('hide');
								$('.tab-btn').children('.btn-item').eq(3).trigger('click');
								$('#body-create-cmn * > input').val('');
							});
						}else{
							var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be saved', button:false, timer: 1000 };
							swal(swal_data).then(function() {
							});
						}
					},
				});
			}
		});

		$('#btn-upload-cmn').click(function(){
			$('#upload_cmn_doc_id').find('option').not(':first').remove();
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_cmn_list');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					$(data).each(function(k,v) {
						$('#upload_cmn_doc_id').append(new Option(v.pickup_no, v.id_pickup));
					});
					$('#modal-upload-cmn').modal('show');
				},
			});
		});

		$('#upload_cmn_doc_id').on('change', function(){
			if($(this).val() == ''){
				tabAddPNSNPickup.rows().remove().draw();
			}else{
				tabAddPNSNPickup.rows().remove().draw();
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/get_rmr_list_by_pickup');?>",
					type : "POST",
					dataType: 'json',
					data: {'id_pickup' : $(this).val()},
					success : function(data){
						$(data.rmr_list).each(function(k,v) {
							tabAddPNSNPickup.row.add([
								'<input type="hidden" name="rmr_list[]" value="'+v.id_request+'">'+
								v.rmr,
								'<input type="text" class="input-locbin-pickup" name="loc_bin[]">',
								'<input type="text" class="input-biasa input-pn-pickup" name="pn_pickup[]">',
								'<input type="text" class="input-biasa input-sn-pickup" name="sn_pickup[]">',
								'<select style="width: 100px;" name="pickup_status[]" class="form-control pickup-stat-sel"></select>',
								'<input type="text" name="remark_pickup[]">'
								]).draw( false );
						});
						$(data.status).each(function(k,v) {
							$('.pickup-stat-sel').append(new Option(v.pickup_status, v.id_statuspickup));
                            // $('.pickup-stat-sel').trigger('change');
                        });
					},
				});
			}
		});

		$('#form-upload-cmn').on('keyup', '.input-locbin-pickup' , function(e){
			if(e.keyCode === 13){
				$(this).closest('tr').find('.input-pn-pickup').trigger('focus');
			}
		});

		$('#form-upload-cmn').on('keyup', '.input-pn-pickup' , function(e){
			if(e.keyCode === 13){
				$(this).closest('tr').find('.input-sn-pickup').trigger('focus');
			}
		});

		$('#form-upload-cmn').on('change', '.pickup-stat-sel', function(){
			if($(this).val() == '2'){
				$(this).closest('tr').find('.input-biasa').val(' ');
				$(this).closest('tr').find('.input-biasa').prop('readonly', true);
			}else{
				$(this).closest('tr').find('.input-biasa').prop('readonly', false);
			}
		});

		$('#loading-upload-cmn').hide();

		$('#upload-cmn-process').click(function(){
			if(typeof $("#upload_cmn_file").prop('files')[0] == 'undefined' || $("#form-upload-cmn").find('input').val() == '' || $(".input-biasa").val() == '' || $(".input-biasa").val() == ''){
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}else{
				var my_formData = new FormData($("#form-upload-cmn")[0]);
				my_formData.append('file', $("#upload_cmn_file").prop('files')[0]);
				$('#loading-upload-cmn').show();
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/upload_cmn_process');?>",
					type : "POST",
					dataType: 'json',
					data: my_formData,
					processData: false,
					contentType: false,
					cache: false,
					success : function(data){
                        // console.log(data);
                        $('#loading-upload-cmn').hide();
                        if(data.stat){
                        	var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
                        	swal(swal_data).then(function() {
                        		$('#upload-cmn-process').hide();
                        		$('#body-upload-cmn * > input').val('');
                        		$('#modal-upload-cmn').modal('hide');
                        		$('.tab-btn').children('.btn-item').eq(3).trigger('click');
                        	});
                        }else{
                        	var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
                        	swal(swal_data).then(function() {
                        	});
                        }
                    },
                });
			}
		});

		$('#upload-cmn-process').hide();

		$('#upload-cmn-check-pn').click(function(){
			var my_formData = new FormData($("#form-upload-cmn")[0]);
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/upload_cmn_check_pn');?>",
				type : "POST",
				dataType: 'json',
				data: my_formData,
				processData: false,
				contentType: false,
				cache: false,
				success : function(data){
                    // console.log(data);
                    if(data.stat){
                    	var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
                    	swal(swal_data).then(function() {
                    	});
                    	$('#upload-cmn-process').show();
                    }else{
                    	var swal_data = { 
                    		title: 'Failed', 
                    		icon: 'error', 
                    		text: data.det, 
                    		buttons: {
                    			confirm: {
                    				text: "Ok",
                    				value: true,
                    				visible: true,
                    				className: "",
                    				closeModal: true
                    			}
                    		}
                    	};
                    	swal(swal_data).then((value) => {
                    	});
                    	$('#upload-cmn-process').hide();
                    }
                },
            });
		});

		$('#edit-process').click(function(){
			if($('#edit_site').val() == '' || $('#edit_time_request').val() == '' || $('#edit_pn_req').val() == '' || $('#edit_pool').val() == null || $('#edit_pn_del').val() == null) {
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}else{
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/edit_process_save');?>",
					type : "POST",
					dataType: 'json',
					data: $('#form-edit-doc').serialize(),
					success : function(data){
						// console.log(data);
						if(data){
							var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully saved', button:false, timer: 1000 };
							swal(swal_data).then(function() {
								$('#modal-edit-doc-request').modal('hide');
								$($('.tab-btn').children('.btn-item')).each(function(k,v){
									if($(this).hasClass('active')){
										$(this).trigger('click');
									}
								});
								if($('#lock-stat-edit').hasClass('unlocked')){
									$('#lock-stat-edit').trigger('click');
								}

								if($('#lock-pool-edit').hasClass('unlocked')){
									$('#lock-pool-edit').trigger('click');
								}
							});
						}else{
							var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be saved', button:false, timer: 1000 };
							swal(swal_data).then(function() {
							});
						}
					},
				});
			}
		});

		$('#create-mdn-process').click(function(){
			if($("#form-create-mdn").find('input').val() == '' || $('#create_mdn_rmr_list').val().length < 1) {
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}else{
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/create_mdn_process');?>",
					type : "POST",
					dataType: 'json',
					data: $('#form-create-mdn').serialize(),
					success : function(data){
						if(data){
							var swal_data = { title: 'Success', icon: 'success', text: 'MDN successfully created', button:false, timer: 1000 };
							swal(swal_data).then(function() {
								$('#modal-create-mdn').modal('hide');
								location.reload();
							});
						}else{
							var swal_data = { title: 'Failed', icon: 'error', text: 'MDN cannot be created', button:false, timer: 1000 };
							swal(swal_data).then(function() {
							});
						}
					},
				});
			}
		});

		$('#id_projecttype').on('change', function(){
			$('#id_customer').removeAttr('disabled');
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_customer_by_project_type');?>",
				type : "POST",
				dataType: 'json',
				data: {'id_projecttype' : $(this).val()},
				success : function(data){
					$('#id_customer').find('option').not(':first').remove();
					$(data).each(function(k,v) {
						$('#id_customer').append(new Option(v.name_customer, v.id_customer));
					});
				},
			});
		});

		$('#id_customer').on('change', function(){
			$('#id_requestor, #id_dop').removeAttr('disabled');
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_requestor_and_dop_by_customer');?>",
				type : "POST",
				dataType: 'json',
				data: {'id_customer' : $(this).val()},
				success : function(data){
					$('#id_requestor').find('option').not(':first').remove();
					$('#id_dop').find('option').not(':first').remove();
					$(data.requestor).each(function(k,v) {
						$('#id_requestor').append(new Option(v.name_requestor, v.id_requestor));
					});
					$(data.dop).each(function(k,v) {
						$('#id_dop').append(new Option(v.name_dop +', address: '+v.alamat_dop, v.id_dop));
					});
				},
			});
		});

		$('#id_dop').on('change', function(){
			if($(this).val() == ''){
				$('#multi-input-doc').hide();
			}else{
				$('#multi-input-doc').show();
				$('#add-pn-request').show();
				$('#saveForm').show();
				$('#saveForm').removeAttr('disabled');
				var date = new Date();
				var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

				$('#input_create_time').val(str);
			}
		});

		$('#saveForm').click(function(){
			if(addPnTable.data().count() > 0) {
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/request_input_process');?>",
					type : "POST",
					dataType: 'json',
					data: $('#this-form').serialize(),
					success : function(data){
						if(data){
							var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully saved', button:false, timer: 1000 };
							swal(swal_data).then(function() {
								location.reload();
							});
						}else{
							var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be saved', button:false, timer: 1000 };
							swal(swal_data).then(function() {
							});
						}
					},
				});
			} else {
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please add pn request', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}
		});

		$('#add-pn-request').click(function(){
			$('#modal-add-pn-request').modal('show');
			if($('#lock-stat').hasClass('unlocked')){
				$('#lock-stat').trigger('click');
			}

		});

		$('#tab-pn-request tbody').on( 'click', '#baru', function () {
			var tr = $(this).closest('tr');
			addPnTable.row(tr).remove().draw();
		});

		$('.pickup-stat-sel').on('change', function(){
			if($(this).val() == 5 || $(this).val() == 6){
				console.log($(this).closest('tr'));
			}
		});

		$('#add_pn_pn_req').on('change', function(){
			if($('#add_pn_pn_req').val() != ''){
				$('#add_pn_pn_del').html('');
				$('#add_pn_pool').html('');
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/get_all_pn_comp_in_warehouse');?>",
					type : "POST",
					dataType: 'json',
					data: {'id_dop' : $('#id_dop').val(), 'pn' : $(this).val(), 'id_project' : $('#id_project').val()},
					success : function(data){
						if(data.length !== 0){
							$('#add_pn_pool').append(new Option(data.name_warehouse, data.id_warehouse));
							$('#add_pn_pool').trigger('change');
							$(data.data).each(function(k,v) {
								$('#add_pn_pn_del').append(new Option(v.name_pn, v.id_pn));
								$('#add_pn_pn_del').trigger('change');
							});
						}
					},
				});
			}
		});

		$('#edit_pn_req').on('change', function(){
			if($('#edit_pn_req').val() != ''){
				$('#edit_pn_del').html('');
				$('#edit_pool').html('');
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/get_all_pn_comp_in_warehouse');?>",
					type : "POST",
					dataType: 'json',
					data: {'id_dop' : $('#edit_id_dop').val(), 'pn' : $(this).val()},
					success : function(data){
						if(data.length !== 0){
							$('#edit_pool').append(new Option(data.name_warehouse, data.id_warehouse));
							$('#edit_pool').trigger('change');
							$(data.data).each(function(k,v) {
								$('#edit_pn_del').append(new Option(v.name_pn, v.id_pn));
								$('#edit_pn_del').trigger('change');
							});
						}
					},
				});
			}
		});

		$('#btn-add-pn-request').click(function(){
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/create_RMR');?>",
				type : "POST",
				dataType: 'json',
				data: {'id_dop' : $('#id_dop').val(), 'id_customer' : $('#id_customer').val(), 'rma' : $('#add_pn_rma').val()},
				success : function(data){
					let check_list;
					if($('#add_pn_checklist').val() == '0'){
						check_list = '';
					} else {
						check_list = $('#add_pn_checklist option:selected').text();
					}
					addPnTable.row.add([
						$('#add_pn_pn_req').val()+
						'<input type="hidden" name="form_pn_req[]" value="'+$('#add_pn_pn_req').val()+'">'+
						'<input type="hidden" name="form_sr_code[]" value="'+$('#add_pn_sr').val()+'">'+
						'<input type="hidden" name="form_rma[]" value="'+$('#add_pn_rma').val()+'">'+
						'<input type="hidden" name="form_site[]" value="'+$('#add_pn_site').val()+'">'+
						'<input type="hidden" name="form_pool[]" value="'+$('#add_pn_pool').val()+'">'+
						'<input type="hidden" name="form_pn_del[]" value="'+$('#add_pn_pn_del').val()+'">'+
						'<input type="hidden" name="form_sla_hour[]" value="'+$('#add_pn_sla_hour').val()+'">'+
						'<input type="hidden" name="form_sla_minute[]" value="'+$('#add_pn_sla_minute').val()+'">'+
						'<input type="hidden" name="form_checklist[]" value="'+$('#add_pn_checklist').val()+'">'+
						'<input type="hidden" name="form_remark[]" value="'+$('#add_pn_remark').val()+'">'+
						'<input type="hidden" name="form_created_rmr[]" value="'+data+'">',
						$('#add_pn_sr').val(),
						$('#add_pn_rma').val(),
						$('#add_pn_site').val(),
						$('#add_pn_pool').text(),
						$('#add_pn_pn_del').text(),
						$('#add_pn_sla_hour').val() + ' Hours ' + $('#add_pn_sla_minute').val() + ' Minutes',
						check_list,
						$('#add_pn_remark').val(),
						data,
						'<button type="button" class="btn btn-xs btn-danger del-device" id="baru">Delete</button>'
						]).draw( false );

					$('#add_pn_pn_req, #add_pn_sr, #add_pn_rma, #add_pn_site, #add_pn_pool, #add_pn_pn_del, #add_pn_sla_hour, #add_pn_sla_minute, #add_pn_remark').html('');

					$('#add_pn_pn_req, #add_pn_sr, #add_pn_rma, #add_pn_site, #add_pn_pool, #add_pn_pn_del, #add_pn_sla_hour, #add_pn_sla_minute, #add_pn_remark').val('');

					if($('#lock-stat').hasClass('unlocked')){
						$('#lock-stat').trigger('click');
					}

					if($('#lock-pool').hasClass('unlocked')){
						$('#lock-pool').trigger('click');
					}
				},
			});
		});

		$( "#lock-stat" ).click(function() {
			$(this).toggleClass('unlocked');
			if($(this).hasClass('unlocked')) {
				$('#add_pn_checklist').attr('readonly', false);
			} else {
				$('#add_pn_checklist').attr('readonly', true);
				$('#add_pn_checklist').val('0');
			}
		});

		$( "#lock-pool" ).click(function() {
			$(this).toggleClass('unlocked');
			if($(this).hasClass('unlocked')) {
				$('#add_pn_pool').attr('readonly', false);
				$('#add_pn_pool').html('');
				$('#add_pn_pn_del').html('');

				$.ajax({
					url: "<?php echo base_url('huawei/transaction/get_all_pn_comp_in_all_warehouse');?>",
					type : "POST",
					dataType: 'json',
					data: {'pn' : $('#add_pn_pn_req').val()},
					success : function(data){
						if(data.length !== 0){
							$(data).each(function(k,v) {
								$('#add_pn_pool').append(new Option(v.name_warehouse, v.id_warehouse));
								$('#add_pn_pool').trigger('change');
							});
						}
					},
				});
			} else {
				$('#add_pn_pool').attr('readonly', true);
				$('#add_pn_pn_req').trigger('change');
			}
		});

		$( "#lock-stat-edit" ).click(function() {
			$(this).toggleClass('unlocked');
			if($(this).hasClass('unlocked')) {
				$('#edit_checklist').attr('readonly', false);
				// $('#edit_checklist').trigger('change');
			} else {
				$('#edit_checklist').attr('readonly', true);
				if($('#edit_checklist_real').val() == '0'){
					$('#edit_checklist_real').val('0');
				}
			}
		});

		$( "#lock-pool-edit" ).click(function() {
			$(this).toggleClass('unlocked');
			if($(this).hasClass('unlocked')) {
				$('#edit_pool').attr('readonly', false);
				$('#edit_pool').html('');
				$('#edit_pn_del').html('');

				$.ajax({
					url: "<?php echo base_url('huawei/transaction/get_all_pn_comp_in_all_warehouse');?>",
					type : "POST",
					dataType: 'json',
					data: {'pn' : $('#edit_pn_req').val()},
					success : function(data){
						if(data.length !== 0){
							$(data).each(function(k,v) {
								$('#edit_pool').append(new Option(v.name_warehouse, v.id_warehouse));
								$('#edit_pool').trigger('change');
							});
						}
					},
				});
			} else {
				$('#edit_pool').attr('readonly', true);
				$('#edit_pn_req').trigger('change');
			}
		});

		$('#add_pn_pool').on('change', function(){
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_pn_comp_by_warehouse');?>",
				type : "POST",
				dataType: 'json',
				data: {'pn' : $('#add_pn_pn_req').val(), 'id_warehouse' : $('#add_pn_pool').val()},
				success : function(data){
					$('#add_pn_pn_del').html('');
					if(data.length !== 0){
						$(data).each(function(k,v) {
							$('#add_pn_pn_del').append(new Option(v.name_pn, v.id_pn));
							$('#add_pn_pn_del').trigger('change');
						});
					}
				},
			});
		});

		$('#edit_pool').on('change', function(){
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_pn_comp_by_warehouse');?>",
				type : "POST",
				dataType: 'json',
				data: {'pn' : $('#edit_pn_req').val(), 'id_warehouse' : $('#edit_pool').val()},
				success : function(data){
					$('#edit_pn_del').html('');
					if(data.length !== 0){
						$(data).each(function(k,v) {
							$('#edit_pn_del').append(new Option(v.name_pn, v.id_pn));
							$('#edit_pn_del').trigger('change');
						});
					}
				},
			});
		});

		$('#btn-create-mdn').click(function(){
			$('#create_mdn_logistic').html('');
			$('#create_mdn_logistic_service').html('');
			$('#create_mdn_rmr_list').html('');
			$('#create_mdn_awb').val('');

			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_create_mdn_data');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					$(data.logistic).each(function(k,v) {
						$('#create_mdn_logistic').append(new Option(v.name_logistic, v.id_logistic));
						$('#create_mdn_logistic').trigger('change');
					});
					$(data.logistic_service).each(function(k,v) {
						$('#create_mdn_logistic_service').append(new Option(v.name_logisticservice, v.id_logisticservice));
						$('#create_mdn_logistic_service').trigger('change');
					});
					$(data.rmr_list).each(function(k,v) {
						$('#create_mdn_rmr_list').append(new Option(v.rmr, v.id_request));
						$('#create_mdn_rmr_list').trigger('change');
					});
					$('#modal-create-mdn').modal('show');
				},
			});
		});

		$('.tab-btn > .btn-item').click(function(){
			$('.tab-btn > .btn-item').removeClass('active');
			$(this).addClass('active');
		});

		$('.tab-btn').children('.btn-item').eq(0).click(function(){
			$('#req-list').hide();
			$('#in-delivery-list').hide();
			$('#to-pickup-list').hide();
			$('#create-req').show();
			$('.tab-btn').show();
		});

		$('.tab-btn').children('.btn-item').eq(1).click(function(){
			$('#req-list').show();
			$('#create-req').hide();
			$('#in-delivery-list').hide();
			$('#to-pickup-list').hide();
			requestList.clear().draw();
			let user_level = "<?= $this->session->userdata('level'); ?>";

			if(user_level == 10 || user_level == 4 || user_level == 69) {
				$('#btn-create-mdn').show();
			} else {
				$('#btn-create-mdn').hide();
			}

			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_all_request_progress');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					let btn_edit = '';
					let btn_add_sn = '';
					let status_request = '';

					$(data).each(function(k,v) {
						if(user_level == 3 || user_level == 2 || user_level == 69){
							btn_edit = '<button onclick="editDoc('+v.id_request+')" class="btn btn-warning btn-sm">Edit</button>';
						} else if (user_level == 10 || user_level == 4 || user_level == 69){
							btn_add_sn = '<button onclick="addSN('+v.id_request+')" class="btn btn-warning btn-sm">Add SN</button>';
						}

						if(v.status_request){
							status_request = v.status_request;
						}

						requestList.row.add([
							k+1,
							v.rmr,
							status_request,
							v.time_request,
							v.name_dop,
							v.name_warehouse,
							v.name_pn,
							btn_edit + btn_add_sn
							]).draw( false );
					});
				},
			});
		});

		let level = '<?= $this->session->userdata('level')?>';
		if(level == 3 || level == 2 || level == 69){
			$('.tab-btn').children('.btn-item').eq(0).trigger('click');
		} else if(level == 10 || level == 4) {
			$('.tab-btn').children('.btn-item').eq(1).trigger('click');
			$('.tab-btn').children('.btn-item').eq(0).hide();
		}

		$('.tab-btn').children('.btn-item').eq(2).click(function(){
			$('#req-list').hide();
			$('#create-req').hide();
			$('#in-delivery-list').show();
			$('#to-pickup-list').hide();
			indeliveryList.clear().draw();
			let user_level = "<?= $this->session->userdata('level'); ?>";

			if(user_level == 10 || user_level == 4 || user_level == 69) {
				$('#btn-upload-mdn').show();
				$('#btn-reupload-mdn').show();
			} else {
				$('#btn-upload-mdn').hide();
				$('#btn-reupload-mdn').hide();
			}

			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_all_request_in_delivery');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					let btn_edit = '';
					let btn_print_dop = '';

					$(data).each(function(k,v) {
						if(user_level == 10 && v.id_pod != null || user_level == 4 && v.id_pod != null || user_level == 69 && v.id_pod != null){
							btn_print_dop = '<a style="color: white;" class="btn btn-primary btn-sm" href="<?=base_url()?>huawei/all_process/docPrintRequest/'+v.id_pod+'/'+1+'" target="_blank">Print MDN</a>';
                            // btn_print_dop = '<a class="btn btn-primary btn-sm">Print MDN</a>';
                        } else if(user_level == 3 || user_level == 69){
                        	btn_edit = '<button onclick="editDoc('+v.id_request+')" class="btn btn-warning btn-sm">Edit</button>';
                        }

                        indeliveryList.row.add([
                        	k+1,
                        	v.rmr,
                        	v.time_request,
                        	v.name_dop,
                        	v.name_warehouse,
                        	'PN: '+v.name_pn+'</br>SN: '+v.name_sn,
                        	v.site_id,
                        	dhm(Math.abs(new Date(v.eta_received) - new Date())),
                        	btn_edit + btn_print_dop
                        	]).draw( false );
                    });
				},
			});
		});

		$('.tab-btn').children('.btn-item').eq(3).click(function(){
			$('#req-list').hide();
			$('#create-req').hide();
			$('#in-delivery-list').hide();
			$('#to-pickup-list').show();
			topickupList.rows().remove().draw();
			let user_level = "<?= $this->session->userdata('level'); ?>";

			if(user_level == 10 || user_level == 4 || user_level == 69) {
				$('#btn-create-cmn').show();
				$('#btn-upload-cmn').show();
				$('#btn-reupload-cmn').show();
			} else {
				$('#btn-create-cmn').hide();
				$('#btn-upload-cmn').hide();
				$('#btn-reupload-cmn').hide();
			}


			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_all_request_to_pickup');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					let btn_edit = '';
					let btn_pickup = '';
					let btn_print_cmn = '';
					let btn_special = '';

					$(data).each(function(k,v) {
						if(user_level == 3 || user_level == 2 || user_level == 69){
							if(v.pickup_request_time == null){
								btn_pickup = '<button onclick="pickAction('+v.id_request+')" class="btn btn-primary btn-sm">Pickup</button>';
								btn_special = '<button style="margin-top: 10px;" onclick="specialAction('+v.id_request+')" class="btn btn-danger btn-sm">Close Pickup</button>';
							}
							btn_edit = '<button style="margin-right: 10px; margin-bottom: 10px;" onclick="editDoc('+v.id_request+')" class="btn btn-warning btn-sm">Edit</button>';
						} else if (user_level == 10 && v.id_pickup != null || user_level == 4 && v.id_pickup != null || user_level == 69 && v.id_pickup != null){
							btn_print_cmn = '<a style="color:white;" href="<?=base_url()?>huawei/all_process/docPrintRequest/'+v.id_pickup+'/'+2+'" target="_blank" class="btn btn-primary btn-sm">Print CMN</a>';
						}

						topickupList.row.add([
							k+1,
							v.rmr,
							v.name_dop,
							v.name_pn,
							v.order+'</br>'+v.rma,
							v.consignee,
							v.pickup_request_time,
							dhm(Math.abs(new Date(v.time_request) - new Date())),
							btn_edit + btn_pickup + btn_print_cmn + btn_special
							]).draw( false );
					});
				},
			});
		});

		$('#add_sn_rmr').on('keyup', function(e){
			if($(this).val() == '') {
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			} else {
				if(e.keyCode === 13){
					$.ajax({
						url: "<?php echo base_url('huawei/transaction/add_sn_by_rmr');?>",
						type : "POST",
						dataType: 'json',
						data: {'add_sn_id_doc' : $('#add_sn_id_doc').val(), 'add_sn_rmr' : $('#add_sn_rmr').val()},
						success : function(data){
							if(data){
								var swal_data = { title: 'Success', icon: 'success', text: 'SN successfully added', button:false, timer: 1000 };
								swal(swal_data).then(function() {
									$('#modal-add-sn-rmr').modal('hide');
									$('.tab-btn').children('.btn-item').eq(1).trigger('click');
								});
							}else{
								var swal_data = { title: 'Failed', icon: 'error', text: 'SN cannot be added', button:false, timer: 1000 };
								swal(swal_data).then(function() {
								});
							}
						},
					});
				}
			}
		});

		$('#btn-upload-mdn').click(function(){
			$('#upload_mdn_pod').html('');
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_pod_list');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					$(data).each(function(k,v) {
						$('#upload_mdn_pod').append(new Option(v.pod_no, v.id_pod));
						$('#upload_mdn_pod').trigger('change');
					});
				},
			});
			$('#modal-upload-file-mdn').modal('show');
		});

		$('#btn-reupload-mdn').click(function(){
			$('#loading-reupload-mdn').hide();
			$('#reupload_mdn_doc_id').html('');
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_pod_list_reupload');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					$(data).each(function(k,v) {
						$('#reupload_mdn_doc_id').append(new Option(v.pod_no, v.id_pod));
						$('#reupload_mdn_doc_id').trigger('change');
					});
				},
			});
			$('#modal-reupload-file-mdn').modal('show');
		});

		$('#reupload_mdn_doc_id').on('change', function(){
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_image_mdn_reupload');?>",
				type : "POST",
				dataType: 'json',
				data: {'doc_id' : $('#reupload_mdn_doc_id').val()},
				success : function(data){
					$('#preview_reupload_mdn').attr('src', '<?=base_url()?>'+data.url_photo_mdn);
				},
			});
		});

		$('#reupload-mdn-process').click(function(){
			if(typeof $("#reupload_mdn").prop('files')[0] == 'undefined'){
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}else{
				var my_formData = new FormData($("#form-reupload-mdn")[0]);
				my_formData.append('file', $("#reupload_mdn").prop('files')[0]);
				$('#loading-reupload-mdn').show();
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/reupload_mdn_process');?>",
					type : "POST",
					dataType: 'json',
					data: my_formData,
					processData: false,
					contentType: false,
					cache: false,
					success : function(data){
						$('#loading-reupload-mdn').hide();
						if(data.stat){
							var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
							swal(swal_data).then(function() {
								$('#body-reupload-mdn * > input').val('');
								$('#modal-reupload-file-mdn').modal('hide');
							});
						}else{
							var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
							swal(swal_data).then(function() {
							});
						}
					},
				});
			}
		});

		$('#btn-reupload-cmn').click(function(){
			$('#loading-reupload-cmn').hide();
			$('#reupload_cmn_doc_id').html('');
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_cmn_list_reupload');?>",
				type : "GET",
				dataType: 'json',
				success : function(data){
					$(data).each(function(k,v) {
						$('#reupload_cmn_doc_id').append(new Option(v.pickup_no, v.id_pickup));
						$('#reupload_cmn_doc_id').trigger('change');
					});
				},
			});
			$('#modal-reupload-file-cmn').modal('show');
		});

		$('#reupload_cmn_doc_id').on('change', function(){
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/get_image_cmn_reupload');?>",
				type : "POST",
				dataType: 'json',
				data: {'doc_id' : $('#reupload_cmn_doc_id').val()},
				success : function(data){
					$('#preview_reupload_cmn').attr('src', '<?=base_url()?>'+data.url_photo_cmn);
				},
			});
		});

		$('#reupload-cmn-process').click(function(){
			if(typeof $("#reupload_cmn").prop('files')[0] == 'undefined'){
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}else{
				var my_formData = new FormData($("#form-reupload-cmn")[0]);
				my_formData.append('file', $("#reupload_cmn").prop('files')[0]);
				$('#loading-reupload-cmn').show();
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/reupload_cmn_process');?>",
					type : "POST",
					dataType: 'json',
					data: my_formData,
					processData: false,
					contentType: false,
					cache: false,
					success : function(data){
						$('#loading-reupload-cmn').hide();
						if(data.stat){
							var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
							swal(swal_data).then(function() {
								$('#body-reupload-cmn * > input').val('');
								$('#modal-reupload-file-cmn').modal('hide');
							});
						}else{
							var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
							swal(swal_data).then(function() {
							});
						}
					},
				});
			}
		});

		$('#loading-upload-mdn').hide();

		$('#upload-mdn-process').click(function(){
			if(typeof $("#upload_mdn_file").prop('files')[0] == 'undefined' || $("#form-upload-mdn").find('input').val() == ''){
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
			}else{
				var my_formData = new FormData($("#form-upload-mdn")[0]);
				my_formData.append('file', $("#upload_mdn_file").prop('files')[0]);
				$('#loading-upload-mdn').show();
				$.ajax({
					url: "<?php echo base_url('huawei/transaction/upload_mdn_process');?>",
					type : "POST",
					dataType: 'json',
					data: my_formData,
					processData: false,
					contentType: false,
					cache: false,
					success : function(data){
						$('#loading-upload-mdn').hide();
						if(data.stat){
							var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
							swal(swal_data).then(function() {
								$('#body-upload-mdn * > input').val('');
								$('#modal-upload-file-mdn').modal('hide');
								$('.tab-btn').children('.btn-item').eq(3).trigger('click');
							});
						}else{
							var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
							swal(swal_data).then(function() {
							});
						}
					},
				});
			}
		});

		$('#pickup_add_process').click(function(){
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/pickup_rmr');?>",
				type : "POST",
				dataType: 'json',
				data: $('#form-pickup-add').serialize(),
				success : function(data){
					if(data){
						var swal_data = { title: 'Success', icon: 'success', text: 'successfully saved', button:false, timer: 1000 };
						swal(swal_data).then(function() {
							$('#modal-pickup-add').modal('hide');
							$('.tab-btn').children('.btn-item').eq(3).trigger('click');
						});
					}
				},
			});
		});

		$('#edit_checklist').on('change', function(){
			$('#edit_checklist_real').val($(this).val());
		});

		$('#close_pickup_process').click(function(){
			$.ajax({
				url: "<?php echo base_url('huawei/transaction/close_pickup');?>",
				type : "POST",
				dataType: 'json',
				data: $('#form-pickup-close').serialize(),
				success : function(data){
					if(data){
						var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
						swal(swal_data).then(function() {
							$('.tab-btn').children('.btn-item').eq(3).trigger('click');
						});
					}else{
						var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
						swal(swal_data).then(function() {
						});
					}
				},
			});
		});
	});

function dhm(t){
	var cd = 24 * 60 * 60 * 1000,
	ch = 60 * 60 * 1000,
	d = Math.floor(t / cd),
	h = Math.floor( (t - d * cd) / ch),
	m = Math.round( (t - d * cd - h * ch) / 60000),
	pad = function(n){ return n < 10 ? '0' + n : n; };
	if( m === 60 ){
		h++;
		m = 0;
	}
	if( h === 24 ){
		d++;
		h = 0;
	}

	var str = [d, pad(h), pad(m)].join(':');
	str = str.split(":");
	var rtn = str[0] + ' Day ' + str[1] + ' Hours ' + str[2] + ' Minutes';
	return rtn;
}

function specialAction(doc_id){
	$('#modal-pickup-close').modal('show');
	$('#close_pickup_id_request').val(doc_id);
}

function editDoc(doc_id){
	$("#modal-edit-doc-request").modal('show');
	$('#edit_id_doc').val(doc_id);

	$.ajax({
		url: "<?php echo base_url('huawei/transaction/get_request_doc_detail');?>",
		type : "POST",
		dataType: 'json',
		data: {'id_request' : doc_id},
		success : function(data){
			$('#edit_time_request').val(data[0].time_request);
			$('#edit_site').val(data[0].site_id);
			$('#edit_pn_req').val(data[0].pn_req);
			$('#edit_id_dop').val(data[0].id_dop);

			$('#edit_pool').append(new Option(data[0].name_warehouse, data[0].id_warehouse));
			$('#edit_pool').val(data[0].id_warehouse);

			$('#edit_pn_del').append(new Option(data[0].pn_del, data[0].id_pn));
			$('#edit_pn_del').val(data[0].id_pn);

			if(data[0].actual_received == null){
				$('#edit_receive_time_request').val('');
				$('#edit_receive_time_request').prop('disabled', true);
			} else {
				$('#edit_receive_time_request').prop('disabled', false);
				$('#edit_receive_time_request').val(data[0].actual_received);
			}

			if(data[0].actual_pickup == null){
				$('#edit_pickup_time_request').val('');
				$('#edit_pickup_time_request').prop('disabled', true);
			} else {
				$('#edit_pickup_time_request').prop('disabled', false);
				$('#edit_pickup_time_request').val(data[0].actual_pickup);
			}

			$('#edit_checklist_real').val(data[0].id_status_request);

			if(data[0].id_status_request == 2 || data[0].id_status_request == 3 || data[0].id_status_request == 4){
				if(!$('#lock-stat-edit').hasClass('unlocked')){
					$('#lock-stat-edit').trigger('click');
					$('#edit_checklist').val(data[0].id_status_request);
				}
			} else {
				if($('#lock-stat-edit').hasClass('unlocked')){
					$('#lock-stat-edit').trigger('click');
				}
			}

			var sla = data[0].sla;
			sla = sla.split(" ");
			$('#edit_sla_hour').val(sla[0]);
			$('#edit_sla_minute').val(sla[2]);
			$('#edit_remark').val(data[0].remark);
		},
	});
}

function addSN(doc_id){
	$("#modal-add-sn-rmr").modal('show');
	$('#add_sn_id_doc').val(doc_id);
}

function pickAction(doc_id){
	$('#modal-pickup-add').modal('show');
	$('#pickup_id_request').val(doc_id);
}

function formatOutput (optionElement) {
	if (!optionElement.id) { return optionElement.text; }
	var split = optionElement.text.split(', address: ');
	var $state = $(
		'<span><strong>' + split[0] + '</strong>, address: ' + split[1]+'</span>'
		);
	return $state;
};

function printSN(doc_id){
	window.open(
		'<?= base_url()?>/huawei/all_process/makePdfSN/'+doc_id,
		'_blank'
		);
}

function printDOC(doc_id){
	window.open(
		'<?= base_url()?>/huawei/all_process/docPrint/'+doc_id,
		'_blank'
		);
}
</script>
