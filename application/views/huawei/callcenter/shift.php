<?php 
if(isset($_GET['bln'])) $setmonth=$_GET['bln']; else $setmonth=date('n');
if(isset($_GET['week'])) $setweek=$_GET['week']; else $setweek=(int)date('W');
if(isset($_GET['thn'])) $setyear=$_GET['thn']; else $setyear=date('Y');
$d=cal_days_in_month(CAL_GREGORIAN, $setmonth, $setyear);
for($i=0;$i<$d;$i++) {
    $days_this_month[]=$i+1;
}

$dto = new DateTime();
for($w=1; $w<=53; $w++) {
    $dto->setISODate($setyear,$w);
    $week[$w]['start']=$dto->format('Y-m-d');
    $week[$w]['fstart']=$dto->format('j M Y');
    $week[$w]['daystart']=$dto->format('l');
    $week[$w]['Ystart']=$dto->format('Y');
    $dto->modify('+6 days');
    $week[$w]['end']=$dto->format('Y-m-d');
    $week[$w]['fend']=$dto->format('j M Y');
    $week[$w]['dayend']=$dto->format('l');
    $week[$w]['Yend']=$dto->format('Y');
}

foreach($jadwal_cc as $row) {
    $jadwal[$row['tanggal']][$row['id_user']][$row['id_shift']]=$row['kode_shift'];
    $bgclr[$row['tanggal']][$row['id_user']][$row['id_shift']]=$row['bgcolor'];
}

?>

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CALL CENTER SHIFT</p>
                </div>
                <?php
                //foreach($_GET as $id=>$val) {
                //    echo "<br>".$id." --> ".$val;
                //}
                //echo "<br>".$cekdb;
                ?>
                <div style="margin-bottom: 34px;" class="input-div">
                    <form id="this-form" method=GET>
                        <div id="customer-list" class="my-form-group">
                            <p class="my-label-input">Year :</p>
                            <select id="id_customer" name="thn" class="form-control select2">
                                <? for($i=2019;$i<=2050;$i++) {?>
                                    <option <?php if($i==$setyear) echo 'selected ';?>value="<?=$i?>"><?=$i?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">Week :</p>
                            <select id="id_project" name="week" class="form-control select2">
                                <?php
                                foreach($week as $w=>$weekcount) {
                                    if($weekcount['Ystart']==$setyear || $weekcount['Yend']==$setyear) {
                                        echo "<option ";
                                        if($w==$setweek) echo "selected ";
                                        echo "value=".$w.">";
                                        echo "[ W".$w." ] : ".$weekcount['fstart']." - ".$weekcount['fend'];
                                        echo "</option>";
                                    }
                                    ?>
                                    <?php
                                    //echo "<br>W".$w." -- ".$weekcount['daystart']."-".$weekcount['fstart']."-".$weekcount['start']." -- ".$weekcount['dayend']."-".$weekcount['end'];
                                }
                                ?>
                            </select>
                        </div>
                        <button style="float: right;" name="chkweek" id="form-proceed" class="btn btn-primary">Go</button>
                        
                        <?php
                        foreach($data_shift as $data_shift) {
                            $nama_shift[$data_shift['id_shift']]=$data_shift['nama_shift'];
                            $kode_shift[$data_shift['id_shift']]=$data_shift['kode_shift'];
                            $bgclr_shift[$data_shift['id_shift']]=$data_shift['bgcolor'];
                        }
                        if($this->session->userdata('level')==2 || $this->session->userdata('level')==69) {
                        ?>
                            <div class="row my-row"><br><br></div>

                            <?php
                            if(isset($_GET['input_date'])) $val_input_date=" value='".$_GET['input_date']."' "; 
                            else $val_input_date="";
                            ?>

                            <div class="my-form-group">
                                <p class="my-label-input">Date :</p>
                                <input type="text" id="input_date" <?=$val_input_date ?> name="input_date" class="form-control input-datetime">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Shift :</p>
                                <select id="id_project" name="input_shift" class="form-control select2">
                                    <?php
                                    foreach($nama_shift as $id=>$val) {
                                        echo "<option value=".$id;
                                        if(isset($_GET['input_shift']) && $_GET['input_shift']==$id) echo " selected ";
                                        echo ">";
                                        echo $val;
                                        echo "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Call Center :</p>
                                <select id="id_project" name="input_user" class="form-control select2">
                                    <?php
                                    foreach($cc_list as $row) {
                                        echo "<option value=".$row['id_user'];
                                        if(isset($_GET['input_user']) && $_GET['input_user']==$row['id_user']) echo " selected ";
                                        echo ">";
                                        echo $row['name_user'];
                                        echo "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <button style="float: right;" name="deleteshift" id="form-proceed" class="btn btn-danger"> Delete </button>
                            <button style="float: right;" name="saveshift" id="form-proceed" class="btn btn-success"> Update </button>
                            <?php
                        }
                        ?>
                    </form>
                </div>
                <?php
                ?>

                <div id="div-chart-col">
                    <div class="inbound-div" style="width: 100%;">
                        <div class="table-responsive">
                            <table id="inbound-list" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <?php
                                        echo "<th rowspan=2 bgcolor='#A0A0A0'>WEEK ".$setweek."</th>";
                                        
                                        $dto->setISODate($setyear,$setweek);
                                        for($g=0; $g<7; $g++) {
                                            if($g>0) $dto->modify('+1 days');
                                            $listdate[$dto->format('Y-m-d')]=$dto->format('j-M-Y');
                                            $daydate[$dto->format('Y-m-d')]=$dto->format('l');
                                        }
                                        foreach($listdate as $tgl=>$dateview) {
                                            echo "<th colspan=3 bgcolor='#A0A0A0'>".$daydate[$tgl]."<br>".$dateview."</th>";
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <?php
                                        foreach($listdate as $tgl=>$dateview) {
                                            foreach($kode_shift as $idshift=>$val) {
                                                echo "<th bgcolor='".$bgclr_shift[$idshift]."'>".$val."</th>";
                                            }
                                        }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $bgcolor="#FFCCCC";
                                    foreach($cc_list as $row) {
                                        //echo $row['id_user'] . "--" . $row['name_user'];
                                        echo "<tr>";
                                            echo "<td bgcolor='#C0C0C0'>".$row['name_user']."</td>";
                                            foreach($listdate as $tgl=>$dateview) {
                                                foreach($kode_shift as $idshift=>$val) {
                                                    if(isset($jadwal[$tgl][$row['id_user']][$idshift])) {
                                                        echo "<td bgcolor='".$bgclr[$tgl][$row['id_user']][$idshift]."'>";
                                                        echo $jadwal[$tgl][$row['id_user']][$idshift];
                                                    } else {
                                                        echo "<td bgcolor='#FFCCCC'>x";
                                                    }
                                                    echo "</td>";
                                                }
                                            }
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <br\>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
</div>

<script>
    $(function () {
        let user_level = '<?= $this->session->userdata('level')?>';

        $('.input-datetime').daterangepicker(
        {
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }
        );
    });
</script>


<style>
    .my-row {
        margin: 10px !important;
        border-radius: 5px;
        /*height: 80%;*/
        display: block !important;
    }

    div.inbound-div, div.outbound-div {
        padding-top: 42px;
    }

    div.dataTables_wrapper div.dataTables_filter, div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        font-size: 12px;
    }

    #div-chart-col {
        display: flex;
    }

    .btn {
        font-size: 12px;
        font-weight: 300;
    }

    .header-jdl {
        display: flex !important;
    }

    .tab-btn {
        margin-top: 20px;
        display: flex;
    }

    .tab-btn > button {
        background: transparent;
        font-size: 12px;
        margin-right: 10px;
        color: #0069D9;
        box-shadow: none !important;
        font-weight: 300;
    }

    .tab-btn > button.active {
        background: #0069D9;
        font-size: 12px;
        margin-right: 10px;
        color: white;
        box-shadow: 0px 3px 5px 1px rgba(0, 0, 0, 0.3) !important;
    }

    .jdl-big {
        margin-top: 18px;
        font-size: 16px;
        border-bottom: 4px solid #dc3545;
    }

    .add-pn-div {
        margin-top: 20px;
    }

    #btn-add-pn {
        font-size: 12px;
        color: white;
        margin-bottom: 22px;
        font-weight: 300;
    }

    .input-div {
        position: relative;
        margin-left: 20px;
        margin-top: 46px;
        width: 60%;
    }

    .table-div, .temp-div, .put-away {
        margin-left: 20px;
        margin-top: 46px;
    }

    .table.table-bordered {
        font-size: 12px;
    }

    .my-label-input {
        font-size: 12px;
        font-weight: 300;
        /* margin-top: 7px; */
        margin-bottom: 20px;
    }

    .form-control {
        height: 30px;
        font-size: 12px;
        font-weight: 300;
        margin-bottom: 10px;
        width: 50%;
    }

    .my-form-group {
        display: flex;
        justify-content: space-between;
    }

    .input-add-pn {
        outline: 0;
        border-radius: .25rem;
        height: 30px;
        font-size: 12px;
        font-weight: 300;
        border: 1px solid #ced4da;
    }

    .my-btn.save:hover{
        background: white;
        color: #dc3545;
    }

    #saveForm {
        position: absolute;
        right: 0;
        margin-top: 34px;
        margin-bottom: 34px;
    }

    #div-chart-col .inbound-div > div.card-body {
        width: 480px;
        height: 480px;
        margin: 0 auto;
    }

    @media only screen and (max-width: 600px) {
        .content, .container, .my-row {
            margin: 0 !important;
            padding: 0 !important;
        }

        .my-row {
            padding: 0 12px !important;
        }

        .inbound-div {
            display: block;
        }

        .input-div {
            width: 100%;
            margin: 0;
            margin-top: 34px;
        }

        #div-chart-col {
            display: block;
        }

        #div-chart-col .inbound-div:first-child {
            width: 100% !important;
            overflow: auto !important;
        }

        #div-chart-col .inbound-div:last-child {
            width: 100% !important;
            overflow: auto !important;
        }

        /*#div-chart-col .inbound-div > div.card-body canvas {
            overflow: auto;
        }*/

        #div-chart-col .inbound-div > div.card-body {
            width: 80vw !important;
            padding: 0;
        }

        #div-chart-col .inbound-div:last-child div:first-child {
            display: block !important;
        }
    }
</style>




