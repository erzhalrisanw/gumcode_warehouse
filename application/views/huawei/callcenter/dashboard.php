<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row my-row">
                <div class="card-col">
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-primary">
                            <i class="fas fa-users"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Request</span>
                                <span class="info-box-number card-top-count1">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-info">
                            <i class="fas fa-clock"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Process</span>
                                <span class="info-box-number card-top-count2">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-warning">
                            <i class="fas fa-truck"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">In Delivery</span>
                                <span class="info-box-number card-top-count3">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-success">
                            <i class="fas fa-thumbs-up"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Success</span>
                                <span class="info-box-number card-top-count4">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-danger">
                            <i class="fas fa-thumbs-down"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">Failed</span>
                                <span class="info-box-number card-top-count5">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4 col-md-2 my-card">
                        <div class="info-box bg-secondary">
                            <i class="fas fa-bell"></i>
                            <div class="info-box-content">
                                <span class="info-box-text">OOS</span>
                                <span class="info-box-number card-top-count6">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!--
                <div class="header-jdl">
                    <p class="jdl-big">CallCenter Email Unflag</p>
                </div>
                -->

                <div class="temp-div table-responsive">
                    <p class="jdl-big">CALLCENTER EMAIL UNFLAG</p>
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>From</th>
                                <th>Subject</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? if(isset($listemail)) {
                                foreach($listemail as $listemail){ ?>
                                <?= '<tr>' ?>
                                <?= '<td>'.$listemail['date'].'</td>' ?>
                                <?= '<td>'.$listemail['from'].'</td>' ?>
                                <?= '<td>'.$listemail['subject'].'</td>' ?>
                                <?= '</tr>' ?>
                            <?  } 
                               }?>
                        </tbody>
                    </table>
                </div>

                <div class="temp-div table-responsive">
                    <p class="jdl-big">REQUEST PROCESSING & INDELIVERY</p>
                    <table id="inprocessList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>REMAIN</th>
                                <th>STATUS</th>
                                <th>ETA</th>
                                <th>RMR</th>
                                <th>RMA</th>
                                <th>DOP</th>
                                <th>FSL</th>
                                <th>TYPE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach($inprocess as $inprocess){ ?>
                                <?= '<tr>' ?>
                                <?= '<td bgcolor="'.$inprocess['bgcolor'].'" >'.$inprocess['sisawaktu'].'</td>' ?>
                                <?= '<td>'.$inprocess['status_request'].'</td>' ?>
                                <?= '<td>'.$inprocess['eta_received'].'</td>' ?>
                                <?= '<td>'.$inprocess['rmr'].'</td>' ?>
                                <?= '<td>'.$inprocess['rma'].'</td>' ?>
                                <?= '<td>'.$inprocess['name_dop'].'</td>' ?>
                                <?= '<td>'.$inprocess['name_warehouse'].'</td>' ?>
                                <?= '<td>'.$inprocess['delivery_type'].'</td>' ?>
                                <?= '</tr>' ?>
                            <? } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {

        var today = new Date(),
        firstDay = new Date(today.getFullYear(), today.getMonth(), 1),
        lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0),
        days_this_month = [];

        let level_user = '<?= $this->session->userdata('level')?>';
        let user_cust = '<?= $this->session->userdata('id_customer')?>';

        while(firstDay <= lastDay){
            days_this_month.push(firstDay.getDate());
            firstDay.setDate(firstDay.getDate() + 1);
        }

        tempList = $('#tempList').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : false,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data ...",
                zeroRecords: "No Data ..."
            }
        });

        inprocessList = $('#inprocessList').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : false,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data ...",
                zeroRecords: "No Data ..."
            }
        });

        $.ajax({
            url: "<?php echo base_url('huawei/home/get_request_data_all_card');?>",
            type : "POST",
            dataType: 'json',
            data: {'days_this_month' : days_this_month},
            success : function(data){
                $('.card-top-count1').text(Number(data.total_request));
                $('.card-top-count2').text(Number(data.total_process));
                $('.card-top-count3').text(Number(data.total_in_delivery));
                $('.card-top-count4').text(Number(data.total_success));
                $('.card-top-count5').text(Number(data.total_failed));
                $('.card-top-count6').text(Number(data.total_oos));
            },
        });

    });
</script>
<style>
    .my-row {
        margin: 10px !important;
        border-radius: 5px;
        /*height: 80%;*/
        display: block !important;
    }

    .card-col {
        display: flex;
        width: 100%;
        justify-content: space-between;
        margin-bottom: 34px;
    }

    .my-card > div {
        box-shadow: 0px 0px 9px 1px rgba(0,0,0,.3);
        border-radius: 8px;
        position: relative;
    }

    .my-card > div > i {
        font-size: 48px !important;
        color: white !important;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        left: 20px;
        opacity: .6;
    }

    .my-card > div > div > span:first-child {
        font-size: 15px;
        text-align: right;
        margin-bottom: 10px;
        color: white;
    }

    div.dataTables_wrapper div.dataTables_filter, div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        font-size: 12px;
    }

    .btn {
        font-size: 12px;
        font-weight: 300;
    }

    .header-jdl {
        display: flex !important;
    }


    .border-kecil {
        color: transparent;
        margin-top: -18px;
        background: #dc3545;
        font-size: 2px;
        width: 30px;
        border: 2px solid #dc3545;
        border-radius: 4px;
        margin-bottom: 34px;
    }

    .my-card > div > div > span:last-child {
        font-size: 20px;
        text-align: right;
        font-style: italic;
        color: white;
    }

    .header-jdl {
        display: flex !important;
        margin-bottom: 34px;
    }

    .jdl-big {
        margin-top: 18px;
        font-size: 16px;
        border-bottom: 4px solid #dc3545;
    }

    .div-chart {
        margin-left: 7.5px;
        width: 100%;
    }

    .iop-div {
        display: flex;
        margin-top: 42px;
        justify-content: space-between;
    }

    .tab-btn {
        margin-top: 20px;
        display: flex;
    }

    .tab-btn > button {
        background: transparent;
        font-size: 12px;
        margin-right: 10px;
        color: #0069D9;
        box-shadow: none !important;
        font-weight: 300;
    }

    .tab-btn > button.active {
        background: #0069D9;
        font-size: 12px;
        margin-right: 10px;
        color: white;
        box-shadow: 0px 3px 5px 1px rgba(0, 0, 0, 0.3) !important;
    }

    .jdl-big {
        margin-top: 18px;
        font-size: 16px;
        border-bottom: 4px solid #dc3545;
    }

    .add-pn-div {
        margin-top: 20px;
    }

    #btn-add-pn {
        font-size: 12px;
        color: white;
        margin-bottom: 22px;
        font-weight: 300;
    }

    .input-div {
        position: relative;
        margin-left: 20px;
        margin-top: 46px;
        width: 60%;
    }

    .table-div, .temp-div, .put-away {
        margin-left: 20px;
        margin-top: 46px;
    }

    .table.table-bordered {
        font-size: 12px;
    }

    .my-label-input {
        font-size: 12px;
        font-weight: 300;
        /* margin-top: 7px; */
        margin-bottom: 20px;
    }

    .form-control {
        height: 30px;
        font-size: 12px;
        font-weight: 300;
        margin-bottom: 10px;
        width: 50%;
    }

    .my-form-group {
        display: flex;
        justify-content: space-between;
    }

    .input-add-pn {
        outline: 0;
        border-radius: .25rem;
        height: 30px;
        font-size: 12px;
        font-weight: 300;
        border: 1px solid #ced4da;
    }

    .my-btn.save:hover{
        background: white;
        color: #dc3545;
    }

    #saveForm {
        position: absolute;
        right: 0;
        margin-top: 34px;
        margin-bottom: 34px;
    }

    @media only screen and (max-width: 600px) {
    }

    @media screen and (max-width: 600px) {
        .my-row {
            padding-top: 24px;
        }

        .card-col {
            flex-flow: row wrap;
        }

        #chart_donat_1, #chart_donat_2, #chart_donat_3, #chart_pie_1, #chart_pie_2 {
            width: 100%;
        }

        .iop-div {
            display: block;
        }

        .stocktake-div {
            display: block;
        }

        .info-box-text {
            font-size: 12px !important;
        }

        .my-card > div > i {
            font-size: 32px !important;
        }

        .my-card > div > div > span:last-child {
            font-size: 16px !important;
        }
    }

    @media screen and (max-width: 1024px) and (min-width: 600px) {
        .my-row {
            padding-top: 24px;
        }

        .card-col {
            flex-flow: row wrap;
        }

        /*#chart_donat_1, #chart_donat_2, #chart_donat_3, #chart_pie_1, #chart_pie_2 {
            width: 100%;
        }*/

        /*.iop-div {
            display: block;
        }

        .stocktake-div {
            display: block;
        }*/

        .info-box-text {
            font-size: 12px !important;
        }

        .my-card > div > i {
            font-size: 32px !important;
        }

        .my-card > div > div > span:last-child {
            font-size: 16px !important;
        }
    }    
</style>