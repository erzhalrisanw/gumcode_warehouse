<div class="content-wrapper">
	<section class="content">
		<div class="container-fluid">
			<div class="row my-row">
				<div class="header-jdl">
					<p class="jdl-big">Change Session</p>
				</div>

				<div id="sel-proceed">
					<div  class="my-form-group">
						<p style="margin-right: 20px; margin-top: 3px;" class="my-label-input">Proceed as :</p>
						<select id="sel-as" name="id_logistic_service" class="form-control select2">
						</select>
					</div>
					<input type="hidden" id="id-domain-redir">
					<div class="btn-process mt-2">
						<button id="btn-proceed" type="button" class="btn btn-primary">Proceed</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<script>
	$(function () {
		var domain = '<?=$this->session->userdata('domain')?>';
		$.ajax({
			url: "<?php echo base_url('huawei/all_process/log_as');?>",
			type : "POST",
			dataType: 'json',
			data: {'id_domain' : domain},
			success : function(data){
				$('#sel-as').html('');
				$(data).each(function(k,v) {
					$('#sel-as').append(new Option(v.name_user, v.id_warehouse));
					$("#sel-as").trigger("change");
				});
			},
		});

		$('#btn-proceed').click(function(){
			$.ajax({
				url: "<?php echo base_url('huawei/all_process/log_as_proceed');?>",
				type : "POST",
				dataType: 'json',
				data: {'id_warehouse' : $('#sel-as').val()},
				success : function(data){
					if(data){
						// var success = new Audio('<?php echo base_url(''); ?>/dist/tone/success_tone.mp3')
						// success.play();
						swal({
							title: "Success",
							icon: 'success',
							button: false,
							timer: 1000
						});
					}
				},
			});
		});
	});
</script>
