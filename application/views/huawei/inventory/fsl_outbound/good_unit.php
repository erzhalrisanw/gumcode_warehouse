<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">FSL Outbound</p>
                </div>
                <div class="tab-btn">
					<div class="btn-item active">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/request.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Create Document</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/list.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Document List</label>
						</div>	
					</div>
                </div>
                <div class="create-doc">
                    <form id="this-form">
                        <div class="input-div">
                            <div class="my-form-group">
                                <p class="my-label-input">Project :</p>
                                <select name="id_project" class="form-control select2">
                                    <?php foreach($proj as $proj) {?>
                                        <option value="<?=$proj['id_project']?>"><?=$proj['name_project']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Outbound Type :</p>
                                <select id="id_boundtype" name="id_boundtype" class="form-control select2" >
                                    <option value="">Please select Outbound type</option>
                                    <?php foreach($ib_type as $ib_type) {?>
                                        <option value="<?=$ib_type['id_io_boundtype']?>"><?=$ib_type['name_io_boundtype']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">To :</p>
                                <select id="id_boundto" name="id_boundto" class="form-control select2">
                                </select>
                            </div>
                        </div>
                    </form>
                    <div class="btn-process mt-4">
					    <button id="saveForm" type="button" class="btn btn-success mr-4">Proceed</button>
				    </div>
                </div>

                <div class="doc-list table-responsive">
                    <table id="docList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Document Number</th>
                                <th>Sent Date</th>
                                <th>Received Date</th>
                                <th>Qty / SN Added</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="modal-add-pn-temp" class="modal fade" role="dialog">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add temp PN & Quantity</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="this-form-temp">
                            <input id="doc_id_temp" name="doc_id_temp" type="hidden" class="form-control" style="width:70%;">
                            <div class="my-form-group">
                                <p class="my-label-input">SR :</p>
                                <input id="sr_temp" name="sr_temp" type="text" class="form-control" style="width:70%;">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">RF :</p>
                                <input id="rf_temp" name="rf_temp" type="text" class="form-control" style="width:70%;">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">PN :</p>
                                <input id="product_number_temp" name="product_number" type="text" class="form-control" style="width:70%;">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Quantity :</p>
                                <input id="qty_temp" name="qty" type="text" class="form-control" style="width:70%;">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="saveFormTemp" type="button" class="btn btn-success">Proceed</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-send" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Loading Good</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="this-form-send">
                            <input id="doc_id_send" name="doc_id_send" type="hidden" class="form-control">
                            <div class="my-form-group">
                                <p class="my-label-input">AWB :</p>
                                <input id="awb_no" name="awb_no" type="text" class="form-control">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Logistic :</p>
                                <select name="id_logistic" class="form-control">
                                    <?php foreach($logistic as $logistic) {?>
                                        <option value="<?=$logistic['id_logistic']?>"><?=$logistic['name_logistic']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Logistic Service :</p>
                                <select name="id_logisticservice" class="form-control">
                                    <?php foreach($logistic_service as $logistic_service) {?>
                                        <option value="<?=$logistic_service['id_logisticservice']?>"><?=$logistic_service['name_logisticservice']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="saveFormSend" type="button" class="btn btn-success">Proceed</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        addPnTable = $('#pnTable').DataTable({
            paging      : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "",
                zeroRecords: ""
            }
        });

        docList = $('#docList').DataTable({
            paging      : true,
            lengthChange: false,
            searching   : true,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        sngListCheck = $('#sn-list-check').DataTable({
            paging      : true,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "",
                zeroRecords: ""
            }
        });

        $('#pa_search_doc').on('keyup', function(e){
            if(e.keyCode === 13){
                $.ajax({
                    url: "<?php echo base_url('huawei/fsl/check_doc_exist_putaway_inbound');?>",
                    type : "POST",
                    dataType: 'json',
                    data: {'doc_no' : $('#pa_search_doc').val()},
                    success : function(data){
                        if(data){
                            $('#put-form-tab').show();
                            $('#putaway-sn-docid').val(data);

                            $('#putaway-sn-bin').trigger('focus');

                            $.ajax({
                                url: "<?php echo base_url('huawei/all_process/get_sn_by_doc_id_inbound');?>",
                                type : "POST",
                                dataType: 'json',
                                data: {'doc_id' : data},
                                success : function(data){
                                    sngListCheck.clear().draw();
                                    $(data).each(function(k,v) {
                                        var check;
                                        if(v.access == '6'){
                                            check = '';
                                        } else if (v.access == '7'){
                                            check = '<i style="font-size: 16px; color: green;" class="fa fa-check"></i>';
                                        }
                                        sngListCheck.row.add([
                                            k+1,
                                            v.pn,
                                            v.sn,
                                            v.loc_bin,
                                            check
                                            ]).draw( false );
                                    });
                                },
                            });
                        }else{
                            var swal_data = { title: 'Failed', icon: 'error', text: 'Document does not exist', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                        }
                    },
                });
            }
        });

        $('.tab-btn > .btn-item').click(function(){
            $('.tab-btn > .btn-item').removeClass('active');
            $(this).addClass('active');
        });

        $('.tab-btn').children('.btn-item').eq(1).click(function(){
            $('#put-form-tab').hide();
            $('.input-div').hide();
            $('.create-doc').hide();
            $('.put-away').hide();
            $('#pa_search_doc').val('');
            docList.clear().draw();

            $.ajax({
                url: "<?php echo base_url('huawei/fsl/get_outbound_fsl_doc_list');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $(data).each(function(k,v) {
                        var btn_add, btn_print_doc, btn_temp, btn_send, btn_close;

                        var btn_delete = '';
                        if(v.sn_added < 1){
                            btn_delete = '<button onclick="deleteDoc('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; width: 100%; margin-bottom: 10px !important;" class="btn btn-xs btn-danger" >Delete</button>';
                        }

                        if(v.status > 0 && v.id_logistic == null){
                            btn_print_doc = '<button onclick="printDOC('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; pointer: cursor; margin-bottom: 10px !important; width: 100%;" class="btn btn-xs btn-success">Print Doc</button>';
                            btn_close = '';
                            btn_temp = '<button onclick="show_modal_add_pn('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; width: 100%; margin-bottom: 10px !important;" class="btn btn-xs btn-primary" >Create Temp</button>';

                            if(v.qty == v.sn_added && v.sn_added > 0){
                                btn_send = '<button onclick="show_modal_send('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; pointer: cursor; margin-bottom: 10px !important; width: 100%;" class="btn btn-xs btn-success">Loading</button>';
                            }else{
                                btn_send = '';
                            }

                            if(v.qty > 0){
                                btn_add = '<a id="btn-add-sn-'+v.id+'" href="<?= base_url().'huawei/all_process/add_sn_good_faulty/'?>'+v.id+'" style="display:block; margin: 0 auto; margin-top: 10px; pointer: cursor; margin-bottom: 10px !important;" class="btn btn-xs btn-warning">Packing</a>';
                            }else{
                                btn_add = '';
                            }
                        } else {
                            if(v.status > 0) {
                                btn_temp = '';
                                btn_send = '';
                                btn_add = '';
                                btn_close = '<button onclick="closeDoc('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; pointer: cursor; margin-bottom: 10px !important; width: 100%;" class="btn btn-xs btn-danger">Close Doc</button>';
                                btn_print_doc = '<button onclick="printDOC('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; pointer: cursor; margin-bottom: 10px !important; width: 100%;" class="btn btn-xs btn-success">Print Doc</button>';
                            } else {
                                btn_temp = '';
                                btn_send = '';
                                btn_add = '';
                                btn_close = '';
                                btn_print_doc = '<button onclick="printDOC('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; pointer: cursor; margin-bottom: 10px !important; width: 100%;" class="btn btn-xs btn-success">Print Doc</button>';
                            }
                        }

                        docList.row.add([
                            k+1,
                            v.doc_no,
                            v.date_send,
                            v.date_received,
                            v.qty +' / '+v.sn_added,
                            btn_temp + btn_add + btn_send + btn_close + btn_print_doc + btn_delete
                            ]).draw( false );
                    });
                },
            });
            $('.doc-list').show();
        });

        $('.tab-btn').children('.btn-item').eq(0).click(function(){
            $('.input-div').show();
            $('.create-doc').show();
            $('.doc-list').hide();
            $('.put-away').hide();
            $('#put-form-tab').hide();
            $('#pa_search_doc').val('');
        });

        $('.tab-btn').children('.btn-item').eq(0).trigger('click');

        $('#pnTable tbody').on( 'click', '#del-pn', function () {
            var tr = $(this).closest('tr');
            addPnTable.row(tr).remove().draw();
        });

        $('#saveForm').click(function(){
            $.ajax({
                url: "<?php echo base_url('huawei/fsl/outbound_good_doc_submit');?>",
                type : "POST",
                dataType: 'json',
                data: $('#this-form').serialize(),
                success : function(data){
                    if(data){
                        var swal_data = {
                            title: data.doc_no,
                            text: data.doc_date,
                            icon: 'success',
                            buttons: {
                                confirm: {
                                    text: "Ok",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: true
                                }
                            }
                        };
                        swal(swal_data).then((value) => {
                            if(value){
                                $('.tab-btn').children('button').eq(1).trigger('click');
                            }
                        });
                    }else{
                        var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be saved, please contact Administrator', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                        });
                    }
                },
            });
        });

        $('#id_boundtype').on('change', function(){
            $.ajax({
                url: "<?php echo base_url('huawei/all_process/change_select_by_boundtype');?>",
                type : "POST",
                dataType: 'json',
                data: {'id_boundtype' : $(this).val()},
                success : function(data){
                    $('#id_boundto').html('');
                    $(data).each(function(k,v) {
                        $('#id_boundto').append(new Option(v.name_warehouse, v.id_warehouse));
                        $("#id_boundto").trigger("change");
                    });
                },
            });
        });

        $('#saveFormTemp').click(function(){
            if($('#qty_temp').val() == '' || $('#sr_temp').val() == '' || $('#rf_temp').val() == '' || $('#product_number_temp').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please fill out the quantity form', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/fsl/add_pn_temp_outbound');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form-temp').serialize(),
                    success : function(data){
                    // console.log(data);
                    if(data){
                        var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully saved', button: false, timer: 1000 };
                        swal(swal_data).then(function() {
                            $('#product_number_temp').val('');
                            $('#qty_temp').val('');
                            $('#rf_temp').val('');
                            $('#rf_temp').trigger('focus');
                        });
                    }else{
                        var swal_data = { title: 'Failed', icon: 'error', text: 'This PN is not registered in database', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                        });
                    }
                },
            });
            }
        });

        $('#saveFormSend').click(function(){
            if($('#awb_no').val() == '' || $('#id_logistic').val() == '' || $('#id_logisticservice').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please fill out the form', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            } else {
                $.ajax({
                    url: "<?php echo base_url('huawei/fsl/outbound_loading_update');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form-send').serialize(),
                    success : function(data){
                        if(data){
                            var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully saved', button: false, timer: 1000 };
                            swal(swal_data).then(function() {
                                $("#modal-send").modal('hide');
                            });
                        }else{
                            var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be saved', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                        }
                    },
                });
            }
        });

        $('#modal-add-pn-temp').on('hidden.bs.modal', function () {
            $('.tab-btn').children('button').eq(1).trigger('click');
        });

        $('#modal-send').on('hidden.bs.modal', function () {
            $('.tab-btn').children('button').eq(1).trigger('click');
        });

        $('#sr_temp').bind('keypress', function(e){
            if(e.keyCode === 13){
                $('#rf_temp').trigger('focus');
            }
        });

        $('#rf_temp').bind('keypress', function(e){
            if(e.keyCode === 13){
                $('#product_number_temp').trigger('focus');
            }
        });

        $('#product_number_temp').bind('keypress', function(e){
            if(e.keyCode === 13){
                $('#qty_temp').trigger('focus');
            }
        });

    });
function show_modal_add_pn(doc_id){
    $("#modal-add-pn-temp").modal('show');
    $("#doc_id_temp").val(doc_id);

    $('#product_number_temp').trigger('focus');
}

function show_modal_send(doc_id){
    $("#modal-send").modal('show');
    $("#doc_id_send").val(doc_id);
}

function deleteDoc(doc_id){
    var swal_data = {
        title: 'Are you sure?',
        text: 'To delete this document?',
        icon: 'warning',
        buttons: {
            cancel: {
                text: "No",
                value: false,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    };
    swal(swal_data).then((value) => {
        if(value){
            $.ajax({
                url: "<?php echo base_url('huawei/cwh/delete_document');?>",
                type : "POST",
                dataType: 'json',
                data: {'doc_id' : doc_id},
                success : function(data){
                    console.log(data);
                    if(data){
                        $('.tab-btn').children('button').eq(1).trigger('click');
                    }
                },
            });
        }
    });
}

function closeDoc(doc_id){
    $.ajax({
        url: "<?php echo base_url('huawei/fsl/close_doc_outbound');?>",
        type : "POST",
        dataType: 'json',
        data: {'doc_id' : doc_id},
        success : function(data){
            if(data){
                $('.tab-btn').children('button').eq(1).trigger('click');
            }
        },
    });
}

function printDOC(doc_id){
    window.open(
        '<?= base_url()?>/huawei/all_process/docPrint/'+doc_id,
        '_blank'
        );
}
</script>
