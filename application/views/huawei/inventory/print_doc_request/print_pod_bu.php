<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.a4 {
			width: 615px;
			height: 862px;
			margin: 40px 40px 40px 40px;
		}

		.header {
			width: 100%;
			border: 1px solid black !important;
			display: flex !important;
			margin-bottom: 0;
			height: 60px;
		}

		.head1 {
			width: 80px;
			height: 60px;
			/*padding: 5px;*/
			/*padding-top: 8px;*/
			padding-bottom: 0;
			/*margin-top: -5px;*/
			border-right: 1px solid black;
		}

		.head1 > img {
			margin-left: 2px;
			/*margin-top: 3px;*/
		}

		.head2 {
			height: 60px;
			/*border-left: 1px solid black;*/
			text-align: center;
			width: 100%;
			/*border: 1px solid black;*/
		}

		.head3 {
			border-left: 1px solid black;
			width: 80px;
		}

		.head3 > img {
			margin-top: 2px;
			margin-left: 5px;
		}

		.head2 > p {
			width: 100%;
		}

		.body1 {
			margin-top: 20px;
			width: 100% !important;
			justify-content: space-between;
		}

		.body2 {
			/*padding-left: 10px;*/
			margin-top: 40px;
			border: 1px solid black;
			width: 100%;
			margin-bottom: 30px;
		}

		.body3 {
			margin-top: 20px;
			display: flex;
			justify-content: space-between !important;
		}

		.body3-1, .body3-2 {
			width: 45%;
		}

		.body1-1, .body1-2{
			width: 45% !important;
		}

		.body1-1 {
			padding-left: 10px;
		}

		.body1-2 {
			padding-right: 10px;
		}

		.head2-p1 {
			font-size: 22px;
			font-weight: 600;
			margin: 0;
			/*margin-bottom: 5px;*/
			margin-top: 15px;
		}

		.head2-p2 {
			font-size: 12px;
			font-weight: 600;
			margin: 0;
		}

		.body1-p-ttl {
			font-size: 12px;
			font-weight: 600;
		}

		.body1-p-normal {
			font-size: 12px;
			font-weight: normal;
		}

		.body1-p-normal-40 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 40px;
		}

		.body2-p {
			font-size: 12px;
			margin-left: 10px;
		}

		.tab-head {
			background: black;
			color: white;
		}

		.body3-p {
			font-size: 12px;
			font-weight: normal;
		}

		.body3-p-30 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 30px;
		}

		table {
			font-size: 12px;
			border-spacing: 0;
			border-bottom: none !important;
			width: 100%;
			border: 1px solid black;
			border-right: none;
			margin-top: 20px;
		}

		th {
			text-align: center !important;
			font-weight: 600;
			color: white;
		}

		td {
			border-bottom: 1px solid black;
			text-align: center !important;
			border-right: 1px solid black;
		}
	</style>
</head>
<body>
	<div class="a4">
		<div class="header">
			<div class="head1">
				<img src="<?= base_url()?>/dist/img/huawei_logo.png" style="width: 60px;">
			</div>
			<div class="head2">
				<p class="head2-p1"><?= $title?></p>
			</div>
			<div class="head3">
				<img src="<?= base_url()?>/dist/img/HIL-logo.png" style="width: 55px;">
			</div>
		</div>

		<div class="body1">
			<p class="body1-p-normal">REFERENCE NUMBER DO : <b><?=$all[0]['pod_no']?></b></p>
			<p class="body1-p-normal">FROM POOL : <?=$all[0]['pool_origin']?></p>
		</div>

		<table>
			<tr class="tab-head">
				<th>NO</th>
				<th>RMR</th>
				<th>DESCRIPTION</th>
				<th>PN</th>
				<th>SR CODE</th>
				<th>RMA</th>
				<th>SITE</th>
			</tr>
			<?php  foreach($all as $k => $v) {?>
				<tr>
					<td><?= $k+1?></td>
					<td><?= $v['rmr']?></td>
					<td style="width: 200px;"><?= $v['des_req']?></td>
					<td><?= $v['pn_req']?></td>
					<td><?= $v['sr']?></td>
					<td><?= $v['rma']?></td>
					<td><?= $v['site']?></td>
				</tr>
			<?php  } ?>
		</table>

		<div class="body1">
			<p class="body1-p-normal"><b>DELIVERY ADDRESS</b> :</p>
			<p class="body1-p-normal">AREA : <?=$all[0]['name_dop']?></p>
		</div>

		<table>
			<tr class="tab-head">
				<th>REQUESTOR</th>
				<th>ADDRESS</th>
				<th>REGION</th>
				<th>EMAIL</th>
				<th>PHONE</th>
			</tr>
			<tr>
				<td><?= $all[0]['name_requestor']?></td>
				<td style="width: 200px;"><?= $all[0]['alamat_dop']?></td>
				<td><?= $all[0]['name_region']?></td>
				<td><?= $all[0]['email']?></td>
				<td><?= $all[0]['phone']?></td>
			</tr>
		</table>

		<div class="body1">
			<p class="body1-p-normal"><b>REQUEST DATE</b> :</p>
		</div>

		<div style="display: flex; justify-content: space-between;">
			<div style="width: 45%;">
				<p style="text-align: center;" class="body1-p-normal"><b>INITIAL REQUEST</b></p>
				<table>
					<tr class="tab-head">
						<th>DATE</th>
						<th>REQUEST BY</th>
					</tr>
					<tr>
						<td><?= $all[0]['time_request']?></td>
						<td><?= $all[0]['name_requestor']?></td>
					</tr>
				</table>
			</div>

			<div style="width: 45%;">
				<p style="text-align: center;" class="body1-p-normal"><b>REQUEST RECEIVED</b></p>
				<table>
					<tr class="tab-head">
						<th>DATE</th>
						<th>PROCESS BY</th>
					</tr>
					<tr>
						<td><?= $all[0]['create_time']?></td>
						<td><?= $all[0]['name_user']?></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="body1">
			<p class="body1-p-normal"><b>DELIVERY MODULE</b> :</p>
		</div>

		<table>
			<tr class="tab-head">
				<th>NO</th>
				<th>RMR</th>
				<th>DESCRIPTION</th>
				<th>PN</th>
				<th>SERIAL NUMBER</th>
			</tr>
			<?php  foreach($all as $k => $v) {?>
				<tr>
					<td><?= $k+1?></td>
					<td><?= $v['rmr']?></td>
					<td style="width: 200px;"><?= $v['des_del']?></td>
					<td><?= $v['pn_del']?></td>
					<td><?= $v['sn']?></td>
				</tr>
			<?php  } ?>
		</table>

		<div class="body1">
			<p class="body1-p-normal"><?= $all[0]['name_deliverytype']?></p>
			<p class="body1-p-normal">ETA AT DOP : <?= $all[0]['eta_received']?></p>
			<p class="body1-p-normal">SLA : <?= $all[0]['sla']?></p>
			<p class="body1-p-normal">PLEASE FILL</p>
			<p class="body1-p-normal">I CONFIRM THAT ALL MODULE HAS BEEN RECEIVED ON________________TO DOP</p>
		</div>

		<div style="display: flex; justify-content: space-between; margin-top: 60px;">
			<div style="width: 45%; text-align: center;">
				<p style="margin-bottom: 50px;" class="body1-p-normal">HARRASIMA</p>
				<p class="body1-p-normal">_____________</p>
			</div>
			<div style="width: 45%; text-align: center;">
				<p style="margin-bottom: 50px;" class="body1-p-normal"><?= $all[0]['name_customer']?></p>
				<p class="body1-p-normal">_____________</p>
			</div>
		</div>
	</div>
</body>
</html>