<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">CWH Inbound</p>
                </div>
                <div class="tab-btn">
					<div class="btn-item active">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/request.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Create Document</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/temporary.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Create Temp</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/list.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Document List</label>
						</div>	
					</div>
					<div class="btn-item">
						<div class="row">
							<img class="tab-btn-img m-auto" src="<?= base_url('') ?>/dist/img/btn-tab/save.svg">
						</div>	
						<div class="row mt-2">
							<label class="tab-btn-label m-auto">Put Away</label>
						</div>	
					</div>
				</div>
                <div class="create-doc">
                    <form id="this-form">
                        <div class="input-div">
                            <div class="my-form-group">
                                <p class="my-label-input">Project :</p>
                                <select name="id_project" class="form-control select2" style="width:50%;">
                                    <?php foreach($proj as $proj) {?>
                                        <option value="<?=$proj['id_project']?>"><?=$proj['name_project']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Inbound Type :</p>
                                <select name="id_boundtype" class="form-control select2" style="width:50%;">
                                    <?php foreach($ib_type as $ib_type) {?>
                                        <option value="<?=$ib_type['id_io_boundtype']?>"><?=$ib_type['name_io_boundtype']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">From :</p>
                                <select name="id_boundfrom" class="form-control select2" style="width:50%;">
                                    <?php foreach($from as $from) {?>
                                        <option value="<?=$from['id_warehouse']?>"><?=$from['name_warehouse']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Logistic :</p>
                                <select name="id_logistic" class="form-control select2" style="width:50%;">
                                    <?php foreach($logistic as $logistic) {?>
                                        <option value="<?=$logistic['id_logistic']?>"><?=$logistic['name_logistic']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Logistic Service :</p>
                                <select name="id_logistic_service" class="form-control select2" style="width:50%;">
                                    <?php foreach($logistic_service as $logistic_service) {?>
                                        <option value="<?=$logistic_service['id_logisticservice']?>"><?=$logistic_service['name_logisticservice']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">AWB Number :</p>
                                <input id="awb_no" name="awb_no" type="text" class="form-control select2" style="width:50%;">
                            </div>
                            <!-- <div class="my-form-group">
                                <p class="my-label-input">Date Received</p>
                                <input id="date_recv" name="date_received" type="text" class="form-control">
                            </div> -->
                            <!-- <div class="my-form-group">
                                <p class="my-label-input">Consignee</p>
                                <input name="consignee" type="text" class="form-control">
                            </div> -->
                            <!-- <div class="add-pn-div">
                                <button type="button" id="btn-add-pn" class="btn btn-primary btn-sm">Add PN</button>
                                <table id="pnTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Product Number</th>
                                            <th>Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div> -->
                        </div>
                    </form>
                    <div class="btn-process mt-4">
					    <button id="saveForm" type="button" class="btn btn-success mr-4">Proceed</button>
				    </div>
                </div>

                <div class="temp-div table-responsive">
                    <table id="tempList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Document Number</th>
                                <th>PN List / Qty</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="doc-list table-responsive">
                    <table id="docList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Document Number</th>
                                <th>Closed Date</th>
                                <th>Qty / SN Added</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div id="put-away-div" class="put-away">
                    <div style="width: 40%;" class="my-form-group">
                        <p class="my-label-input">Search Doc :</p>
                        <input id="pa_search_doc" name="pa_search_doc" type="text" class="form-control">
                    </div>

                    <div style="margin-top: 40px;" id="put-form-tab" class="table-responsive">
                        <form id="putawayForm">
                            <input type="hidden" id="putaway-sn-docid" name="putaway-sn-docid">
                            <div style="width: 40%;" class="my-form-group">
                                <p class="my-label-input">Loc Bin :</p>
                                <input id="putaway-sn-bin" name="putaway-sn-bin" type="text" class="form-control">
                            </div>
                            <div style="width: 40%;" class="my-form-group">
                                <p class="my-label-input">Part Number :</p>
                                <input id="putaway-sn-pn" name="putaway-sn-pn" type="text" class="form-control">
                            </div>
                            <div style="width: 40%;" class="my-form-group">
                                <p class="my-label-input">Serial Number :</p>
                                <input id="putaway-sn-sn" name="putaway-sn-sn" type="text" class="form-control">
                            </div>
                        </form>

                        <table style="margin-top: 30px;" id="sn-list-check" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Part Number</th>
                                    <th>Serial Number</th>
                                    <th>Loc Bin</th>
                                    <th>Checked</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div id="modal-add-pn-temp" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add temp PN & Quantity</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="this-form-temp">
                            <input id="doc_id_temp" name="doc_id_temp" type="hidden" class="form-control">
                            <div class="my-form-group">
                                <p class="my-label-input">PN :</p>
                                <input id="product_number_temp" name="product_number" type="text" class="form-control">
                            </div>
                            <div class="my-form-group">
                                <p class="my-label-input">Quantity :</p>
                                <input id="qty_temp" name="qty" type="text" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="saveFormTemp" type="button" class="btn btn-success">Proceed</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        let user_level = '<?= $this->session->userdata('level')?>';
        if(user_level != 4){
            $('#put-away-div').addClass('unselectable');
        }
        
        addPnTable = $('#pnTable').DataTable({
            paging      : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "",
                zeroRecords: ""
            }
        });

        docList = $('#docList').DataTable({
            paging      : true,
            lengthChange: false,
            searching   : true,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        tempList = $('#tempList').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        sngListCheck = $('#sn-list-check').DataTable({
            paging      : true,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "",
                zeroRecords: ""
            }
        });

        $('#pa_search_doc').on('keyup', function(e){
            if(e.keyCode === 13){
                $.ajax({
                    url: "<?php echo base_url('huawei/cwh/check_doc_exist_putaway_inbound');?>",
                    type : "POST",
                    dataType: 'json',
                    data: {'doc_no' : $('#pa_search_doc').val()},
                    success : function(data){
                        if(data){
                            $('#put-form-tab').show();
                            $('#putaway-sn-docid').val(data);

                            $('#putaway-sn-bin').trigger('focus');

                            $.ajax({
                                url: "<?php echo base_url('huawei/all_process/get_sn_by_doc_id_inbound');?>",
                                type : "POST",
                                dataType: 'json',
                                data: {'doc_id' : data},
                                success : function(data){
                                    sngListCheck.clear().draw();
                                    $(data).each(function(k,v) {
                                        var check;
                                        if(v.access == '6'){
                                            check = '';
                                        } else if ((v.access == '7' && v.date_send == null) || (v.access == '1' && v.date_send != null)){
                                            check = '<i style="font-size: 16px; color: green;" class="fa fa-check"></i>';
                                        }
                                        sngListCheck.row.add([
                                            k+1,
                                            v.pn,
                                            v.sn,
                                            v.loc_bin,
                                            check
                                            ]).draw( false );
                                    });
                                },
                            });
                        }else{
                            var swal_data = { title: 'Failed', icon: 'error', text: 'Document does not exist', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                        }
                    },
                });
            }
        });

        $('#putaway-sn-bin').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#putaway-sn-pn').trigger('focus');
            }
        });        

        $('#putaway-sn-pn').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#putaway-sn-sn').trigger('focus');
            }
        });

        $('#putaway-sn-sn').on('keyup', function(e) {
            if(e.keyCode === 13){
                if($('#putaway-sn-bin').val() == '' || $('#putaway-sn-pn').val() == '' || $('#putaway-sn-sn').val() == ''){
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Please fill out the input form', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                } else {
                    $.ajax({
                        url: "<?php echo base_url('huawei/cwh/putaway_process_inbound');?>",
                        type : "POST",
                        dataType: 'json',
                        data: $('#putawayForm').serialize(),
                        success : function(data){
                            if(!data.stat) {
                                var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
                                swal(swal_data).then(function() {
                                });
                            } else {
                                sngListCheck.clear().draw();
                                var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
                                swal(swal_data).then(function() {
                                    $.ajax({
                                        url: "<?php echo base_url('huawei/all_process/get_sn_by_doc_id_inbound');?>",
                                        type : "POST",
                                        dataType: 'json',
                                        data: {'doc_id' : $('#putaway-sn-docid').val()},
                                        success : function(data){
                                            $(data).each(function(k,v) {
                                                var check;
                                                if(v.access == '6'){
                                                    check = '';
                                                } else if ((v.access == '7' && v.date_send == null) || (v.access == '1' && v.date_send != null)){
                                                    check = '<i style="font-size: 16px; color: green;" class="fa fa-check"></i>';
                                                }
                                                sngListCheck.row.add([
                                                    k+1,
                                                    v.pn,
                                                    v.sn,
                                                    v.loc_bin,
                                                    check
                                                    ]).draw( false );
                                            });
                                        },
                                    });

                                    $('#putaway-sn-pn').trigger('focus');
                                    $('#putaway-sn-pn').val('');
                                    $('#putaway-sn-sn').val('');
                                });
                            }
                        },
                    });
                }
            }
        });

        $('.tab-btn > .btn-item').click(function(){
            $('.tab-btn > .btn-item').removeClass('active');
            $(this).addClass('active');
        });

        $('.tab-btn').children('.btn-item').eq(3).click(function(){
            $('.create-doc').hide();
            $('.temp-div').hide();
            $('.doc-list').hide();
            $('.put-away').show();
        });

        $('.tab-btn').children('.btn-item').eq(2).click(function(){
            $('#put-form-tab').hide();
            $('.create-doc').hide();
            $('.temp-div').hide();
            $('.put-away').hide();
            $('#pa_search_doc').val('');
            docList.clear().draw();

            $.ajax({
                url: "<?php echo base_url('huawei/cwh/get_inbound_cwh_doc_list');?>",
                type : "GET",
                dataType: 'json',
                success : function(data){
                    $(data).each(function(k,v) {
                        var btn_close, btn_add, btn_recv, date_close, btn_print, btn_print_doc;
                        var btn_delete = '';
                        if(v.sn_added < 1 && v.date_send == null){
                            btn_delete = '<button onclick="deleteDoc('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; width: 100%; margin-bottom: 10px !important;" class="btn btn-xs btn-danger" >Delete</button>';
                        }

                        if(v.date_received == null){
                            if(v.close_date == null){
                                date_close = '-';
                            } else {
                                date_close = v.close_date;
                            }

                            if(v.date_send != null && v.close_date != null){

                                btn_add = '';
                                btn_recv = '<button id="btn-recv-doc-'+v.id+'" onclick="receive('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; width: 100%; margin-bottom: 10px !important;" class="btn btn-xs btn-success" >Receive</button>';
                            } else if(v.date_send == null && v.close_date == null) {
                                if(v.id_boundtype == '1' || v.id_boundtype == '2' || v.id_boundtype == '11'){
                                    btn_add = '<a id="btn-add-sn-'+v.id+'" href="<?= base_url().'huawei/all_process/add_sn_good_faulty/'?>'+v.id+'" style="display:block; margin: 0 auto; margin-top: 10px; pointer: cursor; margin-bottom: 10px !important;" class="btn btn-xs btn-primary" >Module Received</a>';
                                    btn_recv = '<button id="btn-recv-doc-'+v.id+'" onclick="receive('+v.id+')" style="display:block; margin: 0 auto; margin-top: 10px; width: 100%; margin-bottom: 10px !important;" class="btn btn-xs btn-success" >Receive</button>';
                                }else{
                                    btn_add = '';
                                    btn_recv = '';
                                }
                            }

                            docList.row.add([
                                k+1,
                                v.doc_no,
                                date_close,
                                v.qty +' / '+v.sn_added,
                                btn_recv+
                                btn_add+
                                btn_delete
                                ]).draw( false );


                            $("#btn-close-doc-"+v.id+"").hide();
                            $("#btn-add-sn-"+v.id+"").hide();
                        } else {
                            if(v.for_close == v.sn_added && v.sn_added > 0 && v.date_send == null){
                                btn_close = '<button id="btn-close-doc-'+v.id+'" onclick="closeDoc('+v.id+')" style="display:block; margin: 0 auto; margin-top: 2px; width: 100%; margin-bottom: 10px;" class="btn btn-xs btn-warning" >Close Doc</button>';
                                btn_print = '<button onclick="printSN('+v.id+')" style="display:block; margin: 0 auto; margin-top: 2px; width: 100%; margin-bottom: 10px;" class="btn btn-xs btn-default" >Print SN</button>';
                            } else {
                                btn_close = '';
                                btn_print = '';
                            }

                            if(v.date_send != null){
                                btn_print = '<button onclick="printSN('+v.id+')" style="display:block; margin: 0 auto; margin-top: 2px; width: 100%; margin-bottom: 10px;" class="btn btn-xs btn-default" >Print SN</button>';
                            }

                            if(v.close_date == null){
                                date_close = '-';
                            } else {
                                date_close = v.close_date;
                            }
                            if(v.status > 0){
                                btn_add = '<a id="btn-add-sn-'+v.id+'" href="<?= base_url().'huawei/all_process/add_sn_good_faulty/'?>'+v.id+'" style="display:block; margin: 0 auto; margin-top: 2px; pointer: cursor; margin-bottom: 10px;" class="btn btn-xs btn-primary" > Module Received</a>';
                                btn_print = '<button onclick="printSN('+v.id+')" style="display:block; margin: 0 auto; margin-top: 2px; width: 100%; margin-bottom: 10px;" class="btn btn-xs btn-default" >Print SN</button>';
                                btn_print_doc = '<a id="btn-print-doc-'+v.id+'" href="<?= base_url()?>huawei/all_process/docPrint/'+v.id+'" target="_blank" style="display:block; margin: 0 auto; margin-top: 2px; width: 100%; margin-bottom: 10px;" class="btn btn-xs btn-primary" >Print Doc</a>';
                            } else {
                                btn_add = '';
                                btn_print = '<button onclick="printSN('+v.id+')" style="display:block; margin: 0 auto; margin-top: 2px; width: 100%; margin-bottom: 10px;" class="btn btn-xs btn-default" >Print SN</button>';
                                btn_print_doc = '<a id="btn-print-doc-'+v.id+'" href="<?= base_url()?>huawei/all_process/docPrint/'+v.id+'" target="_blank" style="display:block; margin: 0 auto; margin-top: 2px; width: 100%; margin-bottom: 10px;" class="btn btn-xs btn-primary" >Print Doc</a>';
                                btn_close = '';
                            }

                            docList.row.add([
                                k+1,
                                v.doc_no,
                                date_close,
                                v.qty +' / '+v.sn_added,
                                btn_add+
                                btn_close+
                                btn_print+
                                btn_print_doc+
                                btn_delete
                                ]).draw( false );

                            if(v.status > 0) {
                                $('#btn-print-doc-'+v.id+'').hide();
                            }
                        }
                    });
},
});

$('.doc-list').show();
});

$('.tab-btn').children('.btn-item').eq(1).click(function(){
    $('#put-form-tab').hide();
    $('.doc-list').hide();
    $('.create-doc').hide();
    $('.put-away').hide();
    $('#pa_search_doc').val('');
    tempList.clear().draw();

    $.ajax({
        url: "<?php echo base_url('huawei/cwh/get_temp_inbound_cwh_list');?>",
        type : "GET",
        dataType: 'json',
        success : function(data){
            $(data).each(function(k,v) {
                var btn_add;
                var pn_list = '';
                $(v.pn_list).each(function(a,i) {
                    pn_list = pn_list + '<p>PN: <strong>'+i.product_number+'</strong> / Qty: '+i.qty+'</p>';
                });

                if(v.id_boundtype == '1' || v.id_boundtype == '2' || v.id_boundtype == '11'){
                    if(v.status == '0'){
                        btn_add = '';
                    } else {
                        btn_add = '<a onclick="show_modal_add_pn('+v.id+')" style="display:block; margin: 0 auto; margin-top: 2px; color: white; cursor: pointer;" class="btn btn-xs btn-primary" >add PN</a>';
                    }
                } else {
                    btn_add = '';
                }

                tempList.row.add([
                    k+1,
                    v.doc_no,
                    pn_list,
                    btn_add
                    ]).draw( false );
            });
        },
    });

    $('.temp-div').show();
});

$('.tab-btn').children('.btn-item').eq(0).click(function(){
    $('.create-doc').show();
    $('.doc-list').hide();
    $('.temp-div').hide();
    $('.put-away').hide();
    $('#put-form-tab').hide();
    $('#pa_search_doc').val('');
});

if(document.referrer.indexOf('add_sn_inbound_good') > -1 && performance.navigation.type != 1){
    $('.tab-btn').children('.btn-item').eq(2).trigger('click');
}else{
    $('.tab-btn').children('.btn-item').eq(0).trigger('click');
}

$('#pnTable tbody').on( 'click', '#del-pn', function () {
    var tr = $(this).closest('tr');
    addPnTable.row(tr).remove().draw();
});

$('#saveForm').click(function(){
    if($('#awb_no').val() == ''){
        var swal_data = { title: 'Failed', icon: 'error', text: 'Please fill out the input form', button:false, timer: 1000 };
        swal(swal_data).then(function() {
        });
    } else {
        $.ajax({
            url: "<?php echo base_url('huawei/cwh/inbound_good_doc_submit');?>",
            type : "POST",
            dataType: 'json',
            data: $('#this-form').serialize(),
            success : function(data){
                if(data){
                    var swal_data = {
                        title: data.doc_no,
                        text: data.doc_date,
                        icon: 'success',
                        buttons: {
                            confirm: {
                                text: "Ok",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        }
                    };
                    swal(swal_data).then((value) => {
                        if(value){
                            $('.tab-btn').children('button').eq(1).trigger('click');
                        }
                    });
                }else{
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be saved, please contact Administrator', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                }
            },
        });
    }
});

$('#saveFormTemp').click(function(){
    if($('#qty_temp').val() == ''){
        var swal_data = { title: 'Failed', icon: 'error', text: 'Please fill out the quantity form', button:false, timer: 1000 };
        swal(swal_data).then(function() {
        });
    } else {
        $.ajax({
            url: "<?php echo base_url('huawei/cwh/add_pn_temp_inbound');?>",
            type : "POST",
            dataType: 'json',
            data: $('#this-form-temp').serialize(),
            success : function(data){
                    // console.log(data);
                    if(data){
                        var swal_data = { title: 'Success', icon: 'success', text: 'PN & Quantity successfully saved', button: false, timer: 1000 };
                        swal(swal_data).then(function() {
                            $('#product_number_temp').val('');
                            $('#qty_temp').val('');
                            $('#product_number_temp').trigger('focus');
                        });
                    }else{
                        var swal_data = { title: 'Failed', icon: 'error', text: 'This PN is not registered in database', button:false, timer: 1000 };
                        swal(swal_data).then(function() {
                        });
                    }
                },
            });
    }
});
$('#product_number_temp').on('keyup', function(e){
    if(e.keyCode === 13){
        $('#qty_temp').trigger('focus');
    }
});

$('#modal-add-pn-temp').on('hidden.bs.modal', function () {
    $('.tab-btn').children('.btn-item').eq(1).trigger('click');
})

});
function show_modal_add_pn(doc_id){
    $("#modal-add-pn-temp").modal('show');
    $("#doc_id_temp").val(doc_id);

    $('#product_number_temp').trigger('focus');
}

function closeDoc(doc_id){
    $.ajax({
        url: "<?php echo base_url('huawei/cwh/close_doc_inbound');?>",
        type : "POST",
        dataType: 'json',
        data: {'doc_id' : doc_id},
        success : function(data){
            if(data){
                $('.tab-btn').children('.btn-item').eq(2).trigger('click');
            }
            // console.log(data);
        },
    });
}

function receive(doc_id){
    $.ajax({
        url: "<?php echo base_url('huawei/cwh/receive_doc_inbound');?>",
        type : "POST",
        dataType: 'json',
        data: {'doc_id' : doc_id},
        success : function(data){
            if(data){
                $('.tab-btn').children('.btn-item').eq(2).trigger('click');
            }
            // console.log(data);
        },
    });
}

function printSN(doc_id){
    window.open(
        '<?= base_url()?>/huawei/all_process/makePdfSN/'+doc_id,
        '_blank'
        );
}

function deleteDoc(doc_id){
    var swal_data = {
        title: 'Are you sure?',
        text: 'To delete this document?',
        icon: 'warning',
        buttons: {
            cancel: {
                text: "No",
                value: false,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    };
    swal(swal_data).then((value) => {
        if(value){
            $.ajax({
                url: "<?php echo base_url('huawei/cwh/delete_document');?>",
                type : "POST",
                dataType: 'json',
                data: {'doc_id' : doc_id},
                success : function(data){
                    console.log(data);
                    if(data){
                        $('.tab-btn').children('.btn-item').eq(2).trigger('click');
                    }
                },
            });
        }
    });
}
</script>
