<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <a href="javascript:history.go(-1)" class="btn btn-primary btn-sm">Back</a>
                <div class="header-jdl">
                    <p class="jdl-big">Add SN</p>
                </div>
                <div class="input-div">
                    <form id="this-form">
                        <input type="hidden" id="add-sn-docid" name="add-sn-docid">
                        <div class="my-form-group">
                            <p class="my-label-input">Locator :</p>
                            <input id="add-sn-bin" name="add-sn-bin" type="text" class="form-control">
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">Part Number :</p>
                            <input id="add-sn-pn" name="add-sn-pn" type="text" class="form-control">
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">Serial Number :</p>
                            <input id="add-sn-sn" name="add-sn-sn" type="text" class="form-control">
                        </div>
                    </form>
                </div>
                <div class="add-pn-div unselectable">
                    <table id="snList" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Part Number</th>
                                <th>Serial Number</th>
                                <th>Locator</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="modal-add-remark" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Remarks</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="this-form-remark">
                            <input id="sn_id" name="sn_id" type="hidden" class="form-control">
                            <input id="remark_doc_id" name="remark_doc_id" type="hidden" class="form-control">
                            <div class="my-form-group">
                                <p class="my-label-input">Remarks :</p>
                                <textarea rows="8" id="sn_remark" name="sn_remark" type="text" class="form-control"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="saveRemark" type="button" class="btn btn-success">Proceed</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#add-sn-docid').val('<?= $doc_id?>');

        let user_level = '<?= $this->session->userdata('level')?>';

       // if(user_level != 4){
       //     $('#this-form > * input').on("cut copy paste",function(e) {
       //         e.preventDefault();
       //     });
       // }

      //  $('#this-form > * input').on({
        //    keypress: function() { typed_into = true; },
          //  change: function() {
            //    if (typed_into) {
              //      alert('type');
                //    typed_into = false;
        //        } else {
        //            alert('not type');
        //        }
        //    }
        //});

        snList = $('#snList').DataTable({
            paging      : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "",
                zeroRecords: ""
            },
            "scrollX": true
        });

        $('#add-sn-bin').trigger('focus');

        $('#add-sn-bin').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#add-sn-pn').trigger('focus');
            }
        });        

        $('#add-sn-pn').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#add-sn-sn').trigger('focus');
            }
        });

        // onselectstart="return false" onpaste="return false;" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off

        $.ajax({
            url: "<?php echo base_url('huawei/all_process/get_sn_by_doc_id_inbound');?>",
            type : "POST",
            dataType: 'json',
            data: {'doc_id' : $('#add-sn-docid').val()},
            success : function(data){
                snList.clear();
                if(data.length > 0){
                    $(data).each(function(k,v) {
                        snList.row.add([
                            k+1,
                            v.pn,
                            v.sn,
                            v.loc_bin,
                            '<button onclick="deleteSN('+v.id_sn+')" style="margin-right: 10px;" class="btn btn-danger btn-sm">Delete</button>'+
                            '<button onclick="openRemarkModal('+v.id_sn+')" class="btn btn-primary btn-sm">Remarks</button'
                            ]).draw( false );
                    });
                }
            },
        });

        $('#saveRemark').click(function(){
            if($('#sn_remark').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please fill the remarks', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                $.ajax({
                    url: "<?php echo base_url('huawei/cwh/add_remark_sn');?>",
                    type : "POST",
                    dataType: 'json',
                    data: $('#this-form-remark').serialize(),
                    success : function(data){
                        if(data){
                            var swal_data = { title: 'Success', icon: 'success', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                            // $("#modal-add-remark").modal('toggle');
                        }else{
                            var swal_data = { title: 'Failed', icon: 'error', button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                        }
                    },
                });
            }
        });

        $('#add-sn-sn').on('keyup', function(e) {
            if(e.keyCode === 13){
                if($('#add-sn-bin').val() == '' || $('#add-sn-pn').val() == '' || $('#add-sn-sn').val() == ''){
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Please fill out the input form', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                } else {
                    if($('#add-sn-pn').val() == $('#add-sn-sn').val()){
                        var swal_data = {
                            title: 'Are you sure?',
                            text: 'That there is no Serial Number in this product?',
                            icon: 'warning',
                            buttons: {
                                cancel: {
                                    text: "No",
                                    value: false,
                                    visible: true,
                                    className: "",
                                    closeModal: true,
                                },
                                confirm: {
                                    text: "Yes",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: true
                                }
                            }
                        };
                        swal(swal_data).then((value) => {
                            if(value){
                                $.ajax({
                                    url: "<?php echo base_url('huawei/all_process/add_sn_io');?>",
                                    type : "POST",
                                    dataType: 'json',
                                    data: $('#this-form').serialize(),
                                    success : function(data){
                                        if(!data.stat){
                                            var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
                                            swal(swal_data).then(function() {
                                            });
                                        }else{
                                            var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
                                            swal(swal_data).then(function() {
                                                $('#product_number_temp').val('');
                                                $('#qty_temp').val('');
                                                snList.clear();
                                                $.ajax({
                                                    url: "<?php echo base_url('huawei/all_process/get_sn_by_doc_id_inbound');?>",
                                                    type : "POST",
                                                    dataType: 'json',
                                                    data: {'doc_id' : $('#add-sn-docid').val()},
                                                    success : function(data){
                                                        if(data.length > 0){
                                                            $(data).each(function(k,v) {
                                                                snList.row.add([
                                                                    k+1,
                                                                    v.pn,
                                                                    v.sn,
                                                                    v.loc_bin,
                                                                    '<button onclick="deleteSN('+v.id_sn+')" style="margin-right: 10px;" class="btn btn-danger btn-sm">Delete</button>'+
                                                                    '<button onclick="openRemarkModal('+v.id_sn+')" class="btn btn-primary btn-sm">Remarks</button'
                                                                    ]).draw( false );
                                                            });
                                                        }
                                                        $('#add-sn-pn').val('');
                                                        $('#add-sn-sn').val('');
                                                        $('#add-sn-pn').trigger('focus');
                                                    },
                                                });
                                            });
                                        }
                                    },
                                });
                            }
                        });
                    } else {
                        $.ajax({
                            url: "<?php echo base_url('huawei/all_process/add_sn_io');?>",
                            type : "POST",
                            dataType: 'json',
                            data: $('#this-form').serialize(),
                            success : function(data){
                                if(!data.stat){
                                    var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
                                    swal(swal_data).then(function() {
                                    });
                                }else{
                                    var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
                                    swal(swal_data).then(function() {
                                        $('#product_number_temp').val('');
                                        $('#qty_temp').val('');
                                        snList.clear();
                                        $.ajax({
                                            url: "<?php echo base_url('huawei/all_process/get_sn_by_doc_id_inbound');?>",
                                            type : "POST",
                                            dataType: 'json',
                                            data: {'doc_id' : $('#add-sn-docid').val()},
                                            success : function(data){
                                                if(data.length > 0){
                                                    $(data).each(function(k,v) {
                                                        snList.row.add([
                                                            k+1,
                                                            v.pn,
                                                            v.sn,
                                                            v.loc_bin,
                                                            '<button onclick="deleteSN('+v.id_sn+')" style="margin-right: 10px;" class="btn btn-danger btn-sm">Delete</button>'+
                                                            '<button onclick="openRemarkModal('+v.id_sn+')" class="btn btn-primary btn-sm">Remarks</button'
                                                            ]).draw( false );
                                                    });
                                                }
                                                $('#add-sn-pn').val('');
                                                $('#add-sn-sn').val('');
                                                $('#add-sn-pn').trigger('focus');
                                            },
                                        });
                                    });
                                }
                            },
                        });
                    }
                }    
            }
        });
});

function openRemarkModal(id_sn){
    $('#sn_remark').val('');
    $.ajax({
        url: "<?php echo base_url('huawei/cwh/get_remark_sn_by_id');?>",
        type : "POST",
        dataType: 'json',
        data: {'id_sn' : id_sn},
        success : function(data){
            $('#sn_remark').val(data[0]['remark']);
        },
    });
    $("#modal-add-remark").modal('show');
    $("#sn_id").val(id_sn);
    $('#remark_doc_id').val('<?= $doc_id?>');
}

function deleteSN(id_sn){
    $.ajax({
        url: "<?php echo base_url('huawei/all_process/delete_sn_add_sn');?>",
        type : "POST",
        dataType: 'json',
        data: {'id_sn' : id_sn, 'id_doc' : '<?= $doc_id?>'},
        success : function(data){
            if(data){
                snList.rows().remove().draw();
                $.ajax({
                    url: "<?php echo base_url('huawei/all_process/get_sn_by_doc_id_inbound');?>",
                    type : "POST",
                    dataType: 'json',
                    data: {'doc_id' : $('#add-sn-docid').val()},
                    success : function(data){
                        if(data.length > 0){
                            $(data).each(function(k,v) {
                                snList.row.add([
                                    k+1,
                                    v.pn,
                                    v.sn,
                                    v.loc_bin,
                                    '<button onclick="deleteSN('+v.id_sn+')" style="margin-right: 10px;" class="btn btn-danger btn-sm">Delete</button>'+
                                    '<button onclick="openRemarkModal('+v.id_sn+')" class="btn btn-primary btn-sm">Remarks</button'
                                    ]).draw( false );
                            });
                        }
                    },
                });
            }
        },
    });
}

</script>
