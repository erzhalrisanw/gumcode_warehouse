<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?=$_SERVER['DOCUMENT_ROOT']?>/application/views/huawei/inventory/print_doc.css">
</head>
<body>
	<div class="a4">
		<div class="header">
			<div class="head1">
				<img src="<?=$_SERVER['DOCUMENT_ROOT']?>/dist/img/HIL-logo.png" style="width: 55px;">
			</div>
			<div class="head2">
				<p class="head2-p1">Document <?= $doc[0]['name_io_boundtype']?></p>
				<p class="head2-p2">PT. Harrasima Inventory Logistic</p>
			</div>
		</div>

		<div class="body1">
			<div class="body1-1">
				<p class="body1-p-ttl">Ship From</p>
				<p class="body1-p-normal">Warehouse: <?=$from_det[0]['name_warehouse']?></p>
				<p class="body1-p-normal">Address: <?=$from_det[0]['address']?>, <?=$from_det[0]['name_city']?>, <?=$from_det[0]['name_country']?></p>
				<p class="body1-p-normal-40">Logistic: <?=$doc[0]['name_logistic']?></p>
				<p class="body1-p-normal">AWB: <?=$doc[0]['awb_no']?></p>
			</div>
			<div class="body1-2">
				<p class="body1-p-ttl">Ship To</p>
				<p class="body1-p-normal">Warehouse: <?=$to_det[0]['name_warehouse']?></p>
				<p class="body1-p-normal">Address: <?=$to_det[0]['address']?>, <?=$to_det[0]['name_city']?>, <?=$to_det[0]['name_country']?></p>
			</div>
		</div>
		<div class="body2">
			<p class="body2-p">No. Doc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?=$doc[0]['doc_no']?></p>
			<p class="body2-p">Doc Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?=$doc[0]['doc_date']?></p>
		</div>

		<table>
			<tr class="tab-head">
				<th>No</th>
				<th>Project</th>
				<th>Description</th>
				<th>Part Number</th>
				<th>Serial Number</th>
				<th>Status</th>
				<th>Remark</th>
			</tr>
			<?php  foreach($sn_det as $k => $v) {?>
				<tr>
					<td><?= $k+1?></td>
					<td><?= $v['name_project']?></td>
					<td><?= $v['description']?></td>
					<td><?= $v['name_pn']?></td>
					<td><?= $v['sn']?></td>
					<td><?= $v['name_stockstatus']?></td>
					<td><?= $v['remark']?></td>
				</tr>
			<?php  } ?>
		</table>

		<div class="body3">
			<div class="body3-1">
				<p class="body3-p">Shipper&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</p>
				<p class="body3-p">Signature&nbsp;&nbsp;:</p>
				<p class="body3-p-30">Date Time&nbsp;:</p>
			</div>
			<div class="body3-2">
				<p class="body3-p">Consignee&nbsp;:</p>
				<p class="body3-p">Signature&nbsp;&nbsp;&nbsp;:</p>
				<p class="body3-p-30">Date Time&nbsp;:</p>
			</div>
		</div>
	</div>
</body>
</html>