<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.a4 {
			width: 615px;
			/*height: 862px;*/
			margin: 40px 40px 40px 40px;
		}

		.header {
			width: 100%;
			border: 1px solid black !important;
			display: flex !important;
			margin-bottom: 0;
			height: 60px;
		}

		.head1 {
			width: 80px;
			height: 60px;
			/*padding: 5px;*/
			/*padding-top: 8px;*/
			padding-bottom: 0;
			/*margin-top: -5px;*/
			border-right: 1px solid black;
		}

		.head1 > img {
			margin-left: 7px;
			margin-top: 3px;
		}

		.head2 {
			height: 60px;
			/*border-left: 1px solid black;*/
			text-align: center;
			width: 100%;
			/*border: 1px solid black;*/
		}

		.head2 > p {
			width: 100%;
		}

		.body1 {
			margin-top: 20px;
			width: 100% !important;
			display: flex;
			border: 1px solid black;
			justify-content: space-between;
		}

		.body2 {
			/*padding-left: 10px;*/
			margin-top: 40px;
			border: 1px solid black;
			width: 100%;
			margin-bottom: 30px;
		}

		.body3 {
			margin-top: 20px;
			display: flex;
			justify-content: space-between !important;
		}

		.body3-1, .body3-2 {
			width: 45%;
		}

		.body1-1, .body1-2{
			width: 45% !important;
		}

		.body1-1 {
			padding-left: 10px;
		}

		.body1-2 {
			padding-right: 10px;
		}

		.head2-p1 {
			font-size: 22px;
			font-weight: 600;
			margin: 0;
			margin-bottom: 5px;
			margin-top: 5px;
		}

		.head2-p2 {
			font-size: 12px;
			font-weight: 600;
			margin: 0;
		}

		.body1-p-ttl {
			font-size: 12px;
			font-weight: 600;
		}

		.body1-p-normal {
			font-size: 12px;
			font-weight: normal;
		}

		.body1-p-normal-40 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 40px;
		}

		.body2-p {
			font-size: 12px;
			margin-left: 10px;
		}

		.tab-head {
			background: black;
			color: white;
		}

		.body3-p {
			font-size: 12px;
			font-weight: normal;
		}

		.body3-p-30 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 30px;
		}

		table {
			font-size: 12px;
			border-spacing: 0;
			border-bottom: none !important;
			border: 1px solid black;
			margin-top: 20px;
			border-right: none;
			table-layout: fixed;
			width: 100%;
		}

		th {
			text-align: center !important;
			font-weight: 600;
			color: white;
		}

		td {
			border-bottom: 1px solid black;
			text-align: center !important;
			border-right: 1px solid black;
			word-break: break-all;
		}
	</style>
</head>
<body>
	<div class="a4">
		<div class="header">
			<div class="head1">
				<img src="<?= base_url()?>/dist/img/HIL-logo.png" style="width: 55px;">
			</div>
			<div class="head2">
				<p class="head2-p1">Document <?= $doc[0]['name_io_boundtype']?></p>
				<p class="head2-p2">PT. Harrasima Inventory Logistic</p>
			</div>
		</div>

		<div class="body1">
			<div class="body1-1">
				<p class="body1-p-ttl">Ship From</p>
				<p class="body1-p-normal">Warehouse: <?=$from_det[0]['name_warehouse']?></p>
				<p class="body1-p-normal">Address: <?=$from_det[0]['address']?>, <?=$from_det[0]['name_city']?>, <?=$from_det[0]['name_country']?></p>
				<p class="body1-p-normal-40">Logistic: <?=$doc[0]['name_logistic']?></p>
				<p class="body1-p-normal">AWB: <?=$doc[0]['awb_no']?></p>
			</div>
			<div class="body1-2">
				<p class="body1-p-ttl">Ship To</p>
				<p class="body1-p-normal">Warehouse: <?=$to_det[0]['name_warehouse']?></p>
				<p class="body1-p-normal">Address: <?=$to_det[0]['address']?>, <?=$to_det[0]['name_city']?>, <?=$to_det[0]['name_country']?></p>
			</div>
		</div>
		<div class="body2">
			<p class="body2-p">No. Doc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?=$doc[0]['doc_no']?></p>
			<p class="body2-p">Doc Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?=$doc[0]['doc_date']?></p>
		</div>

		<div>
			<table>
				<col width="25px"/>
				<col width="55px"/>
				<col width="210px"/>
				<col width="65px"/>
				<col width="150px"/>
				<col width="50px"/>
				<col width="60px"/>
				<tr class="tab-head">
					<th>No</th>
					<th>Project</th>
					<th>Description</th>
					<th>Part Number</th>
					<th>Serial Number</th>
					<th>Status</th>
					<th>Remark</th>
				</tr>
				<?php  foreach($sn_det as $k => $v) {?>
					<tr>
						<td><?= $k+1?></td>
						<td><?= $v['name_project']?></td>
						<td style="width: 200px;"><?= $v['description']?></td>
						<td><?= $v['name_pn']?></td>
						<td><?= $v['sn']?></td>
						<td><?= $v['name_stockstatus']?></td>
						<td><?= $v['remark']?></td>
					</tr>
				<?php  } ?>
			</table>
		</div>

		<div class="body3">
			<div class="body1-1">
				<p class="body3-p">Shipper&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</p>
				<p class="body3-p">Signature&nbsp;&nbsp;:</p>
				<p class="body3-p-30">Date Time&nbsp;:</p>
			</div>
			<div class="body1-2">
				<p class="body3-p">Consignee&nbsp;:</p>
				<p class="body3-p">Signature&nbsp;&nbsp;&nbsp;:</p>
				<p class="body3-p-30">Date Time&nbsp;:</p>
			</div>
		</div>
	</div>
</body>
</html>