<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Progress Stocktake</p>
                </div>

                <div class="inbound-div table-responsive">
                    <table id="inbound-list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Warehouse</th>
                                <th>Good Available Checked</th>
                                <th>Faulty Available Checked</th>
                                <th>Good Available Unchecked</th>
                                <th>Faulty Available Unchecked</th>
                                <th>Total Module </th>
                                <th>Progress Stoketake</th>
                                <th>Indelivery</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div id="modal-insert-inbound" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Cek SN Unchecked</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body table-responsive">
                                <table id="check-list" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>SN </th>
                                            <th>Module Status</th>
                                            <th>Locator</th>
                                            <th>Access Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-success">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="modal-list-ho" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">List SN</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body table-responsive">
                                <table id="list_HO" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>PN </th>
                                            <th>SN </th>
                                            <th>Module Status</th>
                                            <th>Locator</th>
                                            <th>Access Status</th>
                                            <th>Last Stocktake</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-success">Close</button>
                            </div>
                        </div>
                    </div>
                </div>                

            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        inboundList = $('#inbound-list').DataTable({
            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        checkList = $('#check-list').DataTable({
            paging      : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            }
        });

        list_HO = $('#list_HO').DataTable({

            paging      : true,
            lengthChange: true,
            searching   : true,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "No Data to Show",
                zeroRecords: "No Data to Show"
            },
            dom: 'Bfrtip',
            'buttons': [
            'excel'
            ]
        });


        $.ajax({
            url: "<?php echo base_url('huawei/counting_stock/get_progress_stocktake');?>",
            type : "GET",
            dataType: 'json',
            success : function(data){
                $(data).each(function(k,v) {
                    var bg_progress, val_progress;
                    if(isNaN(((v.no_cek/v.total)*100).toFixed(2))){
                        bg_progress = 'pg-danger';
                        val_progress = 0;
                    }else if(((v.no_cek/v.total)*100).toFixed(2) == 100){
                        bg_progress = 'pg-success';
                        val_progress = 100;
                    }else if(((v.no_cek/v.total)*100).toFixed(2) < 100 && ((v.no_cek/v.total)*100).toFixed(2) >= 75){
                        bg_progress = 'pg-info';
                        val_progress = ((v.no_cek/v.total)*100).toFixed(2);
                    }else if(((v.no_cek/v.total)*100).toFixed(2) < 75 && ((v.no_cek/v.total)*100).toFixed(2) >= 50){
                        bg_progress = 'pg-orange';
                        val_progress = ((v.no_cek/v.total)*100).toFixed(2);
                    }else if(((v.no_cek/v.total)*50).toFixed(2) < 75 && ((v.no_cek/v.total)*100).toFixed(2) >= 25){
                        bg_progress = 'pg-warning';
                        val_progress = ((v.no_cek/v.total)*100).toFixed(2);
                    }else if(((v.no_cek/v.total)*50).toFixed(2) < 25 && ((v.no_cek/v.total)*100).toFixed(2) > 0){
                        bg_progress = 'pg-indigo';
                        val_progress = ((v.no_cek/v.total)*100).toFixed(2);
                    }

                    let btn = '';
                    
                    let user_level = "<?= $this->session->userdata('level'); ?>";

                    if(user_level == 4 || user_level == 5 || user_level == 12 || user_level == 69) {
                        btn = '<button class="btn btn-warning" onclick="openModalInbound('+v.id_warehouse+')">Uncek List</button>';    
                    } else {
                        btn = '';
                    }

                    let btn_cek = '';

                    if(user_level == 4 || user_level == 5 || user_level == 11 || user_level == 12 || user_level == 13 || user_level == 69 ) {
                        btn_cek = '<button style="margin-top:10px" class="btn btn-primary" onclick="openModallistHO('+v.id_warehouse+')">List Module</button>';    
                    } else {
                        btn_cek = '';
                    }

                    if(v.indelivery){
                        v.indelivery = v.indelivery;
                    }else{
                        v.indelivery = 0;
                    }
                    inboundList.row.add([
                        k+1,
                        v.name_warehouse,
                        v.good_cek,     
                        v.faulty_cek,
                        v.good_uncek,
                        v.faulty_uncek,
                        v.total,
                        '<span>'+
                        '<p style="margin: 0; margin-bottom: 7px; font-size: 12px; text-align: center;">'+val_progress+'%</p>'+
                        '<div class="progress" style="height: 7px;">'+
                        '<div class="progress-bar progress-bar-striped progress-bar-animated '+bg_progress+'" role="progressbar" style="width: '+val_progress+'%;"></div>'+
                        '</div>'+
                        '</span>',
                        v.indelivery,
                        btn + btn_cek
                        ]).draw( false );
                });
            },
        });
    });

    function openModalInbound(id_request){
        checkList.rows().remove();
        $.ajax({
            url: "<?php echo base_url('huawei/counting_stock/uncek_list');?>",
            type : "POST",
            dataType: 'json',
            data:{'cek' : id_request},
            success : function(data){
                $(data).each(function(k,v) {
                    checkList.row.add([
                        k+1,
                        v.sn,
                        v.name_stockstatus,     
                        v.locbin,
                        v.name_stockaccess,
                        ]).draw( false );
                });
                $('#modal-insert-inbound').modal('show');
            },
        });
    }

    function openModallistHO(id_warehouse){
        list_HO.rows().remove().draw();
        $.ajax({
            url: "<?php echo base_url('huawei/counting_stock/list_HO');?>",
            type : "POST",
            dataType: 'json',
            data:{'list' : id_warehouse},
            success : function(data){
                // console.log(data);
                $(data).each(function(k,v) {
                    list_HO.row.add([
                        k+1,
                        v.name_pn,
                        v.sn,
                        v.name_stockstatus,     
                        v.locbin,
                        v.name_stockaccess,
                        v.stocktake_time
                        ]).draw( false );
                });
                $('#modal-list-ho').modal('show');
            },
        });
    }

</script>
