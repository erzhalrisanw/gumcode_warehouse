<div class="content-wrapper">
	<section class="content">
		<div class="container-fluid">
			<div class="row my-row">
				<div class="header-jdl">
					<p class="jdl-big">Document Print</p>
				</div>
				<div class="tab-btn">
					<button class="btn btn-primary">Inbound</button>
					<button class="btn btn-primary">Outbound</button>
					<button class="btn btn-primary">Request</button>
					<button class="btn btn-primary">Pickup</button>
				</div>

				<div id="div-inbound" class="input-div">
					<div style="border-bottom: 1px solid rgba(0,0,0,.3); margin-bottom: 24px; padding-bottom: 24px;">
						<form id="inbound-form">
							<div class="my-form-group">
								<input id="ib_doc_no" name="ib_doc_no" type="text" class="form-control" placeholder="Document Number">
							</div>
							<div class="my-form-group">
								<input class="form-control input-datetime" type="text" name="ib_created_date" id="ib_created_date" placeholder="Created Date">
							</div>
							<div class="my-form-group" style="display: block; margin-top: 24px;">
								<p class="my-label-input" style="margin-bottom: 0;">From :</p>
								<select name="ib_from" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach($warehouse as $ib_from) {?>
										<option value="<?=$ib_from['id_warehouse']?>"><?=$ib_from['name_warehouse']?></option>
									<?php } ?>
								</select>
							</div>
							<div class="my-form-group" style="display: block; margin-top: 24px;">
								<p class="my-label-input" style="margin-bottom: 0;">To :</p>
								<select name="ib_to" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach($warehouse as $ib_to) {?>
										<option value="<?=$ib_to['id_warehouse']?>"><?=$ib_to['name_warehouse']?></option>
									<?php } ?>
								</select>
							</div>
						</form>
						<button style="margin-top: 24px;" id="ib_search" type="button" class="btn btn-success">Search</button>
					</div>

					<div class="loadingio-spinner-spinner-audzu8pl3mb search_loading">
						<div class="ldio-yh420ne86p">
							<div>   
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
						</div>
					</div>

					<div id="ib_table_div" class="table-responsive">
						<table id="ib_table" class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Document Number</th>
									<th>Created Date</th>
									<th>From</th>
									<th>To</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>

				<div id="div-outbound" class="input-div">
					<div style="border-bottom: 1px solid rgba(0,0,0,.3); margin-bottom: 24px; padding-bottom: 24px;">
						<form id="outbound-form">
							<div class="my-form-group">
								<input id="ob_doc_no" name="ob_doc_no" type="text" class="form-control" placeholder="Document Number">
							</div>
							<div class="my-form-group">
								<input class="form-control input-datetime" type="text" name="ob_created_date" id="ob_created_date" placeholder="Created Date">
							</div>
							<div class="my-form-group" style="display: block; margin-top: 24px;">
								<p class="my-label-input" style="margin-bottom: 0;">From :</p>
								<select name="ob_from" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach($warehouse as $ob_from) {?>
										<option value="<?=$ob_from['id_warehouse']?>"><?=$ob_from['name_warehouse']?></option>
									<?php } ?>
								</select>
							</div>
							<div class="my-form-group" style="display: block; margin-top: 24px;">
								<p class="my-label-input" style="margin-bottom: 0;">To :</p>
								<select name="ob_to" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach($warehouse as $ob_to) {?>
										<option value="<?=$ob_to['id_warehouse']?>"><?=$ob_to['name_warehouse']?></option>
									<?php } ?>
								</select>
							</div>
						</form>
						<button style="margin-top: 24px;" id="ob_search" type="button" class="btn btn-success">Search</button>
					</div>

					<div class="loadingio-spinner-spinner-audzu8pl3mb search_loading">
						<div class="ldio-yh420ne86p">
							<div>   
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
						</div>
					</div>

					<div id="ob_table_div" class="table-responsive">
						<table id="ob_table" class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Document Number</th>
									<th>Created Date</th>
									<th>From</th>
									<th>To</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>

				<div id="div-request" class="input-div">
					<div style="border-bottom: 1px solid rgba(0,0,0,.3); margin-bottom: 24px; padding-bottom: 24px;">
						<form id="request-form">
							<div class="my-form-group">
								<input class="form-control input-datetime-range" type="text" name="req_date_range" id="req_date_range" placeholder="Date Range">
							</div>
							<div class="my-form-group" style="display: block; margin-top: 24px;">
								<p class="my-label-input" style="margin-bottom: 0;">Warehouse :</p>
								<select name="req_warehouse" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach($warehouse as $req_warehouse) {?>
										<option value="<?=$req_warehouse['id_warehouse']?>"><?=$req_warehouse['name_warehouse']?></option>
									<?php } ?>
								</select>
							</div>
						</form>
						<button style="margin-top: 24px;" id="req_search" type="button" class="btn btn-success">Search</button>
					</div>

					<div class="loadingio-spinner-spinner-audzu8pl3mb search_loading">
						<div class="ldio-yh420ne86p">
							<div>   
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
						</div>
					</div>

					<div id="req_table_div" class="table-responsive">
						<table id="req_table" class="table table-bordered">
							<thead>
								<tr>
									<!-- <th>No</th> -->
									<th>POD Document</th>
									<th>Request Date</th>
									<th>Warehouse</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>

				<div id="div-pickup" class="input-div">
					<div style="border-bottom: 1px solid rgba(0,0,0,.3); margin-bottom: 24px; padding-bottom: 24px;">
						<form id="pickup-form">
							<div class="my-form-group">
								<input class="form-control input-datetime-range" type="text" name="pickup_date_range" id="pickup_date_range" placeholder="Date Range">
							</div>
							<div class="my-form-group" style="display: block; margin-top: 24px;">
								<p class="my-label-input" style="margin-bottom: 0;">Warehouse :</p>
								<select name="pickup_warehouse" class="form-control select2">
									<option value="">Empty</option>
									<?php foreach($warehouse as $pickup_warehouse) {?>
										<option value="<?=$pickup_warehouse['id_warehouse']?>"><?=$pickup_warehouse['name_warehouse']?></option>
									<?php } ?>
								</select>
							</div>
						</form>
						<button style="margin-top: 24px;" id="pickup_search" type="button" class="btn btn-success">Search</button>
					</div>

					<div class="loadingio-spinner-spinner-audzu8pl3mb search_loading">
						<div class="ldio-yh420ne86p">
							<div>   
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
							<div>
							</div>
						</div>
					</div>

					<div id="pickup_table_div" class="table-responsive">
						<table id="pickup_table" class="table table-bordered">
							<thead>
								<tr>
									<!-- <th>No</th> -->
									<th>Pickup Document</th>
									<th>Request Date</th>
									<th>Warehouse</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
</div>
<script>
	$(function () {
		$('.input-datetime').daterangepicker(
		{
			singleDatePicker: true,
			showDropdowns: true,
			locale: {
				format: 'YYYY-MM-DD'
			},
			autoUpdateInput: false
		}
		);

		$('.input-datetime-range').daterangepicker(
		{
			showDropdowns: true,
			autoUpdateInput: false
		}
		);

		$('.input-datetime').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD'));
		});

		$('.input-datetime-range').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD')+' - '+picker.endDate.format('YYYY-MM-DD'));
		});

		ib_table = $('#ib_table').DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			info        : true,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "No Data to Show",
				zeroRecords: "No Data to Show"
			}
		});

		ob_table = $('#ob_table').DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			info        : true,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "No Data to Show",
				zeroRecords: "No Data to Show"
			}
		});

		req_table = $('#req_table').DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			info        : true,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "No Data to Show",
				zeroRecords: "No Data to Show"
			},
			rowsGroup: [0, 4],

		});

		pickup_table = $('#pickup_table').DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			info        : true,
			autoWidth   : true,
			responsive  : true,
			language: {
				emptyTable: "No Data to Show",
				zeroRecords: "No Data to Show"
			},
			rowsGroup: [0, 4],

		});

		$('.search_loading').hide();

		$('.tab-btn > button').click(function(){
			$('.tab-btn > button').removeClass('active');
			$(this).addClass('active');
		});

		$('.tab-btn').children('button').eq(3).click(function(){
			pickup_table.clear().draw();
			$('#div-pickup').show();
			$('#div-inbound').hide();
			$('#div-outbound').hide();
			$('#div-request').hide();
			$('#ib_table_div').hide();
			$('#ob_table_div').hide();
			$('#req_table_div').hide();
			$('#pickup_table_div').hide();
		});

		$('.tab-btn').children('button').eq(2).click(function(){
			req_table.clear().draw();
			$('#div-pickup').hide();
			$('#div-inbound').hide();
			$('#div-outbound').hide();
			$('#div-request').show();
			$('#ib_table_div').hide();
			$('#ob_table_div').hide();
			$('#req_table_div').hide();
			$('#pickup_table_div').hide();
		});

		$('.tab-btn').children('button').eq(1).click(function(){
			ob_table.clear().draw();
			$('#div-pickup').hide();
			$('#div-inbound').hide();
			$('#div-outbound').show();
			$('#div-request').hide();
			$('#ib_table_div').hide();
			$('#ob_table_div').hide();
			$('#req_table_div').hide();
			$('#pickup_table_div').hide();
			$('#ob_doc_no').val('');
		});

		$('.tab-btn').children('button').eq(0).click(function(){
			ib_table.clear().draw();
			$('#div-pickup').hide();
			$('#div-inbound').show();
			$('#div-outbound').hide();
			$('#div-request').hide();
			$('#ib_table_div').hide();
			$('#ob_table_div').hide();
			$('#req_table_div').hide();
			$('#pickup_table_div').hide();
			$('#ib_doc_no').val('');
		});

		$('.tab-btn').children('button').eq(0).trigger('click');

		$('#ib_search').click(function(){
			ib_table.clear().draw();
			$('.search_loading').show();
			$.ajax({
				url: "<?php echo base_url('huawei/download/search_inbound_doc');?>",
				type : "POST",
				dataType: 'json',
				data: $('#inbound-form').serialize(),
				success : function(data){
					$(data).each(function(k,v) {
						ib_table.row.add([
							k+1,
							v.doc_no,
							v.doc_date,
							v.from,
							v.to,
							'<a class="btn btn-primary btn-sm" href="<?=base_url()?>huawei/all_process/docPrint/'+v.id+'" target="_blank">Open</a>'
							]).draw();
					});
					$('#ib_table_div').show();
					$('.search_loading').hide();
				},
			});
		});

		$('#ob_search').click(function(){
			ob_table.clear().draw();
			$('.search_loading').show();
			$.ajax({
				url: "<?php echo base_url('huawei/download/search_outbound_doc');?>",
				type : "POST",
				dataType: 'json',
				data: $('#outbound-form').serialize(),
				success : function(data){
					$(data).each(function(k,v) {
						ob_table.row.add([
							k+1,
							v.doc_no,
							v.doc_date,
							v.from,
							v.to,
							'<a class="btn btn-primary btn-sm" href="<?=base_url()?>huawei/all_process/docPrint/'+v.id+'" target="_blank">Open</a>'
							]).draw();
					});
					$('#ob_table_div').show();
					$('.search_loading').hide();
				},
			});
		});

		$('#req_search').click(function(){
			req_table.clear().draw();
			$('.search_loading').show();
			$.ajax({
				url: "<?php echo base_url('huawei/download/search_request_doc');?>",
				type : "POST",
				dataType: 'json',
				data: $('#request-form').serialize(),
				success : function(data){
					$(data).each(function(k,v) {
						req_table.row.add([
							// k+1,
							v.pod_no,
							v.time_request,
							v.name_warehouse,
							v.status_request,
							'<a class="btn btn-primary btn-sm" href="<?=base_url()?>'+v.url_photo_mdn+'" target="_blank">Open</a>'
							]).draw();
					});
					$('#req_table_div').show();
					$('.search_loading').hide();
				},
			});
		});

		$('#pickup_search').click(function(){
			pickup_table.clear().draw();
			$('.search_loading').show();
			$.ajax({
				url: "<?php echo base_url('huawei/download/search_pickup_doc');?>",
				type : "POST",
				dataType: 'json',
				data: $('#pickup-form').serialize(),
				success : function(data){
					$(data).each(function(k,v) {
						pickup_table.row.add([
							// k+1,
							v.pickup_no,
							v.time_request,
							v.name_warehouse,
							v.pickup_status,
							'<a class="btn btn-primary btn-sm" href="<?=base_url()?>'+v.url_photo_cmn+'" target="_blank">Open</a>'
							]).draw();
					});
					$('#pickup_table_div').show();
					$('.search_loading').hide();
				},
			});
		});
	});
</script>
