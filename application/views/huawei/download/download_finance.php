<div class="content-wrapper">
	<section class="content">
		<div class="container-fluid">
			<div class="row my-row">
				<div class="header-jdl">
					<p class="jdl-big">Download Finance</p>
				</div>

				<div id="div-download" class="input-div">
					<form id="download-form" action="<?=base_url()?>huawei/all_process/excel_finance" method="post">
						<div class="my-form-group">
							<p class="my-label-input">Date Range :</p>
							<input class="form-control input-datetime-range" type="text" name="date" id="date" placeholder="Date Range">
						</div>
						<button style="margin-top: 24px;" id="download_process" type="submit" class="btn btn-success">Download</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<script>
	$(function () {
		$('#loading-download').hide();
		$('.input-datetime-range').daterangepicker(
		{
			showDropdowns: true,
			autoUpdateInput: false
		}
		);
		$('.input-datetime-range').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD')+' - '+picker.endDate.format('YYYY-MM-DD'));
		});

		// $('#download_process').click(function(){
		// 	if($('#type').val() == ''){
		// 		var swal_data = { title: 'Failed', icon: 'error', text: 'Please select detail!', button:false, timer: 1000 };
		// 		swal(swal_data).then(function() {
		// 		});
		// 	}else{
		// 		$('#loading-download').show();
		// 		$.ajax({
		// 			url: "<?php echo base_url('huawei/all_process/excel_inventory_stock');?>",
		// 			type : "POST",
		// 			dataType: 'json',
		// 			data: $('#download-form').serialize(),
		// 			success : function(data){
		// 				// console.log(data);
		// 				$('#loading-download').hide();
		// 				window.location.href=data;
		// 			},
		// 		});
		// 	}
		// });
	});
</script>
