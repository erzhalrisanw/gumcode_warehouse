<div class="content-wrapper">
	<section class="content">
		<div class="container-fluid">
			<div class="row my-row">
				<div class="header-jdl">
					<p class="jdl-big">Download Inventory Stock</p>
				</div>

				<div id="div-download" class="input-div">
					<form id="download-form" action="<?=base_url()?>huawei/all_process/excel_inventory_stock" method="post">
						<div class="my-form-group">
							<p class="my-label-input">Detail :</p>
							<select name="type" id="type" class="form-control">
								<option value="">Please select detail</option>
								<option value="1">Available Stock</option>
								<option value="2">All Stock</option>
								<option value="3">All Faulty</option>
								<option value="4">Distribution</option>
								<option value="5">Return to CWH</option>
								<option value="6">Transfer Stock</option>
								<option value="7">Inbound new or handover</option>
								<option value="8">Outbound handover</option>
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Project :</p>
							<select name="id_project" class="form-control">
								<option value="">Empty</option>
								<?php foreach($project as $project) {?>
									<option value="<?=$project['id_project']?>"><?=$project['name_project']?></option>
								<?php } ?>
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Warehouse :</p>
							<select name="id_warehouse" id="id_warehouse" class="form-control select2">
								<option value="">Empty</option>
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Status :</p>
							<select name="id_stockstatus" id="id_stockstatus" class="form-control">
								<option value="">Empty</option>
								<?php foreach($stock_status as $stock_status) {?>
									<option value="<?=$stock_status['id_stockstatus']?>"><?=$stock_status['name_stockstatus']?></option>
								<?php } ?>
							</select>
						</div>
						<div class="my-form-group">
							<p class="my-label-input">Date Range :</p>
							<input class="form-control input-datetime-range" type="text" name="date" id="date" placeholder="Date Range">
						</div>
						<button style="margin-top: 24px;" id="download_process" type="submit" class="btn btn-success">Download</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<script>
	$(function () {
		$('#loading-download').hide();
		$('.input-datetime-range').daterangepicker(
		{
			showDropdowns: true,
			autoUpdateInput: false
		}
		);
		$('.input-datetime-range').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD')+' - '+picker.endDate.format('YYYY-MM-DD'));
		});

		$('#type').on('change', function(){
			$.ajax({
				url: "<?php echo base_url('huawei/download/get_warehouse_by_download_inventory_type');?>",
				type : "POST",
				dataType: 'json',
				data: {'type' : $(this).val()},
				success : function(data){
					$('#id_warehouse').find('option').not(':first').remove();
					$(data).each(function(k,v) {
						$('#id_warehouse').append(new Option(v.name_warehouse, v.id_warehouse));
					});
				},
			});
		});

		// $('#download_process').click(function(){
		// 	if($('#type').val() == ''){
		// 		var swal_data = { title: 'Failed', icon: 'error', text: 'Please select detail!', button:false, timer: 1000 };
		// 		swal(swal_data).then(function() {
		// 		});
		// 	}else{
		// 		$('#loading-download').show();
		// 		$.ajax({
		// 			url: "<?php echo base_url('huawei/all_process/excel_inventory_stock');?>",
		// 			type : "POST",
		// 			dataType: 'json',
		// 			data: $('#download-form').serialize(),
		// 			success : function(data){
		// 				// console.log(data);
		// 				$('#loading-download').hide();
		// 				window.location.href=data;
		// 			},
		// 		});
		// 	}
		// });

		$('#download-form').submit(function() {
			if($('#type').val() == ''){
				var swal_data = { title: 'Failed', icon: 'error', text: 'Please select detail!', button:false, timer: 1000 };
				swal(swal_data).then(function() {
				});
				return false;
			}
		});
	});
</script>
