<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.a4 {
			width: 615px;
			height: 862px;
			margin: 40px 40px 40px 40px;
		}

		.header {
			width: 100%;
			border: 1px solid black !important;
			display: flex !important;
			margin-bottom: 0;
			height: 60px;
		}

		.head1 {
			width: 80px;
			height: 60px;
			/*padding: 5px;*/
			/*padding-top: 8px;*/
			padding-bottom: 0;
			/*margin-top: -5px;*/
			border-right: 1px solid black;
		}

		.head1 > img {
			margin-left: 2px;
			/*margin-top: 3px;*/
		}

		.head2 {
			height: 60px;
			/*border-left: 1px solid black;*/
			text-align: center;
			width: 100%;
			/*border: 1px solid black;*/
		}

		.head3 {
			border-left: 1px solid black;
			width: 80px;
		}

		.head3 > img {
			margin-top: 2px;
			margin-left: 5px;
		}

		.head2 > p {
			width: 100%;
		}

		.body1 {
			margin-top: 20px;
			width: 100% !important;
			justify-content: space-between;
		}

		.body2 {
			/*padding-left: 10px;*/
			margin-top: 40px;
			border: 1px solid black;
			width: 100%;
			margin-bottom: 30px;
		}

		.body3 {
			margin-top: 20px;
			display: flex;
			justify-content: space-between !important;
		}

		.body3-1, .body3-2 {
			width: 45%;
		}

		.body1-1, .body1-2{
			width: 45% !important;
		}

		.body1-1 {
			padding-left: 10px;
		}

		.body1-2 {
			padding-right: 10px;
		}

		.head2-p1 {
			font-size: 22px;
			font-weight: 600;
			margin: 0;
			/*margin-bottom: 5px;*/
			margin-top: 15px;
		}

		.head2-p2 {
			font-size: 12px;
			font-weight: 600;
			margin: 0;
		}

		.body1-p-ttl {
			font-size: 12px;
			font-weight: 600;
		}

		.body1-p-normal {
			font-size: 12px;
			font-weight: normal;
		}

		.body1-p-normal-40 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 40px;
		}

		.body2-p {
			font-size: 12px;
			margin-left: 10px;
		}

		.tab-head {
			background: black;
			color: white;
		}

		.body3-p {
			font-size: 12px;
			font-weight: normal;
		}

		.body3-p-30 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 30px;
		}

		table {
			font-size: 12px;
			border-spacing: 0;
			border-bottom: none !important;
			width: 100%;
			border: 1px solid black;
			margin-top: 20px;
			border-right: none;
		}

		th {
			text-align: center !important;
			font-weight: 600;
			color: black;
			border-bottom: 1px solid black;
			border-right: 1px solid black;
			background: #E9823C;
			line-height: 20px;
		}

		td {
			border-bottom: 1px solid black;
			text-align: center !important;
			border-right: 1px solid black;
			line-height: 20px;
		}
	</style>
</head>
<body>
	<div class="a4">
		<table>
			<tr>
				<th style="background: transparent !important;" rowspan="2" colspan="2"><img src="<?= base_url()?>/dist/img/HIL-logo.png" style="width: 55px;"></th>
				<th style="width: 230px;">Origin</th>
				<th style="width: 230px;">Destination</th>
				<th style="width: 40px;">Qty</th>
				<th>Weight (kg)</th>
			</tr>
			<tr>
				<td style="width: 200px;"><?= $from[0]['name_warehouse']?></td>
				<td style="width: 200px;"><?= $to[0]['name_warehouse']?></td>
				<td><?= $qty?></td>
				<td><?= $weight?></td>
			</tr>
			<tr>
				<th colspan="6">Document Number</th>
			</tr>
			<tr>
				<td style="color: center;" colspan="6"><?= $doc[0]['doc_no']?></td>
			</tr>
			<tr>
				<td style="color: white;" colspan="6">a</td>
			</tr>
			<tr>
				<th colspan="3">Sender Information</th>
				<th colspan="3">Consignee Information</th>
			</tr>
			<tr>
				<td colspan="3"><?= $from[0]['name_user']?></td>
				<td colspan="3"><?= $to[0]['name_user']?></td>
			</tr>
			<tr>
				<th style="text-align: left;" colspan="3">Sender Address</th>
				<th style="text-align: left;" colspan="3">Delivery Address</th>
			</tr>
			<tr>
				<td colspan="3"><?= $from[0]['warehouse_address']?></td>
				<td colspan="3"><?= $to[0]['warehouse_address']?></td>
			</tr>
			<tr>
				<th style="text-align: left;" colspan="3">Contact Number</th>
				<th style="text-align: left;" colspan="3">Contact Number</th>
			</tr>
			<tr>
				<td colspan="3"><?= !$from[0]['no_telp'] ? '<p style="color: white; margin: 0;">a</p' : $from[0]['no_telp']?></td>
				<td colspan="3"><?= !$to[0]['no_telp'] ? '<p style="color: white; margin: 0;">a</p' : $to[0]['no_telp']?></td>
			</tr>
			<tr>
				<td style="color: white;" colspan="3">a</td>
				<td style="color: white;" colspan="3">a</td>
			</tr>
			<tr>
				<th colspan="3">Sender</th>
				<th colspan="3">Consignee</th>
			</tr>
			<tr>
				<td style="text-align: right !important; color: white !important;" colspan="3">a</td>
				<td style="text-align: right !important; color: white !important;" colspan="3">a</td>
			</tr>
			<tr>
				<th colspan="3">Signature</th>
				<th colspan="3">Signature</th>
			</tr>
			<tr>
				<td style="color: white; border-bottom: none;" colspan="3">a</td>
				<td style="color: white; border-bottom: none;" colspan="3">a</td>
			</tr>
			<tr>
				<td style="color: white; border-bottom: none;" colspan="3">a</td>
				<td style="color: white; border-bottom: none;" colspan="3">a</td>
			</tr>
			<tr>
				<td style="color: white; border-bottom: none;" colspan="3">a</td>
				<td style="color: white; border-bottom: none;" colspan="3">a</td>
			</tr>
			<tr>
				<th colspan="3">Delivery Date</th>
				<th colspan="3">Received Date</th>
			</tr>
			<tr>
				<td style="color: white;" colspan="3">a</td>
				<td style="color: white;" colspan="3">a</td>
			</tr>
			<!-- <?php foreach($all as $k => $v) {?>
				<tr>
					<td><?= $k+1?></td>
					<td><?= $v['rmr']?></td>
					<td style="width: 200px;"><?= $v['des_req']?></td>
					<td><?= $v['pn_req']?></td>
					<td style="color: white;">________________</td>
					<td style="color: white;">________________</td>
				</tr>
			<?php } ?> -->
		</table>
		<!-- <p style="font-size: 12px;">* Please write your name.</p> -->
	</div>
</body>
</html>