<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.a4 {
			width: 615px;
			height: 862px;
			margin: 40px 40px 40px 40px;
		}

		.header {
			width: 100%;
			border: 1px solid black !important;
			display: flex !important;
			margin-bottom: 0;
			height: 60px;
		}

		.head1 {
			width: 80px;
			height: 60px;
			/*padding: 5px;*/
			/*padding-top: 8px;*/
			padding-bottom: 0;
			/*margin-top: -5px;*/
			border-right: 1px solid black;
		}

		.head1 > img {
			margin-left: 2px;
			/*margin-top: 3px;*/
		}

		.head2 {
			height: 60px;
			/*border-left: 1px solid black;*/
			text-align: center;
			width: 100%;
			/*border: 1px solid black;*/
		}

		.head3 {
			border-left: 1px solid black;
			width: 80px;
		}

		.head3 > img {
			margin-top: 2px;
			margin-left: 5px;
		}

		.head2 > p {
			width: 100%;
		}

		.body1 {
			margin-top: 20px;
			width: 100% !important;
			justify-content: space-between;
		}

		.body2 {
			/*padding-left: 10px;*/
			margin-top: 40px;
			border: 1px solid black;
			width: 100%;
			margin-bottom: 30px;
		}

		.body3 {
			margin-top: 20px;
			display: flex;
			justify-content: space-between !important;
		}

		.body3-1, .body3-2 {
			width: 45%;
		}

		.body1-1, .body1-2{
			width: 45% !important;
		}

		.body1-1 {
			padding-left: 10px;
		}

		.body1-2 {
			padding-right: 10px;
		}

		.head2-p1 {
			font-size: 22px;
			font-weight: 600;
			margin: 0;
			/*margin-bottom: 5px;*/
			margin-top: 15px;
		}

		.head2-p2 {
			font-size: 12px;
			font-weight: 600;
			margin: 0;
		}

		.body1-p-ttl {
			font-size: 12px;
			font-weight: 600;
		}

		.body1-p-normal {
			font-size: 12px;
			font-weight: normal;
		}

		.body1-p-normal-40 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 40px;
		}

		.body2-p {
			font-size: 12px;
			margin-left: 10px;
		}

		.tab-head {
			background: black;
			color: white;
		}

		.body3-p {
			font-size: 12px;
			font-weight: normal;
		}

		.body3-p-30 {
			font-size: 12px;
			font-weight: normal;
			margin-top: 30px;
		}

		table {
			font-size: 12px;
			border-spacing: 0;
			border-bottom: none !important;
			width: 100%;
			border: 1px solid black;
			margin-top: 20px;
			border-right: none;
		}

		th {
			text-align: center !important;
			font-weight: 600;
			color: black;
			border-bottom: 1px solid black;
			border-right: 1px solid black;
			background: #E9823C;
			line-height: 20px;
		}

		td {
			border-bottom: 1px solid black;
			text-align: center !important;
			border-right: 1px solid black;
			line-height: 20px;
		}
	</style>
</head>
<body>
	<div class="a4">
		<table>
			<tr>
				<th style="background: transparent !important;" colspan="3"><img src="<?= base_url()?>/dist/img/harrasima-logo.png" style="width: 180px;"></th>
				<th style="background: transparent !important;" colspan="3"><img src="<?= base_url()?>/dist/img/huawei_logo.png" style="width: 90px;"></th>
			</tr>
			<tr>
				<th colspan="6">Document Number</th>
			</tr>
			<?php for($i = 0; $i < count($doc_no); $i++) {?>
				<tr>
					<td colspan="6"><?= $doc_no[$i]?></td>
				</tr>
			<?php } ?>
			<tr>
				<th colspan="3">Sender</th>
				<th colspan="3">Consignee</th>
			</tr>
			<tr>
				<td style="width: 150px;" colspan="2"><?= $pic_pool[0]['name_user']?></td>
				<td style="text-align: right !important; color: white !important;">a</td>
				<td style="width: 150px;" colspan="2"><?= $pic_dop ?></td>
				<td style="text-align: right !important; color: white !important;">a</td>
			</tr>
			<tr>
				<th style="width: 150px;" colspan="2">Sender Address</th>
				<th>Signature</th>
				<th style="width: 150px;" colspan="2">Delivery Address</th>
				<th>Signature</th>
			</tr>
			<tr>
				<td style="width: 150px;" colspan="2"><?= $pic_pool[0]['warehouse_address']?></td>
				<td></td>
				<td style="width: 150px;" colspan="2"><?= $pic_pool[0]['alamat_dop']?></td>
				<td></td>
			</tr>
			<tr>
				<th style="width: 150px;" colspan="2">Contact Number</th>
				<th>Delivery Date</th>
				<th style="width: 150px;" colspan="2">Contact Number</th>
				<th>Received Date</th>
			</tr>
			<tr>
				<td style="width: 150px;" colspan="2"><?= $pic_pool[0]['no_telp']?></td>
				<td style="text-align: right !important; color: white !important;">a</td>
				<td style="width: 150px;" colspan="2"><?= $pic_dop_telp?></td>
				<td style="text-align: right !important; color: white !important;">a</td>
			</tr>
		</table>
		<!-- <p style="font-size: 12px;">* Please write your name.</p> -->
	</div>
</body>
</html>