<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Create AWB</p>
                </div>

                <div class="inbound-div">
                    <div class="my-form-group">
                        <p class="my-label-input">From :</p>
                        <select id="input_from" class="form-control select2">
                            <?php foreach($from as $from) {?>
                                <option value="<?=$from['id_warehouse']?>"><?=$from['name_warehouse']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">To :</p>
                        <select id="input_to" class="form-control select2">
                            <?php foreach($to as $to) {?>
                                <option value="<?=$to['id_warehouse']?>"><?=$to['name_warehouse']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Qty :</p>
                        <input type="text" id="input_qty" class="form-control" name="input_qty">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Weight :</p>
                        <input type="text" id="input_weight" class="form-control" name="input_weight">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Document :</p>
                        <select id="input_doc" name="input_doc" class="form-control select2">
                        </select>
                    </div>
                    <button style="float: right; margin-top: 34px;" id="print-awb" class="btn btn-success">Print</button>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        $('#print-awb').click(function(){
            if($('#input_qty').val() == '' || $('#input_weight').val() == ''){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                window.open('<?=base_url()?>huawei/all_process/print_awb/'+$('#input_from').val()+'/'+$('#input_to').val()+'/'+$('#input_qty').val()+'/'+$('#input_weight').val()+'/'+$('#input_doc').val(), '_blank');
            }
        });

        $('#input_to').on('change', function(){
            $.ajax({
                url: "<?php echo base_url('huawei/finance/get_doc_by_from_to');?>",
                type : "POST",
                dataType: 'json',
                data: {'from' : $('#input_from').val(), 'to' : $('#input_to').val()},
                success : function(data){
                    $('#input_doc').html('');
                    $(data).each(function(k,v) {
                        $('#input_doc').append(new Option(v.doc_no, v.id));
                        $('#input_doc').trigger('change');
                    });
                },
            });
        });

        $('#input_from').on('change', function(){
            $.ajax({
                url: "<?php echo base_url('huawei/finance/get_doc_by_from_to');?>",
                type : "POST",
                dataType: 'json',
                data: {'from' : $('#input_from').val(), 'to' : $('#input_to').val()},
                success : function(data){
                    $('#input_doc').html('');
                    $(data).each(function(k,v) {
                        $('#input_doc').append(new Option(v.doc_no, v.id));
                        $('#input_doc').trigger('change');
                    });
                },
            });
        });

        $('#input_doc').on('change', function(){
            $.ajax({
                url: "<?php echo base_url('huawei/finance/get_qty_by_doc');?>",
                type : "POST",
                dataType: 'json',
                data: {'doc_id' : $(this).val()},
                success : function(data){
                    $('#input_qty').val(data);
                },
            });
        });
    });
</script>
