<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Create Time Sheet</p>
                </div>

                <div class="inbound-div">
                    <div id="input_city_div" class="my-form-group">
                        <p class="my-label-input">City :</p>
                        <select id="input_city" name="input_city" class="form-control select2">
                            <option value="">Please select City</option>
                            <?php foreach($city as $city) {?>
                                <option value="<?=$city['id_city']?>"><?=$city['name_city']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div id="input_dop_div" style="width: 100%; margin-bottom: 20px;" class="">
                        <p class="my-label-input">DOP :</p>
                        <select id="input_dop" name="input_dop" class="form-control select2">
                            <?if($this->session->userdata('level') == 10) ?>
                                <option value="">Please select DOP</option>
                                <?php foreach($dop as $dop) {?>
                                    <option value="<?=$dop['id_dop']?>"><?=$dop['name_dop']?>, address : <?=$dop['alamat_dop']?></option>
                                <?php } ?>
                            <?php ?>
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">Date :</p>
                        <input type="text" id="input_date" name="input_date" class="form-control input-datetime">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">POD/CMN :</p>
                        <select id="input_pod_cmn" name="input_pod_cmn[]" class="form-control select2" multiple="multiple">
                        </select>
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">PIC :</p>
                        <input type="text" id="input_pic" name="input_pic" class="form-control">
                    </div>
                    <div class="my-form-group">
                        <p class="my-label-input">No Telp :</p>
                        <input type="number" id="input_telp" name="input_telp" class="form-control">
                    </div>
                    <button style="float: right; margin-top: 34px;" id="print-awb" class="btn btn-success">Print</button>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        let user_level = '<?= $this->session->userdata('level')?>';

        $('.input-datetime').daterangepicker(
        {
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }
        );

        if(user_level == 10){
            $('#input_city_div').hide();
            $('#print-awb').click(function(){
                if($('#input_pic').val() == '' || $('#input_telp').val() == '' || $('#input_pod_cmn').val() == '' || $('#input_dop').val() == ''){
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                }else{
                    window.open('<?=base_url()?>huawei/all_process/print_time_sheet/'+$('#input_dop').val()+'/'+encodeURIComponent($('#input_pod_cmn').val())+'/'+$('#input_pic').val()+'/'+$('#input_telp').val(), '_blank');
                }
            });
        }else{
            $('#print-awb').click(function(){
                if($('#input_city').val() == '' || $('#input_pic').val() == '' || $('#input_telp').val() == '' || $('#input_pod_cmn').val() == '' || $('#input_dop').val() == ''){
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                }else{
                    window.open('<?=base_url()?>huawei/all_process/print_time_sheet/'+$('#input_dop').val()+'/'+encodeURIComponent($('#input_pod_cmn').val())+'/'+$('#input_pic').val()+'/'+$('#input_telp').val(), '_blank');
                }
            });
        }

        $('#input_city').on('change', function(){
            $('#input_dop').html('');
            $.ajax({
                url: "<?php echo base_url('huawei/finance/get_dop_by_city');?>",
                type : "POST",
                dataType: 'json',
                data: {'id_city' : $(this).val()},
                success : function(data){
                    $(data).each(function(a,b) {
                        $('#input_dop').append(new Option(b.name_dop+ ', address : '+b.alamat_dop, b.id_dop));
                        $('#input_dop').trigger('change');
                    });
                },
            });
        });

        $('#input_dop').on('change', function(){
            $('#input_date').trigger('change');
        });

        $('#input_date').on('change', function(){
            $('#input_pod_cmn').html('');
            $.ajax({
                url: "<?php echo base_url('huawei/finance/get_pod_cmn_list_by_dop');?>",
                type : "POST",
                dataType: 'json',
                data: {'id_dop' : $('#input_dop').val(), 'date' : $(this).val()},
                success : function(data){
                    // console.log(data);
                    if(data.pod.length > 0 || data.pickup.length > 0){
                        var $pod = $('<optgroup label="POD">');
                        var $cmn = $('<optgroup label="CMN">');
                        $('#input_pod_cmn').append($pod);
                        $('#input_pod_cmn').append($cmn);
                        $(data.pod).each(function(a,b) {
                            $pod.append(new Option(b.pod_no, b.pod_no));
                        });
                        $(data.pickup).each(function(c,d) {
                            $cmn.append(new Option(d.pickup_no, d.pickup_no));
                        });
                    }
                },
            });
        });
    });
</script>
