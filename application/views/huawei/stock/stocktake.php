<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Stocktake</p>
                </div>
                <div class="put-away">
                    <div style="width: 100%;" class="my-form-group">
                        <p class="my-label-input" style="width: 30%;">Search Loc Bin :</p>
                        <input id="pa_search_doc" name="pa_search_doc" type="text" class="form-control" placeholder="Masukkan lokasi dan tekan enter">
                    </div>

                    <div style="margin-top: 40px;" id="put-form-tab">
                        <form id="stocktake-form">
                            <input id="stocktake-sn-bin" name="stocktake-sn-bin" type="hidden" class="form-control">
                            <div style="width: 100%;" class="my-form-group">
                                <p class="my-label-input" style="width: 30%;">Part Number :</p>
                                <input id="stocktake-sn-pn" name="stocktake-sn-pn" type="text" class="form-control" placeholder="Masukkan Part Number">
                            </div>
                            <div style="width: 100%;" class="my-form-group">
                                <p class="my-label-input" style="width: 30%;">Serial Number :</p>
                                <input id="stocktake-sn-sn" name="stocktake-sn-sn" type="text" class="form-control" placeholder="Masukkan Serial Number">
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table style="margin-top: 30px;" id="sn-list-check" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Part Number</th>
                                        <th>Serial Number</th>
                                        <th>Loc Bin</th>
                                        <th>Checked</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        let user_level = '<?= $this->session->userdata('level')?>';
        if(user_level != 4){
            $('#stocktake-form > * input').on("cut copy paste",function(e) {
                e.preventDefault();
            });
        }

        sngListCheck = $('#sn-list-check').DataTable({
            paging      : true,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "",
                zeroRecords: ""
            }
        });

        $('#pa_search_doc').trigger('focus');

        $('#put-form-tab').hide();

        $('#stocktake-sn-pn').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#stocktake-sn-sn').trigger('focus');
            }
        });

        $('#pa_search_doc').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#put-form-tab').hide();
                sngListCheck.clear().draw();
                $.ajax({
                    url: "<?php echo base_url('huawei/counting_stock/get_sn_by_loc_bin');?>",
                    type : "POST",
                    dataType: 'json',
                    data: {'loc_bin' : $('#pa_search_doc').val()},
                    success : function(data){
                        $('#put-form-tab').show();
                        $('#stocktake-sn-pn').trigger('focus');
                        $('#stocktake-sn-bin').val($('#pa_search_doc').val());

                        $(data).each(function(k,v) {
                            var check;
                            if(v.status_cek == '0'){
                                check = '';
                            } else {
                                check = '<i style="font-size: 16px; color: green;" class="fa fa-check"></i>';
                            }
                            sngListCheck.row.add([
                                k+1,
                                v.name_pn,
                                v.sn,
                                v.locbin,
                                check
                                ]).draw( false );
                        });
                    },
                });
            }
        });

        var fail = new Audio('<?php echo base_url(''); ?>/dist/tone/fail_tone.mp3');
        var success = new Audio('<?php echo base_url(''); ?>/dist/tone/success_tone.mp3');

        $('#stocktake-sn-sn').on('keyup', function(e){
            if(e.keyCode === 13){
                if($("#stocktake-sn-bin").val() == '' || $("#stocktake-sn-pn").val() == '' || $("#stocktake-sn-sn").val() == ''){
                    fail.play();
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Input cannot be empty!', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                }else{
                    $.ajax({
                        url: "<?php echo base_url('huawei/counting_stock/upd_sn_by_sn');?>",
                        type : "POST",
                        dataType: 'json',
                        data: $('#stocktake-form').serialize(),
                        success : function(data){
                            if(data){
                                success.play();
                                var swal_data = { title: 'Success', icon: 'success', text: 'Data successfully updated', button:false, timer: 1000 };
                                swal(swal_data).then(function() {
                                    sngListCheck.clear().draw();
                                    $.ajax({
                                        url: "<?php echo base_url('huawei/counting_stock/get_sn_by_loc_bin');?>",
                                        type : "POST",
                                        dataType: 'json',
                                        data: {'loc_bin' : $('#stocktake-sn-bin').val()},
                                        success : function(data){
                                            $('#stocktake-sn-pn').val('');
                                            $('#stocktake-sn-sn').val('');
                                            $('#stocktake-sn-pn').trigger('focus');
                                            $(data).each(function(k,v) {
                                                var check;
                                                if(v.status_cek == '0'){
                                                    check = '';
                                                } else {
                                                    check = '<i style="font-size: 16px; color: green;" class="fa fa-check"></i>';
                                                }
                                                sngListCheck.row.add([
                                                    k+1,
                                                    v.name_pn,
                                                    v.sn,
                                                    v.locbin,
                                                    check
                                                    ]).draw( false );
                                            });
                                        },
                                    });
                                });
                            }else{
                                fail.play();
                                var swal_data = { title: 'Failed', icon: 'error', text: 'Data cannot be updated', button:false, timer: 1000 };
                                swal(swal_data).then(function() {
                                });
                            }
                        },
                    });
                }
            }
        });
    });
</script>
