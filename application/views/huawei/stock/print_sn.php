<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Print SN</p>
                </div>
                <div class="put-away">
                    <div style="margin-top: 40px;" id="put-form-tab">
                        <div style="width: 40%;" class="my-form-group">
                            <p class="my-label-input">Loc Bin :</p>
                            <input id="locbin" type="text" class="form-control">
                        </div>
                        <div style="width: 40%;" class="my-form-group">
                            <p class="my-label-input">Part Number :</p>
                            <input id="pn" type="text" class="form-control" >
                        </div>
                        <div style="width: 40%;" class="my-form-group">
                            <p class="my-label-input">Serial Number :</p>
                            <input id="sn" type="text" class="form-control" >
                        </div>

                        <button style="position: fixed; z-index: 3; margin-top: 34px;" type="button" class="btn btn-primary" id="print_sn">Print</button>
                        <div style="margin-top: 34px;" class="table-responsive">
                            <form id="input_form" action="<?= base_url()?>huawei/counting_stock/print_sn_manually" method="post" target="_blank">
                                <table style="margin-top: 30px; padding-top: 54px;" id="list_input" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Loc Bin</th>
                                            <th>Part Number</th>
                                            <th>Serial Number</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        tab_list_input = $('#list_input').DataTable({
            paging      : false,
            lengthChange: false,
            searching   : true,
            ordering    : false,
            info        : false,
            autoWidth   : true,
            responsive  : true,
            language: {
                emptyTable: "",
                zeroRecords: ""
            }
        });

        $('#print_sn').attr('disabled', true);

        $('#print_sn').click(function(){
            $('#input_form').trigger('submit');
        });

        // $('#list_input').hide();

        $('#locbin').trigger('focus');

        $('#locbin').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#pn').trigger('focus');
            }
        });

        $('#pn').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#sn').trigger('focus');
            }
        });

        $('#sn').on('keyup', function(e){
            if(e.keyCode === 13){
                if($("#locbin").val() == '' || $("#pn").val() == '' || $("#sn").val() == ''){
                    var swal_data = { title: 'Failed', icon: 'error', text: 'Input cannot be empty!', button:false, timer: 1000 };
                    swal(swal_data).then(function() {
                    });
                }else{
                    tab_list_input.row.add([
                        '<input type="hidden" name="locbin[]" value="'+$('#locbin').val()+'">'+
                        '<input type="hidden" name="pn[]" value="'+$('#pn').val()+'">'+
                        '<input type="hidden" name="sn[]" value="'+$('#sn').val()+'">'+
                        $('#locbin').val(),
                        $('#pn').val(),
                        $('#sn').val(),
                        '<button class="btn btn-danger btn-sm delete-row">Delete</button>'
                        ]).draw( false );

                    $('#locbin, #pn, #sn').val('');
                    $('#locbin').trigger('focus');

                    $('#print_sn').attr('disabled', false);
                }
            }
        });

        $('#list_input tbody').on( 'click', '.delete-row', function () {
            var tr = $(this).closest('tr');
            tab_list_input.row(tr).remove().draw();

            if(!tab_list_input.data().count()){
                $('#print_sn').attr('disabled', true);
            }
        });
    });
</script>
