<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row my-row">
                <div class="header-jdl">
                    <p class="jdl-big">Change Password</p>
                </div>

                <div class="inbound-div">
                    <form id="change_profile_form" enctype="multipart/form-data">
                        <div class="my-form-group">
                            <p class="my-label-input">Name User :</p>
                            <input type="text" id="name_user" class="form-control" value="<?=$user->name_user?>" name="name_user">
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">Phone Number :</p>
                            <input type="number" id="no_telp" class="form-control" value="<?=$user->no_telp?>" name="no_telp">
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">Old Password :</p>
                            <input type="password" id="old_password" class="form-control" name="old_password">
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">New Password :</p>
                            <input type="password" id="password" class="form-control" name="password">
                        </div>
                        <div class="my-form-group">
                            <p class="my-label-input">Photo Profile :</p>
                            <input type="file" id="photo_profile" class="form-control" name="photo_profile">
                        </div>
                    </form>
                    <button style="float: right;" id="change_profile_proceed" class="btn btn-primary"><i id="loading-upload-cmn" style="margin-right: 10px;" class="fa fa-spinner fa-spin"></i>Proceed</button>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    $(function () {
        $('#loading-upload-cmn').hide();
        $('#change_profile_proceed').click(function(){
            if($("#name_user").val() == '' || $("#no_telp").val() == '' || ($('#old_password').val() != '' && $('#password').val() == '')){
                var swal_data = { title: 'Failed', icon: 'error', text: 'Please check your data again!', button:false, timer: 1000 };
                swal(swal_data).then(function() {
                });
            }else{
                var my_formData = new FormData($("#change_profile_form")[0]);
                my_formData.append('file', $("#photo_profile").prop('files')[0]);
                $('#loading-upload-cmn').show();
                $.ajax({
                    url: "<?php echo base_url('login/change_profile_proceed');?>",
                    type : "POST",
                    dataType: 'json',
                    data: my_formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success : function(data){
                        $('#loading-upload-cmn').hide();
                        if(data.stat){
                            var swal_data = { title: 'Success', icon: 'success', text: data.det, button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                                if(data.change_pass){
                                    location.replace('<?=base_url()?>/login');
                                }
                            });
                        }else{
                            var swal_data = { title: 'Failed', icon: 'error', text: data.det, button:false, timer: 1000 };
                            swal(swal_data).then(function() {
                            });
                        }
                    },
                });
            }
        });
    });
</script>
<style>
    .my-row {
        margin: 10px !important;
        border-radius: 5px;
        /*height: 80%;*/
        display: block !important;
    }

    div.inbound-div, div.outbound-div {
        padding-top: 42px;
        width: 50%;
    }

    div.dataTables_wrapper div.dataTables_filter, div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        font-size: 12px;
    }

    .btn {
        font-size: 12px;
        font-weight: 300;
    }

    .header-jdl {
        display: flex !important;
    }

    .tab-btn {
        margin-top: 20px;
        display: flex;
    }

    .tab-btn > button {
        background: transparent;
        font-size: 12px;
        margin-right: 10px;
        color: #0069D9;
        box-shadow: none !important;
        font-weight: 300;
    }

    .tab-btn > button.active {
        background: #0069D9;
        font-size: 12px;
        margin-right: 10px;
        color: white;
        box-shadow: 0px 3px 5px 1px rgba(0, 0, 0, 0.3) !important;
    }

    .jdl-big {
        margin-top: 18px;
        font-size: 16px;
        border-bottom: 4px solid #dc3545;
    }

    .add-pn-div {
        margin-top: 20px;
    }

    #btn-add-pn {
        font-size: 12px;
        color: white;
        margin-bottom: 22px;
        font-weight: 300;
    }

    .input-div {
        position: relative;
        margin-left: 20px;
        margin-top: 46px;
        width: 60%;
    }

    .table-div, .temp-div, .put-away {
        margin-left: 20px;
        margin-top: 46px;
    }

    .table.table-bordered {
        font-size: 12px;
    }

    .my-label-input {
        font-size: 12px;
        font-weight: 300;
        /* margin-top: 7px; */
        margin-bottom: 20px;
    }

    .form-control {
        height: 30px;
        font-size: 12px;
        font-weight: 300;
        margin-bottom: 10px;
        width: 50%;
    }

    .my-form-group {
        display: flex;
        justify-content: space-between;
    }

    .input-add-pn {
        outline: 0;
        border-radius: .25rem;
        height: 30px;
        font-size: 12px;
        font-weight: 300;
        border: 1px solid #ced4da;
    }

    .my-btn.save:hover{
        background: white;
        color: #dc3545;
    }

    #saveForm {
        position: absolute;
        right: 0;
        margin-top: 34px;
        margin-bottom: 34px;
    }

    @media only screen and (max-width: 600px) {
    }
</style>