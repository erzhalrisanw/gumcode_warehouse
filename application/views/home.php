<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Company Profile</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/jqvmap/jqvmap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>/plugins/summernote/summernote-bs4.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>

<body>
  <div class="my-wrapper">
    <div class="my-header">
      <a href="#"><img class="head-logo" src="<?php echo base_url(''); ?>/dist/img/logo-gasplus.png"></a>
      <div class="head-pic">
        <img class="head-pic-img" src="<?php echo base_url(''); ?>/dist/img/google-play-badge.png">
      </div>
    </div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <?php
        foreach ($slide as $key => $value) {
          if ($key < 1) {
            echo '<div class="item active">';
            echo '<img class="carousel-item" src="' . base_url('') . '/public/img_slide/' . $value['url'] . '">';
            echo '</div>';
          } else {
            echo '<div class="item">';
            echo '<img class="carousel-item" src="' . base_url('') . '/public/img_slide/' . $value['url'] . '">';
            echo '</div>';
          }
        }
        ?>
      </div>
      <ol class="carousel-indicators">
        <?php
        foreach ($slide as $key => $value) {
          if ($key < 1) {
            echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>';
          } else {
            echo '<li data-target="#myCarousel" data-slide-to="' . $key . '"></li>';
          }
        }
        ?>
      </ol>
    </div>
    <span class="my-carousel-footer">
      <span class="my-carousel-footer-inner">
        <a class="my-carousel-arrow" href="#myCarousel" data-slide="prev">
          <span class="fas fa-chevron-left carousel-arr"></span>
        </a>
        <p class="carousel-total-item"></p>
        <a class="my-carousel-arrow" href="#myCarousel" data-slide="next">
          <span class="fas fa-chevron-right carousel-arr"></span>
        </a>
      </span>
    </span>
    <div class="text-slogan">
      <p>Mulai dari pengiriman sampai pembayaran penuh kemudahan.</p>
      <p>Antar belanjaan sampai depan pintu rumahmu.</p>
      <p>Lupakan ribetnya belanja dengan troli.</p>
      <p>Lupakan ribetnya parkir kendaraan.</p>
    </div>
    <p class="text-layanan">Layanan</p>
    <span class="btn-layanan-col">
      <button id="btn-all-prod" class="btn-layanan" onclick="setProdShow('all')">Semua</button>
      <?php
      foreach ($category as $cat) {
        echo '<button id="btn-all-prod" class="btn-layanan" onclick="setProdShow(' . $cat['id'] . ')">' . $cat['name'] . '</button>';
      }
      ?>
    </span>
    <div class="layanan-card-col">
    </div>
    <div class="layanan-card-page">
      <button id="btn_prev" class="layanan-card-page-btn" onclick="prevPage()">PREV</button>
      <button id="btn_next" class="layanan-card-page-btn" onclick="nextPage()">NEXT</button>
    </div>
  </div>
  <script src="<?php echo base_url(''); ?>/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <script src="<?php echo base_url(''); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/sparklines/sparkline.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/jquery-knob/jquery.knob.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/summernote/summernote-bs4.min.js"></script>
  <script src="<?php echo base_url(''); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <script src="<?php echo base_url(''); ?>/dist/js/adminlte.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script>
  <script type="text/javascript">
    let data_prod = [];
    $(document).ready(function() {
      $('.carousel').carousel({
        interval: 3000,
        pause: true
      });

      $(".carousel").swipe({

        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

          if (direction == 'left') $(this).carousel('next');
          if (direction == 'right') $(this).carousel('prev');

        },
        allowPageScroll: "vertical"

      });

      totalItems = $('.item').length;
      $('.carousel-total-item').html("1 of " + totalItems);
      $('.carousel').on('slid.bs.carousel', function() {
        var carouselData = $(this).data('bs.carousel');
        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
        var text = (currentIndex + 1) + " of " + totalItems;
        $('.carousel-total-item').html(text);
      });
    });

    let currPage = 1;
    let itemPerPage = 0;
    if (window.outerWidth > 600) {
      itemPerPage = 9;
    } else if (window.outerWidth < 600) {
      itemPerPage = 4;
    }

    function prevPage() {
      if (currPage > 1) {
        currPage--;
        changePage(currPage);
      }
    }

    function nextPage() {
      if (currPage < numPages()) {
        currPage++;
        changePage(currPage);
      }
    }

    function changePage(page) {
      var btn_next = document.getElementById("btn_next");
      var btn_prev = document.getElementById("btn_prev");
      if (page < 1) page = 1;
      if (page > numPages()) page = numPages();
      $('.layanan-card-col').html('');

      if (data_prod.length < itemPerPage) {
        for (var z = 0; z < data_prod.length; z++) {
          $(".layanan-card-col").append(
            '<span class="layanan-card">' +
            '<img class = "layanan-card-img"src = "<?php echo base_url(''); ?>/public/img_prod/' + data_prod[z].prod_url + '"> ' +
            '<p class = "layanan-card-text">' + data_prod[z].prod_name + '</p></span>');
        }
      } else {
        if (page == numPages()) {
          for (var z = (page - 1) * itemPerPage; z < data_prod.length; z++) {
            $(".layanan-card-col").append(
              '<span class="layanan-card">' +
              '<img class = "layanan-card-img"src = "<?php echo base_url(''); ?>/public/img_prod/' + data_prod[z].prod_url + '"> ' +
              '<p class = "layanan-card-text">' + data_prod[z].prod_name + '</p></span>');
          }
        } else {
          for (var i = (page - 1) * itemPerPage; i < (page * itemPerPage); i++) {
            $(".layanan-card-col").append(
              '<span class="layanan-card">' +
              '<img class = "layanan-card-img"src = "<?php echo base_url(''); ?>/public/img_prod/' + data_prod[i].prod_url + '"> ' +
              '<p class = "layanan-card-text">' + data_prod[i].prod_name + '</p></span>');
          }
        }
      }

      if (page == 1) {
        btn_prev.style.display = "none";
      } else {
        btn_prev.style.display = "block";
      }

      if (page == numPages()) {
        btn_next.style.display = "none";
      } else {
        btn_next.style.display = "block";
      }
    }

    function numPages() {
      return Math.ceil(data_prod.length / itemPerPage);
    }

    $('.btn-layanan').click(function() {
      $('.btn-layanan').removeClass('active');
      $(this).addClass('active');
    });

    $("span.btn-layanan-col > button:first-child").trigger('click');

    function setProdShow(id) {
      if (isNaN(id)) {
        $.ajax({
          url: "<?php echo base_url('index.php/home/getAllProduct'); ?>",
          type: "GET",
          dataType: 'json',
          success: function(data) {
            data_prod = data;
            changePage(1);
          },
        });
      } else {
        $.ajax({
          url: "<?php echo base_url('index.php/home/getOneCategory'); ?>",
          type: "POST",
          dataType: 'json',
          data: {
            "id": id
          },
          success: function(data) {
            data_prod = data;
            changePage(1);
          },
        });
      }
    }
  </script>
</body>

</html>
<style>
  .my-wrapper {
    width: 800px;
    margin: 0 auto;
    margin-top: 20px;
    font-family: 'Poppins';
    position: relative;
  }

  .my-header {
    width: 95%;
    margin: 0 auto;
  }

  .head-logo {
    width: 140px;
  }

  .head-pic {
    height: 300px;
    margin-top: 40px;
    box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, .3);
  }

  .head-pic-img {
    width: 400px;
    display: block !important;
    margin: 0 auto;
  }

  #myCarousel {
    margin-top: 100px;
  }

  .my-carousel-footer {
    display: block !important;
    margin-top: 10px;
    position: absolute;
    left: 50%;
  }

  .my-carousel-footer-inner {
    display: flex;
    position: relative;
    left: -50%;
  }

  .my-carousel-arrow {
    background: #dc3545;
    color: white !important;
    text-decoration: none !important;
    padding: 5px 10px;
    font-size: 18px;
  }

  .my-carousel-arrow:hover {
    color: #dc3545 !important;
    background: white;
  }

  .carousel-arr:visited {
    color: black;
  }

  .carousel-total-item {
    color: #dc3545;
    word-spacing: 5px;
    margin: 0 40px;
    font-size: 16px;
    margin-top: 7px;
    font-weight: 300;
    width: 80px;
    text-align: center;
  }

  .text-slogan {
    color: #dc3545;
    font-size: 16px;
    font-weight: 400;
    text-align: center;
    margin-top: 150px;
  }

  .text-slogan>p {
    margin-bottom: 0;
  }

  .text-layanan {
    color: #dc3545;
    font-size: 36px;
    font-weight: 600;
    text-align: center;
    margin-top: 100px;
  }

  .carousel-item {
    height: 200px !important;
    width: 100% !important;
  }

  .btn-layanan-col {
    display: flex;
    width: 700px;
    margin: 0 auto;
    margin-top: 40px;
    flex-flow: row wrap;
    justify-content: space-between;
  }

  .btn-layanan {
    font-size: 16px;
    border: .5 solid #707070;
    background: transparent;
    font-weight: 300;
    padding: 5px 40px;
    border-radius: 4px;
    box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.3);
  }

  .btn-layanan.active {
    color: white;
    background: #dc3545;
    border-color: #dc3545;
  }

  .btn-layanan:hover {
    color: white;
    background: #dc3545;
    border-color: #dc3545;
  }

  .btn-layanan:focus {
    outline: 0;
  }

  .layanan-card-col {
    display: flex;
    flex-flow: row wrap;
    justify-content: flex-start;
    margin: 0 auto;
    margin-top: 30px;
    margin-bottom: 30px;
    width: 720px;
  }

  .layanan-card {
    box-shadow: inset 0px 0px 4px 2px rgba(0, 0, 0, .3);
    width: 200px;
    margin: 20px 20px;
    border-radius: 10px;
  }

  .layanan-card-img {
    height: 150px !important;
    width: 120px !important;
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: 10px;
  }

  .layanan-card-text {
    font-size: 16px;
    font-weight: 300;
    color: #dc3545;
    text-align: center;
    margin-bottom: 15px;
    margin-top: 15px;
  }

  .layanan-card-page {
    display: flex;
    margin-bottom: 50px;
    margin-top: 25px;
    position: absolute;
    right: 0;
  }

  .layanan-card-page-btn {
    border: none;
    color: white;
    background: black;
    border: .5px solid black;
    font-size: 14px;
    padding: 5px 20px;
    border-radius: 4px;
    margin-left: 10px;
  }

  .layanan-card-page-btn:focus {
    outline: 0;
  }

  .layanan-card-page-btn:hover {
    color: black;
    background: white;
  }

  .carousel-indicators {
    display: none !important;
  }

  @media only screen and (max-width: 600px) {
    .my-wrapper {
      width: 100%;
      margin: 0;
      margin-top: 10px;
    }

    .head-logo {
      width: 120px;
      margin-left: 10px;
    }

    .my-header {
      width: 100%;
      margin: 0;
    }

    .head-pic {
      width: 100%;
      box-shadow: none;
      border-top: 1px solid rgba(0, 0, 0, .5);
      border-bottom: 1px solid rgba(0, 0, 0, .5);
      height: 200px;
    }

    #myCarousel {
      margin-top: 40px;
      padding: 0 20px;
    }

    .carousel-item {
      height: 100px !important;
      width: 100% !important;
    }

    .my-carousel-footer {
      display: none !important;
    }

    .carousel-indicators {
      display: block !important;
      top: 110px !important;
    }

    .carousel-indicators li {
      border: 1px solid #dc3545 !important;
      height: 8px !important;
      width: 8px !important;
      margin: 4px !important;
    }

    .carousel-indicators li.active {
      background: #dc3545 !important;
      margin: 4px !important;
    }

    .text-slogan {
      margin-top: 70px;
      font-size: 11px;
    }

    .text-layanan {
      margin-top: 40px;
      margin-bottom: 0;
      font-size: 22px;
    }

    .btn-layanan-col {
      margin-top: 20px;
      width: 100% !important;
      padding: 0 45px !important;
    }

    .btn-layanan {
      background: transparent !important;
      border: none !important;
      color: black !important;
      padding: 0 !important;
      font-size: 14px;
      font-weight: 600;
      border-radius: 0 !important;
      box-shadow: none !important;
    }

    .btn-layanan.active {
      border-bottom: 2px solid #dc3545 !important;
    }

    .layanan-card-col {
      margin-bottom: 30px;
      width: 100%;
      justify-content: space-between !important;
      padding: 0 20px;
    }

    .layanan-card {
      box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.3);
      border-radius: 10px;
      border: 1px solid rgba(0, 0, 0, .5);
      width: 45% !important;
      margin: 10px 0;
    }

    .layanan-card-img {
      height: 80px !important;
      width: 50px !important;
      display: block;
      margin-left: auto;
      margin-right: auto;
      margin-top: 10px;
    }

    .layanan-card-text {
      font-size: 10px;
      font-weight: 400;
      color: black;
      text-align: center;
      margin-bottom: 10px;
      margin-top: 0;
      border-top: 1px solid rgba(0, 0, 0, .5);
      padding-top: 10px;
    }

    .layanan-card-page {
      display: flex;
      margin: 0 20px;
      margin-bottom: 25px;
      margin-top: 0px;
      position: absolute;
      right: 0;
    }

    .layanan-card-page-btn:hover {
      color: white !important;
      background: black !important;
    }
  }
</style>